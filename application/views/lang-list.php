<div class="right_col">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Language List</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    List of language
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Code</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(isset($langs_list) && !empty($langs_list)): ?>
                                    <?php foreach( $langs_list as $lang): ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $lang['ID'] ?></td>
                                        <td><?php echo $lang['name']; ?></td>
                                        <td><?php echo $lang['code']; ?></td>
                                        <td><?php echo ($lang['active'] == 1) ? 'active' : 'inactive'; ?></td>
                                        <td>
                                            <a href="<?php echo base_url('manage/lang/'.$lang['ID']); ?>" class="edit" title="Edit" >
                                                <i class="fa fa-pencil-square-o fa-fw"></i>
                                            </a>
                                            <!--<a href="<?php echo base_url('delete/lang/'.$lang['ID']); ?>" class="delete"><i class="fa fa fa-times fa-fw fa-fw"></i></a>-->
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                     <tr class="odd gradeX" >
                                         <td colspan="4">No lang found.</td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>

