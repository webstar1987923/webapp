<!-- Page Content -->
<?php // print_r($parent); ?>
<div class="right_col">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-6">
                    <h1 class="page-header">Assign parents</h1>
                </div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <form role="form" action="" method="post" enctype="multipart/form-data">
        <?php $response = $this->session->flashdata('response'); 
        if(!empty($response) || isset($code)):
            $class = (!empty($response)) ? $response['class'] :  $code['class'];
            $msg   = (!empty($response)) ? $response['msg']  : $code['msg'];
        ?>
        <div class="alert alert-<?php echo $class ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
        <?php endif; ?>
        <div class="row">

            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Student informations
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <tbody> 
                                    <tr>
                                        <th>ID</th>
                                        <td><?php echo $student['sid'] ?></td>
                                    </tr>
                                    <tr>
                                        <th>Pic</th>
                                        <?php  $student['pic'] = ($student['pic']) ? $student['pic'] :'nopic.png'; ?>
                                        <td><img src="<?php echo base_url('uploads/'.$student['pic']); ?>" /></td>
                                    </tr>
                                    <tr>
                                        <th>Name</th>
                                        <td><?php echo $student['name'] ?></td>
                                    </tr>
                                    <tr>
                                        <th>Email</th>
                                        <td><?php echo $student['email_id'] ?></td>
                                    </tr>
<!--                                    <tr>
                                        <th>Status</th>
                                        <td><?php echo ($student['status'] == 1) ? 'active' : 'inactive'; ?></td>
                                    </tr>-->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Parents Informations
                    </div>
                    <div class="panel-body">
<!--                        <ul class="list-unstyled">
                            <?php if($parents): ?>
                            <?php foreach($parents as $parent): ?>
                            <li><input type="checkbox"
                                       value="<?php echo $parent['user_no']; ?>"
                                       name="parents[]"
                                       <?php if(in_array($parent['user_no'], $assigned_parents)){echo 'checked=""'; } ?>
                                       > <?php echo $parent['first_name']?$parent['first_name']:$parent['username']; ?></li>
                            <?php endforeach; ?>
                            <?php else: ?>
                            <p>No parents found.</p>
                            <?php endif; ?>
                        </ul>-->
                        
                        <select id="my-select" name="parents[]">
                            <option value="">Not assigned</option>
                            <?php foreach($parents as $parent): ?>
                                <option value="<?php echo $parent['user_no']; ?>" <?php if(in_array($parent['user_no'], $assigned_parents)){echo 'selected=""'; } ?>>
                                    <?php echo $parent['first_name']?$parent['first_name']:$parent['username']; ?>
                                </option>
                            <?php endforeach; ?>
                        </select>

                        <script type="text/javascript">
                            $(function() {
                                // initialize sol
                                $('#my-select').searchableOptionList({
                                    allowNullSelection: true,
                                    maxHeight: '300px'
                                });
                            });
                        </script>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Actions
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <input type="hidden" name="action" value="<?php echo $action; ?>" >
                                    <input  type="submit" class="btn btn-primary pull-right" value="Save">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- /#page-wrapper -->