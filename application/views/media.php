<!-- Page Content -->
<?php // print_r($course); ?>
<div class="right_col">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-6">
                    <h1 class="page-header"> Medias</h1>
                </div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <?php
    $response = $this->session->flashdata('response');
    if (!empty($response) || isset($code)):
        $class = (!empty($response)) ? $response['class'] : $code['class'];
        $msg = (!empty($response)) ? $response['msg'] : $code['msg'];
        ?>
        <div class="alert alert-<?php echo $class ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-lg-12">
            <div id="dropzone">
                <form action="<?php echo base_url('/media/upload'); ?>" class="dropzone" id="my-awesome-dropzone">
                    <div class="dz-message">
                        Drop files here or click to upload.<br />
                        <span class="note">(This is just a demo dropzone. Selected files are <strong>not</strong> actually uploaded.)</span>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    Media Lists
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="list-group clearfix" id="media-list">
                            <?php if ($medias): ?>
                                <?php foreach ($medias as $key => $file) { ?>
                                    <div class="col-lg-2 media-item" data-id="<?php echo $file['ID']; ?>">
                                        <a class="delete-media" data-id="<?php echo $file['ID']; ?>">
                                            <i class="fa fa-times-circle-o fa-2x"></i>
                                        </a>
                                        <div class="thumbnail">
                                            <img src="<?php echo $file['url']; ?>" />
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /#page-wrapper -->