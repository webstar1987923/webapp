<div class="right_col">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Class List</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    List of Class
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body blocking">
                    <?php $this->load->view('search-bar',array('plchldr' => "Search by name")); ?>
                    <form action="<?php echo base_url('ajax/delete/course'); ?>" class="ajax-form" method="POST" >
                        <div id="list-load">
                            <div class="table-responsive">
                            <table class="table table-striped jambo_table bulk_action" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th class="column-title">ID</th>
                                    <th class="column-title">Name</th>
                                    <th class="column-title">Code</th>
                                    <th class="column-title">Students</th>
                                    <!--<th>Status</th>-->
<!--                                    <th>Language</th>-->
                                    <th class="column-title">Action</th>
                                    <th class="column-title"><input type="checkbox" class="flat" name="all" id="all" ></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if(isset($courses) && !empty($courses)): ?>
                                    <?php
//                                    $courses = array_reverse($courses);
                                    foreach( $courses as $course): ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $course['cid'] ?></td>
                                        <td><?php echo $course['name']; ?></td>
                                        <td><?php echo $course['code']; ?></td>
                                        <td><?php echo $course['student_count']; ?></td>
                                        <!--<td><?php echo ($course['status'] == 1) ? 'active' : 'inactive'; ?></td>-->
<!--                                        <td><?php foreach($langs as $lang){ echo '<a href="'.base_url('manage/course/'.$course['cid'].'?lang_id='.$lang['ID']).'" title="'.$lang['code'].'">'.$lang['code'].'</a>&nbsp;&nbsp;'; }?></td>-->
                                        <td>
                                            <a href="<?php echo base_url('manage/course/'.$course['cid']); ?>" title="Edit" class="edit" ><i class="fa fa-pencil-square-o fa-fw"></i></a>
                                            <a href="<?php echo base_url('assign/student/'.$course['cid']); ?>" class="assigns" title="Assign students"><i class="fa fa-tasks fa-fw"></i></a>
                                            <a href="<?php echo base_url('delete/course/'.$course['cid']); ?>" title="Delete" class="delete"><i class="fa fa fa-times fa-fw fa-fw"></i></a>
                                        </td>
                                        <td><input type="checkbox" name="delete_ids[]" value="<?php echo $course['cid'] ?>" class="select_me flat" ></td>
                                    </tr>
                                    <?php endforeach; ?>
                                    <?php else: ?>
                                         <tr class="odd gradeX" >
                                             <td colspan="6">No class found.</td>
                                        </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                            <!-- /.table-responsive -->
                        <?php $this->load->view('paginations'); ?>
                        </div>
                    </form>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>

