<!-- Page Content -->
<?php // print_r($course); ?>
<div class="right_col">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-6">
                    <h1 class="page-header">Assign students</h1>
                </div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <form role="form" action="" method="post" enctype="multipart/form-data">
        <?php $response = $this->session->flashdata('response'); 
        if(!empty($response) || isset($code)):
            $class = (!empty($response)) ? $response['class'] :  $code['class'];
            $msg   = (!empty($response)) ? $response['msg']  : $code['msg'];
        ?>
        <div class="alert alert-<?php echo $class ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
        <?php endif; ?>
        <div class="row">

            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Class informations
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <tbody> 
                                    <tr>
                                        <th>ID</th>
                                        <td><?php echo $course['cid'] ?></td>
                                    </tr>
                                    <tr>
                                        <th>Code</th>
                                        <td><?php echo $course['code'] ?></td>
                                    </tr>
                                    <tr>
                                        <th>Name</th>
                                        <td><?php echo $course['name'] ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Student List
                    </div>
                    <div class="panel-body height_fixed overflow_scroll">
                        <ul class="list-unstyled">
                            <?php if($students): ?>
                            <?php foreach($students as $student): ?>
                            <li><input type="checkbox"
                                       value="<?php echo $student['sid']; ?>"
                                       name="students[]"
                                       <?php if(in_array($student['sid'], $assigned_students)){echo 'checked=""'; } ?> > 
                                        <?php echo $student['name']; ?> 
                                <span class="pull-right"><?php echo "Class : ".$student['st_courses']; ?> </span>
                            </li>
                            <?php endforeach; ?>
                            <?php else: ?>
                            <p>No classes found.</p>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Actions
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <input type="hidden" name="action" value="<?php echo $action; ?>" >
                                    <input  type="submit" class="btn btn-primary pull-right" value="Save">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- /#page-wrapper -->