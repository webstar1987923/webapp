<!-- Page Content -->
<?php // print_r($school); ?>
<div class="right_col">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-6">
                    <h1 class="page-header"> Super Admin Info</h1>
                </div>
                <div class="col-lg-6 full-height">
<!--                    <div class="pull-right" >
                        <?php 
                        $lid = (isset($lang_id)) ? $lang_id : 0;
                        if($langs &&  $action == 2): ?>
                            <?php foreach($langs as $lang): ?>
                                <a href="<?php echo '?lang_id='.$lang['ID']; ?>" class="btn btn-circle <?php echo ($lid && $lang['ID'] == $lid) ? 'active' : ''; ?>" ><?php echo $lang['code']; ?></a>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>-->
                </div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <form role="form" action="" method="post" enctype="multipart/form-data">
        <?php $response = $this->session->flashdata('response'); 
        if(!empty($response) || isset($code)):
            $class = (!empty($response)) ? $response['class'] :  $code['class'];
            $msg   = (!empty($response)) ? $response['msg']  : $code['msg'];
        ?>
        <div class="alert alert-<?php echo $class ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
        <?php endif; ?>
        <div class="row">

            <div class="col-lg-6">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        super admin informations
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Super Admin Name : </label>
                                    <input class="form-control" name="school[school_name]" placeholder="Enter Super Admin Name" value="<?php if(isset($school['school_name']) ) echo $school['school_name'];  ?>" >
                                </div>
                                <div class="form-group">
                                    <label>Super Admin Detail : </label>
                                    <input class="form-control" name="school[detail]" placeholder="Enter Super Admin Detail" value="<?php if(isset($school['detail']) ) echo $school['detail'];  ?>" >
                                </div>                     
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           
            <div class="col-lg-6">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        Actions
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
<!--                                    <input type="hidden" name="school[lang_id]" value="<?php echo (isset($lang_id)) ? $lang_id : $active_lang['ID']; ?>" >
                                    <?php if(isset($lang_id)): ?>
                                        <input type="hidden" name="school[parent]" value="<?php echo (isset($parent)) ? $parent : 0; ?>" >
                                    <?php endif; ?>
                                    <?php if(isset($new_lang_varient)  && !$new_lang_varient ): ?>
                                        <input type="hidden" name="lang_varient" value="1" >
                                    <?php endif; ?>
                                        -->
                                    <input type="hidden" name="action" value="<?php echo $action; ?>" >
                                    <input  type="submit" class="btn btn-primary pull-right" value="Save">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- /#page-wrapper -->