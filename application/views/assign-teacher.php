<!-- Page Content -->
<?php // print_r($course); ?>
<div class="right_col">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-6">
                    <h1 class="page-header"><?php echo $section_header; ?></h1>
                </div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <form role="form" action="" method="post" enctype="multipart/form-data">
        <?php
        $response = $this->session->flashdata('response');
        if (!empty($response) || isset($code)):
            $class = (!empty($response)) ? $response['class'] : $code['class'];
            $msg = (!empty($response)) ? $response['msg'] : $code['msg'];
            ?>
            <div class="alert alert-<?php echo $class ?> alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $msg; ?>
            </div>
        <?php endif; ?>
        <div class="row">

            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        User informations
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <tbody> 
                                    <tr>
                                        <th>ID</th>
                                        <td><?php echo $teacher['user_no'] ?></td>
                                    </tr>
<!--                                    <tr>
                                        <th>Pic</th>
                                        <td><?php echo $teacher['pic'] ?></td>
                                    </tr>-->
                                    <tr>
                                        <th>Name</th>
                                        <td><?php echo $teacher['first_name'] . ' ' . $teacher['last_name']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Email</th>
                                        <td><?php echo $teacher['email_id'] ?></td>
                                    </tr>									<!-- mrin sec -->									<?php if (isset($teacher['user_type'])) {
            
        }
        ?>									<!-- mrin : End -->
<!--                                    <tr>
                                        <th>Status</th>
                                        <td><?php echo ($teacher['status'] == 1) ? 'active' : 'inactive'; ?></td>
                                    </tr>-->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Courses Informations
                    </div>
                    <div class="panel-body">
                        <ul class="list-unstyled">

<?php if ($courses): ?>
                                <li>
                                    <div class="col-lg-6">
                                        <input type="checkbox" value="1" name="all" class="flat">
                                        <strong>Select All</strong>
                                    </div>									   
                                </li>
    <?php foreach ($courses as $course): ?>
                                    <li><div class="col-lg-6">
                                            <input class="flat" type="checkbox" value="<?php echo $course['cid']; ?>" name="courses[]"
                                                                     <?php
                                                                     if (in_array($course['cid'], $assigned_courses)) {
                                                                         echo 'checked=""';
                                                                     }
                                                                     ?>
                                                                     > <strong>Class : </strong> <?php echo $course['name']; ?>									   
                                        </div>									   
                                        <!--                                        <div class="col-lg-6">									    mrin section for allow to edit Absent of that claas students 									   
                                                                                    <span style="margin-left:10px;"> 
                                                                                        <input type="checkbox" name="absent_edit_allow[<?= $course['cid'] ?>]" value="1" <?php
                                                                     if (in_array($course['cid'], $is_allow_edit_absent)) {
                                                                         echo 'checked';
                                                                     }
                                                                     ?> > Edit Absent 
                                                                                    </span>
                                                                                    <span style="margin-left:10px;"> 
                                                                                        <input type="text" class="form-control" name="absent_edit_allow_note[<?= $course['cid'] ?>]" placeholder="Your note" style="margin-top:5px;" value="<?php
                                    if (isset($absent_notes[$course['cid']])) {
                                        echo $absent_notes[$course['cid']];
                                    }
                                    ?>" >
                                                                                    </span>									    mrin : End Absent edit permition 									   
                                                                                </div>									   -->
                                    </li>
                                            <?php
                                            if ($course['semno'] && false):
                                                $sems_selected = isset($assigned_sems[$course['cid']]) ? $assigned_sems[$course['cid']] : array();
                                                ?>
                                        <ul class="list-group list-inline">
                                        <?php for ($i = 1; $i <= $course['semno']; $i++): ?>
                                                <li class=""><strong>S- : </strong><?php echo $i; ?> <input
                                            <?php
                                            if (in_array($i, $sems_selected)) {
                                                echo 'checked=""';
                                            }
                                            ?>
                                                        type="checkbox" class="flat" value="<?php echo $i; ?>" name="semnos[<?php echo $course['cid']; ?>][]"></li>
            <?php endfor; ?>
                                        </ul>
        <?php endif; ?>
    <?php endforeach; ?>
<?php else: ?>
                                <p>No courses found.</p>
<?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Actions
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <input type="hidden" name="action" value="<?php echo $action; ?>" >
                                    <input type="hidden" name="uid" value="<?php echo $teacher['user_no'] ?>" >
                                    <input  type="submit" class="btn btn-primary pull-right" value="Save">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    $( document ).ready(function() {
        $('input[name="all"]').on('ifChecked', function (event){
            $(this).closest("input").attr('checked', true);      
            checkAll(this);
        });
        $('input[name="all"]').on('ifUnchecked', function (event) {
            $(this).closest("input").attr('checked', false);
            checkAll(this);
        });
    });
    function checkAll(checkbox) {
        if ($(checkbox).prop('checked')){
            $('input[name="courses[]"]').prop('checked', true);
        }else{
            $('input[name="courses[]"]').prop('checked', false);
        }
        $('input[name="courses[]"]').iCheck('update');
    }
</script>
<!-- /#page-wrapper -->