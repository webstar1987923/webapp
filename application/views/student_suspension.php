<!--<script src="<?php echo base_url('/assets/js/jquery.mCustomScrollbar.concat.min.js'); ?>"></script>
<link rel="stylesheet" href="<?php echo base_url('/assets/css/jquery.mCustomScrollbar.css'); ?>" />-->

<div class="right_col">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Student Suspension</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <!-- /.row -->

    
<!--        <div class="row">
            <div class="col-lg-12">
                <div class="row">

                    <div class="col-lg-4 pull-right">
                        <h4 style="font-size:14px;text-align: center;">الإدارة العامة لمنطقة الفروانية التعليمية<br>
                            مدرسة عبداللطيف ثنيان الغانم الثانوية بنين
                        </h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h4 style="font-size:16px;text-align: center;">
                            <b><?= date("d-m-Y",strtotime($start_date)) ?>
                                إحصائية الغياب اليومي للطلاب بتاريخ</b></h4>
                    </div>
                </div>
            </div>
        </div>-->


    <div class="panel-heading">List of Classes</div>
    <div class="panel-body blocking">
        <div class="row clearfix">
            <div class="alert alert-success hide" id="success_message">Suspended date has been saved</div>
        </div>
        <div class="row">
            <div class="col-lg-4 manage-at__scroll" style="padding:0px;">
                <table class="table table-striped table-bordered table-hover" >
                    <tbody>
                        <tr>
                            <th class="text-left">Class Names</th>
                        </tr>
                        
                        <?php foreach($courses as $course){ ?>
                            <tr class="odd gradeX" >
                                <td class="text-left<?php if ($course['cid']==$cid){?> <?php $course_name = $course['name'];?>table-row__selected<?php } ?>" >
                                    <a style="font-weight: bold;color:#333" href="<?php echo base_url('lists/studentsuspension/'.$course['cid']); ?>"><?php print $course['name'];?></a>
                                </td>
                            </tr>
                            <?php $counter++;?>
                        <?php } ?>    
                    </tbody>
                </table>
            </div>
            <div class="col-lg-4 manage-at__scroll" style="padding:0px;">
                <table class="table table-striped table-bordered table-hover" >
                    <tbody>
                        <tr>
                            <th class="text-left">Student Names</th>
                        </tr>
                        <?php if (!empty($students)){?>
                            <?php foreach($students as $student){?>
                                <tr class="odd gradeX" >
                                    <td class="text-left<?php if ($student['sid']==$sid){?> <?php $student_name = $student['name'];?>table-row__selected<?php } ?>" <?php if ($student['sid']==$sid){?>table-row__selected<?php } ?>>
                                        <a style="font-weight: bold;color:#333" href="<?php echo base_url('lists/studentsuspension/'.$cid.'/'.$student['sid']); ?>"><?php print $student['name'];?></a>
                                    </td>
                                </tr>
                            <?php } ?>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="col-lg-4 manage-at__scroll" style="padding:0px;">
                <table class="table table-striped table-bordered table-hover" id="student_attendance" >
                    <tbody>
                        <tr>
                            <th class="text-center">Suspend Days</th>
                        </tr>
                        <?php if ($sid){?>
                            <tr class="gradeX odd" >
                                <td align="center" >
                                    <div class="form-group col-lg-6">
                                        <label for="start_date">Stard Date : </label>
                                        <input type="text" class="form-control pull-right datepicker frmfld" id="start_date" name="start_date" placeholder="Start Date" value="<?=  date('Y-m-d') ?>" >
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label>Days : </label>
                                        <select class="form-control" name="days" id="days">
                                            <?php for($i=1;$i<=30;$i++){?>
                                                <option><?php print $i;?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group form-inline col-lg-12 text-right">
                                        <button type="button" class="btn btn-primary pull-right" id="save_attendance" onclick="saveStudentSuspend(this, <?php print $sid;?>, <?php print $cid;?>);">Save</button>&nbsp;
                                        <button type="button" class="btn btn-primary pull-left printreport<?php if (empty($suspend)){?> hide<?php } ?>" id="print_attendance">Print</button>&nbsp;
                                    </div>
                                </td>
                            </tr>
                            <tr class="gradeX odd" >
                                <td align="center" id="student_suspends">
                                    <?php $this->load->view('suspend_history',array('suspend' => $suspend)); ?>
                                </td>
                            </tr>
                            <tr class="gradeX odd" >
                                <td align="center" id="student_medical">
                                    <?php $this->load->view('medical_history',array('medical' => $medical, 'hide_remove'=>1)); ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div style="display:none;">

    <div id="reportprintdiv">
        <?php $this->load->view('student_suspension_report',array('suspend' => $suspend, 'current_school'=>$current_school, 'course_name'=>$course_name, 'student_name'=>$student_name, 'studentid'=>$studentid)); ?>
    </div>
</div>

<style>
    .table-row__selected{
        background-color: #F4D03F !important;
    }
    .table-row__selected td{
        background-color: #F4D03F !important;
    }
    .grey_tr{background-color:#ccc !important;-webkit-print-color-adjust: exact;}
    .grey_tr td{background-color:#ccc !important;}
    .manage-at__scroll{
        overflow-y:scroll; 
        overflow-x:hidden; 
        height:600px;
    }
</style>
<script>
    
    
    function saveStudentSuspend(button, student, cid){
        $(button).blur();
        $.ajax({
            method: "POST",
            url: "<?php echo base_url('ajax/saveStudentSuspend'); ?>",
            data: {'student': student, 'start_date': $('#start_date').val(), 'days':$('#days').val(), 'cid': cid},
            success: function(html){
                $('#print_attendance').removeClass('hide');
                $('#student_suspends').html(html);
                if (!$('#student_suspends').find('.alert-danger').length){
                    $('#success_message').removeClass('hide');
                    setTimeout(function(){
                        $('#success_message').addClass('hide');
                    }, 3*1000);
                }
                refreshPrintReport(student, cid);
            }
        });
    }
    
    function refreshPrintReport(student, cid){
        $.ajax({
            method: "POST",
            url: "<?php echo base_url('ajax/suspensionPrintReport'); ?>",
            data: {'student': student, 'cid': cid},
            success: function(html){
                $('#reportprintdiv').html(html);
            }
        });
    }
    
    function removeStudentSuspend(button, id){
        $(button).blur();
        $.ajax({
            method: "POST",
            url: "<?php echo base_url('ajax/removeStudentSuspend'); ?>",
            data: {'id': id},
            success: function(html){
                $('#student_suspends').html(html);
                if ($('#student_suspends').find('tr').length==0){
                    $('#print_attendance').addClass('hide');
                }else{
                    <?php if ($sid){?>
                        refreshPrintReport(<?php print $sid;?>, <?php print $cid;?>);
                    <?php } ?>
                }
            }
        });
    }
    $(document).ready(function () {
        $(".printreport").on('click', printSuspending);
//        $(".manage-at__scroll").mCustomScrollbar({setHeight: 300});
    });

    $('.datepicker').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd'
    });
    function printSuspending() {
        var row = 1;
        if (row > 0) {
            var mywindow = window.open('', 'Student Suspension');
            //var printContents = document.getElementById('absent_table').innerHTML;
            var printContents = $('#reportprintdiv').html();
            var Cbody = "<html><head><title>Student Suspension</title><link href='<?= base_url('assets/css/bootstrap.min.css') ?>' rel='stylesheet'><link href='<?= base_url('assets/css/style.css') ?>?v=7' rel='stylesheet'><link href='<?= base_url('assets/css/sb-admin-2.css') ?>' rel='stylesheet'><link href='<?= base_url('assets/font-awesome-4.1.0/css/font-awesome.min.css') ?>' rel='stylesheet'><style rel='stylesheet'> table tr td { line-height: 1 !important;} .table{margin-bottom:10px !important;} h4,h5{margin-top:0px!important; margin-bottom: 5px!important;}@media print {.page-break {page-break-after: always;}} p{font-size:18px;}</style></head><body><div id='wrapper'><div id='page-wrapper' style='margin:0px !important'><div class='row'><div class='col-lg-12' style='float:none'><div class='table-responsive'>";

            mywindow.document.write(Cbody);
            mywindow.document.writeln(printContents);
            mywindow.document.write("</div></div></div></div></div></body></html>");

            setTimeout(function () {
                mywindow.print();
                mywindow.document.close();
                mywindow.close();
            }, 1000);
        }
    }
</script>


