<!-- Page Content -->
<?php // print_r($course); ?>
<div class="right_col">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-6">
                    <h1 class="page-header"> Class registration</h1>
                </div>
                <div class="col-lg-6 full-height">
<!--                    <div class="pull-right" >
                        <?php 
                        $lid = (isset($lang_id)) ? $lang_id : 0;
                        if($langs &&  $action == 2): ?>
                            <?php foreach($langs as $lang): ?>
                                <a href="<?php echo '?lang_id='.$lang['ID']; ?>" class="btn btn-circle <?php echo ($lid && $lang['ID'] == $lid) ? 'active' : ''; ?>" ><?php echo $lang['code']; ?></a>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>-->
                </div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <form role="form" action="" method="post" enctype="multipart/form-data">
        <?php $response = $this->session->flashdata('response'); 
        if(!empty($response) || isset($code)):
            $class = (!empty($response)) ? $response['class'] :  $code['class'];
            $msg   = (!empty($response)) ? $response['msg']  : $code['msg'];
        ?>
        <div class="alert alert-<?php echo $class ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
        <?php endif; ?>
        <div class="row">

            <div class="col-lg-6">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        Course informations
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Class code : </label>
                                    <input class="form-control" name="course[code]" placeholder="Enter class code" value="<?php if(isset($course['code']) ) echo $course['code'];  ?>" >
                                </div>
                                <div class="form-group">
                                    <label>Class Name : </label>
                                    <input class="form-control" name="course[name]" placeholder="Enter class name" value="<?php if(isset($course['name']) ) echo $course['name'];  ?>" >
                                </div>
                                
                                <?php if(! $this->input->get('lang_id')): ?>
                                <div class="form-group">
                                    <label>No of seminar : </label>
                                    <input class="form-control" name="course[semno]" type="number" max="7" min="1" placeholder="Enter no of seminar" value="<?php if(isset($course['semno']) ) echo $course['semno'];  ?>" >
                                </div>
<!--                                <div class="form-group">
                                    <label>Course Status :</label>
                                    <div class="form-inline">
                                        <label class="radio-inline">
                                            <input type="radio" name="course[status]" id="active-status" value="1" <?php if(isset($course['status'])){ if($course['status'] == 1) echo 'checked=""';}else{ echo 'checked=""'; } ?> >Active
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="course[status]" id="inactive-status" <?php if(isset($course['status'])){ if($course['status'] == 0) echo 'checked=""';} ?> value="0" >In active
                                        </label>
                                    </div>
                                </div>-->
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Optional Informations
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                  <div class="form-group">
                                    <label>Class Description : </label>
                                    <textarea class="form-control" name="course[desc]" placeholder="Enter class description" ><?php if(isset($course['desc']) ) echo $course['desc'];  ?></textarea>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        Actions
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
<!--                                    <input type="hidden" name="course[lang_id]" value="<?php echo (isset($lang_id)) ? $lang_id : $active_lang['ID']; ?>" >
                                    <?php if(isset($lang_id)): ?>
                                        <input type="hidden" name="course[parent]" value="<?php echo (isset($parent)) ? $parent : 0; ?>" >
                                    <?php endif; ?>
                                    <?php if(isset($new_lang_varient)  && !$new_lang_varient ): ?>
                                        <input type="hidden" name="lang_varient" value="1" >
                                    <?php endif; ?>
                                        -->
                                     <input type="hidden" name="school_id" value="<?php echo $this->session->userdata('school_id'); ?>" >        
                                    <input type="hidden" name="action" value="<?php echo $action; ?>" >
                                    <input  type="submit" class="btn btn-primary pull-right" value="Save">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- /#page-wrapper -->