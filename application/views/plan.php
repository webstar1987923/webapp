<!-- Page Content -->
<?php // print_r($course); ?>
<div class="right_col">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-6">
                    <h1 class="page-header"> Plan registration</h1>
                </div>
                <div class="col-lg-6 full-height">
<!--                    <div class="pull-right" >
                        <?php 
                        $lid = (isset($lang_id)) ? $lang_id : 0;
                        if($langs &&  $action == 2): ?>
                            <?php foreach($langs as $lang): ?>
                                <a href="<?php echo '?lang_id='.$lang['ID']; ?>" class="btn btn-circle <?php echo ($lid && $lang['ID'] == $lid) ? 'active' : ''; ?>" ><?php echo $lang['code']; ?></a>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>-->
                </div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <form role="form" action="" method="post" enctype="multipart/form-data">
        <?php $response = $this->session->flashdata('response'); 
        if(!empty($response) || isset($code)):
            $class = (!empty($response)) ? $response['class'] :  $code['class'];
            $msg   = (!empty($response)) ? $response['msg']  : $code['msg'];
        ?>
        <div class="alert alert-<?php echo $class ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
        <?php endif; ?>
        <div class="row">

            <div class="col-lg-6">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        Plan informations
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Plan Name : </label>
                                    <input class="form-control" name="plan[name]" placeholder="Enter plan name" value="<?php if(isset($plan['name']) ) echo $plan['name'];  ?>" >
                                </div>
                                <div class="form-group">
                                    <label>Plan Amount : </label>
                                    <input class="form-control" name="plan[amount]" placeholder="Enter plan amount" value="<?php if(isset($plan['amount']) ) echo $plan['amount'];  ?>" >
                                </div> 
                                 <div class="form-group">
                                    <label>Plan Duration : </label>
                                    <input type="radio" name="plan[duration]" value="30" <?=$plan['duration']=="30" ? "checked" : ""?> >one month
                                    <input type="radio" name="plan[duration]" value="180" <?=$plan['duration']=="180" ? "checked" : ""?> >Six month
                                    <input type="radio" name="plan[duration]" value="365" <?=$plan['duration']=="365" ? "checked" : ""?> >one Year
                                </div> 
                                 <div class="form-group">
                                    <label>Plan Discount Title : </label>
                                    <input class="form-control" name="plan[title]" placeholder="Enter plan title" value="<?php if(isset($plan['title']) ) echo $plan['title'];  ?>" >
                                </div>                        
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          
            <div class="col-lg-6">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        Actions
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
<!--                                    <input type="hidden" name="plan[lang_id]" value="<?php echo (isset($lang_id)) ? $lang_id : $active_lang['ID']; ?>" >
                                    <?php if(isset($lang_id)): ?>
                                        <input type="hidden" name="plan[parent]" value="<?php echo (isset($parent)) ? $parent : 0; ?>" >
                                    <?php endif; ?>
                                    <?php if(isset($new_lang_varient)  && !$new_lang_varient ): ?>
                                        <input type="hidden" name="lang_varient" value="1" >
                                    <?php endif; ?>
                                        -->
                                    <input type="hidden" name="action" value="<?php echo $action; ?>" >
                                    <input  type="submit" class="btn btn-primary pull-right" value="Save">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- /#page-wrapper -->