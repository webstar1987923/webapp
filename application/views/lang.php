<!-- Page Content -->
<?php // print_r($lang); ?>
<div class="right_col">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Course Registration</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <form role="form" action="" method="post" enctype="multipart/form-data">
        <?php $response = $this->session->flashdata('response'); 
        if(!empty($response) || isset($code)):
            $class = (!empty($response)) ? $response['class'] :  $code['class'];
            $msg   = (!empty($response)) ? $response['msg']  : $code['msg'];
        ?>
        <div class="alert alert-<?php echo $class ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
        <?php endif; ?>
        <div class="row">

            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Language informations
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Language Name : </label>
                                    <input class="form-control" name="lang[name]" placeholder="Enter lang name" value="<?php if(isset($lang['name']) ) echo $lang['name'];  ?>" >
                                </div>
                                <div class="form-group">
                                    <label>Language code : </label>
                                    <input class="form-control" name="lang[code]" placeholder="Enter lang code" value="<?php if(isset($lang['code']) ) echo $lang['code'];  ?>" >
                                </div>
                                <div class="form-group">
                                    <label>Is default :</label>
                                    <div class="form-inline">
                                        <label class="radio-inline">
                                            <input type="radio" name="lang[active]" id="active-status" value="1" <?php if(isset($lang['active'])){ if($lang['active'] == 1) echo 'checked=""';}else{ echo 'checked=""'; } ?> >Active
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="lang[active]" id="inactive-status" <?php if(isset($lang['active'])){ if($lang['active'] == 0) echo 'checked=""';} ?> value="0" >In active
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Optional Informations
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Actions
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <input type="hidden" name="action" value="<?php echo $action; ?>" >
                                    <input  type="submit" class="btn btn-primary pull-right">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- /#page-wrapper -->