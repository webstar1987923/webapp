<div class="right_col">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">News List</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    List of News
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body blocking">
                    <?php $this->load->view('search-bar',array('plchldr' => "Search by name")); ?>
                    <form action="<?php echo base_url('ajax/delete/plan'); ?>" class="ajax-form" method="POST" >
                        <div id="list-load">
                            <div class="table-responsive">
                            <table class="table table-striped jambo_table bulk_action" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th class="column-title">ID</th>
                                    <th class="column-title">PIC</th>
                                    <th class="column-title">Title</th>
                                    <th class="column-title">Content</th>                                    
                                    <th class="column-title">Status</th>
                                    <!--<th>Status</th>-->
<!--                                    <th>Language</th>-->
                                    <th class="column-title">Action</th>
                                    
                                </tr>
                                </thead>
                                <tbody>
                                <?php if(isset($news) && !empty($news)): ?>
                                    <?php
//                                    $courses = array_reverse($courses);

                                    foreach($news as $newss): 
                                    $school = $this->db->where('id',$newss['school_id'])->get('abs_school')->row();
                                    ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $newss['id'] ?></td>
                                        <td align="center">
                                                        <?php
                                                        $newss['pic'] = ($newss['pic']) ? $newss['pic'] : 'nopic.png';
                                                        ?>
                                                        <img src="<?php echo base_url('uploads/' . $newss['pic']); ?>" height="35" width="40" />
                                                        <?php ?>
                                                    </td>
                                        <td><?php echo $newss['title']; ?></td>
                                        <td><?php echo $newss['content']; ?></td>                                                                               
                                        <td><?php echo ($newss['status'] == 1) ? 'active' : 'inactive'; ?></td>
<!--                                        <td><?php foreach($langs as $lang){ echo '<a href="'.base_url('manage/news/'.$newss['id'].'?lang_id='.$lang['ID']).'" title="'.$lang['code'].'">'.$lang['code'].'</a>&nbsp;&nbsp;'; }?></td>-->
                                        <td>
                                            <a href="<?php echo base_url('manage/news/'.$newss['id']); ?>" title="Edit" class="edit" ><i class="fa fa-pencil-square-o fa-fw"></i></a> 
                                            <a href="<?php echo base_url('delete/news/'. $newss['id']); ?>" title="Delete" class="delete"><i class="fa fa-times fa-fw"></i></a>                                           
                                           
                                        </td>
                                        
                                    </tr>
                                    <?php endforeach; ?>
                                    <?php else: ?>
                                         <tr class="odd gradeX" >
                                             <td colspan="6">No class found.</td>
                                        </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                            <!-- /.table-responsive -->
                        <?php $this->load->view('paginations'); ?>
                        </div>
                    </form>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>

