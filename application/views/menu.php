<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="<?php echo base_url('dashboard'); ?>" class="site_title"><i class="fa fa-clock-o"></i> <span>Admin panel</span></a>
        </div>

        <div class="clearfix"></div>

        <!-- menu profile quick info -->
<!--        <div class="profile clearfix">
            <div class="profile_pic">
                <img src="/images/img.jpg" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php echo $this->session->userdata('user_name'); ?></h2>
            </div>
        </div>
         /menu profile quick info 

        <br />-->

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <!--<h3>General</h3>-->
                <ul class="nav side-menu">
                    <?php if ($this->session->userdata('is_school') == true) { ?>

                        <?php foreach ($menu_items as $link) { ?>
                            <?php if (in_array($this->session->userdata('user_type'), $link['access'])) { ?>
                                <?php
                                $class = '';
                                $controller = $this->uri->segment(1);
                                $slug = $this->uri->segment(2);
                                //             echo $slug;
                                if ($link['controller'] == $controller && $link['slug'] == $slug) {
                                    $class = "class='active'";
                                }
                                $uri = $link['controller'] . '/' . $link['slug'];
                                $attr_val = '';
                                if (!empty($link['attr'])) {
                                    $attr_arr = array();
                                    foreach ($link['attr'] as $key => $value) {
                                        $attr_arr[] = "$key = '$value'";
                                    }
                                    $attr_val = implode(' ', $attr_arr);
                                }
                                ?>
                                <li <?php if (!empty($link['childs']) && $link['active']) { ?>class="active"<?php } ?>>
                                    <a href="<?php print (isset($link['controller']))?base_url($uri):"javascript:void(0)";?>" <?php echo $class . ' ' . $attr_val; ?> >
                                        <i class="fa <?php echo $link['icon']; ?> fa-fw"></i>

                                        <?php echo $link['title']; ?>
                                        <?php if (isset($link['childs']) && $link['childs']): ?>
                                            <span class="fa fa-chevron-left"></span>
                                        <?php endif; ?>  
                                    </a>
                                    <?php if (isset($link['childs']) && $link['childs']):
                                        ?>

                                        <ul class="nav child_menu">
                                            <?php
                                            foreach ($link['childs'] as $child_link):

                                                $class = '';
                                                $controller = $this->uri->segment(1);
                                                $slug = $this->uri->segment(2);
                                                //             echo $slug;
                                                if ($child_link['controller'] == $controller && $child_link['slug'] == $slug) {
                                                    $class = "class='current-page'";
                                                }
                                                $uri = $child_link['controller'] . '/' . $child_link['slug'];
                                                $attr_val = '';
                                                if (!empty($child_link['attr'])) {
                                                    $attr_arr = array();
                                                    foreach ($child_link['attr'] as $key => $value) {
                                                        $attr_arr[] = "$key = '$value'";
                                                    }
                                                    $attr_val = implode(' ', $attr_arr);
                                                }
                                                ?>
                                                <li <?php print $class;?>>
                                                    <a href="<?php echo base_url($uri); ?>" <?php echo $attr_val; ?> >
                                                        <!--<i class="fa <?php echo $child_link['icon']; ?> fa-fw"></i>-->
                                                        <?php echo $child_link['title']; ?></a>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    <?php endif; ?>                        
                                </li>
                            <?php } ?>
                        <?php } ?>
                    <?php } elseif ($this->session->userdata('is_admin') == true) { ?>

                        <?php foreach ($menu_items_admin as $link) { ?>
                            <?php
                            $class = '';
                            $controller = $this->uri->segment(1);
                            $slug = $this->uri->segment(2);
                            //             echo $slug;
                            if ($link['controller'] == $controller && $link['slug'] == $slug) {
                                $class = "class='active'";
                            }
                            $uri = $link['controller'] . '/' . $link['slug'];
                            $attr_val = '';
                            if (!empty($link['attr'])) {
                                $attr_arr = array();
                                foreach ($link['attr'] as $key => $value) {
                                    $attr_arr[] = "$key = '$value'";
                                }
                                $attr_val = implode(' ', $attr_arr);
                            }
                            ?>
                            <li <?php if (!empty($link['childs']) && $link['active']) { ?>class="active"<?php } ?>>
                                <a href="<?php echo base_url($uri); ?>" <?php echo $class . ' ' . $attr_val; ?> >
                                    <i class="fa <?php echo $link['icon']; ?> fa-fw"></i>

                                    <?php echo $link['title']; ?>
                                    <?php if (isset($link['childs']) && $link['childs']): ?>
                                        <span class="fa-chevron-left"></span>
                                    <?php endif; ?>  
                                </a>
                                <?php if (isset($link['childs']) && $link['childs']):
                                    ?>

                                    <ul class="nav child_menu">
                                        <?php
                                        foreach ($link['childs'] as $child_link):

                                            $class = '';
                                            $controller = $this->uri->segment(1);
                                            $slug = $this->uri->segment(2);
                                            //             echo $slug;
                                            if ($child_link['controller'] == $controller && $child_link['slug'] == $slug) {
                                                $class = "class='active'";
                                            }
                                            $uri = $child_link['controller'] . '/' . $child_link['slug'];
                                            $attr_val = '';
                                            if (!empty($child_link['attr'])) {
                                                $attr_arr = array();
                                                foreach ($child_link['attr'] as $key => $value) {
                                                    $attr_arr[] = "$key = '$value'";
                                                }
                                                $attr_val = implode(' ', $attr_arr);
                                            }
                                            ?>
                                            <li <?php print $class;?>>
                                                <a href="<?php echo base_url($uri); ?>" <?php echo  $attr_val; ?> >
                                                    <!--<i class="fa <?php echo $child_link['icon']; ?> fa-fw"></i>-->
                                                    <?php echo $child_link['title']; ?></a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>                        
                            </li>
                        <?php } ?>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->

    </div>
</div>
<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <img src="/images/img.jpg" alt=""><?php echo $this->session->userdata('user_name'); ?>
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                        <li><a href="javascript:;"> <?php echo $this->session->userdata('user_name'); ?></a></li>
                        <li><a href="<?php echo base_url('login/logout'); ?>"><i class="fa fa-sign-out pull-right"></i> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</div>