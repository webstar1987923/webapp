<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

     function course($user_no) {
       $courses =  $this->s_model->getDatas('courses',array('parent' => 0));
       $data['courses'] = $courses;
       
       $teacher =  $this->s_model->getData('users',array('user_no' => $user_no));
       $data['teacher'] = $teacher;
       $assigned_courses = $this->s_model->getDatas('teacher_courses',array('user_no' => $user_no));
       $stcs = array();
       if(!empty($assigned_courses)){
            foreach($assigned_courses as $course){
                $stcs[] = $course['cid'];
            }
       }
       $data['assigned_courses'] = $stcs;
       $data['action'] = 1;
       
       if($this->post('action') == 1){
            $post_cporses = $this->post('courses');
//            print_r('asads');
//            print_r($_POST);
            if(!empty($post_cporses)){
                $this->s_model->deleteRecord('teacher_courses',array('sid' => $user_no));
                foreach($post_cporses as $cs){
                    $this->s_model->insertRecord('teacher_courses',array(
                        'cid' => $cs,
                        'user_no' => $user_no,
                        'asssigned_by' => $this->session->userdata('user_no')
                    ));
                }
                $this->redirectTO('assign/teacher/'.$user_no, 'The teacher is successfully assigned.');
            }else{
                 
                $data['code']['status'] = 4;
                $data['code']['class'] = 'danger';
                $data['code']['msg'] = $this->langLine('select_course');
               
                $this->s_lib->loadView('assign-teacher',$data);
                return false;
            }
       }
       $this->s_lib->loadView('assign-teacher',$data);
    }