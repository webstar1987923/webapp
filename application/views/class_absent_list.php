﻿<div class="right_col">

    <div class="row">

        <div class="col-lg-12">

            <h1 class="page-header">Class Absent</h1>

        </div>

        <!-- /.col-lg-12 -->

    </div>

    <!-- /.row -->

    <div class="row">

        <div class="col-lg-12">

            <div class="panel panel-default">

                <div class="panel-heading">

                    List of Classes

                </div>

                <!-- /.panel-heading -->

                <div class="panel-body blocking">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body blocking">
                                    <div class="form-group form-inline">
                                        <form action="" method="post" class="col-lg-12">
                                            <input type="hidden" name="action" value="1" >
                                            <div class="form-group col-lg-4">
                                                <label>Class Date</label>
                                                <input type="text" class="form-control datepicker" autocomplete="off" name="start_date" placeholder="Choose Date" value="<?= $start_date ?>" >
                                            </div>
                                            <div class="col-lg-1"><label>And</label></div>
                                            <div class="col-lg-6">
                                                <?php if ($courses): ?>

                                                    <label>Courses</label>

                                                    <ul class="list-inline">
                                                        <?php
                                                        if (count($courses) > 0) {
                                                            ?>
                                                            <li class="radio"><input type="radio" class="choose_course flat" name="course_id[]" value="-1" <?php
                                                                if (in_array('-1', $course_id)) {
                                                                    echo "checked";
                                                                } else {
                                                                    echo "";
                                                                }
                                                                ?> >All Classes</li>
                                                                <?php foreach ($courses as $course): ?>

                                                                <li class="radio"><input type="radio" class="choose_course flat" name="course_id[]" value="<?php echo $course['cid']; ?>" <?php
                                                                    if (in_array($course['cid'], $course_id)) {
                                                                        echo "checked";
                                                                    } else {
                                                                        echo "";
                                                                    }
                                                                    ?> ><?php echo $course['name']; ?></li>

                                                                <?php
                                                            endforeach;
                                                        }
                                                        ?>

                                                    </ul>

                                                <?php endif; ?>
                                            </div>
                                            <div class="col-lg-1">
                                                <label></label>
                                                <button type="submit" class="btn btn-default">Filter</button> 
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php $print_div_counter = 0; ?>
                    <?php
                    if ($action) {
                        ?>
                        <div id="list-load">

                            <div class="table-responsive">
                                <?php
                                $row = 0;
                                if (is_array($students) && !empty($students)) {
                                    $row = count($students);
                                    ?>
                                    <div class="row" style="margin-bottom:10px;">
                                        <div class="col-lg-9"></div>
                                        <div class="col-lg-3">
                                            <button type="button" class="btn btn-primary" id="printreport">Print</button>
                                            <a href="<?= base_url('lists/classabsent_excl/' . $start_date . '/' . $course_id[0]) ?>" style="text-decoration: none;" target="_new" >
                                                <button type="button" class="btn btn-general" style="margin-left:15px;text-decoration: none;">Excel Export</button>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="row"></div>
                                    <?php
                                }
                                ?>
                                <?php if ($course_id[0] != '-1') { ?>
                                    <div class="form-group">
                                        <h5><strong><u>
                                                    <?php
                                                    if ($action) {
                                                        echo "Revealed the daily absences Date " . $start_date . " Class " . $course_code;
                                                    }
                                                    ?>
                                                </u></strong></h5>
                                    </div>
                                    <table class="table table-striped table-bordered table-hover table-for-print" data-value="<?php print $print_div_counter; ?>" row="<?= $row ?>">

                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Student ID</th>
                                                <th>Student Name</th>
                                                <th>Sem-1</th>
                                                <th>Sem-2</th>
                                                <th>Sem-3</th>
                                                <th>Sem-4</th>
                                                <th>Sem-5</th>
                                                <th>Sem-6</th>
                                                <th>Sem-7</th>
                                            </tr>
                                        </thead>

                                        <tbody>

                                            <?php
                                            if (isset($students) && !empty($students)):

                                                $row_count = 0;
                                                $total_student = count($students);
                                                $sem_1_present = 0;
                                                $sem_2_present = 0;
                                                $sem_3_present = 0;
                                                $sem_4_present = 0;
                                                $sem_5_present = 0;
                                                $sem_6_present = 0;
                                                $sem_7_present = 0;
                                                foreach ($students as $student):
                                                    $sem_reports = $student['sem_report'];
                                                    // print_r($sem_reports);die;
                                                    ?>
                                                    <tr class="odd gradeX">
                                                        <td><?php echo ++$row_count; ?></td>
                                                        <td><?php echo $student['student_id'] ?></td>
                                                        <td><?php echo $student['name']; ?></td>
                                                        <?php
                                                        if (is_array($sem_reports)) {
                                                            for ($i=0; $i < 7; $i++) { 
                                                                $sem_report = $sem_reports[$i];
                                                                $sng = "<i class='fa fa-minus'></i>";
                                                                $is_increase = false;
                                                                if (is_array($sem_report)) {
                                                                    switch ($sem_report['attendance']) {
                                                                        case 0:
                                                                            $sng = "<i class='fa fa-times'></i>";
                                                                            break;
                                                                        case 1:
                                                                            $is_increase = true;
                                                                            $sng = "<i class='fa fa-check'></i>";
                                                                            break;
                                                                        case 2:
                                                                            $sng = "<i class='fa fa-times'></i>";
                                                                            break;
                                                                        case 3:
                                                                            $is_increase = true;
                                                                            $sng = "<i class='fa fa-circle-o'></i>";
                                                                            break;
                                                                        default:
                                                                            $sng = "<i class='fa fa-minus'></i>";
                                                                            break;
                                                                    }
                                                                }
                                                                switch ($i + 1) {
                                                                    case 1:
                                                                        if ($is_increase) {
                                                                            $sem_1_present++;
                                                                        }
                                                                        break;
                                                                    case 2:
                                                                        if ($is_increase) {
                                                                            $sem_2_present++;
                                                                        }
                                                                        break;
                                                                    case 3:
                                                                        if ($is_increase) {
                                                                            $sem_3_present++;
                                                                        }
                                                                        break;
                                                                    case 4:
                                                                        if ($is_increase) {
                                                                            $sem_4_present++;
                                                                        }
                                                                        break;
                                                                    case 5:
                                                                        if ($is_increase) {
                                                                            $sem_5_present++;
                                                                        }
                                                                        break;
                                                                    case 6:
                                                                        if ($is_increase) {
                                                                            $sem_6_present++;
                                                                        }
                                                                        break;
                                                                    case 7:
                                                                        if ($is_increase) {
                                                                            $sem_7_present++;
                                                                        }
                                                                        break;
                                                                    default:
                                                                        break;
                                                                }
                                                                ?>
                                                                <td align="center"><?php echo $sng; ?></td>	
                                                            <?php }} ?>
                                                    </tr>
                                                <?php endforeach; ?>
                                                <!-- Mrin total details section  -->
                                                <?php
                                                //print_r($sem_teachers);
                                                //echo (($total_student-$sem_1_present)>0)?($total_student-$sem_1_present):'-';
                                                for ($i = 0; $i < 4; $i++) {
                                                    $text = '';
                                                    $row_1 = '';
                                                    $row_2 = '';
                                                    $row_3 = '';
                                                    $row_4 = '';
                                                    $row_5 = '';
                                                    $row_6 = '';
                                                    $row_7 = '';
                                                    switch ($i) {
                                                        case 0:
                                                            $text = "Total Number Of Student";
                                                            $row_1 = $total_student;
                                                            $row_2 = $total_student;
                                                            $row_3 = $total_student;
                                                            $row_4 = $total_student;
                                                            $row_5 = $total_student;
                                                            $row_6 = $total_student;
                                                            $row_7 = $total_student;
                                                            break;
                                                        case 1:
                                                            $text = "Actual Attendance Number";
                                                            $row_1 = (!$sem_1_present) ? '-' : $sem_1_present;
                                                            $row_2 = (!$sem_2_present) ? '-' : $sem_2_present;
                                                            $row_3 = (!$sem_3_present) ? '-' : $sem_3_present;
                                                            $row_4 = (!$sem_4_present) ? '-' : $sem_4_present;
                                                            $row_5 = (!$sem_5_present) ? '-' : $sem_5_present;
                                                            $row_6 = (!$sem_6_present) ? '-' : $sem_6_present;
                                                            $row_7 = (!$sem_7_present) ? '-' : $sem_7_present;
                                                            break;
                                                        case 2:
                                                            $text = "Absent Student Number";
                                                            $row_1 = (($total_student - $sem_1_present) > 0) ? ($total_student - $sem_1_present) : '-';
                                                            $row_2 = (($total_student - $sem_2_present) > 0) ? ($total_student - $sem_2_present) : '-';
                                                            $row_3 = (($total_student - $sem_3_present) > 0) ? ($total_student - $sem_3_present) : '-';
                                                            $row_4 = (($total_student - $sem_4_present) > 0) ? ($total_student - $sem_4_present) : '-';
                                                            $row_5 = (($total_student - $sem_5_present) > 0) ? ($total_student - $sem_5_present) : '-';
                                                            $row_6 = (($total_student - $sem_6_present) > 0) ? ($total_student - $sem_6_present) : '-';
                                                            $row_7 = (($total_student - $sem_7_present) > 0) ? ($total_student - $sem_7_present) : '-';
                                                            break;
                                                        case 3:
                                                            $text = "Teacher Name";
                                                            $row_1 = '';
                                                            $row_2 = '';
                                                            $row_3 = '';
                                                            $row_4 = '';
                                                            $row_5 = '';
                                                            $row_6 = '';
                                                            $row_7 = '';
                                                            if (count($sem_teachers) > 6) {
                                                                $row_1 = $sem_teachers[0]['name'];
                                                                $row_2 = $sem_teachers[1]['name'];
                                                                $row_3 = $sem_teachers[2]['name'];
                                                                $row_4 = $sem_teachers[3]['name'];
                                                                $row_5 = $sem_teachers[4]['name'];
                                                                $row_6 = $sem_teachers[5]['name'];
                                                                $row_7 = $sem_teachers[6]['name'];
                                                            }
                                                            break;
                                                        default:
                                                            break;
                                                    }
                                                    ?>
                                                    <tr>
                                                        <td colspan="3" align="center"><?php echo $text; ?></td>
                                                        <td align="center"><?php echo $row_1; ?></td>
                                                        <td align="center"><?php echo $row_2; ?></td>
                                                        <td align="center"><?php echo $row_3; ?></td>
                                                        <td align="center"><?php echo $row_4; ?></td>
                                                        <td align="center"><?php echo $row_5; ?></td>
                                                        <td align="center"><?php echo $row_6; ?></td>
                                                        <td align="center"><?php echo $row_7; ?></td>
                                                    <tr>
                                                        <?php
                                                    }
                                                    ?>
                                                    <!-- end section -->
                                                <?php else: ?>

                                                <tr class="odd gradeX" >

                                                    <td colspan="10">No students found.</td>

                                                </tr>
                                            <?php endif; ?>
                                        </tbody>

                                    </table>
                                <?php }else { ?>

                                    <?php foreach ($students as $cid => $class_students) { ?>
                                        <div class="form-group">
                                            <h5><strong><u>
                                                        <?php
                                                        if ($action) {
                                                            echo "Revealed the daily absences Date " . $start_date . " Class " . $courses[$cid]['code'];
                                                        }
                                                        ?>
                                                    </u></strong></h5>
                                        </div>
                                        <?php $row = count($class_students); ?>
                                        <table class="table table-striped table-bordered table-hover table-for-print" data-value="<?php print $print_div_counter; ?>" row="<?= $row ?>">

                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Student ID</th>
                                                    <th>Student Name</th>
                                                    <th>Sem-1</th>
                                                    <th>Sem-2</th>
                                                    <th>Sem-3</th>
                                                    <th>Sem-4</th>
                                                    <th>Sem-5</th>
                                                    <th>Sem-6</th>
                                                    <th>Sem-7</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <?php
                                                if (isset($class_students) && !empty($class_students)):

                                                    $row_count = 0;
                                                    $total_student = count($class_students);
                                                    $sem_1_present = 0;
                                                    $sem_2_present = 0;
                                                    $sem_3_present = 0;
                                                    $sem_4_present = 0;
                                                    $sem_5_present = 0;
                                                    $sem_6_present = 0;
                                                    $sem_7_present = 0;
                                                    foreach ($class_students as $student):
                                                        $sem_reports = $student['sem_report'];
                                                        // print_r($sem_reports);die;
                                                        ?>
                                                        <tr class="odd gradeX">
                                                            <td><?php echo ++$row_count; ?></td>
                                                            <td><?php echo $student['student_id'] ?></td>
                                                            <td><?php echo $student['name']; ?></td>
                                                            <?php
                                                            if (is_array($sem_reports)) {
                                                                for ($i=0; $i < 7; $i++) { 
                                                                    $sem_report = $sem_reports[$i];
                                                                    $sng = "<i class='fa fa-minus'></i>";
                                                                    $is_increase = false;
                                                                    if (is_array($sem_report)) {
                                                                        switch ($sem_report['attendance']) {
                                                                            case 0:
                                                                                $sng = "<i class='fa fa-times'></i>";
                                                                                break;
                                                                            case 1:
                                                                                $is_increase = true;
                                                                                $sng = "<i class='fa fa-check'></i>";
                                                                                break;
                                                                            case 2:
                                                                                $sng = "<i class='fa fa-times'></i>";
                                                                                break;
                                                                            case 3:
                                                                                $is_increase = true;
                                                                                $sng = "<i class='fa fa-circle-o'></i>";
                                                                                break;
                                                                            default:
                                                                                $sng = "<i class='fa fa-minus'></i>";
                                                                                break;
                                                                        }
                                                                    }
                                                                    switch ($i + 1) {
                                                                        case 1:
                                                                            if ($is_increase) {
                                                                                $sem_1_present++;
                                                                            }
                                                                            break;
                                                                        case 2:
                                                                            if ($is_increase) {
                                                                                $sem_2_present++;
                                                                            }
                                                                            break;
                                                                        case 3:
                                                                            if ($is_increase) {
                                                                                $sem_3_present++;
                                                                            }
                                                                            break;
                                                                        case 4:
                                                                            if ($is_increase) {
                                                                                $sem_4_present++;
                                                                            }
                                                                            break;
                                                                        case 5:
                                                                            if ($is_increase) {
                                                                                $sem_5_present++;
                                                                            }
                                                                            break;
                                                                        case 6:
                                                                            if ($is_increase) {
                                                                                $sem_6_present++;
                                                                            }
                                                                            break;
                                                                        case 7:
                                                                            if ($is_increase) {
                                                                                $sem_7_present++;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            break;
                                                                    }
                                                                    ?>
                                                                    <td align="center"><?php echo $sng; ?></td>	
                                                                <?php }} ?>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                    <!-- Mrin total details section  -->
                                                    <?php
                                                    //print_r($sem_teachers);
                                                    //echo (($total_student-$sem_1_present)>0)?($total_student-$sem_1_present):'-';
                                                    for ($i = 0; $i < 4; $i++) {
                                                        $text = '';
                                                        $row_1 = '';
                                                        $row_2 = '';
                                                        $row_3 = '';
                                                        $row_4 = '';
                                                        $row_5 = '';
                                                        $row_6 = '';
                                                        $row_7 = '';
                                                        switch ($i) {
                                                            case 0:
                                                                $text = "Total Number Of Student";
                                                                $row_1 = $total_student;
                                                                $row_2 = $total_student;
                                                                $row_3 = $total_student;
                                                                $row_4 = $total_student;
                                                                $row_5 = $total_student;
                                                                $row_6 = $total_student;
                                                                $row_7 = $total_student;
                                                                break;
                                                            case 1:
                                                                $text = "Actual Attendance Number";
                                                                $row_1 = (!$sem_1_present) ? '-' : $sem_1_present;
                                                                $row_2 = (!$sem_2_present) ? '-' : $sem_2_present;
                                                                $row_3 = (!$sem_3_present) ? '-' : $sem_3_present;
                                                                $row_4 = (!$sem_4_present) ? '-' : $sem_4_present;
                                                                $row_5 = (!$sem_5_present) ? '-' : $sem_5_present;
                                                                $row_6 = (!$sem_6_present) ? '-' : $sem_6_present;
                                                                $row_7 = (!$sem_7_present) ? '-' : $sem_7_present;
                                                                break;
                                                            case 2:
                                                                $text = "Absent Student Number";
                                                                $row_1 = (($total_student - $sem_1_present) > 0) ? ($total_student - $sem_1_present) : '-';
                                                                $row_2 = (($total_student - $sem_2_present) > 0) ? ($total_student - $sem_2_present) : '-';
                                                                $row_3 = (($total_student - $sem_3_present) > 0) ? ($total_student - $sem_3_present) : '-';
                                                                $row_4 = (($total_student - $sem_4_present) > 0) ? ($total_student - $sem_4_present) : '-';
                                                                $row_5 = (($total_student - $sem_5_present) > 0) ? ($total_student - $sem_5_present) : '-';
                                                                $row_6 = (($total_student - $sem_6_present) > 0) ? ($total_student - $sem_6_present) : '-';
                                                                $row_7 = (($total_student - $sem_7_present) > 0) ? ($total_student - $sem_7_present) : '-';
                                                                break;
                                                            case 3:
                                                                $text = "Teacher Name";
                                                                $row_1 = '';
                                                                $row_2 = '';
                                                                $row_3 = '';
                                                                $row_4 = '';
                                                                $row_5 = '';
                                                                $row_6 = '';
                                                                $row_7 = '';
                                                                if (count($sem_teachers) > 6) {
                                                                    $row_1 = $sem_teachers[0]['name'];
                                                                    $row_2 = $sem_teachers[1]['name'];
                                                                    $row_3 = $sem_teachers[2]['name'];
                                                                    $row_4 = $sem_teachers[3]['name'];
                                                                    $row_5 = $sem_teachers[4]['name'];
                                                                    $row_6 = $sem_teachers[5]['name'];
                                                                    $row_7 = $sem_teachers[6]['name'];
                                                                }
                                                                break;
                                                            default:
                                                                break;
                                                        }
                                                        ?>
                                                        <tr>
                                                            <td colspan="3" align="center"><?php echo $text; ?></td>
                                                            <td align="center"><?php echo $row_1; ?></td>
                                                            <td align="center"><?php echo $row_2; ?></td>
                                                            <td align="center"><?php echo $row_3; ?></td>
                                                            <td align="center"><?php echo $row_4; ?></td>
                                                            <td align="center"><?php echo $row_5; ?></td>
                                                            <td align="center"><?php echo $row_6; ?></td>
                                                            <td align="center"><?php echo $row_7; ?></td>
                                                        <tr>
                                                            <?php
                                                        }
                                                        ?>
                                                        <!-- end section -->
                                                    <?php else: ?>

                                                    <tr class="odd gradeX" >

                                                        <td colspan="10">No students found.</td>

                                                    </tr>
                                                <?php endif; ?>
                                            </tbody>

                                        </table>
                                        <?php $print_div_counter++; ?>
                                    <?php } ?>
                                <?php } ?>
                            </div>

                            <!-- /.table-responsive -->

                            <?php //$this->load->view('paginations');    ?>

                        </div>

                    </div>

                    <!-- /.panel-body -->

                </div>

                <?php
            } else {
                ?>
                <div class="list-load" style="height:200px;"></div>
                <?php
            }
            ?>
            <!-- /.panel -->

        </div>

        <!-- /.col-lg-12 -->

    </div>

    <!-- /.row -->

</div>

<!-- arabic print view -->


<?php $print_div_counter = 0; ?>
<?php if ($course_id[0] != '-1') { ?>
    <div style="display:none;">
        <div id="reportprintdiv_<?php print $print_div_counter; ?>">
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-4 pull-left">
                        <h4 style="text-align: right;padding-top:10px;display:block;margin-left:30px;">
                            سجل متابعة الطلاب
                        </h4>
                    </div>
                    <div class="col-lg-8 pull-right" style="text-align: right;">
                        <img src="<?= base_url('uploads/report_logo/report_logo_right.png') ?>" width="50" height="60" style="page-break-inside: avoid;display:inline-block;vertical-align:top;margin-right:100px;">
                        <h4 style="display: inline-block;text-align: center;">وزارة التربية<br/><?php print $current_school['detail'];?><br><?php print $current_school['school_name'];?></h4>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top:10px;">
                <div class="col-lg-12 text-center" style='margin-bottom:0px!important;'>
                <h5><strong style="font-size: 16px;"><u>
                            <?php
                            if ($action) {
                                //echo "كشفت التاريخ الغياب اليومي ".$start_date." صنف ".$course_code;
                                echo "كشف الغياب اليومي بتاريخ " . $start_date . " صف " . $course_code;
                                //echo $course_code." كشف الغياب اليومي بتاريخ ".$start_date." صف ";
                            }
                            ?>
                            </u>
                        </strong></h5>
                </div>
            </div>
            <table class="table table-striped table-bordered table-hover" data-value="<?php print $print_div_counter; ?>" row="<?= $row ?>">
                <thead>
                    <tr>
                        <td align="center" style="padding:2px;margin:0px;">7 - الحصة</td>
                        <td align="center" style="padding:2px;margin:0px;">6 - الحصة</td>
                        <td align="center" style="padding:2px;margin:0px;">5 - الحصة</td>
                        <td align="center" style="padding:2px;margin:0px;">4 - الحصة</td>
                        <td align="center" style="padding:2px;margin:0px;">3 - الحصة</td>
                        <td align="center" style="padding:2px;margin:0px;">2 - الحصة</td>
                        <td align="center" style="padding:2px;margin:0px;">1 - الحصة</td>
                        <td align="center" style="width:250px !important;padding:2px;margin:0px;">أسم الطالب</td>
                        <!--<td align="center">هوية الطالب</td>-->
                        <td align="center" style="padding:2px;margin:0px;">م</td>
                    </tr>
                </thead>

                <tbody>

                    <?php
                    if (isset($students) && !empty($students)):

                        $row_count = 0;
                        $total_student = count($students);
                        $sem_1_present = 0;
                        $sem_2_present = 0;
                        $sem_3_present = 0;
                        $sem_4_present = 0;
                        $sem_5_present = 0;
                        $sem_6_present = 0;
                        $sem_7_present = 0;
                        foreach ($students as $student):
                            $sem_reports = $student['sem_report'];
                            //print_r($sem_reports);
                            ?>
                            <tr class="odd gradeX">

                                <?php
                                if (is_array($sem_reports)) {
                                    for ($i=6; $i >= 0; $i--) { 
                                        $sem_report = $sem_reports[$i];
                                        $sng = "<i class='fa fa-minus'></i>";
                                        $is_increase = false;
                                        if (is_array($sem_report)) {
                                            switch ($sem_report['attendance']) {
                                                case 0:
                                                    $sng = "<i class='fa fa-times'></i>";
                                                    break;
                                                case 1:
                                                    $is_increase = true;
                                                    $sng = "<i class='fa fa-check'></i>";
                                                    break;
                                                case 2:
                                                    $sng = "<i class='fa fa-times'></i>";
                                                    break;
                                                case 3:
                                                    $is_increase = true;
                                                    $sng = "<i class='fa fa-circle-o'></i>";
                                                    break;
                                                default:
                                                    $sng = "<i class='fa fa-minus'></i>";
                                                    break;
                                            }
                                        }
                                        switch ($i + 1) {
                                            case 1:
                                                if ($is_increase) {
                                                    $sem_1_present++;
                                                }
                                                break;
                                            case 2:
                                                if ($is_increase) {
                                                    $sem_2_present++;
                                                }
                                                break;
                                            case 3:
                                                if ($is_increase) {
                                                    $sem_3_present++;
                                                }
                                                break;
                                            case 4:
                                                if ($is_increase) {
                                                    $sem_4_present++;
                                                }
                                                break;
                                            case 5:
                                                if ($is_increase) {
                                                    $sem_5_present++;
                                                }
                                                break;
                                            case 6:
                                                if ($is_increase) {
                                                    $sem_6_present++;
                                                }
                                                break;
                                            case 7:
                                                if ($is_increase) {
                                                    $sem_7_present++;
                                                }
                                                break;
                                            default:
                                                break;
                                        }
                                        ?>
                                        <td align="center"><?php echo $sng; ?></td>	
                                <?php }} ?>

                                <td align="right" style="padding:3px;margin:0px;"><?php echo $student['name']; ?></td>
                                <!--<td align="right"><?php echo $student['student_id'] ?></td>-->
                                <td align="center" style="padding:3px;margin:0px;"><?php echo str_pad(++$row_count, 2, 0, STR_PAD_LEFT); ?></td>

                            </tr>
                        <?php endforeach; ?>
                        <!-- Mrin total details section  -->
                        <?php
                        //print_r($sem_teachers);
                        //echo (($total_student-$sem_1_present)>0)?($total_student-$sem_1_present):'-';
                        for ($i = 0; $i < 4; $i++) {
                            $text = '';
                            $row_1 = '';
                            $row_2 = '';
                            $row_3 = '';
                            $row_4 = '';
                            $row_5 = '';
                            $row_6 = '';
                            $row_7 = '';
                            switch ($i) {
                                case 0:
                                    $text = "إجمالي عدد الطلاب";
                                    $row_1 = $total_student;
                                    $row_2 = $total_student;
                                    $row_3 = $total_student;
                                    $row_4 = $total_student;
                                    $row_5 = $total_student;
                                    $row_6 = $total_student;
                                    $row_7 = $total_student;
                                    break;
                                case 1:
                                    $text = "عدد الحضور الفعلي"; //"Actual Attendance Number";
                                    $row_1 = (!$sem_1_present) ? '-' : $sem_1_present;
                                    $row_2 = (!$sem_2_present) ? '-' : $sem_2_present;
                                    $row_3 = (!$sem_3_present) ? '-' : $sem_3_present;
                                    $row_4 = (!$sem_4_present) ? '-' : $sem_4_present;
                                    $row_5 = (!$sem_5_present) ? '-' : $sem_5_present;
                                    $row_6 = (!$sem_6_present) ? '-' : $sem_6_present;
                                    $row_7 = (!$sem_7_present) ? '-' : $sem_7_present;
                                    break;
                                case 2:
                                    $text = "عدد الطلاب الغائبين"; //"Absent Student Number";
                                    $row_1 = (($total_student - $sem_1_present) > 0) ? ($total_student - $sem_1_present) : '-';
                                    $row_2 = (($total_student - $sem_2_present) > 0) ? ($total_student - $sem_2_present) : '-';
                                    $row_3 = (($total_student - $sem_3_present) > 0) ? ($total_student - $sem_3_present) : '-';
                                    $row_4 = (($total_student - $sem_4_present) > 0) ? ($total_student - $sem_4_present) : '-';
                                    $row_5 = (($total_student - $sem_5_present) > 0) ? ($total_student - $sem_5_present) : '-';
                                    $row_6 = (($total_student - $sem_6_present) > 0) ? ($total_student - $sem_6_present) : '-';
                                    $row_7 = (($total_student - $sem_7_present) > 0) ? ($total_student - $sem_7_present) : '-';
                                    break;
                                case 3:
                                    $text = "إسم المعلم"; //"Teacher Name";
                                    $row_1 = '';
                                    $row_2 = '';
                                    $row_3 = '';
                                    $row_4 = '';
                                    $row_5 = '';
                                    $row_6 = '';
                                    $row_7 = '';
                                    if (count($sem_teachers) > 6) {
                                        $row_1 = $sem_teachers[0]['name'];
                                        $row_2 = $sem_teachers[1]['name'];
                                        $row_3 = $sem_teachers[2]['name'];
                                        $row_4 = $sem_teachers[3]['name'];
                                        $row_5 = $sem_teachers[4]['name'];
                                        $row_6 = $sem_teachers[5]['name'];
                                        $row_7 = $sem_teachers[6]['name'];
                                    }
                                    break;
                                default:
                                    break;
                            }
                            ?>
                            <tr>
                                <td align="center" style="padding:3px;margin:0px;"><?php echo $row_7; ?></td>
                                <td align="center" style="padding:3px;margin:0px;"><?php echo $row_6; ?></td>
                                <td align="center" style="padding:3px;margin:0px;"><?php echo $row_5; ?></td>
                                <td align="center" style="padding:3px;margin:0px;"><?php echo $row_4; ?></td>
                                <td align="center" style="padding:3px;margin:0px;"><?php echo $row_3; ?></td>
                                <td align="center" style="padding:3px;margin:0px;"><?php echo $row_2; ?></td>
                                <td align="center" style="padding:3px;margin:0px;"><?php echo $row_1; ?></td>
                                <td colspan="2" align="center" style="padding:3px;margin:0px;"><?php echo $text; ?></td>
                            <tr>
                                <?php
                            }
                            ?>
                            <!-- end section -->
                        <?php else: ?>

                        <tr class="odd gradeX" >

                            <td colspan="9" style="padding:3px;margin:0px;">No students found.</td>

                        </tr>
                    <?php endif; ?>
                </tbody>

            </table>
            <div class="form-group" style="text-align: left; margin-bottom:0px!important;">
                <h5><strong>مدير المدرسة</strong></h5>
            </div>
        </div>
    </div>
<?php }else { ?>
    <?php foreach ($students as $cid => $class_students) { ?>
        <?php $row = count($class_students); ?>
        <div style="display:none;">
            <div id="reportprintdiv_<?php print $print_div_counter; ?>">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-lg-4 pull-left">
                            <h4 style="text-align: right;padding-top:10px;display:block;margin-left:30px;">
                                سجل متابعة الطلاب
                            </h4>
                        </div>
                        <div class="col-lg-8 pull-right" style="text-align: right;">
                            <img src="<?= base_url('uploads/report_logo/report_logo_right.png') ?>" width="50" height="60" style="page-break-inside: avoid;display:inline-block;vertical-align:top;margin-right:100px;">
                            <h4 style="display: inline-block;text-align: center;">وزارة التربية<br/><?php print $current_school['detail'];?><br><?php print $current_school['school_name'];?></h4>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-top:10px;">
                    <div class="col-lg-12 text-center" style='margin-bottom:0px!important;'>
                        <h5 style="text-align: center;"><strong style="font-size: 16px;">
                                    <u><?php
                                    if ($action) {
                                        //echo "كشفت التاريخ الغياب اليومي ".$start_date." صنف ".$course_code;
                                        echo "كشف الغياب اليومي بتاريخ " . $start_date . " صف " . $courses[$cid]['name'];
                                        //echo $course_code." كشف الغياب اليومي بتاريخ ".$start_date." صف ";
                                    }
                                    ?></u>
                         </strong></h5>
                    </div>
                </div>
                <table class="table table-striped table-bordered table-hover" data-value="<?php print $print_div_counter; ?>" row="<?= $row ?>">
                    <thead>
                        <tr>
                            <tr>
                            <td align="center" style="padding:2px;margin:0px;">7 - الحصة</td>
                            <td align="center" style="padding:2px;margin:0px;">6 - الحصة</td>
                            <td align="center" style="padding:2px;margin:0px;">5 - الحصة</td>
                            <td align="center" style="padding:2px;margin:0px;">4 - الحصة</td>
                            <td align="center" style="padding:2px;margin:0px;">3 - الحصة</td>
                            <td align="center" style="padding:2px;margin:0px;">2 - الحصة</td>
                            <td align="center" style="padding:2px;margin:0px;">1 - الحصة</td>
                            <td align="center" style="width:250px !important;padding:2px;margin:0px;">أسم الطالب</td>
                            <!--<td align="center">هوية الطالب</td>-->
                            <td align="center" style="padding:2px;margin:0px;">م</td>	
                        </tr>
                    </thead>

                    <tbody>

                        <?php
                        if (isset($class_students) && !empty($class_students)):

                            $row_count = 0;
                            $total_student = count($class_students);
                            $sem_1_present = 0;
                            $sem_2_present = 0;
                            $sem_3_present = 0;
                            $sem_4_present = 0;
                            $sem_5_present = 0;
                            $sem_6_present = 0;
                            $sem_7_present = 0;
                            foreach ($class_students as $student):
                                $sem_reports = $student['sem_report'];
                                //print_r($sem_reports);
                                ?>
                                <tr class="odd gradeX">

                                    <?php
                                    if (is_array($sem_reports)) {
                                        for ($i=6; $i >= 0; $i--) { 
                                            $sem_report = $sem_reports[$i];
                                            $sng = "<i class='fa fa-minus'></i>";
                                            $is_increase = false;
                                            if (is_array($sem_report)) {
                                                switch ($sem_report['attendance']) {
                                                    case 0:
                                                        $sng = "<i class='fa fa-times'></i>";
                                                        break;
                                                    case 1:
                                                        $is_increase = true;
                                                        $sng = "<i class='fa fa-check'></i>";
                                                        break;
                                                    case 2:
                                                        $sng = "<i class='fa fa-times'></i>";
                                                        break;
                                                    case 3:
                                                        $is_increase = true;
                                                        $sng = "<i class='fa fa-circle-o'></i>";
                                                        break;
                                                    default:
                                                        $sng = "<i class='fa fa-minus'></i>";
                                                        break;
                                                }
                                            }
                                            switch ($i + 1) {
                                                case 1:
                                                    if ($is_increase) {
                                                        $sem_1_present++;
                                                    }
                                                    break;
                                                case 2:
                                                    if ($is_increase) {
                                                        $sem_2_present++;
                                                    }
                                                    break;
                                                case 3:
                                                    if ($is_increase) {
                                                        $sem_3_present++;
                                                    }
                                                    break;
                                                case 4:
                                                    if ($is_increase) {
                                                        $sem_4_present++;
                                                    }
                                                    break;
                                                case 5:
                                                    if ($is_increase) {
                                                        $sem_5_present++;
                                                    }
                                                    break;
                                                case 6:
                                                    if ($is_increase) {
                                                        $sem_6_present++;
                                                    }
                                                    break;
                                                case 7:
                                                    if ($is_increase) {
                                                        $sem_7_present++;
                                                    }
                                                    break;
                                                default:
                                                    break;
                                            }
                                            ?>
                                            <td align="center"><?php echo $sng; ?></td>	
                                        <?php }} ?>

                                    <td align="right" style="padding:3px;margin:0px;"><?php echo $student['name']; ?></td>
                                    <td align="center" style="padding:3px;margin:0px;"><?php echo str_pad(++$row_count, 2, 0, STR_PAD_LEFT); ?></td>

                                </tr>
                            <?php endforeach; ?>
                            <!-- Mrin total details section  -->
                            <?php
                            //print_r($sem_teachers);
                            //echo (($total_student-$sem_1_present)>0)?($total_student-$sem_1_present):'-';
                            for ($i = 0; $i < 4; $i++) {
                                $text = '';
                                $row_1 = '';
                                $row_2 = '';
                                $row_3 = '';
                                $row_4 = '';
                                $row_5 = '';
                                $row_6 = '';
                                $row_7 = '';
                                switch ($i) {
                                    case 0:
                                        $text = "إجمالي عدد الطلاب";
                                        $row_1 = $total_student;
                                        $row_2 = $total_student;
                                        $row_3 = $total_student;
                                        $row_4 = $total_student;
                                        $row_5 = $total_student;
                                        $row_6 = $total_student;
                                        $row_7 = $total_student;
                                        break;
                                    case 1:
                                        $text = "عدد الحضور الفعلي"; //"Actual Attendance Number";
                                        $row_1 = (!$sem_1_present) ? '-' : $sem_1_present;
                                        $row_2 = (!$sem_2_present) ? '-' : $sem_2_present;
                                        $row_3 = (!$sem_3_present) ? '-' : $sem_3_present;
                                        $row_4 = (!$sem_4_present) ? '-' : $sem_4_present;
                                        $row_5 = (!$sem_5_present) ? '-' : $sem_5_present;
                                        $row_6 = (!$sem_6_present) ? '-' : $sem_6_present;
                                        $row_7 = (!$sem_7_present) ? '-' : $sem_7_present;
                                        break;
                                    case 2:
                                        $text = "عدد الطلاب الغائبين"; //"Absent Student Number";
                                        $row_1 = (($total_student - $sem_1_present) > 0) ? ($total_student - $sem_1_present) : '-';
                                        $row_2 = (($total_student - $sem_2_present) > 0) ? ($total_student - $sem_2_present) : '-';
                                        $row_3 = (($total_student - $sem_3_present) > 0) ? ($total_student - $sem_3_present) : '-';
                                        $row_4 = (($total_student - $sem_4_present) > 0) ? ($total_student - $sem_4_present) : '-';
                                        $row_5 = (($total_student - $sem_5_present) > 0) ? ($total_student - $sem_5_present) : '-';
                                        $row_6 = (($total_student - $sem_6_present) > 0) ? ($total_student - $sem_6_present) : '-';
                                        $row_7 = (($total_student - $sem_7_present) > 0) ? ($total_student - $sem_7_present) : '-';
                                        break;
                                    case 3:
                                        $text = "إسم المعلم"; //"Teacher Name";
                                        $row_1 = '';
                                        $row_2 = '';
                                        $row_3 = '';
                                        $row_4 = '';
                                        $row_5 = '';
                                        $row_6 = '';
                                        $row_7 = '';
                                        if (count($sem_teachers) > 6) {
                                            $row_1 = $sem_teachers[0]['name'];
                                            $row_2 = $sem_teachers[1]['name'];
                                            $row_3 = $sem_teachers[2]['name'];
                                            $row_4 = $sem_teachers[3]['name'];
                                            $row_5 = $sem_teachers[4]['name'];
                                            $row_6 = $sem_teachers[5]['name'];
                                            $row_7 = $sem_teachers[6]['name'];
                                        }
                                        break;
                                    default:
                                        break;
                                }
                                ?>
                                <tr>
                                    <td align="center" style="padding:3px;margin:0px;"><?php echo $row_7; ?></td>
                                    <td align="center" style="padding:3px;margin:0px;"><?php echo $row_6; ?></td>
                                    <td align="center" style="padding:3px;margin:0px;"><?php echo $row_5; ?></td>
                                    <td align="center" style="padding:3px;margin:0px;"><?php echo $row_4; ?></td>
                                    <td align="center" style="padding:3px;margin:0px;"><?php echo $row_3; ?></td>
                                    <td align="center" style="padding:3px;margin:0px;"><?php echo $row_2; ?></td>
                                    <td align="center" style="padding:3px;margin:0px;"><?php echo $row_1; ?></td>
                                    <td colspan="2" align="center" style="padding:3px;margin:0px;"><?php echo $text; ?></td>
                                <tr>
                                    <?php
                                }
                                ?>
                                <!-- end section -->
                            <?php else: ?>

                            <tr class="odd gradeX" >

                                <td colspan="9" style="padding:3px;margin:0px;">No students found.</td>

                            </tr>
                        <?php endif; ?>
                    </tbody>

                </table>
                <div class="form-group" style="text-align: left; margin-bottom:0px!important;">
                    <h5><strong>مدير المدرسة</strong></h5>
                </div>
            </div>
        </div>
        <?php $print_div_counter++; ?>
    <?php } ?>
<?php } ?>




<!-- end : -->

<script>
    var printContents = '';
    $(document).ready(function () {
        $("#printreport").on('click', printsection);
    });

    function printsection_old() {
        $.each($('.table-for-print'), function (index, item) {
            var row = $(item).attr('row');
            if (row > 0) {
                // print function need to implementing
                var printContents = $(item).html();
                //console.log(printContents);
                var originalContents = document.body.innerHTML;
                var Cbody = "<html><head><title>Absent Report</title><link href='<?= base_url('assets/css/bootstrap.min.css') ?>' rel='stylesheet'><link href='<?= base_url('assets/css/sb-admin-2.css') ?>' rel='stylesheet'><link href='<?= base_url('assets/font-awesome-4.1.0/css/font-awesome.min.css') ?>' rel='stylesheet'><link href='<?= base_url('assets/css/style.css') ?>?v=7' rel='stylesheet'></head><body><div id='wrapper'><div id='page-wrapper'><div class='row'><div class='col-lg-12'><div class='table-responsive'><table class='table table-bordered table-hover'>" + printContents + "</table></div></div></div></div></div></body></html>";
                //console.log(Cbody);
                document.body.innerHTML = Cbody;
                window.print();
                document.body.innerHTML = originalContents;
            }
        });

    }
    
    
    function printsection() {
        var printContents = '';
        var counter = 0;
        $.each($('.table-for-print'), function (index, item) {

            var row = parseInt($(item).attr('row'));
//            alert(row);

            if (row > 0) {
//                alert($(item).attr('data-value'));

                printContents += '<div class="page-break">' + $('#reportprintdiv_' + $(item).attr('data-value')).html() + '</div>';
                counter++;
            }

        });
        
//        alert(counter);
        var mywindow = window.open('', 'Class Absent Report');

        var Cbody = "<html><head><title>Absent Report</title><link href='<?= base_url('assets/css/bootstrap.min.css') ?>' rel='stylesheet'><link href='<?= base_url('assets/css/sb-admin-2.css') ?>' rel='stylesheet'><link href='<?= base_url('assets/font-awesome-4.1.0/css/font-awesome.min.css') ?>' rel='stylesheet'><link href='<?= base_url('assets/css/style.css') ?>?v=7' rel='stylesheet'><style rel='stylesheet'> table tr td { line-height: 1 !important;} .table{margin-bottom:10px !important;} h4,h5{margin-top:0px!important; margin-bottom: 5px!important;}@media print {.page-break {page-break-after: always;}}</style></head><body><div id='wrapper'><div id='page-wrapper' style='margin:0px !important'><div class='row'><div class='col-lg-12' style='float:none'><div class='table-responsive'>";
//        mywindow.document.write(Cbody);

//        var print  = ''
        var print  = '<script>setTimeout(function () {window.print();window.document.close();window.close();}, 2000);<'+'/'+'script>'
        mywindow.document.write(Cbody + printContents + "</div></div></div></div></div>"+print+"</body></html>");
//        mywindow.document.writeln(printContents);
//        mywindow.document.write();
    }
    
    $('.datepicker').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd'
    });
</script>

