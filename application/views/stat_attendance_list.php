<div class="right_col">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Statistical Attendance</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <!-- /.row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body blocking">
                    <div class="form-group form-inline col-lg-9">
                        <form action="" method="post" class="col-lg-12">
                            <div class="form-group col-lg-4">
                                <input type="hidden" name="action" value="1">
                                <input type="text" class="form-control datepicker" name="start_date" placeholder="Choose Date" value="<?= $start_date ?>" >
                            </div>
                            <div class="col-lg-5">
                                <ul class="list-inline">
                                    <li><label style="font-weight: normal;"><input type="radio" class="choose_course" <?php if ($semno == 1) { ?>checked<?php } ?> name="semno" value="1" >Seminar-1</label></li>
                                    <li><label style="font-weight: normal;"><input type="radio" class="choose_course" <?php if ($semno == 2) { ?>checked<?php } ?> name="semno" value="2"  >Seminar-2</label></li>
                                </ul>
                            </div>
                            <div class="col-lg-2">
                                <button type="submit" class="btn btn-default">Filter</button> 
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    if (isset($action) && $action == 1) {
        ?>
    
<!--        <div class="row">
            <div class="col-lg-12">
                <div class="row">

                    <div class="col-lg-4 pull-right">
                        <h4 style="font-size:14px;text-align: center;">الإدارة العامة لمنطقة الفروانية التعليمية<br>
                            مدرسة عبداللطيف ثنيان الغانم الثانوية بنين
                        </h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h4 style="font-size:16px;text-align: center;">
                            <b><?= date("d-m-Y",strtotime($start_date)) ?>
                                إحصائية الغياب اليومي للطلاب بتاريخ</b></h4>
                    </div>
                </div>
            </div>
        </div>-->


        <div class="panel-heading">List of Class Groups</div>
        <div class="panel-body blocking">
            <div id="list-load">
                <div class="row" style="margin-bottom:10px;">
                    <div class="col-lg-8"></div>
                    <div class="col-lg-4">
                        <button type="button" class="btn btn-primary printreport">Print</button>
                        <!--<a href="<?= base_url('lists/statecsv/' . strtotime($start_date)) ?>" class="btn btn-general" style="margin-left:15px;">Excel Export</a>-->
                    </div>
                </div>
            </div>
            <div class="form-group">

                <h5 style="text-align: center;"><strong><u>
                    <?php
                    if (!empty($start_date)) {
                        echo "Statistical daily absences Date " . date("d/m/Y", strtotime($start_date));
                    }
                    ?>
                </u></strong></h5>
            </div>
            <table class="table table-striped table-bordered table-hover" data-value="<?php print $print_div_counter; ?>" row="<?= $row ?>">
                <tbody>
                    <?php
                        $absent_student_count = 0;
                        $present_student_count = 0;
                        $total_student_count = 0;
                    ?>
                    <?php foreach($class_groups as $key=>$course_values){ ?>
                        <?php
                        if ($course_values['ID']!=2){
                            $absent_seminar_student_count = 0;
                            $present_seminar_student_count = 0;
                            $total_seminar_student_count = 0;
                        }
                        ?>
                        <?php if ($course_values['ID']!=2){ ?>
                        <tr>
                            <td align="center">ملاحظات</td>
                            <td align="center">الغياب</td>
                            <td align="center">الحضور</td>
                            <td align="center">الإجمالي</td>
                            <td align="center">الصف</td>
                        </tr>
                        <?php } ?>
                        <?php foreach($course_values['classess'] as $attendance){ ?>
                            <tr class="odd gradeX">
                                <?php 
                                    $absent_student_count = $absent_student_count+$attendance['total_absent']*1;
                                    $present_student_count = $present_student_count+$attendance['total_attendance']*1;
                                    $total_student_count = $total_student_count+$attendance['total_student']*1;

                                    $absent_seminar_student_count += $attendance['total_absent']*1;
                                    $present_seminar_student_count += $attendance['total_attendance']*1;
                                    $total_seminar_student_count += $attendance['total_student']*1;

                                ?>

                                <td align="center"></td>
                                <td align="center"><?php print $attendance['total_absent']*1;?></td>
                                <td align="center"><?php print $attendance['total_attendance']*1;?></td>
                                <td align="center"><?php print $attendance['total_student']*1;?></td>
                                <td align="center"><?php print $attendance['course_name'];?></td>
                            </tr>
                        <?php } ?>
                        <tr class="odd gradeX grey_tr">
                            <td align="center"></td>
                            <td align="center"><?php print $absent_seminar_student_count*1;?></td>
                            <td align="center"><?php print $present_seminar_student_count*1;?></td>
                            <td align="center"><?php print $total_seminar_student_count*1;?></td>
                            <td align="center">المجموع</td>
                        </tr>
                    <?php } ?>
                    <tr class="odd gradeX">
                        <td align="center"></td>
                        <td align="center"><?php print $absent_student_count;?></td>
                        <td align="center"><?php print $present_student_count;?></td>
                        <td align="center"><?php print $total_student_count;?></td>
                        <td align="center">إجمالي المدرسة</td>
                    </tr>
                    <tr class="odd gradeX grey_tr">
                        <td align="center" colspan="2"></td>
                        <td align="center"></td>
                        <td align="center"><?php print number_format($present_student_count/($total_student_count)*100, 2, '.', '');?>%</td>
                        <td align="center">% نسبة الحضور</td>
                    </tr>
                </tbody>
            </table>
        </div>
<?php } ?>
</div>
<div style="display:none;">
    <div id="reportprintdiv">
        
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-lg-4 pull-left">
                            <h4 style="text-align: right;padding-top:10px;display:block;margin-left:30px;">
                                سجل متابعة الطلاب
                            </h4>
                        </div>
                        <div class="col-lg-8 pull-right" style="text-align: right;">
                            <img src="<?= base_url('uploads/report_logo/report_logo_right.png') ?>" width="50" height="60" style="page-break-inside: avoid;display:inline-block;vertical-align:top;margin-right:100px;">
                            <h4 style="display: inline-block;text-align: center;">وزارة التربية<br/><?php print $current_school['detail'];?><br><?php print $current_school['school_name'];?></h4>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-top:20px;">
                    <div class="col-lg-12 text-center">
                        <h4 style="font-size:16px;text-align: center;">
                            <b><u><?= date("d-m-Y", strtotime($start_date)) ?>
                                إحصائية الغياب اليومي للطلاب بتاريخ</u></b></h4>
                    </div>
                </div>
            </div>
        </div>
        
        <table class="table table-striped table-bordered table-hover" data-value="<?php print $print_div_counter; ?>" row="<?= $row ?>">
            <tbody>
<?php
$absent_student_count = 0;
$present_student_count = 0;
$total_student_count = 0;
?>
                <?php foreach ($class_groups as $key => $course_values) { ?>
                    <?php
                    if ($course_values['ID'] != 2) {
                        $absent_seminar_student_count = 0;
                        $present_seminar_student_count = 0;
                        $total_seminar_student_count = 0;
                    }
                    ?>
                    <?php if ($course_values['ID'] != 2) { ?>
                        <tr>
                            <td align="center">ملاحظات</td>
                            <td align="center">الغياب</td>
                            <td align="center">الحضور</td>
                            <td align="center">الإجمالي</td>
                            <td align="center">الصف</td>
                        </tr>
    <?php } ?>
    <?php foreach ($course_values['classess'] as $attendance) { ?>
                        <tr class="odd gradeX">
                        <?php
                        $absent_student_count = $absent_student_count + $attendance['total_absent'] * 1;
                        $present_student_count = $present_student_count + $attendance['total_attendance'] * 1;
                        $total_student_count = $total_student_count + $attendance['total_student'] * 1;

                        $absent_seminar_student_count += $attendance['total_absent'] * 1;
                        $present_seminar_student_count += $attendance['total_attendance'] * 1;
                        $total_seminar_student_count += $attendance['total_student'] * 1;
                        ?>

                            <td align="center"></td>
                            <td align="center"><?php print $attendance['total_absent'] * 1; ?></td>
                            <td align="center"><?php print $attendance['total_attendance'] * 1; ?></td>
                            <td align="center"><?php print $attendance['total_student'] * 1; ?></td>
                            <td align="center"><?php print $attendance['course_name']; ?></td>
                        </tr>
    <?php } ?>
                        <tr class="odd gradeX grey_tr">
                            <td align="center"></td>
                            <td align="center"><?php print $absent_seminar_student_count * 1; ?></td>
                            <td align="center"><?php print $present_seminar_student_count * 1; ?></td>
                            <td align="center"><?php print $total_seminar_student_count * 1; ?></td>
                            <td align="center">المجموع</td>
                        </tr>
<?php } ?>
                <tr class="odd gradeX">
                    <td align="center"></td>
                    <td align="center"><?php print $absent_student_count; ?></td>
                    <td align="center"><?php print $present_student_count; ?></td>
                    <td align="center"><?php print $total_student_count; ?></td>
                    <td align="center">إجمالي المدرسة</td>
                </tr>
                <tr class="odd gradeX grey_tr">
                    <td align="center" colspan="2"></td>
                    <td align="center"></td>
                    <td align="center"><?php print number_format($present_student_count / ($total_student_count) * 100, 2, '.', ''); ?>%</td>
                    <td align="center">% نسبة الحضور</td>
                </tr>
            </tbody>
        </table>
        <div class="form-group" style="text-align: left; margin-bottom:0px!important;">
            <h5><strong>مدير المدرسة</strong></h5>
        </div>

    </div>
</div>
<style>
    .grey_tr{background-color:#ccc !important;-webkit-print-color-adjust: exact;}
    .grey_tr td{background-color:#ccc !important;}
</style>
<script>
    $(document).ready(function () {
        $(".printreport").on('click', printsection);
    });

    $('.datepicker').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd'
    });
    function printsection_old() {
        var row = $("#absent_table").attr('row');
        if (row > 0) {
            // print function need to implementing
            var printContents = document.getElementById('absent_table').innerHTML;
            //console.log(printContents);
            var originalContents = document.body.innerHTML;
            var Cbody = "<html><head><title>Absent Report</title><link href='<?= base_url('assets/css/bootstrap.min.css') ?>' rel='stylesheet'><link href='<?= base_url('assets/css/style.css') ?>?v=7' rel='stylesheet'><link href='<?= base_url('assets/css/sb-admin-2.css') ?>' rel='stylesheet'></head><body><div id='wrapper'><div id='page-wrapper'><div class='row'><div class='col-lg-12'><div class='table-responsive'><table class='table table-bordered table-hover'>" + printContents + "</table></div></div></div></div></div></body></html>";
            //console.log(Cbody);
            document.body.innerHTML = Cbody;
            window.print();
            document.body.innerHTML = originalContents;
        }
    }
    function printsection() {
        var row = 1;
        if (row > 0) {
            var mywindow = window.open('', 'Statistical Report');
            //var printContents = document.getElementById('absent_table').innerHTML;
            var printContents = $('#reportprintdiv').html();

            var Cbody = "<html><head><title>Statistical Report</title><link href='<?= base_url('assets/css/bootstrap.min.css') ?>' rel='stylesheet'><link href='<?= base_url('assets/css/style.css') ?>?v=7' rel='stylesheet'><link href='<?= base_url('assets/css/sb-admin-2.css') ?>' rel='stylesheet'><link href='<?= base_url('assets/font-awesome-4.1.0/css/font-awesome.min.css') ?>' rel='stylesheet'><style rel='stylesheet'> table tr td { padding:0px!important; line-height: 1.1!important;}.grey_tr td{background-color:#ccc !important;}.grey_tr{background-color:#ccc !important;-webkit-print-color-adjust: exact;} h4,h5{margin-top:0px!important; margin-bottom: 5px!important;}@media print {.page-break {page-break-after: always;}}</style></head><body><div id='wrapper'><div id='page-wrapper' style='margin:0px !important'><div class='row'><div class='col-lg-12' style='float:none'><div class='table-responsive'>";

            mywindow.document.write(Cbody);
            mywindow.document.writeln(printContents);
            mywindow.document.write("</div></div></div></div></div></body></html>");

            setTimeout(function () {
                mywindow.print();
                mywindow.document.close();
                mywindow.close();
            }, 1000);
        }
    }
</script>


