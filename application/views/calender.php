<!-- Page Content -->
<?php // print_r($course); ?>
<div class="right_col">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-6">
                    <h1 class="page-header"><?php echo $heading; ?></h1>
                </div>
<!--                <div class="col-lg-6">
                    <h5><a class="btn btn-primary pull-right">Edit</a></h5>
                </div>-->
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
     <form role="form" action="" method="post" enctype="multipart/form-data">
    <?php $response = $this->session->flashdata('response'); 
    if(!empty($response) || isset($code)):
        $class = (!empty($response)) ? $response['class'] :  $code['class'];
        $msg   = (!empty($response)) ? $response['msg']  : $code['msg'];
    ?>
    <div class="alert alert-<?php echo $class ?> alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php echo $msg; ?>
    </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Calender
                    <p class="label label-primary pull-right">**Click on a date to lock and click again to unlock it.</p>
                </div>
                <div class="panel-body">
                    <div id='fullcalendar'></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-green">
                <div class="panel-heading">
                    Actions
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <input type="hidden" id="dates-values" name="dates" value="<?php echo $holiday_string; ?>" >
                                <input type="hidden" id="remove_date" name="remove_date" value="" >
                                <input type="hidden" name="action" value="<?php echo $action; ?>" >
                                <input  type="submit" class="btn btn-primary pull-right" value="Save">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</div>
<!-- /#page-wrapper -->