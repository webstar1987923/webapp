<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
       
        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" id="submenu" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            
            <li class="sidebar-search-wrapper">
                <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
                <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
                
                <!-- END RESPONSIVE QUICK SEARCH FORM -->
            </li>
           <?php $menu = $this->uri->segment(2); 
           	if($menu == 'Dashboard' || $menu == ''){ ?> <li class='nav-item start active open'> <?php } else{ ?> <li class='nav-item'><?php }  ?>
            
                <a href="<?php echo base_url(); ?>admin" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    <span class="selected"></span>                               
                </a>                                
            </li>
           
            <li class="heading">
                <h3 class="uppercase">News</h3>
            </li>
            <?php if($menu == 'News'){ ?> <li class='nav-item start active open'> <?php } else{ ?> <li class='nav-item'><?php }  ?>
            
                <a href="<?php echo base_url(); ?>admin/News" class="nav-link nav-toggle">
                    <i class="icon-user"></i>
                    <span class="title">News List</span>
                </a>               
            </li>    
                                                        
        </ul>
        <!-- END SIDEBAR MENU -->
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>

<script type="text/javascript">
   
    $(window).load(function() {
    $('#submenu li').click(function() {
    $(this).siblings('li').removeClass('active');
    $(this).addClass('active');
    });
});

</script>