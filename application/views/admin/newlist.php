   <!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <h1 class="page-title"> News </h1>
        <div class="clearfix"></div>
            <!-- END DASHBOARD STATS 1-->                        
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase">List</span>
                        </div>
                        <div class="tools"> </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>Title</th>                   
                                    <th>Content</th>
                                    <th>Date</th>
                                    <th>Action</th>                    
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                           
                             if($news){
                             $i = 1;
                             foreach($news as $value){ 
                             $content = substr($value->content, 0, 20);
                             ?>
                             
                        	<tr>
                                    <td><?php echo $i++ ; ?></td>
                                    <td><?php echo $value->title; ?></td>
                                    <td><?php echo $content; ?>...</td>
                                    <td><?php echo $value->date; ?></td>
                                    <td><a href="" class="btn dark btn-sm btn-outline sbold uppercase">
                                    <i class="fa fa-share"></i> Edit </a> <button  data-val="<?php echo $value->id; ?>" class="btn dark btn-sm btn-outline sbold uppercase" id="delete"> <i class="fa fa-share"></i> Delete </button></td>                                                                                            
                                </tr>
                            <?php } } ?>    
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>                            
        </div>     
    </div>
        <!-- END CONTENT BODY -->
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#delete').click(function(){
		alert('tata');
		var id = $(this).attr('data-val');
			var baseurl = "<?php echo base_url(); ?>";
			$.ajax({
				url: baseurl+'admin/News/delete',
				type: 'POST',
				data : 'id='+id,
				success : function(data){
				$.notify("Deleted Successfully", {
				  animate: {
				    enter: 'animated rollIn',
				    exit: 'animated rollOut'
				  }
				});
				}
			});
		});
	});
</script>

    