<!-- Page Content -->
<?php // print_r($course); ?>
<div class="right_col">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-6">
                    <h1 class="page-header"><?php echo $heading; ?></h1>
                </div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <?php
    $response = $this->session->flashdata('response');
    if (!empty($response) || isset($code)):
        $class = (!empty($response)) ? $response['class'] : $code['class'];
        $msg = (!empty($response)) ? $response['msg'] : $code['msg'];
        ?>
        <div class="alert alert-<?php echo $class ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
    <?php endif; ?>
    <?php if (isset($attendace_controller) && $attendace_controller && isset($step) && $step == 2) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Give attendance
                    </div>
                    <div class="panel-body">
                        <form role="form" action="" method="post" class="form-inline" enctype="multipart/form-data">
                            <div class="form-group">
                                <label>IMPORT ATTENDANCE: </label>
                                <input type="hidden" name="cid" value="<?php echo $cid; ?>" />
                                <input type="hidden" value="2" name="action" class="btn btn-primary" />
                                <input type="submit" value="Import" name="submit" class="btn btn-primary" />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.col-lg-12 -->
        </div> 
    <?php } ?>
    <?php if (!isset($step)) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Upload exel file
                    </div>
                    <div class="panel-body">
                        <form role="form" action="" method="post" class="<?php if (!isset($attendace_controller)) { ?> form-inline <?php } ?>" enctype="multipart/form-data">
                            <?php if (isset($attendace_controller) && $attendace_controller) { ?>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Class : </label> 
                                            <select name="course_id" class="form-control" required="">
                                                <option value="">Select</option>
                                                <?php
                                                if ($courses) {
                                                    foreach ($courses as $course) {
                                                        ?>
                                                        <option value="<?php echo $course['cid']; ?>" ><?php echo $course['name']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        
<!--                                        <div class="form-group">
                                            <label>Date of attendance: </label> 
                                            <div class="input-group date">
                                                <input type="text" name="date" class="form-control">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                                            </div>
                                            <p class="help-block" style="font-weight: bold;">Choose date if necessary [all attendance will take with this date.]</p>
                                        </div>-->

                                    </div>
                                </div>
                            <?php } ?>
                            <div class="form-group form-inline">
                                <label>File: </label>
                                <input type="file" name="file" required="" class="filestyle col-lg-8">
                                <input type="hidden" value="1" name="action" >
                                <input type="submit" value="Import" class="btn btn-primary" >
                                <p class="help-block">Allowed Type : exel Allowed Size : 5MB</p>
                            </div>														<!-- if courses presents then show checked boxed -->							<?php 								if(isset($courses) && !empty($courses)){									?>									<br/>									<div class="form-group form-inline">										<label>Courses: </label>										<ul class="list-inline">									<?php									foreach($courses as $course){										?>										<li><input type="radio" class="choose_course" name="course_id" value="<?php echo $course['cid']; ?>" <?php if(in_array($course['cid'],$course_id)){ echo "checked";}else{ echo "";}?> ><?php  echo $course['name']; ?></li>											<?php									}									?>										</ul>									</div>									<?php								}							?>							<!-- end : courses -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
<!-- /#page-wrapper -->