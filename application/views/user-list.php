<div class="right_col">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo $page_title; ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <?php $no_row = 6; ?>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo ucfirst($page_title); ?>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body blocking">
                    <?php $this->load->view('search-bar',array('plchldr' => "Search by name or email or user ID")); ?>
                    <form action="<?php echo base_url('ajax/delete/'.$utype); ?>" class="ajax-form" method="POST" >
                        <div class="courses-choose" style="display:none;">
                            <?php if ($courses): ?>
                            <h4>Courses</h4>
                            <ul class="list-inline">
                                <li class="checkbox"><input type="checkbox" autocomplete="off" class="choose_course flat" name="all" value="1"  >Select All</li>
                                <?php foreach ($courses as $course): ?>
                                    <li class="checkbox"><input type="checkbox" class="flat" autocomplete="off" name="course_id[]" value="<?php echo $course['cid']; ?>"  ><?php  echo $course['name']; ?></li>
                                <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                        </div>
                        <div id="list-load">
                            <div class="table-responsive">
                                <table class="table table-striped jambo_table bulk_action" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th class="column-title">User ID</th>
                                            <th class="column-title">Name</th>
                                            <th class="column-title">Email</th>
                                            <?php if(isset($teacher) && $teacher): $no_row++; ?>
                                            <th class="column-title">Classes</th>
                                            <?php endif; ?>
                                            <?php if(isset($parents) && $parents): $no_row++; ?>
                                            <th class="column-title">Student Name</th>
                                            <?php endif; ?>
                                            <th class="column-title">Type</th>
                                            <th class="column-title">Action</th>
                                            <th class="column-title">
                                                <input type="checkbox" name="all" id="all" class="flat" autocomplete="off" >
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if(isset($users) && !empty($users)): ?>
                                            <?php 
        //                                    $users = array_reverse($users);
                                            foreach( $users as $user): ?>
                                            <tr class="odd gradeX">
                                                <td><?php echo $user['username'] ?></td>
                                                <td><?php echo $user['first_name'].' '.$user['last_name'] ?></td>
                                                <td><?php echo $user['email_id'] ?></td>
                                                <?php if(isset($teacher) && $teacher): ?>
                                                <td><?php echo $user['classes']; ?></td>
                                                <?php endif; ?>
                                                <?php if(isset($parents) && $parents): ?>
                                                <td><?php echo $user['childrens']; ?></td>
                                                <?php endif; ?>
                                                <td><?php  if($user['user_type'] == 1) {echo 'Admin';}elseif($user['user_type'] == 2){ echo 'Teacher';}elseif($user['user_type'] == 3){echo 'Moderator';}elseif($user['user_type'] == 4){echo 'Parent';}elseif($user['user_type'] == 5){echo 'Register';}elseif($user['user_type'] == 6){echo 'Medical';}elseif($user['user_type'] == 7){echo 'Viewer';} ?></td>
                                                <td>
                                                    <a href="<?php echo base_url('manage/user/'.$user['user_no']); ?>" title="Edit" class="edit" >
                                                        <i class="fa fa-pencil-square-o fa-fw"></i></a>
                                                    <?php if(isset($teacher) && $teacher): ?>
                                                    <a href="<?php echo base_url('assign/users/'.$user['user_no']); ?>" title="Assign Course" class="assign" >
                                                        <i class="fa  fa-plus-square-o fa-fw"></i></a>
                                                    <?php endif; ?>
                                                    <a href="<?php echo base_url('delete/user/'.$user['user_no']); ?>" title="Delete" class="delete"><i class="fa fa fa-times fa-fw fa-fw"></i></a>
                                                </td>
                                                <td><input type="checkbox" autocomplete="off" name="delete_ids[]" value="<?php echo $user['user_no']; ?>" class="flat select_me" ></td>
                                            </tr>
                                            <?php endforeach; ?>
                                        <?php else: ?>
                                             <tr class="odd gradeX" >
                                                 <td colspan="<?php echo $no_row; ?>">No users found.</td>
                                            </tr>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                            <?php $this->load->view('paginations'); ?>
                        </div>
                    </form>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>

<script>

$( document ).ready(function() {
    $('.choose_course').on('ifChecked', function (event){
        $(this).closest("input").attr('checked', true);        
        checkAllCourses(this);
    });
    $('.choose_course').on('ifUnchecked', function (event) {
        $(this).closest("input").attr('checked', false);
        checkAllCourses(this);
    });
});


function checkAllCourses(checkbox){
    if ($(checkbox).prop('checked')){
        $('input[name="course_id[]"]').prop('checked', true);
    }else{
        $('input[name="course_id[]"]').prop('checked', false);
    }
    $('input[name="course_id[]"]').iCheck('update');
}
function checkAllUsers(checkbox){
    if ($(checkbox).prop('checked')){
        $('input[name="delete_ids[]"]').prop('checked', true);
        $('#dataTables-example').find('tr').addClass('selected');
    }else{
        $('input[name="delete_ids[]"]').prop('checked', false);
        $('#dataTables-example').find('tr').removeClass('selected');
    }
    $('input[name="delete_ids[]"]').iCheck('update');
}
</script>