<!-- Page Content -->
<div class="right_col">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-6">
                    <h1 class="page-header"> Ad Management</h1>
                </div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <?php
    $response = $this->session->flashdata('response');
    if (!empty($response) || isset($code)):
        $class = (!empty($response)) ? $response['class'] : $code['class'];
        $msg = (!empty($response)) ? $response['msg'] : $code['msg'];
        ?>
        <div class="alert alert-<?php echo $class ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-lg-12">
            <form action="" method="post">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        Create Add
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Ad name : </label>
                                    <input class="form-control" name="ads[name]" required="" placeholder="Ads name" value="<?php if (isset($ads['name'])) echo $ads['name']; ?>" >
                                </div>
                                <div class="form-group">
                                    <div class="form-inline">
                                        <label>Ad type : </label>
                                        <?php $type =  ($action == 'save') ? 'checked="checked"' : '';?>
                                        <label class="radio-inline">
                                            <input type="radio" name="ads[type]" id="static" <?php echo $type; ?> <?php if (isset($ads['type']) && $ads['type'] == 'static') echo "checked"; ?> value="static">Static
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="ads[type]" id="slider" <?php if (isset($ads['type']) && $ads['type'] == 'slider') echo "checked"; ?> value="slider">Slider
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>The ads selected media : </label>
                                    <!--<button id="media" class="btn btn-primary">Select media</button>-->
                                </div>
                                <div class="form-group">
                                    <div class="container-fluid">
                                        <div class="add-media media-select-list" id="selected-media">
                                            <?php if ($action == 'update'): ?>
                                                <?php if ($ad_medias): ?>
                                                    <?php foreach ($ad_medias as $key => $file) { ?>
                                                        <div class="col-lg-2 media-item selected" data-id="<?php echo $file['ID']; ?>">
                                                            <a class="delete-atached-media" data-id="<?php echo $file['ID']; ?>"  data-attached="<?php echo $ads['ID'] ?>">
                                                                <i class="fa fa-times-circle-o fa-2x"></i>
                                                            </a>
                    <!--                                                                        <a class="select-media" data-id="<?php echo $file['ID']; ?>">
                                                                                    <i class="fa  fa-check-circle-o fa-2x"></i>
                                                                                </a>
                                                                                <a class="delete-selected-media" data-id="<?php echo $file['ID']; ?>">
                                                                                    <i class="fa fa-times-circle-o fa-2x"></i>
                                                                                </a>-->
                                                            <div class="thumbnail">
                                                                <img src="<?php echo $file['url']; ?>" />
                                                            </div>
                                                            <input type="checkbox" value="<?php echo $file['ID']; ?>" checked="checked" name="ads[media][]" />
                                                        </div>
                                                    <?php } ?>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Ad media : </label>
                                    <!-- Large modal -->
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">Select Media</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-green">
                    <div class="panel-heading">
                        Actions
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <input type="hidden" name="action" value="<?php echo $action; ?>" >
                                    <input  type="submit" class="btn btn-primary pull-right" value="Save">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>  
        </div>
    </div>
</div>
<?php $this->load->view('media-selector') ?>
<!-- /#page-wrapper -->
<script type="text/javascript">
    
    jQuery(function ($) {
        $('body').on('image.selected', function (e, data) {
            console.log('event', data);
            var html = '';
            if (data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    html += '<div class="col-lg-2 media-item">' +
                            '<a class="delete-choosed-media" data-id="'+data[i]._id+'">'+
                                '<i class="fa fa-times-circle-o fa-2x"></i></a>'+
                            '<div class="thumbnail">' +
                            '<img src="' + data[i].href + '" />' +
                            '</div>' +
                            '<input type="hidden" value="' + data[i]._id + '" name="ads[media][]" />' +
                            '</div>';
                }
            }
            $('#selected-media').append(html);


//            var html = '<div class="col-lg-2 media-item">'+
//                    '<div class="thumbnail">'+
//                                        '<img src="" />'+
//                                    '</div>'+
//                                    '<input type="checkbox" value="" name="ads[media][]" />'+
//                                '</div>';
        });
    });
</script>