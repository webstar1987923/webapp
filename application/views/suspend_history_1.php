<?php if (!empty($medical_exists_dates)){?>
    <div class="alert alert-danger">
        لا يمكن القيام بذلك لأنه يوجد مراجعة طبيه مسجله بنفس اليوم المحدد
        <?php // print implode(', ', $suspend_exists_dates);?>
    </div>
<?php } ?>
<h4 class="media-heading">Suspend Dates</h4>
<table class="table table-striped table-bordered table-hover">
    <tr class="gradeX odd" >
        <th align="center" >
            #
        </th>
        <th align="center" >
            Start Date
        </th>
        <th align="center" >
            End Date
        </th>
        <th align="center" >
            Days
        </th>
        <?php if (empty($hide_remove)){?>
            <th align="center" >
                Action
            </th>
        <?php } ?>
    </tr>
    <?php $counter = 1;?>
    <?php foreach($suspend as $item){?>
        <?php $dates = unserialize($item['list_days']);?>
        <tr class="gradeX odd" >
            <td align="center" >
                <?php print $counter;?>
            </td>
            <td align="center" >
                <?php print $item['start_date'];?>
            </td>
            <td align="center" >
                <?php print $item['end_date'];?>
            </td>
            <td align="center" >
                <?php print $item['amount_days'];?>
            </td>
            <?php if (empty($hide_remove)){?>
                <td align="center" >
                    <a href="javascript:void(0)" onclick="removeStudentSuspend(this, <?php print $item['id'];?>)">Remove</a>
                </td>
            <?php } ?>
        </tr>
        <?php $counter++;?>
    <?php } ?>
</table>