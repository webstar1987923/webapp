<div class="right_col">

    <div class="row">

        <div class="col-lg-12">

            <h1 class="page-header">Absent Report</h1>

        </div>

        <!-- /.col-lg-12 -->

    </div>

    <!-- /.row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <form action="" method="post">
                    <div class="row">

                        <div class="form-group col-lg-3">
                            <label>Number of Absent : </label>
                            <input type="text" required class="form-control frmfld" name="absent_number" placeholder="Number of Absent" value="<?= (isset($filtercond['absent_number'])) ? $filtercond['absent_number'] : '' ?>" >
                        </div>
                        <div class="form-group col-lg-3">
                            <label>Choose Class : </label>
                            <select name="class_id[]" class="form-control frmfld" multiple size="3" autocomplete="off" onchange="getCourseStudents(this)">
                                <option value="" <?php if (empty($class_id)){?>selected<?php } ?>>Choose Class</option>
                                <?php
                                if (!empty($courses)) {
                                    //print_r($courses);
                                    foreach ($courses as $course) {
                                        echo "<option value='" . $course['cid'] . "' " . (in_array($course['cid'], $class_id) ? 'selected' : '') . ">" . $course['name'] . "</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-lg-3">
                                <label>Absence : </label>
                            <select name="excused" class="form-control frmfld" autocomplete="off">
                                <option value="">All</option>
                                <option value="0" <?php if ($filtercond['excused']==="0"){?>selected<?php } ?>>Unacceptable</option>
                                <option value="1" <?php if ($filtercond['excused']==="1"){?>selected<?php } ?>>Acceptable</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-3" style="padding-top: 24px;">
                            <button type="submit" class="btn btn-primary">Filter</button> 
                            <?php if ($absents): ?>
                                <button type="button" class="btn btn-primary printreport">Print</button>
                                <!--<button type="submit" name="excel_export" value='1' class="btn btn-primary">Excel Export</button> -->
                            <?php endif; ?>
                        </div>
                    </div>
                </form>
                
                <div class="form-group">

                    <h5 style="text-align: center;"><strong><?php
                            if ($filtercond['excused']==="0"){
                                echo "كشف الغياب بعذر غير مقبول";
                            }elseif ($filtercond['excused']==="1"){
                                echo "كشف الغياب بعذر مقبول";
                            }else{
                                echo "كشف الغياب بعذر مقبول وعذر غير مقبول";
                            }
                            
                            ?></strong></h5>

                </div>
                
                <table class="table table-bordered table-hover" id="absent_table_ds" row="<?= ($absents) ? 1 : 0 ?>">
                    <thead>
                        <tr>
                            <th>Student ID</th>
                            <th>Name</th>
                            <th>Class</th>
                            <th>Total absents</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php if ($absents): ?>

                            <?php foreach ($absents as $key => $value) { ?>
                                <tr>
                                    <td align="center"><?php echo $value['student_id']; ?></td>
                                    <td align="center"><?php echo $value['student_name']; ?></td>
                                    <td align="center"><?php echo $value['class_name']; ?></td>
                                    <td align="center"><?php echo $value['abscount']; ?></td>
                                </tr>

                            <?php } ?>

                        <?php else: ?>

                            <tr><td colspan="5">No absent list found.</td></tr>

                        <?php endif; ?>

                    </tbody>

                </table>

            </div>

        </div>

    </div>

</div>

<!-- print section table view -->
<style rel='stylesheet'>
    table tr th {
        text-align :center!important;
    }
</style>
<div style="display:none;">

    <div id="reportprintdiv">
        <div class="row">
            <div class="col-lg-12">
                <div class="col-lg-4 pull-left">
                    <h4 style="text-align: right;padding-top:10px;display:block;margin-left:30px;">
                        سجل متابعة الطلاب
                    </h4>
                </div>
                <div class="col-lg-8 pull-right" style="text-align: right;">
                    <img src="<?= base_url('uploads/report_logo/report_logo_right.png') ?>" width="50" height="60" style="page-break-inside: avoid;display:inline-block;vertical-align:top;margin-right:100px;">
                    <h4 style="display: inline-block;text-align: center;">وزارة التربية<br/><?php print $current_school['detail'];?><br><?php print $current_school['school_name'];?></h4>
                </div>
            </div>
        </div>
        <div class="row" style="margin-top:20px;">
            <div class="form-group" style="text-align: center; margin-bottom:0px !important;">
                <h5><strong style="font-size: 16px;"><u><?php
                        if ($filtercond['excused']==="0"){
                            echo "كشف الغياب بعذر غير مقبول";
                        }elseif ($filtercond['excused']==="1"){
                            echo "كشف الغياب بعذر مقبول";
                        }else{
                            echo "كشف الغياب بعذر مقبول وعذر غير مقبول";
                        }

                        ?></u></strong></h5>

            </div>
            <table class="table table-bordered table-hover" id="absent_table" row="<?= ($absents) ? 1 : 0 ?>">
                <thead>
                    <tr>
                        <th style="text-align:center;padding:4px;margin:0px;">الإجمالي</th>
                        <th style="text-align:center;padding:4px;margin:0px;">الفصل</th>
                        <th style="text-align:center;padding:4px;margin:0px;">اسم الطالب</th>
                        <th style="text-align:center;padding:4px;margin:0px;">م</th>
                        <!--<td align="center">الرقم المدني</td>-->
                    </tr>
                </thead>
                <tbody>

    <?php
    if ($absents):
        $i = 1;
        ?>

                        <?php foreach ($absents as $key => $value) { ?>

                            <tr>
                                <td align="center" style="padding:4px;margin:0px;"><?php echo $value['abscount']; ?></td>
                                <td align="center" style="padding:4px;margin:0px;"><?php echo $value['class_name']; ?></td>
                                <td align="right" style="padding:4px;margin:0px;"><?php echo $value['student_name']; ?></td>
                                <!--<td align="right"><?php echo $value['student_id']; ?></td>-->
                                <td align="center" style="padding:4px;margin:0px;"><?php echo str_pad($i++, 2, '0', STR_PAD_LEFT) ?></td>
                            </tr>

        <?php } ?>

    <?php else: ?>

                        <tr><td style="padding:4px;margin:0px;" colspan="6">No absent list found.</td></tr>

                    <?php endif; ?>

                </tbody>
            </table>
            <div class="form-group" style="text-align: left; margin-bottom:0px!important;">
                <h5><strong>مدير المدرسة</strong></h5>
            </div>
        </div>
    </div>
</div>
<!-- end -->

<!-- /#page-wrapper -->
<script>
    $(document).ready(function () {
        $(".printreport").on('click', printsection);
    });

    $('.datepicker').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd'
    });
    $(".resetbutton").click(function () {
        $.each($(".frmfld"), function (i, item) {
            $(item).val('');
        });
    });
    var baseurl = "<?php echo base_url(); ?>";
    function getCourseStudents(element){
        $.each($('select[name="studentid"] option'), function(index, item){
            if (parseInt($(item).val())>0){
                $(item).remove();
            }
        });
        if ($(element).val()){
            $('select[name="studentid"]').parent().removeClass('hide');
            $.ajax({
                    url: baseurl+'ajax/getCourseStudents/'+$(element).val(),
                    type: 'POST',
                    success : function(response){
                        $.each(response, function(index, item){
                            $('select[name="studentid"]').append($("<option></option>")
                                           .attr("value",item.sid)
                                           .text(item.name)); 
                        });
                    }
            });
        }else{
            $('select[name="studentid"]').parent().addClass('hide');
        }
    }
    /*
     $(".printreport").on('click',function(){
     printsection();
     });
     */
    function printsection_old() {
        var row = $("#absent_table").attr('row');
        if (row > 0) {
            // print function need to implementing
            var printContents = document.getElementById('absent_table').innerHTML;
            //console.log(printContents);
            var originalContents = document.body.innerHTML;
            var Cbody = "<html lang='ar'><head><title>Absent Report</title><link href='<?= base_url('assets/css/bootstrap.min.css') ?>' rel='stylesheet'><link href='<?= base_url('assets/css/style.css') ?>?v=7' rel='stylesheet'><link href='<?= base_url('assets/css/sb-admin-2.css') ?>' rel='stylesheet'></head><body><div id='wrapper'><div id='page-wrapper'><div class='row'><div class='col-lg-12'><div class='table-responsive'><table class='table table-bordered table-hover'>" + printContents + "</table></div></div></div></div></div></body></html>";
            //console.log(Cbody);
            document.body.innerHTML = Cbody;
            window.print();
            document.body.innerHTML = originalContents;
        }
    }
    function printsection() {
        var row = $("#absent_table").attr('row');
        if (row > 0) {
            var mywindow = window.open('', 'Absent List');
            var printContents = $('#reportprintdiv').html();
            //console.log(printContents);
            var Cbody = "<html lang='ar'><head><title>Absent Report</title><link href='<?= base_url('assets/css/bootstrap.min.css') ?>' rel='stylesheet'><link href='<?= base_url('assets/css/style.css') ?>?v=7' rel='stylesheet'><link href='<?= base_url('assets/css/sb-admin-2.css') ?>' rel='stylesheet'><link href='<?= base_url('assets/font-awesome-4.1.0/css/font-awesome.min.css') ?>' rel='stylesheet'><style rel='stylesheet'> table tr td {line-height: 1.1!important;} h4,h5{margin-top:0px!important; margin-bottom: 5px!important;}@media print {.page-break {page-break-after: always;}}</style></head><body><div id='wrapper'><div id='page-wrapper' style='margin:0px !important'><div class='row'><div class='col-lg-12' style='float:none'><div class='table-responsive'>";

            mywindow.document.write(Cbody);
            mywindow.document.write(printContents);
            mywindow.document.write("</div></div></div></div></div></body></html>");
            setTimeout(function () {
                mywindow.print();
                mywindow.close();
            }, 1000);
        }
    }
    /*
     $(".printreport").click(function(){
     printsection();
     });*/

</script>
