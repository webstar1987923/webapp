<div class="right_col">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Ads List</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->	 <div class="row">		<div class="col-lg-12">			<div class="panel panel-default">				<div class="panel-heading">Add/Edit Ads Image</div>				<div class="panel-body blocking">					<div class="form-group form-inline col-lg-12">						<form action="<?=base_url('admanage/ads')?>" method="post" class="col-lg-12" enctype="multipart/form-data">							<input type="hidden" name="action" value="1" >							<input type="hidden" name="ads_image_id" value="<?=$ads_image_id?>" >							<div class="col-lg-2">								<?php if(!empty($old_file_name)){									?>									<img src="<?=base_url('uploads/slideshow_ads/'.$old_file_name)?>" width="60" height="60">									<?php								}								?>															</div>							<div class="form-group col-lg-4">								<label>Ads Image</label>								<input type="file" class="form-control" name="image">							</div>														<div class="col-lg-1">								<button type="submit" class="btn btn-default">Upload</button> 							</div>						</form>					</div>					<div style="color:red;"><?=$error_mgs?></div>					<div style="color:green;"><?=$success_mgs?></div>				</div>							</div>		</div>	 </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    List of Ads
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body blocking">
                        <div id="list-load">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="adstable" row="<?=$row?>">
                                    <thead>
                                        <tr>
                                            <th>No.</th>                                            <th>Ads ID</th>
                                            <th>Image</th>											<th>Status</th>											<th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (isset($ads_images) && !empty($ads_images)):
                                            foreach ($ads_images as $ads_image):
                                                ?>
                                                <tr class="odd gradeX">													<td><?php echo ++$row_count;?></td>
                                                    <td><?php echo $ads_image['ID'] ?></td>
                                                    <td align="center"><img src="<?=base_url('uploads/slideshow_ads/'.$ads_image['image'])?>" width="60"height="60"></td>													<td><?php 														if($ads_image['status']){															echo "Active";														}														else{															echo "In Active";														}													?></td>													<td>														<a href="<?php echo base_url('admanage/ads/'.$ads_image['ID']); ?>" title="Edit" class="edit" ><i class="fa fa-pencil-square-o fa-fw"></i></a>														<a href="<?php echo base_url('delete/ads/'.$ads_image['ID']); ?>" title="Delete" class="delete"><i class="fa fa fa-times fa-fw fa-fw"></i></a>														<?php															if($ads_image['status']){																// click to in-active																?>																<a href="<?php echo base_url('admanage/changestatus/'.$ads_image['ID']); ?>" title="Click to in-active" class="edit" ><i class="fa fa-ban"></i></a>																<?php															}															else{																// click to active 																?>																<a href="<?php echo base_url('admanage/changestatus/'.$ads_image['ID'].'/1'); ?>" title="Click to active" class="edit" ><i class="fa fa-check-square-o"></i></a>																<?php															}														?>													</td>
                                                </tr>
											<?php endforeach; ?>
										<?php else: ?>
                                            <tr class="odd gradeX" >
                                                <td colspan="5">No Ads found.</td>
                                            </tr>
										<?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
<?php //$this->load->view('paginations'); ?>
                        </div>
                </div>
                <!-- /.panel-body -->
            </div>			<!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<script>var printContents='';$(document).ready(function(){	$(".printreport").on('click',printsection);});function printsection_old(){	var row = $("#dataTables-example").attr('row');	if(row>0){		// print function need to implementing		var printContents = document.getElementById('dataTables-example').innerHTML;		//console.log(printContents);		var originalContents = document.body.innerHTML;		var Cbody = "<html><head><title>Absent Report</title><link href='<?=base_url('assets/css/bootstrap.min.css')?>' rel='stylesheet'><link href='<?=base_url('assets/css/style.css')?>' rel='stylesheet'><link href='<?=base_url('assets/css/sb-admin-2.css')?>' rel='stylesheet'></head><body><div id='wrapper'><div id='page-wrapper'><div class='row'><div class='col-lg-12'><div class='table-responsive'><table class='table table-bordered table-hover'>"+printContents+"</table></div></div></div></div></div></body></html>";		//console.log(Cbody);		document.body.innerHTML = Cbody;		window.print();		document.body.innerHTML = originalContents;	}}function printsection(){	var row = $("#dataTables-example").attr('row');	if(row>0){		var mywindow = window.open('','Absent List');		//var printContents = document.getElementById('dataTables-example').innerHTML;		var printContents = $('#dataTables-example').html();		console.log(printContents);				var Cbody = "<html><head><title>Absent Report</title><link href='<?=base_url('assets/css/bootstrap.min.css')?>' rel='stylesheet'><link href='<?=base_url('assets/css/style.css')?>' rel='stylesheet'><link href='<?=base_url('assets/css/sb-admin-2.css')?>' rel='stylesheet'><link href='<?=base_url('assets/font-awesome-4.1.0/css/font-awesome.min.css')?>' rel='stylesheet'></head><body><div id='wrapper'><div id='page-wrapper'><div class='row'><div class='col-lg-12'><div class='table-responsive'><table class='table table-bordered table-hover'>";		//+printContents+"</table></div></div></div></div></div></body></html>";		        mywindow.document.write(Cbody);        mywindow.document.write(printContents);        mywindow.document.write("</table></div></div></div></div></div></body></html>");        mywindow.print();        mywindow.close();		mywindow='';	}}$('.datepicker').datepicker({    autoclose: true,	format:'yyyy-mm-dd'});</script>
