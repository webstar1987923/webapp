<!-- Large modal -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel">Media</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="list-group clearfix media-select-list" id="media-select-list" >
                        <?php if ($medias): ?>
                            <?php foreach ($medias as $key => $file) { ?>
                                <div class="col-lg-2 media-item" data-id="<?php echo $file['ID']; ?>"
                                     data-href="<?php echo $file['url']; ?>">
        <!--                                    <a class="select-media" data-id="<?php echo $file['ID']; ?>">
                                        <i class="fa  fa-check-circle-o fa-2x"></i>
                                    </a>
                                    <a class="delete-selected-media" data-id="<?php echo $file['ID']; ?>">
                                        <i class="fa fa-times-circle-o fa-2x"></i>
                                    </a>-->
                                    <div class="thumbnail">
                                        <img src="<?php echo $file['url']; ?>" />
                                    </div>
                                    <input type="checkbox" value="<?php echo $file['ID']; ?>" name="ads[media][]" />
                                </div>
                            <?php } ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a class="btn btn-default" href="<?php echo base_url('media'); ?>" >Upload New Medias</a>
                <button type="button" class="btn btn-primary" id="addToMedia">Attach Media</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(function ($) {
        $('body').on('click', '#addToMedia', function (e) {
            var ids = [];
            var adder = $(this).attr('data-adder');
            e.preventDefault();
            $('#media-select-list .selected').each(function (i, e) {
                var id = $(e).attr('data-id');
                var href = $(e).attr('data-href');
                ids.push({
                    _id: id,
                    href: href
                });
            });
//            e.data = ids;
            console.log('Adder on click attach', adder);
            $('button[data-dismiss="modal"]').click();
            $('#media-select-list .selected').removeClass('selected');
            $('body').trigger('image.selected', [ids, adder]);
        });
        $('.bs-example-modal-lg').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var recipient = button.attr('data-adder');
            console.log('recipient', recipient);
            var modal = $(this);
            modal.find('#addToMedia').attr('data-adder', recipient);
        });
        $('.bs-example-modal-lg').on('hide.bs.modal', function (event) {
            var modal = $(this);
            console.log('hideing');
            modal.find('#addToMedia').attr('data-adder', '');
        });

    });
</script>
