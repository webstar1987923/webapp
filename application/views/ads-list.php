<div class="right_col">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo $page_title; ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <?php
    $response = $this->session->flashdata('response');
    if (!empty($response) || isset($code)):
        $class = (!empty($response)) ? $response['class'] : $code['class'];
        $msg = (!empty($response)) ? $response['msg'] : $code['msg'];
        ?>
        <div class="alert alert-<?php echo $class ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php
                    echo ucfirst($page_title);
//                        print_r($ads);
                    ?>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body blocking">
                    <?php $this->load->view('search-bar', array('plchldr' => "Search by name")); ?>
                    <form action="<?php echo base_url('ajax/delete/' . $utype); ?>" class="ajax-form" method="POST" >
                        <div id="list-load">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Ads ID</th>
                                            <th>Name</th>
                                            <th>Images</th>
                                            <th>Action</th>
                                            <!--<th><input type="checkbox" name="all" id="all" ></th>-->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (isset($ads) && !empty($ads)): ?>
                                            <?php foreach ($ads as $user): ?>
                                                <tr class="odd gradeX">
                                                    <td><?php echo $user['ID'] ?></td>
                                                    <td><?php echo $user['name']; ?></td>
                                                    <td><?php echo $user['img_count'] ?></td>
                                                    <td>
                                                        <a href="<?php echo base_url('admanage/manage/' . $user['ID']); ?>" title="Edit" class="edit" >
                                                            <i class="fa fa-pencil-square-o fa-fw"></i>
                                                        </a>
                                                        <a href="<?php echo base_url('admanage/delete/' . $user['ID']); ?>" title="Delete" class="delete">
                                                            <i class="fa fa fa-times fa-fw fa-fw"></i>
                                                        </a>
                                                    </td>
                                                    <!--<td><input type="checkbox" name="delete_ids[]" value="<?php echo $user['user_no']; ?>" class="select_me" ></td>-->
                                                </tr>
                                            <?php endforeach; ?>
                                        <?php else: ?>
                                            <tr class="odd gradeX" >
                                                <td colspan="6">No users found.</td>
                                            </tr>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                            <?php $this->load->view('paginations'); ?>
                        </div>
                    </form>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>

