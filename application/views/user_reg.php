<!-- Page Content -->

<?php // print_r($user); ?>

<div class="right_col">

    <div class="row">

        <div class="col-lg-12">

            <div class="row">

                <div class="col-lg-6">

                    <h1 class="page-header">User registration</h1>

                </div>

<!--                <div class="col-lg-6 full-height">

                    <div class="pull-right" >

                        <?php 

                        $lid = (isset($lang_id)) ? $lang_id : 0;

                        if($langs &&  $action == 2): ?>

                            <?php foreach($langs as $lang): ?>

                                <a href="<?php echo '?lang_id='.$lang['ID']; ?>" class="btn btn-circle <?php echo ($lid && $lang['ID'] == $lid) ? 'active' : ''; ?>" ><?php echo $lang['code']; ?></a>

                            <?php endforeach; ?>

                        <?php endif; ?>

                    </div>

                </div>-->

            </div>

        </div>

        <!-- /.col-lg-12 -->

    </div>

    <!-- /.row -->

    <form role="form" action="" method="post" enctype="multipart/form-data">

        <?php $response = $this->session->flashdata('response'); 

        if(!empty($response) || isset($code)):

            $class = (!empty($response)) ? $response['class'] :  $code['class'];

            $msg   = (!empty($response)) ? $response['msg']  : $code['msg'];

        ?>

        <div class="alert alert-<?php echo $class ?> alert-dismissable">

            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

            <?php echo $msg; ?>

        </div>

        <?php endif; ?>

        <div class="row">

            <div class="col-lg-6">

                <div class="panel panel-yellow">

                    <div class="panel-heading">

                        Required informations

                    </div>

                    <div class="panel-body">

                        <div class="row">

                            <div class="col-lg-12">

                                <div class="form-group">

                                    <label>Email ID : </label>

                                    <input class="form-control" type="email" name="user[email_id]" required="" placeholder="Enter email" value="<?php if(isset($user['email_id']) ) echo $user['email_id'];  ?>" >

                                </div>

                                <div class="form-group">

                                    <label> Name: </label>

                                    <input class="form-control" type="text" name="user[first_name]" required="" placeholder="Enter name" value="<?php if(isset($user['first_name']) ) echo $user['first_name'];  ?>">

                                </div>

                                <div class="form-group">

                                    <label>User ID : </label>

                                    <input class="form-control" type="text" maxlength="20" name="user[username]" required="" placeholder="Enter User ID" value="<?php if(isset($user['username']) ) echo $user['username'];  ?>">

                                </div>

<!--                                <div class="form-group">

                                    <label>Last Name : </label>

                                    <input class="form-control" type="text" name="user[last_name]" placeholder="Enter last name" value="<?php if(isset($user['last_name']) ) echo $user['last_name'];  ?>">

                                </div>-->

                                <div class="form-group">

                                    <label>Password : </label>

                                    <input class="form-control" type="password" name="user[password]" placeholder="Enter password" >

                                </div>

                                <div class="form-group">

                                    <label>Confirm password : </label>

                                    <input class="form-control" type="password" name="user[confpassword]" placeholder="Enter confirm password">

                                </div>
<!--                                <div class="form-group">

                                    <label>Username : </label>

                                    <input class="form-control" type="text" name="user[username]" placeholder="Enter username" value="<?php if(isset($user['username']) ) echo $user['username'];  ?>">

                                </div>-->

                                <div class="form-group">

                                    <label>User Type :</label>

                                    <div class="form-inline">
                                        <label class="radio-inline">
                                            <input type="radio" class="flat" name="user[user_type]" id="optionsRadiosInline2" <?php if(isset($user['user_type'])){ if($user['user_type']== 2) echo 'checked=""';}else{ echo 'checked=""'; }    ?> value="2" >Teacher
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" class="flat" name="user[user_type]" id="optionsRadiosInline1" <?php if(isset($user['user_type'])){ if($user['user_type']== 1) echo 'checked=""';}?> value="1" >Admin
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" class="flat" name="user[user_type]" id="optionsRadiosInline3" <?php if(isset($user['user_type'])){ if($user['user_type']== 3) echo 'checked=""';}?> value="3">Moderator
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" class="flat" name="user[user_type]" id="optionsRadiosInline4" <?php if(isset($user['user_type'])){ if($user['user_type']== 4) echo 'checked=""';}?> value="4">Parent
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" class="flat" name="user[user_type]" id="optionsRadiosInline5" <?php if(isset($user['user_type'])){ if($user['user_type']== 5) echo 'checked=""';}?> value="5">Register
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" class="flat" name="user[user_type]" id="optionsRadiosInline6" <?php if(isset($user['user_type'])){ if($user['user_type']== 6) echo 'checked=""';}?> value="6">Medical
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" class="flat" name="user[user_type]" id="optionsRadiosInline7" <?php if(isset($user['user_type'])){ if($user['user_type']== 7) echo 'checked=""';}?> value="7">Viewer
                                        </label>
                                    </div>
                                </div>
                            <?php if($user['user_type']== 3):?>
                                <div class="form-group">

                                    <label>Edit Permission :</label>

                                    <div class="form-inline">
                                    
                                        <input type="checkbox" name="user[moderatorAttenEditPower]" id="moderatorAttenEditPower" value="1" <?php if(isset($user['moderatorAttenEditPower'])){ if($user['moderatorAttenEditPower'] == 1) echo 'checked="checked"';} ?>> &nbsp;&nbsp;Allow Attendance Edit
                                    </div>

                                </div>
                            <?php endif; ?>
                             <?php if($user['user_type']== 2):?>
                                <div class="form-group">

                                    <label>Edit Permission :</label>

                                    <div class="form-inline">
                                    
                                        <input type="checkbox" name="user[TeacherAttenEditPower]" id="TeacherAttenEditPower" value="1" <?php if(isset($user['TeacherAttenEditPower'])){ if($user['TeacherAttenEditPower'] == 1) echo 'checked="checked"';} ?> class="allow-edit"> &nbsp;&nbsp;Allow Attendance Edit
                                    </div>
                                </div>
                                <div class="form-group">
                                   <!--   <label>Input Seconds :</label> -->
                                     <div class="form-inline">
                                            
                                             <input class="form-control" type="number" name="user[editTimeForTeacher]" id="editTimeForTeacher" value="<?php  if(isset($user['TeacherAttenEditPower'])){ if($user['TeacherAttenEditPower'] == 1){ echo $user['editTimeForTeacher']; }else{ echo '0';}}  ?>" disabled="" readonly="">
                                             <br>
                                             <p class="desc" style="margin: 10px 0px;   font-size: 12px;">Note: Please input time in seconds.</p>
                                    </div>

                                </div>
                            <?php endif; ?>

<!--                                <div class="form-group">

                                    <label>Status :</label>

                                    <div class="form-inline">

                                        <label class="radio-inline">

                                            <input type="radio" name="user[status]" id="active-status" value="1" <?php if(isset($user['status'])){ if($user['status'] == 1) echo 'checked=""';}else{ echo 'checked=""'; } ?> >Active

                                        </label>

                                        <label class="radio-inline">

                                            <input type="radio" name="user[status]" id="inactive-status" <?php if(isset($user['status'])){ if($user['status'] == 0) echo 'checked=""';} ?> value="0" >In active

                                        </label>

                                    </div>

                                </div>-->

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <div class="col-lg-6">

                <div class="panel panel-primary">

                    <div class="panel-heading">

                        Optional Informations

                    </div>

                    <div class="panel-body">

                        <div class="row">

                            <div class="col-lg-12">

                                <div class="form-group">

                                    <label>Address : </label>

                                    <textarea class="form-control" name="user[meta][address]" placeholder="Enter address"><?php if(isset($meta['address']) ) echo $meta['address'];  ?></textarea>

                                </div>

                                <div class="form-group">

                                    <label>Personal Description : </label>

                                    <textarea class="form-control" name="user[meta][pinfo]" placeholder="Enter personal info"><?php if(isset($meta['pinfo']) ) echo $meta['pinfo'];  ?></textarea>

                                    <input type="hidden" name="school_id" value="<?php echo $this->session->userdata('school_id') ?>">

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <div class="col-lg-6">

                <div class="panel panel-green">

                    <div class="panel-heading">

                        Actions

                    </div>

                    <div class="panel-body">

                        <div class="row">

                            <div class="col-lg-12">

                                <div class="form-group">

                                    <input type="hidden" name="action" value="<?php echo $action; ?>" >

                                    <input  type="submit" class="btn btn-primary pull-right" value="Save">

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </form>

</div>

<!-- /#page-wrapper -->

<script type="text/javascript">
    
    var allowEdit=document.getElementById("TeacherAttenEditPower");
    allowEdit.addEventListener('change',function(){

            if(this.checked)
            {
                document.getElementById("editTimeForTeacher").removeAttribute("disabled");
                document.getElementById("editTimeForTeacher").removeAttribute("readonly");
            }
            else{
                document.getElementById("editTimeForTeacher").setAttribute("disabled", "disabled");
                document.getElementById("editTimeForTeacher").setAttribute("readonly", "readonly");
            }
    })
</script>