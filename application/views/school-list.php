<div class="right_col">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">School List</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    List of Schools
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body blocking">
                    <?php $this->load->view('search-bar',array('plchldr' => "Search by name")); ?>
                    <form action="<?php echo base_url('ajax/delete/plan'); ?>" class="ajax-form" method="POST" >
                        <div id="list-load">
                            <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>School Name</th>                                    
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>Contact</th>
                                    <th>Payment Status</th>
                                    <th>Status</th>
                                    <th>Start subcribe</th>
                                    <th>End subcribe</th>
                                    <!--<th>Status</th>-->
<!--                                    <th>Language</th>-->
                                    <th>Action</th>
                                    
                                </tr>
                                </thead>
                                <tbody>
                                <?php 
                                 if(isset($school) && !empty($school)): ?>
                                    <?php
                                   
                                     
//                                    $courses = array_reverse($courses);
					
                                    foreach($school as $schooll): 
                                    $subs_plan = $this->db->where('id', $schooll['subscription_id'])->get('abs_subscribe_plans')->row();
                                    ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $schooll['id'] ?></td>                                        
                                        <td><?php echo $schooll['school_name']; ?></td>                                        
                                        <td><?php echo $schooll['user_id'] ?></td>                                        
                                        <td><?php echo $schooll['email_id']; ?></td>
                                        <td><?php echo $schooll['contact']; ?></td>  
                                        <td><?php echo $subs_plan->status; ?></td>                                        
                                        <td><?php echo ($schooll['status'] == 1) ? 'active' : 'inactive'; ?></td>
<!--                                        <td><?php foreach($langs as $lang){ echo '<a href="'.base_url('manage/school/'.$schooll['id'].'?lang_id='.$lang['ID']).'" title="'.$lang['code'].'">'.$lang['code'].'</a>&nbsp;&nbsp;'; }?></td>-->
                                        <td><?php echo date('d/m/Y H:i a', strtotime($schooll['Reg_date'])); ?></td> 
                                        <td><?php echo date('d/m/Y H:i a', strtotime($schooll['exp_date'])); ?></td> 
                                        <td>
                                            <a href="<?php echo base_url('manage/school/'.$schooll['id']); ?>" title="Edit" class="edit" ><i class="fa fa-pencil-square-o fa-fw"></i></a>                                            
                                            <a href="<?php echo base_url('delete/school/'.$schooll['id']); ?>" title="Delete" class="delete"><i class="fa fa-times fa-fw"></i></a>   
                                        </td>
                                        
                                    </tr>
                                    <?php endforeach; ?>
                                    <?php else: ?>
                                         <tr class="odd gradeX" >
                                             <td colspan="6">No class found.</td>
                                        </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                            <!-- /.table-responsive -->
                        <?php $this->load->view('paginations'); ?>
                        </div>
                    </form>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>

