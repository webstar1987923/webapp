<!-- Page Content -->

<?php // print_r($user); ?>

<div class="right_col">

    <div class="row">

        <div class="col-lg-12">

            <div class="row">

                <div class="col-lg-6">

                    <h1 class="page-header"><?= $page_title ?></h1>

                </div>

            </div>

        </div>

        <!-- /.col-lg-12 -->

    </div>

    <!-- /.row -->

    <form role="form" action="" method="post" enctype="multipart/form-data">

        <?php
        $response = $this->session->flashdata('response');

        if (!empty($response) || isset($code)):

            //$class = (!empty($response)) ? $response['class'] :  $code['class'];
            // $msg   = (!empty($response)) ? $response['msg']  : $code['msg'];
            ?>

            <div class="alert alert-success alert-dismissable">

                <!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>-->

                <?php echo $response; ?>

            </div>

        <?php endif; ?>

        <div class="row">

            <div class="col-lg-6">

                <div class="panel panel-yellow">

                    <div class="panel-heading">

                        Group Informations

                    </div>

                    <div class="panel-body">

                        <div class="row">

                            <div class="col-lg-12">

                                <div class="form-group">

                                    <label>Group Name : </label>
                                    <input type="hidden" name="school_id" value="<?php echo $this->session->userdata('school_id'); ?>">
                                    <input class="form-control" type="text" name="group_name" required="1" placeholder="Enter group name" value="<?= set_value('group_name', $group_name) ?>" >
                                    <input type="hidden" name="group_id" value="<?= $group_id ?>" >

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <div class="col-lg-6">

                <div class="panel panel-primary">

                    <div class="panel-heading">

                        Class Informations

                    </div>

                    <div class="panel-body">

                        <div class="row">

                            <div class="col-lg-6">
                                <?php
                                $counter = 0;
                                //$courses=array('1','2','3','4');
                                if (isset($courses) && !empty($courses)) {
                                    foreach ($courses as $course) {
                                        $checked = '';
                                        if (in_array($course['cid'], $class_ids)) {
                                            $checked = 'checked';
                                        }
                                        ?>
                                <?php if (ceil(count($courses)/2)==$counter){ ?>
                                    </div>
                                    <div class="col-lg-6">
                                <?php } ?>
                                        <div class="form-group">
                                            <input class="flat" type="checkbox" name="class_ids[]"value="<?= $course['cid'] ?>" <?= $checked ?> >
                                            <label><?= $course['name'] ?></label>
                                        </div>
                                        <?php
                                        $counter++;
                                    }
                                }
                                ?>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <div class="col-lg-6">

                <div class="panel panel-green">

                    <div class="panel-heading">

                        Actions

                    </div>

                    <div class="panel-body">

                        <div class="row">

                            <div class="col-lg-12">

                                <div class="form-group">

                                    <input type="hidden" name="action" value="<?php echo $action; ?>" >

                                    <input  type="submit" class="btn btn-primary pull-right" value="Save">

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </form>

</div>

<!-- /#page-wrapper -->