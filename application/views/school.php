<!-- Page Content -->
<?php // print_r($school); ?>
<div class="right_col">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-6">
                    <h1 class="page-header"> School registration</h1>
                </div>
                <div class="col-lg-6 full-height">
<!--                    <div class="pull-right" >
                        <?php 
                        $lid = (isset($lang_id)) ? $lang_id : 0;
                        if($langs &&  $action == 2): ?>
                            <?php foreach($langs as $lang): ?>
                                <a href="<?php echo '?lang_id='.$lang['ID']; ?>" class="btn btn-circle <?php echo ($lid && $lang['ID'] == $lid) ? 'active' : ''; ?>" ><?php echo $lang['code']; ?></a>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>-->
                </div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <form role="form" action="" method="post" enctype="multipart/form-data">
        <?php $response = $this->session->flashdata('response'); 
        if(!empty($response) || isset($code)):
            $class = (!empty($response)) ? $response['class'] :  $code['class'];
            $msg   = (!empty($response)) ? $response['msg']  : $code['msg'];
        ?>
        <div class="alert alert-<?php echo $class ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
        <?php endif; ?>
        <div class="row">

            <div class="col-lg-6">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        school informations
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>School Name : </label>
                                    <input class="form-control" name="school[school_name]" placeholder="Enter School Name" value="<?php if(isset($school['school_name']) ) echo $school['school_name'];  ?>" >
                                </div>
                                <div class="form-group">
                                    <label>School Detail : </label>
                                    <input class="form-control" name="school[detail]" placeholder="Enter School Detail" value="<?php if(isset($school['detail']) ) echo $school['detail'];  ?>" >
                                </div>
                                
                                 <div class="form-group">
                                    <label>Username : </label>
                                    <input class="form-control" name="school[user_id]" placeholder="Enter Username " value="<?php if(isset($school['user_id']) ) echo $school['user_id'];  ?>" >
                                </div>
                                 <div class="form-group">
                                    <label>Password : </label>
                                    <input class="form-control" name="school[password]" placeholder="Enter Password " value="<?php if(isset($school['password']) ) echo $school['password'];  ?>" >
                                </div>                                
                               
                                <div class="form-group">
                                    <label>Email Id : </label>
                                    <input class="form-control" name="school[email_id]" type="email"  placeholder="Enter Email Id" value="<?php if(isset($school['email_id']) ) echo $school['email_id'];  ?>" >
                                </div>
                                
                                <div class="form-group">
                                    <label>Contact : </label>
                                    <input class="form-control" name="school[contact]" type="text" placeholder="Enter Contact" value="<?php if(isset($school['contact']) ) echo $school['contact'];  ?>" >
                                </div>

                                 <div class="form-group">
                                    <label>Delay Rule : </label>
                                    <input class="form-control" name="school[delay_rule]" min="1" type="number" placeholder="Enter Delay Rule" value="<?php if(isset($school['delay_rule']) ) echo $school['delay_rule'];?>" >
                                </div>

                               <?php if(isset($new_lang_varient)  && !$new_lang_varient ): ?>
                                    <?php if(isset($school['pic']) && $school['pic']): ?>
                                        <div class="form-group">
                                            <label>School Logo </label>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="thumbnail">
                                                        <img src="<?php echo ($school['pic']) ? base_url('uploads/' . $school['pic']) : ''; ?>" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <div class="form-group">
                                        <label>Picture: </label>
                                        <input type="file" name="file" class="filestyle">
                                        <p class="help-block">Allowed Type : JGP,PNG. Allowed Size : 1000 KB</p>
                                    </div>
                                <?php endif; ?>
                               <div class="form-group">
                                    <label>school Status :</label>
                                    <div class="form-inline">
                                        <label class="radio-inline">
                                            <input type="radio" name="school[status]" id="active-status" value="1" <?php if(isset($school['status'])){ if($school['status'] == 1) echo 'checked=""';}else{ echo 'checked=""'; } ?> >Active
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="school[status]" id="inactive-status" <?php if(isset($school['status'])){ if($school['status'] == 0) echo 'checked=""';} ?> value="0" >In active
                                        </label>
                                    </div>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           
            <div class="col-lg-6">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        Actions
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
<!--                                    <input type="hidden" name="school[lang_id]" value="<?php echo (isset($lang_id)) ? $lang_id : $active_lang['ID']; ?>" >
                                    <?php if(isset($lang_id)): ?>
                                        <input type="hidden" name="school[parent]" value="<?php echo (isset($parent)) ? $parent : 0; ?>" >
                                    <?php endif; ?>
                                    <?php if(isset($new_lang_varient)  && !$new_lang_varient ): ?>
                                        <input type="hidden" name="lang_varient" value="1" >
                                    <?php endif; ?>
                                        -->
                                    <input type="hidden" name="action" value="<?php echo $action; ?>" >
                                    <input  type="submit" class="btn btn-primary pull-right" value="Save">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- /#page-wrapper -->