<div class="row clearfix">
    <div class="form-group form-inline col-lg-9">
        <form role="form" action="" method="GET">
            <input class="form-control search-input " name="s" placeholder="<?php echo (isset($plchldr)) ? $plchldr : ''; ?>" value="<?php echo $this->input->get('s'); ?>">
            <button type="submit" class="btn btn-default">Search</button>
        </form>
    </div>
    <div class="form-group form-inline col-lg-3">
        
        <button type="button" class="btn btn-default pull-right" id="delete-objects">Delete</button>&nbsp;
        <?php if($utype=="student" || $utype=="teachers"): ?>
        <button type="button" class="btn btn-default pull-right" data-section="<?php print $utype;?>" id="assign-objects" style="margin-right: 10px">Assign Class</button>&nbsp;&nbsp;&nbsp;&nbsp;
        <?php endif; ?>
<!--        <select name="action" class="form-control pull-right" onchange="return changeOnAction(this.value)">
            <option value="">Action</option>
            <option value="delete">Delete</option>
        </select>-->
    </div>
</div>
