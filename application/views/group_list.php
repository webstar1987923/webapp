<div class="right_col">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo $page_title; ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo ucfirst($page_title); ?>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body blocking">
                    <?php //$this->load->view('search-bar',array('plchldr' => "Search by name or email or user ID")); ?>
                    <form action="<?php echo base_url('ajax/delete/'.$utype); ?>" class="ajax-form" method="POST" >
                        <div id="list-load">
                            <div class="table-responsive">
                                <table class="table table-striped jambo_table bulk_action" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th class="column-title">Group ID</th>
                                            <th class="column-title">Name</th>
                                            <th class="column-title">Class Name(s)</th>
                                            <th class="column-title">Action</th>
                                            <!--<th><input type="checkbox" name="all" id="all" ></th>-->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if(isset($groups) && !empty($groups)): ?>
                                            <?php
                                            foreach( $groups as $group): ?>
                                            <tr class="odd gradeX">
                                                <td><?php echo $group['ID'] ?></td>
                                                <td><?php echo $group['group_name']?></td>
                                                <td><?php echo $group['class_names']?></td>
                                                <td>
                                                    <a href="<?php echo base_url('manage/coursegroup/'.$group['ID']); ?>" title="Edit" class="edit" ><i class="fa fa-pencil-square-o fa-fw"></i></a>
                                                    <a href="<?php echo base_url('delete/group/'.$group['ID']); ?>" title="Delete" class="delete"><i class="fa fa fa-times fa-fw fa-fw"></i></a>
                                                </td>
                                                <!--<td><input type="checkbox" name="delete_ids[]" value="<?php echo $group['ID']; ?>" class="select_me" ></td>-->
                                            </tr>
                                            <?php endforeach; ?>
                                        <?php else: ?>
                                             <tr class="odd gradeX" >
                                                 <td colspan="4">No group found.</td>
                                            </tr>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                            <?php $this->load->view('paginations'); ?>
                        </div>
                    </form>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>

