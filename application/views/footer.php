<?php if ($this->session->userdata('logged_in')) { ?>
            <footer>
                <div class="pull-right">
                    All rights reserved.
                </div>
                <div class="clearfix"></div>
            </footer>
        </div>
    </div>
<?php } ?>
<!-- footer content -->
        
    <!-- /footer content -->
    

   <!-- FastClick -->
    <script src="<?php echo base_url() . "assets/"; ?>vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url() . "assets/"; ?>vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo base_url() . "assets/"; ?>vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo base_url() . "assets/"; ?>vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo base_url() . "assets/"; ?>vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url() . "assets/"; ?>vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo base_url() . "assets/"; ?>vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo base_url() . "assets/"; ?>vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo base_url() . "assets/"; ?>vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url() . "assets/"; ?>vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo base_url() . "assets/"; ?>vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo base_url() . "assets/"; ?>vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo base_url() . "assets/"; ?>vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo base_url() . "assets/"; ?>vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo base_url() . "assets/"; ?>vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo base_url() . "assets/"; ?>vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="<?php echo base_url() . "assets/"; ?>vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="<?php echo base_url() . "assets/"; ?>vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="<?php echo base_url() . "assets/"; ?>vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url() . "assets/"; ?>vendors/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url() . "assets/"; ?>vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url() . "assets/"; ?>js/custom.min.js"></script>
    
    
    <!-- Block UI Version -->

    <script src="<?php echo base_url() . "assets/"; ?>vendor/dropzone.js"></script>
    <!-- ajax form -->
    <script src="<?php echo base_url() . "assets/"; ?>vendor/jquery.form.min.js"></script>

    <script src="<?php echo base_url() . "assets/"; ?>vendor/fullcalendar.min.js"></script>
    <script src="<?php echo base_url() . "assets/"; ?>vendor/jquery.blockUI.js"></script>
    <!-- Bootstrap Core JavaScript -->

<!-- Custom Theme JavaScript -->

<script type="text/javascript">
<?php
if (!isset($holiday_array)) {
    $holiday_array = array();
}
?>
    $(function () {
        $('.date input[type="text"]').datepicker({
            format: 'yyyy-mm-dd'
        });
        $('body').on('click','.media-select-list .media-item input[type="checkbox"]',function(e){
            e.stopPropagation();
        });
        $('body').on('click','.media-select-list .media-item',function(e){
            e.preventDefault();
//            $(this).parent().addClass('selected');
//            var input = $(this).parent().find('input[type="checkbox"]');
            var input = $(this).find('input[type="checkbox"]');
            
             if($(this).hasClass('selected')){
//                 alert('asd');
                $(this).removeClass('selected');
            }else{
//                alert('asd asdasd');
                $(this).addClass('selected');
            }
//            if(!input.is(':checked')){
                input.trigger('click');
//            }
            
        });
        $('body').on('click','.media-select-list .media-item .delete-atached-media',function(e){
            var el = $(this).parent();
            var _id = $(this).attr('data-id');
            var _ad_id = $(this).attr('data-attached');
            $.post('<?php echo base_url('media/removeAtttached'); ?>',{
                id : _id,
                adId : _ad_id
            },function(data){
                if(data == 'done'){
                    el.detach();
                }else{
                    console.log('DaTA',data);
                    alert('some error occured');
                }
            });
        });
        $('body').on('click','.media-select-list .media-item .delete-choosed-media',function(e){
            var el = $(this).parent();
            el.detach();
        });
        
        
        Dropzone.options.myAwesomeDropzone = {
            init: function () {
                this.on("success", function (file) {
                    var _this = this;
                    _this.removeFile(file);
                });
            },
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 2, // MB
            
            complete: function (file) {
                if (file._removeLink) {
                    file._removeLink.textContent = this.options.dictRemoveFile;
                }
//                console.log('File', file);
                $('#media-list').append(file.xhr.response);
//                $('.thumbnail img').attr('src', file.xhr.response);
                if (file.previewElement) {
                    return file.previewElement.classList.add("dz-complete");
                }

            },
            accept: function(file, done) {
                 console.log('File', file);
                 if(file.type.indexOf('image/') < 0){
                     done("Only Image file is supported.");
                 }else{
                     done();
                 }
            }
        };
//        myAwesomeDropzone.on("complete", function (file) {
//            myAwesomeDropzone.removeFile(file);
//        });
        $('body').on('click','.delete-media',function(e){
            e.preventDefault();
            var el = $(this).parent();
            var _id = $(this).attr('data-id');
            $.post('<?php echo base_url('media/remove'); ?>',{
                id : _id
            },function(data){
                if(data == 'done'){
                    el.detach();
                }else{
                    console.log('DaTA',data);
                    alert('some error occured');
                }
            });
        });
        var dates = <?php echo json_encode($holiday_array); ?>;
        $(":file").filestyle();
        var options = {
//                target:        '#output1',   // target element(s) to be updated with server response 
            beforeSubmit: showRequest, // pre-submit callback 
            success: showResponse  // post-submit callback 

                    // other available options: 
                    //url:       url         // override for form's 'action' attribute 
                    //type:      type        // 'get' or 'post', override for form's 'method' attribute 
                    //dataType:  null        // 'xml', 'script', or 'json' (expected server response type) 
                    //clearForm: true        // clear all form fields after successful submit 
                    //resetForm: true        // reset the form after successful submit 

                    // $.ajax options can be used here too, for example: 
                    //timeout:   3000 
        };
        function showRequest(formData, jqForm, options) {
            var queryString = $.param(formData);


            if (!formData.length > 0) {
                return false
            }
            $('.blocking').block({
                message: '<h3 style="margin-top: 10px;">Processing</h3>',
                css: {border: '2px solid #a00'}
            });
//                alert(JSON.stringify(formData));
            //alert('About to submit: \n\n' + queryString); 

            return true;
        }

        // post-submit callback 
        function showResponse(responseText, statusText, xhr, $form) {
            if (responseText != '') {
                $('#list-load').load(responseText + ' #list-load', function () {
                    $('.blocking').unblock();
                });
            } else {
                $('.blocking').unblock();
            }
//                alert('status: ' + statusText + '\n\nresponseText: \n' + responseText + 
//                    '\n\nThe output div should have already been updated with the responseText.'); 
        }

        $('#delete-objects').click(function (e) {
            e.preventDefault();
            if (!$("input[name='delete_ids[]']:checked").val()) {
                alert('Please choose some rows.');
                return false;
            }
            if (!confirm("Are you sure ?")) {
                return false;
            }
            else {
                $('.ajax-form').ajaxSubmit(options);
            }
        });
        $('.delete').click(function (e) {
//                e.preventDefault();
            var a = confirm('Do you want to delete?');
            if (a) {
                return true;
            } else {
                return false;
            }
        });
        $( document ).ready(function() {
            $('#all').on('ifChecked', function (event){
                $(this).closest("input").attr('checked', true);      
                checkAllCheckboxes(this);
            });
            $('#all').on('ifUnchecked', function (event) {
                $(this).closest("input").attr('checked', false);
                checkAllCheckboxes(this);
            });
        });
        
        function checkAllCheckboxes(checkbox){
            if ($(checkbox).prop('checked')){
                $('.select_me').prop('checked', true);
                $.each($('.select_me'), function(index, item){
                    $(item).closest('tr').addClass('selected');
                });
            }else{
                $('.select_me').prop('checked', false);
                $(checkbox).closest('table').find('tr.selected').removeClass('selected');
            }
            $('.select_me').iCheck('update');
        }
        
        
        var formattedDate = new Date();
        var d = formattedDate.getDate();
        var m =  formattedDate.getMonth();
        m += 1;  // JavaScript months are 0-11
        var y = formattedDate.getFullYear();
        var formated_date = y+'-'+m+'-'+d;
        var formated_date2 = d+'/'+m+'/'+y;
        
        d = (String(d).length == 1) ? '0'+d : d; 
        m = (String(m).length == 1) ? '0'+m : m; 
         
        var day = y+'-'+m+'-'+d;
        
        $('#fullcalendar').fullCalendar({
            defaultDate: day,
            editable: true,
            eventLimit: true, // allow "more" link when too many events
            dayClick: function (date, jsEvent, view) {
                $(this).css('background-color', 'red').css('color', '#FFF');

                var val = $('#dates-values').val();
                
//                console.log(date.format());
//                console.log(val.indexOf(date.format()));
                
                if (val.indexOf(date.format()) < 0) {
                    if (val == '') {
                        val = val + date.format();
                    } else {
                        val = val + ',' + date.format();
                    }
                } else {
                    var rm_val = $('#remove_date').val();
                    if (rm_val == '') {
                        rm_val = rm_val + date.format();
                    } else {
                        rm_val = rm_val + ',' + date.format();
                    }
                    $('#remove_date').val(rm_val);
                    val = val.replace(date.format(), "");
//                    console.log($(this))
                    $(this).css('background-color', '#fff').css('color', '#000');
                }

                $('#dates-values').val(val);

//                            alert('Clicked on: ' + date.format());
//
//                            alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
//
//                            alert('Current view: ' + view.name);

                // change the day's background color just for fun


            },
            dayRender: function (date, cell) {
                if (dates.length > 0) {
                    for (var i = 0; i < dates.length; i++) {
                        if (date.format() == dates[i]) {
                            $(cell).css('background-color', 'red').css('color', '#FFF');
                            break;
                        }
                    }
                }
            },
            selectable: true
//			events: [
//				{
//					title: 'All Day Event',
//					start: '2014-11-01'
//				},
//				{
//					title: 'Long Event',
//					start: '2014-11-07',
//					end: '2014-11-10'
//				},
//				{
//					id: 999,
//					title: 'Repeating Event',
//					start: '2014-11-09T16:00:00'
//				},
//				{
//					id: 999,
//					title: 'Repeating Event',
//					start: '2014-11-16T16:00:00'
//				},
//				{
//					title: 'Conference',
//					start: '2014-11-11',
//					end: '2014-11-13'
//				},
//				{
//					title: 'Meeting',
//					start: '2014-11-12T10:30:00',
//					end: '2014-11-12T12:30:00'
//				},
//				{
//					title: 'Lunch',
//					start: '2014-11-12T12:00:00'
//				},
//				{
//					title: 'Meeting',
//					start: '2014-11-12T14:30:00'
//				},
//				{
//					title: 'Happy Hour',
//					start: '2014-11-12T17:30:00'
//				},
//				{
//					title: 'Dinner',
//					start: '2014-11-12T20:00:00'
//				},
//				{
//					title: 'Birthday Party',
//					start: '2014-11-13T07:00:00'
//				},
//				{
//					title: 'Click for Google',
//					url: 'http://google.com/',
//					start: '2014-11-28'
//				}
//			]
        });
//                $('body').on('click','.date-delete',function(e){
//                alert('asdas');
//                    e.preventDefault();
//                    e.stopPropagation();
//                    var date_data = $(this).attr('data-date');
//                    var date_values = $('#dates-values').val();
//                    if(date_values == ''){
//                        return false;
//                    }else{
//                        date_values.replace(date_data+",","");
//                    }
//                    $('#dates-values').val(date_values);
//                })
        var cousre_choose = false;

//        $('.choose_course').click(function(){
//            cousre_choose = true;
//        });
        $('#assign-objects').click(function (e) {
            e.preventDefault();
            if (!cousre_choose) {
                if (!$("input[name='delete_ids[]']:checked").val()) {
                    alert('Please choose some students.');
                } else {
                    $('.courses-choose').slideDown('slow');
                    cousre_choose = true;
                }
            } else {
                
                if ($(this).attr('data-section')=="teachers"){
                        if (!$("input[name='course_id[]']:checked").length) {
                            alert('Please choose one class.');
                        } else {
                            options.url = "<?php echo base_url('ajax/assign/tcourse'); ?>";
                            $('.ajax-form').ajaxSubmit(options);
                            $('.courses-choose').slideUp('slow');
                            cousre_choose = false;
                            $("input[name='course_id[]']").removeAttr('checked');
                            $("input[name='delete_ids[]']").removeAttr('checked');
                        }
                    }else{
                        if (!$("input[name='course_id']:checked").val()) {
                            alert('Please choose one class.');
                        } else {
                            options.url = "<?php echo base_url('ajax/assign/course'); ?>";
                            $('.ajax-form').ajaxSubmit(options);
                            $('.courses-choose').slideUp('slow');
                            cousre_choose = false;
                            $("input[name='course_id']").removeAttr('checked');
                            $("input[name='delete_ids[]']").removeAttr('checked');
                        }
                    }
                    
                    

            }

        });
    });
</script>
    
  </body>
</html>
