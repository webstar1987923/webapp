<div class="login_wrapper">
    <div class="animate form login_form">
        <section class="login_content">
            <form role="form" method="post" action="<?php echo base_url('login/goLogin'); ?>">
                <h1>Please Sign In</h1>
                <div id="message">
                    <?php
                        $msg = $this->session->flashdata('msg');
                        if($msg){
                    ?>
                        <p> <?php echo $msg; ?></p>
                    <?php } ?>
                </div>
                <div>
                    <select class="form-control loginSchool" name="school">
                        <option value="">Select School</option>
                        <?php if($schools){  foreach($schools as $school){ ?>                         		
                            <option <?php if($school->id === $_COOKIE['school']){ echo 'selected="selected"'; } ?> value="<?php echo $school->id; ?>"><?php echo $school->school_name; ?></option>
                        <?php } } ?>	
                    </select>                                
                </div>
                <br>
                <div>
                    <input class="form-control" placeholder="E-mail or username" name="email_id" type="text" autofocus value="<?php if(isset( $_COOKIE['email'])){ echo $_COOKIE['email']; } ?>">
                </div>
                <div>
                    <input class="form-control" placeholder="Password" name="password" type="password" value="<?php if(isset( $_COOKIE['password'])){ echo $_COOKIE['password']; } ?>">
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                        <div class="checkbox loginRemember">
                            <label>
                                <input name="remember" type="checkbox" value="Remember Me">Remember Me
                            </label>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                        <input type="submit" class="btn btn-default submit loginSubmit" value="Login">
                    </div>
                </div>
            </form>
        </section>
    </div>
</div>