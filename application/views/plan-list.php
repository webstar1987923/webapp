<div class="right_col">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Plan List</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    List of Plans
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body blocking">
                    <?php $this->load->view('search-bar',array('plchldr' => "Search by name")); ?>
                    <form action="<?php echo base_url('ajax/delete/plan'); ?>" class="ajax-form" method="POST" >
                        <div id="list-load">
                            <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Amount</th>
                                    <th>Status</th>
                                    <!--<th>Status</th>-->
<!--                                    <th>Language</th>-->
                                    <th>Action</th>
                                    
                                </tr>
                                </thead>
                                <tbody>
                                <?php if(isset($plans) && !empty($plans)): ?>
                                    <?php
//                                    $courses = array_reverse($courses);
                                    foreach( $plans as $plan): ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $plan['id'] ?></td>
                                        <td><?php echo $plan['name']; ?></td>
                                        <td><?php echo $plan['amount']; ?></td>                                        
                                        <td><?php echo ($plan['status'] == 1) ? 'active' : 'inactive'; ?></td>
<!--                                        <td><?php foreach($langs as $lang){ echo '<a href="'.base_url('manage/plan/'.$plan['id'].'?lang_id='.$lang['ID']).'" title="'.$lang['code'].'">'.$lang['code'].'</a>&nbsp;&nbsp;'; }?></td>-->
                                        <td>
                                            <a href="<?php echo base_url('manage/plan/'.$plan['id']); ?>" title="Edit" class="edit" ><i class="fa fa-pencil-square-o fa-fw"></i></a>                                            
                                            <a href="<?php echo base_url('delete/plan/'. $plan['id']); ?>" title="Delete" class="delete"><i class="fa fa-times fa-fw"></i></a>   
                                        </td>
                                        
                                    </tr>
                                    <?php endforeach; ?>
                                    <?php else: ?>
                                         <tr class="odd gradeX" >
                                             <td colspan="6">No class found.</td>
                                        </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                            <!-- /.table-responsive -->
                        <?php $this->load->view('paginations'); ?>
                        </div>
                    </form>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>

