<!-- Page Content -->
<?php // print_r($student); ?>
<div class="right_col">
    <div class="row">
        <div class="col-lg-12">
           <div class="row">
                <div class="col-lg-6">
                    <h1 class="page-header">Student registration</h1>
                </div>
                <div class="col-lg-6 full-height">
<!--                    <div class="pull-right" >
                        <?php 
                        $lid = (isset($lang_id)) ? $lang_id : 0;
                        if($langs &&  $action == 2): ?>
                            <?php $langs[] = $active_lang; ?>
                            <?php foreach($langs as $lang): ?>
                                <a href="<?php echo '?lang_id='.$lang['ID']; ?>" class="btn btn-circle <?php echo ($lid && $lang['ID'] == $lid) ? 'active' : ''; ?>" ><?php echo $lang['code']; ?></a>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>-->
                </div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <form role="form" action="" method="post" enctype="multipart/form-data">
        <?php $response = $this->session->flashdata('response'); 
        if(!empty($response) || isset($code)):
            $class = (!empty($response)) ? $response['class'] :  $code['class'];
            $msg   = (!empty($response)) ? $response['msg']  : $code['msg'];
        ?>
        <div class="alert alert-<?php echo $class ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
        <?php endif; ?>
        <div class="row">

            <div class="col-lg-6">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        Required informations
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label> Name: </label>
                                    <input class="form-control" type="text" required="" name="student[name]" placeholder="Enter name" value="<?php if(isset($student['name']) ) echo $student['name'];  ?>">
                                </div>
                                <div class="form-group">
                                    <label>Student ID: </label>
                                    <input class="form-control" type="tel" required="" maxlength="20" name="student[student_id]" placeholder="Enter Student id" value="<?php if(isset($student['student_id']) ) echo $student['student_id'];  ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Optional Informations
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <?php if(isset($new_lang_varient)  && !$new_lang_varient ): ?>
                                <div class="form-group">
                                    <label>Email ID : </label>
                                    <input class="form-control"  <?php if(isset($update_lang_varient) && $update_lang_varient) echo 'disabled=""'; ?> type="email" name="student[email_id]" placeholder="Enter email" value="<?php if(isset($student['email_id']) ) echo $student['email_id'];  ?>" >
                                </div>
                                <?php endif; ?>
                                <?php if(isset($new_lang_varient)  && !$new_lang_varient ): ?>
                                    <?php if(isset($student['pic']) && $student['pic']): ?>
                                        <div class="form-group">
                                            <label>Pic </label>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="thumbnail">
                                                        <img src="<?php echo ($student['pic']) ? $this->config->item('upload_url').$student['pic'] : ''; ?>" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <div class="form-group">
                                        <label>Profile picture: </label>
                                        <input type="file" name="file" class="filestyle">
                                        <p class="help-block">Allowed Type : JGP,PNG. Allowed Size : 1000 KB</p>
                                    </div>
                                <?php endif; ?>
                                <div class="form-group">
                                    <label>Phone no : </label>
                                    <input class="form-control" type="tel" name="student[phone_no]" placeholder="Enter phone no" value="<?php if(isset($student['phone_no']) ) echo $student['phone_no'];  ?>">
                                </div>
                                <div class="form-group">
                                    <label>Address : </label>
                                    <textarea class="form-control" name="student[meta][address]" placeholder="Enter address"><?php if(isset($meta['address']) ) echo $meta['address'];  ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Personal Description : </label>
                                    <textarea class="form-control" name="student[meta][pinfo]" placeholder="Enter personal info"><?php if(isset($meta['pinfo']) ) echo $meta['pinfo'];  ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        Actions
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
<!--                                    <input type="hidden" name="student[lang_id]" value="<?php if(isset($student['lang_id']))echo $student['lang_id']; else echo (isset($lang_id)) ? $lang_id : $active_lang['ID']; ?>" >
                                    <?php if(isset($lang_id)): ?>
                                        <input type="hidden" name="student[parent]" value="<?php echo (isset($parent)) ? $parent : 0; ?>" >
                                    <?php endif; ?>
                                    <?php if(isset($new_lang_varient)  && !$new_lang_varient ): ?>
                                        <input type="hidden" name="lang_varient" value="1" >
                                    <?php endif; ?>
                                        -->
                                         <input type="hidden" name="student[school_id]" value="<?php echo $this->session->userdata('school_id'); ?>" >  
                                    <input type="hidden" name="action" value="<?php echo $action; ?>" >
                                    <input  type="submit" class="btn btn-primary pull-right" value="Save">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- /#page-wrapper -->