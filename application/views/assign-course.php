<!-- Page Content -->
<?php // print_r($course); ?>
<div class="right_col">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-6">
                    <h1 class="page-header">Assign class</h1>
                </div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <form role="form" action="" method="post" enctype="multipart/form-data">
        <?php $response = $this->session->flashdata('response'); 
        if(!empty($response) || isset($code)):
            $class = (!empty($response)) ? $response['class'] :  $code['class'];
            $msg   = (!empty($response)) ? $response['msg']  : $code['msg'];
        ?>
        <div class="alert alert-<?php echo $class ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
        <?php endif; ?>
        <div class="row">

            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Student informations
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <tbody> 
                                    <tr>
                                        <th>ID</th>
                                        <td><?php echo $student['sid'] ?></td>
                                    </tr>
                                    <tr>
                                        <th>Pic</th>
                                        <?php  $student['pic'] = ($student['pic']) ? $student['pic'] :'nopic.png'; ?>
                                        <td><img src="<?php echo base_url('uploads/'.$student['pic']); ?>" /></td>
                                    </tr>
                                    <tr>
                                        <th>Name</th>
                                        <td><?php echo $student['name'] ?></td>
                                    </tr>
                                    <tr>
                                        <th>Email</th>
                                        <td><?php echo $student['email_id'] ?></td>
                                    </tr>
<!--                                    <tr>
                                        <th>Status</th>
                                        <td><?php echo ($student['status'] == 1) ? 'active' : 'inactive'; ?></td>
                                    </tr>-->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Class Informations
                    </div>
                    <div class="panel-body">
                        <ul class="col-lg-6 list-unstyled">
                            <?php if($courses): ?>
                            <?php $counter = 0;?>
                            <?php foreach($courses as $course): ?>
                                <?php if (ceil(count($courses)/2)==$counter){ ?>
                                    </ul>
                                    <ul class="col-lg-6 list-unstyled">
                                <?php } ?>
                                <li>
                                    <input type="radio" class="student-courses flat"
                                           value="<?php echo $course['cid']; ?>"
                                           name="courses[]" onclick="assignStudentCourse(this)"
                                           <?php if(in_array($course['cid'], $assigned_courses)){echo 'checked=""'; } ?>
                                           > <?php echo $course['code']; ?>
                                </li>
                                <?php $counter++;?>
                            <?php endforeach; ?>
                            <?php else: ?>
                            <p>No classes found.</p>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Actions
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <input type="hidden" name="action" value="<?php echo $action; ?>" >
                                    <input  type="submit" class="btn btn-primary pull-right" value="Save">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    function assignStudentCourse(input){
        $.each($('input.student-courses'), function(index, item){
            if ($(item).val()!=$(input).val()){
                $(item).prop('checked', false);
            }
        });
    }
    
</script>
<!-- /#page-wrapper -->