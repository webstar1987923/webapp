<div class="right_col">

    <div class="row">

        <div class="col-lg-12">

            <h1 class="page-header">Dashboard</h1>

        </div>

        <!-- /.col-lg-12 -->

    </div>

    <!-- /.row -->

    <div class="row">
<?php if($this->session->userdata('is_school') == true) { ?>
        <div class="col-lg-3 col-md-7 col-xs-12">

            <div class="panel ">

                <div class="panel-heading">

                    <div class="row">

                        <div class="col-xs-3">

                            <i class="fa fa-graduation-cap fa-5x"></i>

                        </div>

                        <div class="col-xs-9 text-right">

                            <div class="huge"><?php echo $student_count; ?></div>

                            <div>Students</div>

                        </div>

                    </div>

                </div>
		
                <a href="<?php echo base_url('lists/student'); ?>">

                    <div class="panel-footer">

                        <span class="pull-left">View Details</span>

                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                        <div class="clearfix"></div>

                    </div>

                </a>
		
            </div>

        </div>
	<?php } if($this->session->userdata('is_school') == true) { ?>
        <div class="col-lg-3 col-md-7 col-xs-12">

            <div class="panel panel-green">

                <div class="panel-heading">

                    <div class="row">

                        <div class="col-xs-3">

                            <i class="fa fa-book fa-5x"></i>

                        </div>

                        <div class="col-xs-9 text-right">

                            <div class="huge"><?php echo $course_count; ?></div>

                            <div>Classes!</div>

                        </div>

                    </div>

                </div>

                <a href="<?php echo base_url('lists/course'); ?>">

                    <div class="panel-footer">

                        <span class="pull-left">View Details</span>

                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                        <div class="clearfix"></div>

                    </div>

                </a>

            </div>

        </div>
	<?php } else{ ?>
	<div class="col-lg-6 col-md-7 col-xs-12">

            <div class="panel panel-green">

                <div class="panel-heading">

                    <div class="row">

                        <div class="col-xs-3">

                            <i class="fa fa-university fa-5x"></i>

                        </div>

                        <div class="col-xs-9 text-right">

                            <div class="huge"><?php echo $school_count; ?></div>

                            <div>Schools!</div>

                        </div>

                    </div>

                </div>

                <a href="<?php echo base_url('lists/school'); ?>">

                    <div class="panel-footer">

                        <span class="pull-left">View Details</span>

                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                        <div class="clearfix"></div>

                    </div>

                </a>

            </div>

        </div>
	<?php } ?>
	<?php if($this->session->userdata('is_school') == true) { ?>
        <div class="col-lg-3 col-md-7 col-xs-12">

            <div class="panel panel-yellow">

                <div class="panel-heading">

                    <div class="row">

                        <div class="col-xs-3">

                            <i class="fa fa-users fa-5x"></i>

                        </div>

                        <div class="col-xs-9 text-right">

                            <div class="huge"><?php echo $teacher_count; ?></div>

                            <div>Teachers</div>

                        </div>

                    </div>

                </div>
		
                <a href="<?php echo base_url('lists/teachers'); ?>">

                    <div class="panel-footer">

                        <span class="pull-left">View Details</span>

                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                        <div class="clearfix"></div>

                    </div>

                </a>
		
            </div>

        </div>
	<?php } if($this->session->userdata('is_school') == true) { ?>
        <div class="col-lg-3 col-md-7 col-xs-12">

            <div class="panel panel-red">

                <div class="panel-heading">

                    <div class="row">

                        <div class="col-xs-3">

                            <i class="fa fa-users fa-5x"></i>

                        </div>

                        <div class="col-xs-9 text-right">

                            <div class="huge"><?php echo $parent_count; ?></div>

                            <div>Parents</div>

                        </div>

                    </div>

                </div>

                <a href="<?php echo base_url('lists/parents'); ?>">

                    <div class="panel-footer">

                        <span class="pull-left">View Details</span>

                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                        <div class="clearfix"></div>

                    </div>

                </a>

            </div>

        </div>
	<?php } else{ ?>
	<div class="col-lg-6 col-md-7 col-xs-12">

            <div class="panel panel-red">

                <div class="panel-heading">

                    <div class="row">

                        <div class="col-xs-3">

                            <i class="fa fa-book fa-5x"></i>

                        </div>

                        <div class="col-xs-9 text-right">

                            <div class="huge"><?php echo $news_count; ?></div>

                            <div>News</div>

                        </div>

                    </div>

                </div>

                <a href="<?php echo base_url('lists/news'); ?>">

                    <div class="panel-footer">

                        <span class="pull-left">View Details</span>

                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                        <div class="clearfix"></div>

                    </div>

                </a>

            </div>

        </div>
	<?php } ?>
	<?php if($this->session->userdata('is_school') == true){ ?>
        
        
        <div class="col-lg-12">

            <div class="table-responsive">

                <!-- mrin edit section -->
                <div class="col-lg-12">
                    <form action="" method="post">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group col-lg-2 col-xs-12">
                                    <label>Stard Date : </label>
                                    <input type="text" class="form-control pull-right datepicker frmfld" onchange="checkHideOption()" name="start_date" placeholder="Start Date" value="<?= (isset($filtercond['start_date'])) ? $filtercond['start_date'] : date('Y-m-d') ?>" >
                                </div>
                                <div class="form-group col-lg-2 col-xs-12">
                                    <label>End Date : </label>
                                    <input type="text" class="form-control pull-right datepicker frmfld" onchange="checkHideOption()" name="end_date" placeholder="End Date" value="<?= (isset($filtercond['end_date'])) ? $filtercond['end_date'] : date('Y-m-d') ?>" >
                                </div>

                                <div class="form-group col-lg-2 col-xs-12">
                                    <div>
                                        <label>Choose Class : </label>
                                        <select name="class_id[]" class="form-control frmfld" multiple size="3" autocomplete="off" onchange="getCourseStudents(this)">
                                            <option value="" <?php if (empty($class_id)){?>selected<?php } ?>>Choose Class</option>
                                            <?php
                                            if (!empty($courses)) {
                                                //print_r($courses);
                                                foreach ($courses as $course) {
                                                    echo "<option value='" . $course['cid'] . "' " . (in_array($course['cid'], $class_id) ? 'selected' : '') . ">" . $course['name'] . "</option>";
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="<?php if (empty($class_students)){?>hide<?php } ?>">
                                        <label>Student name : </label>
                                        <select name="studentid" class="form-control frmfld" autocomplete="off">
                                            <option value="0" <?php if (empty($studentid)) { ?>selected<?php } ?>>Choose Student</option>
                                            <?php foreach($class_students as $student){?>
                                                <option value="<?php print $student['sid'];?>" <?php if ($student['sid']==$studentid) { ?>selected<?php } ?>><?php print $student['name'];?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-lg-2 col-xs-12">
                                    <label>Seminar number : </label>
                                    <select name="semno[]" class="form-control frmfld" multiple size="3">
                                        <option value="" <?php if (empty($semno)) { ?>selected<?php } ?>>Choose Seminar</option>
                                        <option value="1" <?php if (in_array(1, $semno)) { ?>selected<?php } ?>>Seminar-1</option>
                                        <option value="2" <?php if (in_array(2, $semno)) { ?>selected<?php } ?>>Seminar-2</option>
                                        <option value="3" <?php if (in_array(3, $semno)) { ?>selected<?php } ?>>Seminar-3</option>
                                        <option value="4" <?php if (in_array(4, $semno)) { ?>selected<?php } ?>>Seminar-4</option>
                                        <option value="5" <?php if (in_array(5, $semno)) { ?>selected<?php } ?>>Seminar-5</option>
                                        <option value="6" <?php if (in_array(6, $semno)) { ?>selected<?php } ?>>Seminar-6</option>
                                        <option value="7" <?php if (in_array(7, $semno)) { ?>selected<?php } ?>>Seminar-7</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-2 col-xs-12">
                                    <div>
                                        <label>Number of Absent : </label>
                                        <input type="text" class="form-control frmfld" name="absent_number" placeholder="Number of Absent" value="<?= (isset($filtercond['absent_number'])) ? $filtercond['absent_number'] : '' ?>" >
                                    </div>
                                    <div class="hide5-option <?php if ((strtotime($filtercond['end_date'])-strtotime($filtercond['start_date']))>=86400){?>hide<?php } ?>">
                                        <label>
                                            <input type="checkbox" class="select_me" name="hide_absent"  <?= (isset($filtercond['hide_absent'])) ? 'checked' : '' ?> value="5" >
                                            Hide >= 5 Sems Absent
                                        </label>
                                        
                                    </div>
                                </div>
                                
                                
                                
                                <div class="form-group col-lg-2 col-xs-12" style="padding-top: 24px;">
                                    <button type="submit" class="btn btn-primary">Filter</button> 
                                    <?php if ($absents): ?>
                                        <?php if ( $filtercond['end_date'] == $filtercond['start_date'] ){?>
                                            <button type="button" name="print_2" value="1" class="btn btn-primary printreport2">Print 2</button>
                                        <?php }else{ ?>
                                            <button type="button" name="print_1" value="1" class="btn btn-primary printreport">Print 1</button>
                                            <button type="button" name="print_3" value="1" class="btn btn-primary printreport3">Print 3</button>
                                        <?php } ?>
                                    <?php endif; ?>
                                </div>

                            </div>
                        </div>
                    </form>

                </div>
                <!-- END : mrin edit -->
                <div class="form-group">

                    <h5><strong><?php
                            echo "Name of absentee students in the period ";
                            if (!empty($filtercond)) {
                                $isPresent = false;
                                if (isset($filtercond['start_date']) && !empty($filtercond['start_date'])) {
                                    if (isset($filtercond['end_date']) && !empty($filtercond['end_date'])) {
                                        echo "from ";
                                    }
                                    echo date('F j,Y', strtotime($filtercond['start_date']));
                                    $isPresent = true;
                                }
                                if (isset($filtercond['end_date']) && !empty($filtercond['end_date'])) {
                                    if ($isPresent) {
                                        echo " to ";
                                    }
                                    echo date('F j,Y', strtotime($filtercond['end_date']));
                                    $isPresent = true;
                                }
                                if (!$isPresent) {
                                    echo date('F j,Y');
                                }
                            } else {
                                echo date('F j,Y');
                            }
                            ?></strong></h5>

                </div>
                <table class="table table-bordered table-hover" id="absent_table_ds" row="<?= ($absents) ? 1 : 0 ?>">
                    <tbody>

                        <?php if ($absents): ?>

                            <?php foreach ($absents as $key => $value) { ?>
                                <tr>
                                    <td align="center">Student Name</td>
                                    <td align="center"><?php echo $value['student_name']; ?></td>
                                    <td align="center">Class Name</td>
                                    <td align="center"><?php echo $value['class_name']; ?></td>
                                    <td align="center">Total Absent</td>
                                    <td align="center"><?php echo $value['abscount']; ?></td>
                                </tr>
                                <?php $counter = 1;?>
                                <tr>
                                    <td style="border:none;text-align:right !important;" align="right" colspan="5">Sems</td>
                                    <td align="center" style="border:none;">Date</td>
                                </tr>
                                <?php foreach($students_seminars_absents[$value['sid']]['days'] as $absent_date=>$absent_seems){?>
                                    <tr>
                                        <td style="border:none;" align="right" colspan="5"><?php print implode(', ', $absent_seems);?></td>
                                        <td style="width: 20%;border:none;" align="center"><?php print $absent_date;?></td>
                                    </tr>
                                    <?php if (isset($notes[$value['sid']][$absent_date]) && $notes[$value['sid']][$absent_date]){?>
                                        <tr>
                                            <td colspan="6" style="border:none;" align="right">
                                                <?php foreach ($notes[$value['sid']][$absent_date] as $note){?>
                                                    <p><?php print $note['note'];?></p>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    <?php $counter++;?>
                                <?php } ?>
                            <?php } ?>

                        <?php else: ?>

                            <tr><td colspan="6">No absent list found.</td></tr>

                        <?php endif; ?>

                    </tbody>

                </table>

            </div>

        </div>
    <?php } ?>
    </div>

</div>
<!-- print section table view -->
<style rel='stylesheet'>
    table tr th {
        text-align :center!important;
    }
</style>
<div style="display:none;">

    <div id="reportprintdiv">
        <div class="row">
            <div class="col-lg-12">
                <div class="col-lg-4 pull-left">
                    <h4 style="text-align: right;padding-top:10px;display:block;margin-left:30px;">
                        سجل متابعة الطلاب
                    </h4>
                </div>
                <div class="col-lg-8 pull-right" style="text-align: right;">
                    <img src="<?= base_url('uploads/report_logo/report_logo_right.png') ?>" width="50" height="60" style="page-break-inside: avoid;display:inline-block;vertical-align:top;margin-right:100px;">
                    <h4 style="display: inline-block;text-align: center;">وزارة التربية<br/><?php print $current_school['detail'];?><br><?php print $current_school['school_name'];?></h4>
                </div>
            </div>
        </div>
        <div class="row" style="margin-top: 20px;">
            <div class="form-group" style="text-align: center; margin-bottom:0px!important;">
                <h5> 
                    <strong style="font-size:16px;"><u> أسماء الطلاب الغائبين في الفترة من </u></strong>
                    <?php
                    if (!empty($filtercond)) {
                        $isPresent = false;


                        if (isset($filtercond['start_date']) && !empty($filtercond['start_date'])) {
                            if ($isPresent) {
                                //echo "<strong><u>"." إلى ".date('d/m/Y',strtotime($filtercond['start_date']))."</u></strong>";
                            } else {
                                //echo "<strong><u>".date('d/m/Y',strtotime($filtercond['start_date']))."</u></strong>";

                                echo "<strong><u>" . date('d/m/Y', strtotime($filtercond['start_date'])) . " إلى </u></strong>";
                            }
                            $isPresent = true;
                        }

                        if (isset($filtercond['end_date']) && !empty($filtercond['end_date'])) {
                            if ($isPresent) {
                                //echo "<strong><u>"." إلى ".date('d/m/Y',strtotime($filtercond['end_date']))."</u></strong>";
                                echo "<strong><u>" . "" . date('d/m/Y', strtotime($filtercond['end_date'])) . " </u></strong>";
                            } else {
                                echo "<strong><u>" . date('d/m/Y', strtotime($filtercond['end_date'])) . "</u></strong>";
                            }
                            $isPresent = true;
                        }


                        if (!$isPresent) {
                            echo "<strong><u>" . date('d/m/Y') . "</u></strong>";
                        }
                    } else {
                        echo "<strong><u>" . date('d/m/Y') . "</u></strong>";
                    }
                    ?></h5>
            </div>
        </div> 
        <div class="row ">
            <table class="table table-bordered table-hover" style="table-layout: fixed;" id="absent_table" row="<?= ($absents) ? 1 : 0 ?>">
                <tbody>

    <?php
    if ($absents):
        $i = 1;
        ?>
                            <?php $counter = 1;?>
                            <?php foreach ($absents as $key => $value) { ?>

                                <tr style="border:none !important;">
                                    <td align="center" style="padding:4px;width:40px;margin:0px;border:1px solid black;"><?php echo $value['abscount']; ?></td>
                                    <td align="right" style="width:90px;padding:4px;margin:0px;border:1px solid black;">الإجمالي</td>
                                    <td align="right" style="width:90px;padding:4px;margin:0px;border:1px solid black;"><?php echo $value['class_name']; ?></td>
                                    <td align="right" style="width:90px;padding:4px;margin:0px;border:1px solid black;">الصف</td>
                                    <td align="right" style="width:250px !important;padding:4px;margin:0px;border:1px solid black;"><?php echo $value['student_name']; ?></td>
                                    <td align="right" style="width:90px;padding:4px;margin:0px;border:1px solid black;">اسم الطالب</td>
                                    <td align="right" style="padding:4px;width:40px;margin:0px;border:1px solid black;"><?php print $counter;?></td>
                                </tr>

                                <tr style="border:none !important;">
                                    <td style="border:none !important;text-align:right;padding:0px 220px 0px 0px;margin:0px;" align="right" colspan="5">الحصة</td>
                                    <td align="right" colspan="2" style="border:none !important;padding:0px;margin:0px;">التاريخ</td>
                                </tr>
                                <?php foreach($students_seminars_absents[$value['sid']]['days'] as $absent_date=>$absent_seems){?>
                                    <tr style="border:none !important;">
                                        <td style="border:none !important;padding:0px 220px 0px 0px;margin:0px;font-size:14px;" align="right" colspan="5"><?php print implode(', ', $absent_seems);?></td>
                                        <td style="border:none !important;padding:0px;margin:0px;" colspan="2" align="right"><?php print $absent_date;?></td>
                                    </tr>
                                    <?php if (isset($notes[$value['sid']][$absent_date]) && $notes[$value['sid']][$absent_date]){?>
                                        <tr style="border:none !important;">
                                            <td colspan="7" style="border:none !important;padding:0px;margin:0px;" align="right">
                                                <?php foreach ($notes[$value['sid']][$absent_date] as $note){?>
                                                    <p>- <?php print $note['note'];?></p>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>

                                <?php } ?>
                                <?php $counter++;?>
                            <?php } ?>



    <?php else: ?>

                        <tr><td colspan="7">No absent list found.</td></tr>

                    <?php endif; ?>

                </tbody>
            </table>
            <div class="form-group" style="text-align: left; margin-bottom:0px!important;">
                <h5><strong>مدير المدرسة</strong></h5>
            </div>
        </div>
    </div>
    <div id="reportprintdiv2">
        <div class="row">
            <div class="col-lg-12">
                <div class="col-lg-4 pull-left">
                    <h4 style="text-align: right;padding-top:10px;display:block;margin-left:30px;">
                        سجل متابعة الطلاب
                    </h4>
                </div>
                <div class="col-lg-8 pull-right" style="text-align: right;">
                    <img src="<?= base_url('uploads/report_logo/report_logo_right.png') ?>" width="50" height="60" style="page-break-inside: avoid;display:inline-block;vertical-align:top;margin-right:100px;">
                    <h4 style="display: inline-block;text-align: center;">وزارة التربية<br/><?php print $current_school['detail'];?><br><?php print $current_school['school_name'];?></h4>
                </div>
            </div>
        </div>
        <div class="row" style="margin-top: 20px;">
            <div class="form-group" style="text-align: center; margin-bottom:0px !important;">
                <h5> 
                    <strong style="font-size:16px;"><u> أسماء الطلاب الغائبين في الفترة من </u></strong>
                    <?php
                    if (!empty($filtercond)) {
                        $isPresent = false;


                        if (isset($filtercond['start_date']) && !empty($filtercond['start_date'])) {
                            if ($isPresent) {
                                //echo "<strong><u>"." إلى ".date('d/m/Y',strtotime($filtercond['start_date']))."</u></strong>";
                            } else {
                                //echo "<strong><u>".date('d/m/Y',strtotime($filtercond['start_date']))."</u></strong>";

                                echo "<strong><u>" . date('d/m/Y', strtotime($filtercond['start_date'])) . " إلى </u></strong>";
                            }
                            $isPresent = true;
                        }

                        if (isset($filtercond['end_date']) && !empty($filtercond['end_date'])) {
                            if ($isPresent) {
                                //echo "<strong><u>"." إلى ".date('d/m/Y',strtotime($filtercond['end_date']))."</u></strong>";
                                echo "<strong><u>" . "" . date('d/m/Y', strtotime($filtercond['end_date'])) . " </u></strong>";
                            } else {
                                echo "<strong><u>" . date('d/m/Y', strtotime($filtercond['end_date'])) . "</u></strong>";
                            }
                            $isPresent = true;
                        }


                        if (!$isPresent) {
                            echo "<strong><u>" . date('d/m/Y') . "</u></strong>";
                        }
                    } else {
                        echo "<strong><u>" . date('d/m/Y') . "</u></strong>";
                    }
                    ?></h5>
            </div>
        </div>
        <table class="table table-bordered table-hover" style="table-layout: fixed;" id="absent_table" row="<?= ($absents) ? 1 : 0 ?>">
            <tbody>
                <tr>
                    <th style="width:70px;text-align: center">الإجمالي</th>
                    <th style="text-align: center">الحصة</th>
                    <th style="width:80px !important;text-align: center">الصف</th>
                    <th style="width:220px !important;text-align: center">اسم الطالب</th>
                    <th style="text-align:center;width:50px;">م</th>
                </tr>

<?php
if ($absents):
    $i = 1;
    ?>
                        
                        <?php foreach ($absents as $key => $value) { ?>
                            <tr style="border:none !important;">
                                <td align="center" style="padding:4px;margin:0px;border:1px solid black;"><?php echo $value['abscount']; ?></td>
                                <td align="right" style="padding:4px;margin:0px;border:1px solid black;font-size:14px;">
                                    <?php foreach(array_slice($students_seminars_absents[$value['sid']]['days'], 0, 1) as $absent_date=>$absent_seems){?>
                                    <?php print implode(', ', $absent_seems);?>
                                    <?php } ?>
                                </td>
                                <td align="right" style="padding:4px;margin:0px;border:1px solid black;"><?php echo $value['class_name']; ?></td>
                                <td align="right" style="padding:4px;width:220px !important;margin:0px;border:1px solid black;"><?php echo $value['student_name']; ?></td>
                                <td align="center" style="padding:4px;margin:0px;"><?php echo str_pad($i++, 2, '0', STR_PAD_LEFT) ?></td>
                            </tr>
                        <?php } ?>
    

<?php else: ?>

                    <tr><td colspan="5">No absent list found.</td></tr>

                <?php endif; ?>

            </tbody>
        </table>
        <div class="form-group" style="text-align: left; margin-bottom:0px!important;">
            <h5><strong>مدير المدرسة</strong></h5>
        </div>
    </div>
    <div id="reportprintdiv3">
        <?php
    $i = 1;
    ?>
        <?php foreach ($absents as $key => $value) { ?>
        
        
            <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-4 pull-left">
                        <h4 style="text-align: right;padding-top:10px;display:block;margin-left:30px;">
                            سجل متابعة الطلاب
                        </h4>
                    </div>
                    <div class="col-lg-8 pull-right" style="text-align: right;">
                        <img src="<?= base_url('uploads/report_logo/report_logo_right.png') ?>" width="50" height="60" style="page-break-inside: avoid;display:inline-block;vertical-align:top;margin-right:100px;">
                        <h4 style="display: inline-block;text-align: center;">وزارة التربية<br/><?php print $current_school['detail'];?><br><?php print $current_school['school_name'];?></h4>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 20px;">
                <div class="form-group" style="text-align: center; margin-bottom:0px !important;">
                    <h5> 
                        <strong style="font-size:16px;"><u> أسماء الطلاب الغائبين في الفترة من </u></strong>
                        <?php
                        if (!empty($filtercond)) {
                            $isPresent = false;


                            if (isset($filtercond['start_date']) && !empty($filtercond['start_date'])) {
                                if ($isPresent) {
                                    //echo "<strong><u>"." إلى ".date('d/m/Y',strtotime($filtercond['start_date']))."</u></strong>";
                                } else {
                                    //echo "<strong><u>".date('d/m/Y',strtotime($filtercond['start_date']))."</u></strong>";

                                    echo "<strong><u>" . date('d/m/Y', strtotime($filtercond['start_date'])) . " إلى </u></strong>";
                                }
                                $isPresent = true;
                            }

                            if (isset($filtercond['end_date']) && !empty($filtercond['end_date'])) {
                                if ($isPresent) {
                                    //echo "<strong><u>"." إلى ".date('d/m/Y',strtotime($filtercond['end_date']))."</u></strong>";
                                    echo "<strong><u>" . "" . date('d/m/Y', strtotime($filtercond['end_date'])) . " </u></strong>";
                                } else {
                                    echo "<strong><u>" . date('d/m/Y', strtotime($filtercond['end_date'])) . "</u></strong>";
                                }
                                $isPresent = true;
                            }


                            if (!$isPresent) {
                                echo "<strong><u>" . date('d/m/Y') . "</u></strong>";
                            }
                        } else {
                            echo "<strong><u>" . date('d/m/Y') . "</u></strong>";
                        }
                        ?></h5>
                </div>
            </div>
        
            <table class="table table-bordered table-hover" style="table-layout: fixed;" id="absent_table" row="<?= ($absents) ? 1 : 0 ?>">
                <tbody>
                    <tr style="border:none !important;">
                        <td align="center" style="padding:4px;margin:0px;border:1px solid black;"><?php echo $value['abscount']; ?></td>
                        <td align="right" style="padding:4px;margin:0px;border:1px solid black;">الإجمالي</td>
                        <td align="right" style="padding:4px;margin:0px;border:1px solid black;"><?php echo $value['class_name']; ?></td>
                        <td align="right" style="padding:4px;margin:0px;border:1px solid black;">الصف</td>
                        <td align="right" style="width:220px !important;padding:4px;margin:0px;border:1px solid black;"><?php echo $value['student_name']; ?></td>
                        <td align="right" style="padding:4px;margin:0px;border:1px solid black;">اسم الطالب</td>
                    </tr>
                    <?php $counter = 1;?>
                    <tr style="border:none !important;">
                        <td style="border:none !important;text-align:right;padding:0px;margin:0px;" align="right" colspan="4">الحصة</td>
                        <td align="right" colspan="2" style="border:none !important;padding:0px;margin:0px;">التاريخ</td>
                    </tr>
                    <?php foreach($students_seminars_absents[$value['sid']]['days'] as $absent_date=>$absent_seems){?>
                        <tr style="border:none !important;">
                            <td style="border:none !important;padding:0px;margin:0px;font-size:14px;" align="right" colspan="4"><?php print implode(', ', $absent_seems);?></td>
                            <td style="border:none !important;padding:0px;margin:0px;" colspan="2" align="right"><?php print $absent_date;?></td>
                        </tr>
                        <?php if (isset($notes[$value['sid']][$absent_date]) && $notes[$value['sid']][$absent_date]){?>
                            <tr style="border:none !important;">
                                <td colspan="6" style="border:none !important;padding:0px;margin:0px;" align="right">
                                    <?php foreach ($notes[$value['sid']][$absent_date] as $note){?>
                                        <p>- <?php print $note['note'];?></p>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                        <?php $counter++;?>
                    <?php } ?>
                </tbody>
            </table>
            <div class="form-group" style="text-align: left; margin-bottom:0px!important;">
                <h5><strong>مدير المدرسة</strong></h5>
            </div>
            <div class="page-break"></div>
        <?php } ?>
    </div>
</div>

<!-- end -->

<!-- /#page-wrapper -->
<script>
    $(document).ready(function () {
        $(".printreport").on('click', printsection);
    });
    $(document).ready(function () {
        $(".printreport2").on('click', printsection2);
    });
    $(document).ready(function () {
        $(".printreport3").on('click', printsection3);
    });
    
    function checkHideOption(){
        if ($('input[name="start_date"]').val()==$('input[name="end_date"]').val()){
            $('.hide5-option').removeClass('hide');
        }else{
            $('.hide5-option').addClass('hide');
        }
    }
    
    $('.datepicker').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd'
    });
    $(".resetbutton").click(function () {
        $.each($(".frmfld"), function (i, item) {
            $(item).val('');
        });
    });
    var baseurl = "<?php echo base_url(); ?>";
    function getCourseStudents(element){
        $.each($('select[name="studentid"] option'), function(index, item){
            if (parseInt($(item).val())>0){
                $(item).remove();
            }
        });
        if ($(element).val() && $(element).val().length==1){
            $('select[name="studentid"]').parent().removeClass('hide');
            $.ajax({
                    url: baseurl+'ajax/getCourseStudents/'+$(element).val(),
                    type: 'POST',
                    success : function(response){
                        $.each(response, function(index, item){
                            $('select[name="studentid"]').append($("<option></option>")
                                           .attr("value",item.sid)
                                           .text(item.name)); 
                        });
                    }
            });
        }else{
            $('select[name="studentid"]').parent().addClass('hide');
        }
    }
    /*
     $(".printreport").on('click',function(){
     printsection();
     });
     */
    function printsection_old() {
        var row = $("#absent_table").attr('row');
        if (row > 0) {
            // print function need to implementing
            var printContents = document.getElementById('absent_table').innerHTML;
            //console.log(printContents);
            var originalContents = document.body.innerHTML;
            var Cbody = "<html lang='ar'><head><title>Absent Report</title><link href='<?= base_url('assets/css/bootstrap.min.css') ?>' rel='stylesheet'><link href='<?= base_url('assets/css/style.css') ?>?v=7' rel='stylesheet'><link href='<?= base_url('assets/css/sb-admin-2.css') ?>' rel='stylesheet'></head><body><div id='wrapper'><div id='page-wrapper'><div class='row'><div class='col-lg-12'><div class='table-responsive'><table class='table table-bordered table-hover'>" + printContents + "</table></div></div></div></div></div></body></html>";
            //console.log(Cbody);
            document.body.innerHTML = Cbody;
            window.print();
            document.body.innerHTML = originalContents;
        }
    }
    function printsection() {
        var row = $("#absent_table").attr('row');
        if (row > 0) {
            var mywindow = window.open('', 'Absent List');
            var printContents = $('#reportprintdiv').html();
            //console.log(printContents);
            var Cbody = "<html lang='ar'><head><title>Absent Report</title><link href='<?= base_url('assets/css/bootstrap.min.css') ?>' rel='stylesheet'><link href='<?= base_url('assets/css/style.css') ?>?v=7' rel='stylesheet'><link href='<?= base_url('assets/css/sb-admin-2.css') ?>' rel='stylesheet'><link href='<?= base_url('assets/font-awesome-4.1.0/css/font-awesome.min.css') ?>' rel='stylesheet'><style rel='stylesheet'> table tr td { } h4,h5{margin-top:0px!important; margin-bottom: 5px!important;}@media print {.page-break {page-break-after: always;}}</style></head><body><div id='wrapper'><div id='page-wrapper' style='margin:0px !important'><div class='row'><div class='col-lg-12' style='float:none'>";

            mywindow.document.write(Cbody);
            mywindow.document.write(printContents);
            mywindow.document.write("</div></div></div></div></body></html>");
            setTimeout(function () {
                mywindow.print();
                mywindow.close();
            }, 1000);
        }
    }
    function printsection2() {
        var row = $("#absent_table").attr('row');
        if (row > 0) {
            var mywindow = window.open('', 'Absent List');
            var printContents = $('#reportprintdiv2').html();
            //console.log(printContents);
            var Cbody = "<html lang='ar'><head><title>Absent Report</title><link href='<?= base_url('assets/css/bootstrap.min.css') ?>' rel='stylesheet'><link href='<?= base_url('assets/css/style.css') ?>?v=7' rel='stylesheet'><link href='<?= base_url('assets/css/sb-admin-2.css') ?>' rel='stylesheet'><link href='<?= base_url('assets/font-awesome-4.1.0/css/font-awesome.min.css') ?>' rel='stylesheet'><style rel='stylesheet'> table tr td { } h4,h5{margin-top:0px!important; margin-bottom: 5px!important;}@media print {.page-break {page-break-after: always;}}</style></head><body><div id='wrapper'><div id='page-wrapper' style='margin:0px !important'><div class='row'><div class='col-lg-12' style='float:none'><div class='table-responsive'>";

            mywindow.document.write(Cbody);
            mywindow.document.write(printContents);
            mywindow.document.write("</div></div></div></div></div></body></html>");
            setTimeout(function () {
                mywindow.print();
                mywindow.close();
            }, 1000);
        }
    }
    function printsection3() {
        var row = $("#absent_table").attr('row');
        if (row > 0) {
            var mywindow = window.open('', 'Absent List');
            var printContents = $('#reportprintdiv3').html();
            //console.log(printContents);
            var Cbody = "<html lang='ar'><head><title>Absent Report</title><link href='<?= base_url('assets/css/bootstrap.min.css') ?>' rel='stylesheet'><link href='<?= base_url('assets/css/style.css') ?>?v=7' rel='stylesheet'><link href='<?= base_url('assets/css/sb-admin-2.css') ?>' rel='stylesheet'><link href='<?= base_url('assets/font-awesome-4.1.0/css/font-awesome.min.css') ?>' rel='stylesheet'><style rel='stylesheet'> table tr td { } h4,h5{margin-top:0px!important; margin-bottom: 5px!important;}@media print {.page-break {page-break-after: always;}}</style></head><body><div id='wrapper'><div id='page-wrapper' style='margin:0px !important'><div class='row'><div class='col-lg-12' style='float:none'><div class='table-responsive'>";

            mywindow.document.write(Cbody);
            mywindow.document.write(printContents);
            mywindow.document.write("</div></div></div></div></div></body></html>");
            setTimeout(function () {
                mywindow.print();
                mywindow.close();
            }, 1000);
        }
    }
    /*
     $(".printreport").click(function(){
     printsection();
     });*/

</script>