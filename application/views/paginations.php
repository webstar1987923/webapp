<div class="row clearfix">
    <div class="col-lg-6">
        <?php echo $pagination ?>
    </div>
    <div class="col-lg-6">
        <nav class="pull-right pagination">
            <span>Showing <strong><?php echo $start; ?></strong> to <strong><?php echo $end; ?></strong> results of <strong><?php echo $total; ?></strong></span>
        </nav>
    </div>
</div>