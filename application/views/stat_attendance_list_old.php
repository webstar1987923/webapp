<div class="right_col">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Statistical Attendance</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <!-- /.row -->

	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body blocking">
					<div class="form-group form-inline col-lg-9">
						<form action="" method="post" class="col-lg-9">
							<input type="hidden" name="action" value="1">
							<input type="text" class="form-control datepicker" name="start_date" placeholder="Choose Date" value="<?=$start_date?>" >
							<button type="submit" class="btn btn-default">Filter</button> 
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<?php 
		if(isset($action) && $action==1){
			?>
			
		<div class="row">

			<div class="col-lg-12">

				<div class="panel panel-default">

					<div class="panel-heading">List of Classes</div>

					<!-- /.panel-heading -->

					<div class="panel-body blocking">
							<div id="list-load">
								<div class="row" style="margin-bottom:10px;">
									<div class="col-lg-8"></div>
									<div class="col-lg-4">
										<button type="button" class="btn btn-primary printreport">Print</button>
										<!--<a href="<?=base_url('lists/statecsv/'.strtotime($start_date))?>" class="btn btn-general" style="margin-left:15px;">Excel Export</a>-->
									</div>
								</div>
								<div class="form-group">
									<h5><strong><u>
										<?php 
											if(!empty($start_date)){
												echo "Statistical daily absences Date ".date("d/m/Y",strtotime($start_date));
											}
										?>
									</u></strong></h5>
								</div>
								<div class="table-responsive">

									<table class="table table-striped table-bordered table-hover" id="absent_table_s" row="1">

										<thead>

											<tr>
												<th>Class Code</th>

												<th>Class Name</th>

												<th>Seminar</th>

												<th>Attendance</th>
											</tr>

										</thead>

										<tbody>

											<?php if (isset($classes) && !empty($classes)):
												//print_r($classes);
												foreach ($classes as $class):

													?>

													<tr class="odd gradeX">

														<td><?php echo $class['code'];?></td>

														<td><?php echo $class['name']; ?></td>

														<td><?php echo "Sem-".$class['semno']; ?></td>

														<td><?php echo $class['total_attendance'];?></td>

													</tr>

		<?php endforeach; ?>

	<?php else: ?>

												<tr class="odd gradeX" >

													<td colspan="4">No class found.</td>

												</tr>

	<?php endif; ?>

										</tbody>

									</table>

								</div>

								<!-- /.table-responsive -->

							</div>

					</div>

					<!-- /.panel-body -->

				</div>

				<!-- /.panel -->

			</div>

			<!-- /.col-lg-12 -->

		</div>

			<?php
		}
	?>
	
    <!-- /.row -->

</div>
<div style="display:none;">
	<div id="reportprintdiv">
		<div class="row">
			<div class="col-lg-12">
				<div class="col-lg-6 pull-left" style="display: block;text-align: center;">
					<img src="<?=base_url('uploads/report_logo/report_logo_left.png')?>" width="100" height="120" style=" page-break-inside: avoid; page-break-after: avoid;">
					<br/>
					<span style="display: block;">
						<b><?=date('Y/m/d')?> : تاريخ</b>
					</span>
				</div>
				<div class="col-lg-6 pull-right" style="text-align: center;display: block;">
					<img src="<?=base_url('uploads/report_logo/report_logo_right.png')?>" width="50" height="60" style="page-break-inside: avoid;">
					<h4 style="display: block;"><b>وزارة التربية والتعليم <br/>منطقة الفروانية التعليمية</b></h4>
					<h5 style="display: block;"><b>الثانوية عبداللطيف ثنيان الغانم</b></h5>
				</div>
			</div>
		</div>
		<div class="form-group" style="text-align: right;">
			<h5><strong><u>
				<?php 
					if(!empty($start_date)){
						echo "الإحصائي اليومي الغياب التسجيل ".date("d/m/Y",strtotime($start_date));
					}
				?>
			</u></strong></h5>
		</div>
		<table class="table table-striped table-bordered table-hover" id="absent_table" row="1">
			<thead>
				<tr>
					<td align="right">الحضور</td>
					<td align="right">الحصة </td>
					<td align="right">اسم الفئة</td>
					<td align="right">رمز الفئة</td>
				</tr>
			</thead>
			<tbody>
				<?php 
				if (isset($classes) && !empty($classes)):
					foreach ($classes as $class):
				?>
				<tr class="odd gradeX">

					<td align="right"><?php echo $class['total_attendance'];?></td>

					<td align="right"><?php echo " الحصة ". $class['semno']; ?></td>
					<td align="right"><?php echo $class['name']; ?></td>

					<td align="right"><?php echo $class['code'];?></td>
				</tr>
				<?php endforeach; ?>

				<?php else: ?>

				<tr class="odd gradeX" >
					<td colspan="4">No class found.</td>
				</tr>
				<?php endif; ?>
			</tbody>
		</table>
	</div>
</div>
<script>
$(document).ready(function(){
	$(".printreport").on('click',printsection);
});

$('.datepicker').datepicker({
    autoclose: true,
	format:'yyyy-mm-dd'
});
function printsection_old(){
	var row = $("#absent_table").attr('row');
	if(row>0){
		// print function need to implementing
		var printContents = document.getElementById('absent_table').innerHTML;
		//console.log(printContents);
		var originalContents = document.body.innerHTML;
		var Cbody = "<html><head><title>Absent Report</title><link href='<?=base_url('assets/css/bootstrap.min.css')?>' rel='stylesheet'><link href='<?=base_url('assets/css/style.css')?>?v=7' rel='stylesheet'><link href='<?=base_url('assets/css/sb-admin-2.css')?>' rel='stylesheet'></head><body><div id='wrapper'><div id='page-wrapper'><div class='row'><div class='col-lg-12'><div class='table-responsive'><table class='table table-bordered table-hover'>"+printContents+"</table></div></div></div></div></div></body></html>";
		//console.log(Cbody);
		document.body.innerHTML = Cbody;
		window.print();
		document.body.innerHTML = originalContents;
	}
}
function printsection(){
	var row = $("#absent_table").attr('row');
	if(row>0){
		var mywindow = window.open('','Statistical Report');
		//var printContents = document.getElementById('absent_table').innerHTML;
		var printContents = $('#reportprintdiv').html();
		
		var Cbody = "<html><head><title>Absent Report</title><link href='<?=base_url('assets/css/bootstrap.min.css')?>' rel='stylesheet'><link href='<?=base_url('assets/css/style.css')?>?v=7' rel='stylesheet'><link href='<?=base_url('assets/css/sb-admin-2.css')?>' rel='stylesheet'><link href='<?=base_url('assets/font-awesome-4.1.0/css/font-awesome.min.css')?>' rel='stylesheet'></head><body><div id='wrapper'><div id='page-wrapper'><div class='row'><div class='col-lg-12'><div class='table-responsive'><table class='table table-bordered table-hover'>";
		//+printContents+"</table></div></div></div></div></div></body></html>";
		
        mywindow.document.write(Cbody);
        mywindow.document.writeln(printContents);
        mywindow.document.write("</table></div></div></div></div></div></body></html>");
		
		setTimeout(function(){
			mywindow.print();
			mywindow.document.close();
			mywindow.close();
		},1000);
		
	}
}
</script>


