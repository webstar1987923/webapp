<!-- Page Content -->
<?php // print_r($course); ?>
<div class="right_col">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-6">
                    <h1 class="page-header"> Add News</h1>
                </div>
                <div class="col-lg-6 full-height">
<!--                    <div class="pull-right" >
                        <?php 
                        $lid = (isset($lang_id)) ? $lang_id : 0;
                        if($langs &&  $action == 2): ?>
                            <?php foreach($langs as $lang): ?>
                                <a href="<?php echo '?lang_id='.$lang['ID']; ?>" class="btn btn-circle <?php echo ($lid && $lang['ID'] == $lid) ? 'active' : ''; ?>" ><?php echo $lang['code']; ?></a>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>-->
                </div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <form role="form" action="" method="post" enctype="multipart/form-data">
        <?php $response = $this->session->flashdata('response'); 
        if(!empty($response) || isset($code)):
            $class = (!empty($response)) ? $response['class'] :  $code['class'];
            $msg   = (!empty($response)) ? $response['msg']  : $code['msg'];
        ?>
        <div class="alert alert-<?php echo $class ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
        <?php endif; ?>
        <div class="row">

            <div class="col-lg-6">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        News informations
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>News Title : </label>
                                    <input class="form-control" name="news[title]" placeholder="Enter news title" value="<?php if(isset($news['title']) ) echo $news['title'];  ?>" >
                                </div>
                                <div class="form-group">
                                    <label>News Content : </label>                                   
                                    <textarea class="form-control" name="news[content]" placeholder="Enter news content" ><?php if(isset($news['content']) ) echo $news['content'];  ?></textarea>
                                </div>   
                                             
                               <?php if(isset($new_lang_varient)  && !$new_lang_varient ): ?>
                                    <?php if(isset($news['pic']) && $news['pic']): ?>
                                        <div class="form-group">
                                            <label>Pic </label>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="thumbnail">
                                                        <img src="<?php echo ($news['pic']) ? base_url('uploads/' . $news['pic']) : ''; ?>" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <div class="form-group">
                                        <label>Picture: </label>
                                        <input type="file" name="file" class="filestyle">
                                        <p class="help-block">Allowed Type : JGP,PNG. Allowed Size : 1000 KB</p>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          
            <div class="col-lg-6">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        Actions
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
<!--                                    <input type="hidden" name="plan[lang_id]" value="<?php echo (isset($lang_id)) ? $lang_id : $active_lang['ID']; ?>" >
                                    <?php if(isset($lang_id)): ?>
                                        <input type="hidden" name="plan[parent]" value="<?php echo (isset($parent)) ? $parent : 0; ?>" >
                                    <?php endif; ?>
                                    <?php if(isset($new_lang_varient)  && !$new_lang_varient ): ?>
                                        <input type="hidden" name="lang_varient" value="1" >
                                        
                                    <?php endif; ?>
                                        -->
                                     <input type="hidden" name="news[school_id]" value="<?php echo $this->session->userdata('school_id'); ?>" >
                                    <input type="hidden" name="action" value="<?php echo $action; ?>" >
                                    <input  type="submit" class="btn btn-primary pull-right" value="Save">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- /#page-wrapper -->