<!-- Page Content -->
<div class="right_col">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-6">
                    <h1 class="page-header">Assign Ads</h1>
                </div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <?php
    $response = $this->session->flashdata('response');
    if (!empty($response) || isset($code)):
        $class = (!empty($response)) ? $response['class'] : $code['class'];
        $msg = (!empty($response)) ? $response['msg'] : $code['msg'];
        ?>
        <div class="alert alert-<?php echo $class ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
    <?php endif; ?>
    <div class="row">
        <form action="" method="post">
            <div class="col-lg-6">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        Left Ad ( 500x270 )
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>The ads selected media : </label>
                                    <!--<button id="media" class="btn btn-primary">Select media</button>-->
                                </div>
                                <div class="form-group">
                                    <div class="container-fluid">
                                        <div class="add-media media-select-list" id="selected-media-left">
                                            <?php if ($action == 'save'): ?>
                                                <?php if (isset($ads['left']) && $ads['left']['medias']): ?>
                                                    <?php foreach ($ads['left']['medias'] as $key => $file) { ?>
                                                        <div class="col-lg-2 media-item selected" data-id="<?php echo $file['ID']; ?>">
                                                            <a class="delete-atached-media" data-id="<?php echo $file['ID']; ?>"  data-attached="<?php echo $ads['left']['ID'] ?>">
                                                                <i class="fa fa-times-circle-o fa-2x"></i>
                                                            </a>
                                                            <div class="thumbnail">
                                                                <img src="<?php echo $file['url']; ?>" />
                                                            </div>
                                                            <input type="checkbox" value="<?php echo $file['ID']; ?>" checked="checked" name="ads[left][media][]" />
                                                            <input type="hidden" value="<?php echo $ads['left']['ID']; ?>" checked="checked" name="ads[left][ID]" />
                                                        </div>
                                                    <?php } ?>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Ad media : </label>
                                    <!-- Large modal -->
                                    <button type="button" class="btn btn-primary"  data-adder="#selected-media-left" data-toggle="modal" data-target=".bs-example-modal-lg">Select Media</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        Right Ad ( 500x270 )
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>The ads selected media : </label>
                                    <!--<button id="media" class="btn btn-primary">Select media</button>-->
                                </div>
                                <div class="form-group">
                                    <div class="container-fluid">
                                        <div class="add-media media-select-list" id="selected-media-right">
                                            <?php if ($action == 'save'): ?>
                                                <?php if (isset($ads['right']) && $ads['right']['medias']): ?>
                                                    <?php foreach ($ads['right']['medias'] as $key => $file) { ?>
                                                        <div class="col-lg-2 media-item selected" data-id="<?php echo $file['ID']; ?>">
                                                            <a class="delete-atached-media" data-id="<?php echo $file['ID']; ?>"  data-attached="<?php echo $ads['right']['ID'] ?>">
                                                                <i class="fa fa-times-circle-o fa-2x"></i>
                                                            </a>
                                                            <div class="thumbnail">
                                                                <img src="<?php echo $file['url']; ?>" />
                                                            </div>
                                                            <input type="checkbox" value="<?php echo $file['ID']; ?>" checked="checked" name="ads[right][media][]" />
                                                            <input type="hidden" value="<?php echo $ads['right']['ID']; ?>" checked="checked" name="ads[right][ID]" />
                                                        </div>
                                                    <?php } ?>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Ad media : </label>
                                    <!-- Large modal -->
                                    <button type="button" class="btn btn-primary"  data-adder="#selected-media-right" data-toggle="modal" data-target=".bs-example-modal-lg">Select Media</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        Bottom Slider ( 500x270 )
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>The ads selected media : </label>
                                    <!--<button id="media" class="btn btn-primary">Select media</button>-->
                                </div>
                                <div class="form-group">
                                    <div class="container-fluid">
                                        <div class="add-media media-select-list" id="selected-media-bottom">
                                            <?php if ($action == 'save'): ?>
                                                <?php if (isset($ads['bottom']) && $ads['bottom']['medias']): ?>
                                                    <?php foreach ($ads['bottom']['medias'] as $key => $file) { ?>
                                                        <div class="col-lg-2 media-item selected sortable" data-id="<?php echo $file['ID']; ?>">
                                                            <a class="delete-atached-media" data-id="<?php echo $file['ID']; ?>"  data-attached="<?php echo $ads['bottom']['ID'] ?>">
                                                                <i class="fa fa-times-circle-o fa-2x"></i>
                                                            </a>
                                                            <div class="thumbnail">
                                                                <img src="<?php echo $file['url']; ?>" />
                                                            </div>
                                                            <input type="checkbox" value="<?php echo $file['ID']; ?>" checked="checked" name="ads[bottom][media][]" />
                                                            <input type="hidden" value="<?php echo $ads['bottom']['ID']; ?>" checked="checked" name="ads[bottom][ID]" />
                                                        </div>
                                                    <?php } ?>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Ad media : </label>
                                    <!-- Large modal -->
                                    <button type="button" data-adder="#selected-media-bottom" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">Select Media</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-green">
                    <div class="panel-heading">
                        Actions
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <input type="hidden" name="action" value="<?php echo $action; ?>" >
                                    <input  type="submit" class="btn btn-primary pull-right" value="Save">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form> 
    </div>
</div>
<?php $this->load->view('media-selector') ?>
<!-- /#page-wrapper -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">

    jQuery(function ($) {
        $('#selected-media-bottom').sortable({
            placeholder: "ui-state-highlight",
            helper:'clone',
            stop: function(event, ui) {
                var sorted = {};
                $("#selected-media-bottom div.sortable").each(function(index, item){
                    sorted[$(item).attr('data-id')] = index;
                });
                $.ajax({
                    method: "POST",
                    url: "<?php echo base_url('ajax/adssort'); ?>",
                    data: {data: sorted}
                });
            }
        });
        
        $('body').on('image.selected', function (e, data, adder) {
            console.log('event', data);
            console.log('adder', adder);

            var guid = '#selected-media';
            if (adder) {
                guid = adder;
            }
            var add_type = 'static';
            if(adder.indexOf('left') >= 0){
                add_type = 'left';
            }else if(adder.indexOf('right') >= 0){
                add_type = 'right';
            }else if(adder.indexOf('bottom') >= 0){
                add_type = 'bottom';
            }
            console.log('add_type', add_type);
            
            console.log('add_type 1', adder.indexOf('left'));
            console.log('add_type 2', adder.indexOf('right'));
            console.log('add_type 3', adder.indexOf('bottom'));
            
            var html = '';
            if (data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    html += '<div class="col-lg-2 media-item">' +
                            '<a class="delete-choosed-media" data-id="' + data[i]._id + '">' +
                            '<i class="fa fa-times-circle-o fa-2x"></i></a>' +
                            '<div class="thumbnail">' +
                            '<img src="' + data[i].href + '" />' +
                            '</div>' +
                            '<input type="hidden" value="' + data[i]._id + '" name="ads['+add_type+'][media][]" />' +
                            '</div>';
                }
            }

            $(guid).append(html);


//            var html = '<div class="col-lg-2 media-item">'+
//                    '<div class="thumbnail">'+
//                                        '<img src="" />'+
//                                    '</div>'+
//                                    '<input type="checkbox" value="" name="ads[media][]" />'+
//                                '</div>';
        });
    });
</script>