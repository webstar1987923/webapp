<div class="row">
        <div class="col-lg-12">
            <div class="col-lg-4 pull-left">
                <h4 style="text-align: right;padding-top:10px;display:block;margin-left:30px;">
                    سجل متابعة الطلاب
                </h4>
            </div>
            <div class="col-lg-8 pull-right" style="text-align: right;">
                <img src="<?= base_url('uploads/report_logo/report_logo_right.png') ?>" width="50" height="60" style="page-break-inside: avoid;display:inline-block;vertical-align:top;margin-right:100px;">
                <h4 style="display: inline-block;text-align: center;">وزارة التربية<br/><?php print $current_school['detail'];?><br><?php print $current_school['school_name'];?></h4>
            </div>
        </div>
    </div>

<?php foreach($suspend as $lastsuspend){
    $dates = unserialize($lastsuspend['list_days']);
    foreach($dates as $lastdate){}
    break;
}?>
<div class="row" style="margin-top:10px;">
    <div class="col-lg-12 text-center" style='margin-bottom:0px!important;'>
    <h5>
        <strong style="font-size: 18px;">
            <u>
                قرار فصل سلوكي للطالب
            </u>
        </strong>
    </h5>
    </div>
</div>
<br>
<p style="text-align: right;font-size:18px;">السيد المحترم – ولي امر الطالب/&nbsp <?php print '<strong><u>'.$student_name.'</u></strong>';?>    &nbsp&nbsp&nbspالصف/&nbsp <?php print '<strong><u>'.$course_name.'</u></strong>';?></p>
<p style="text-align: right;font-size:18px;">نحيطكم علماً بأن إدارة المدرسة قررت فصله كالتالي</p>
<p style="text-align: right;font-size:18px;">عدد أيام الفصل  <?php print '<strong><u>'.$lastsuspend['amount_days'].'</u></strong>';?>  يوماً &nbsp&nbsp&nbspمن تاريخ  <?php print '<strong><u>'.date('Y-m-d', strtotime($lastsuspend['start_date'])).'</u></strong>';?>  &nbsp&nbsp&nbspإلى تاريخ  <?php print '<strong><u>'.date('Y-m-d', strtotime($lastdate)).'</u></strong>';?></p>
<p style="text-align: right;font-size:18px;">
    وذلك بسبب:<strong><u> الخروج عن النظام العام للمدرسة</u></strong>
    </p>
<div class="row" style="margin:5px 0px;">
    <div class="col-lg-12" style="font-size:18px;width:100%;text-align: right;margin-bottom:20px;margin-left:20px;">
        __________ وعليه ترجو إدارة المدرسة من ولي أمر الطالب الحضور يوم __________ الموافق
    </div>
</div>
<div class="row" style="margin:5px 0px;">
    <div class="col-lg-12 pull-left" style="font-size:18px;text-align: left;margin-bottom:20px;margin-left:20px;">
    في تمام الساعة _________ وذلك لاتخاذ اللازم
    </div>
</div>

<?php if (!empty($suspend)){?>
        <table class="table table-striped table-bordered table-hover">
            <tr>
                <th style="text-align:center;font-size:18px;font-weight:bold;" colspan="5">
                 كشف بأيام الفصل
                </th>
            </tr>
            <tr class="gradeX odd" >
                <th style="text-align:center;font-size:18px;font-weight:normal;" >
                    ملاحظات
                </th>
                <th style="text-align:center;font-size:18px;font-weight:normal;width:20%" >
                    إلى تاريخ
                </th>
                <th style="text-align:center;font-size:18px;font-weight:normal;width:20%" >
                    من تاريخ
                </th>
                <th style="text-align:center;font-size:18px;font-weight:normal;width:8%" >
                    المدة
                </th>
                <th style="text-align:center;font-size:18px;font-weight:normal;width:8%" >
                    م
                </th>
            </tr>
            <?php $counter = 1;?>
            <?php foreach($suspend as $item){?>
                <?php $dates = unserialize($item['list_days']);?>
                <tr class="gradeX odd" >
                    <td align="center" >

                    </td>
                    <td align="center" style="width:20%" >
                        <?php print date('Y-m-d', strtotime($item['start_date']));?>
                    </td>
                    <td align="center" style="width:20%" >
                        <?php 
                            foreach($dates as $date){}
                            print $date;
                        ?>
                    </td>
                    <td align="center" style="width:8%">
                        <?php print $item['amount_days'];?>
                    </td>
                    <td align="center" style="width:8%">
                        <?php print $counter;?>
                    </td>
                </tr>
                <?php $counter++;?>
            <?php } ?>
        </table>
<?php } ?>
<div class="form-group" style="text-align: left; margin-bottom:0px!important;">
    <h5><strong>مدير المدرسة</strong></h5>
</div>