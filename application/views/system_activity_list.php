<div class="right_col">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Users Activaties List</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <!-- /.row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body blocking">
                    <div class="form-group form-inline col-lg-9">
                        <form action="" method="post" class="col-lg-9">
                            <input type="hidden" name="action" value="1">
                            <input type="text" class="form-control datepicker" name="start_date" placeholder="Choose Date" value="<?= $start_date ?>" >
                            <button type="submit" class="btn btn-default">Filter</button> 
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    if (isset($action) && $action == 1) {
        ?>

        <div class="row">

            <div class="col-lg-12">

                <div class="panel panel-default">

                    <div class="panel-heading">List of Users</div>

                    <!-- /.panel-heading -->

                    <div class="panel-body blocking">

                        <?php //$this->load->view('search-bar', array('plchldr' => "Search by name or email or student id")); ?>

                        <form action="<?php echo base_url('ajax/delete/student'); ?>" class="ajax-form" method="POST" >

                            <div class="courses-choose">

                                <?php if ($courses): ?>

                                    <h4>Courses</h4>

                                    <ul class="list-inline">

                                        <?php foreach ($courses as $course): ?>

                                            <li><input type="radio" class="choose_course" name="course_id" value="<?php echo $course['cid']; ?>"  ><?php echo $course['code']; ?></li>

                                        <?php endforeach; ?>

                                    </ul>

                                <?php endif; ?>

                            </div>

                            <div id="list-load">

                                <div class="table-responsive">

                                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">

                                        <thead>

                                            <tr>

                                                <th>UserID</th>

                                                <th>Name</th>

                                                <th>Type</th>
                                                <th>Create Date</th>
                                                <th>Last Update</th>
                                                <th>Activities</th>

                                            </tr>

                                        </thead>

                                        <tbody>

                                            <?php if (isset($users) && !empty($users)): ?>

                                                <?php
                                                foreach ($users as $user):
                                                    ?>

                                                    <tr class="odd gradeX">

                                                        <td><?php echo $user['username'] ?></td>

                                                        <td><?php echo $user['first_name'] . " " . $user['last_name']; ?></td>
                                                        <td><?php
                                        if ($user['user_type'] == 3) {
                                            echo "Moderator";
                                        } elseif ($user['user_type'] == 1) {
                                            echo "Admin";
                                        } else {
                                            echo "Teacher";
                                        };
                                                    ?></td>
                                                        <td><?php
                                                            echo date('Y/m/d', strtotime($user['created']));
                                                            ?>
                                                            <?php
                                                            echo date('h:i a', strtotime($user['created']));
                                                            ?>
                                                        </td>
                                                        <td><?php
                                                            echo date('Y/m/d', strtotime($user['update_time']));
                                                            ?>
            <?php
            echo date('h:i a', strtotime($user['update_time']));
            ?>
                                                        </td>





                                                        <td><?php
                                                if ($user['updated_by'] > 0) {
                                                    echo "Modify absent of class " . $user['course_name'] . " at Sem-" . $user['semno'];
                                                } elseif ($user['entered_by'] > 0) {
                                                    if ($user['attendance']) {
                                                        echo "Add attendance for class " . $user['course_name'] . " as sem-" . $user['semno'];
                                                    } else {
                                                        echo "Add absent for class " . $user['course_name'] . " as sem-" . $user['semno'];
                                                    }
                                                } else {
                                                    echo "Add note to student for class " . $user['course_name'];
                                                }
            ?></td>

                                                    </tr>

                                                        <?php endforeach; ?>

                                                    <?php else: ?>

                                                <tr class="odd gradeX" >

                                                    <td colspan="8">No users found.</td>

                                                </tr>

                                            <?php endif; ?>

                                        </tbody>

                                    </table>

                                </div>

                                <!-- /.table-responsive -->

    <?php $this->load->view('paginations'); ?>

                            </div>

                        </form>

                    </div>

                    <!-- /.panel-body -->

                </div>

                <!-- /.panel -->

            </div>

            <!-- /.col-lg-12 -->

        </div>

    <?php
}
?>

    <!-- /.row -->

</div>
<script>
    $('.datepicker').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd'
    });
</script>


