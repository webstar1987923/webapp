<div class="row">
    <div class="col-lg-12">
        <div class="col-lg-4 pull-left">
            <h4 style="text-align: right;padding-top:10px;display:block;margin-left:30px;">
                سجل متابعة الطلاب
            </h4>
        </div>
        <div class="col-lg-8 pull-right" style="text-align: right;">
            <img src="<?= base_url('uploads/report_logo/report_logo_right.png') ?>" width="50" height="60" style="page-break-inside: avoid;display:inline-block;vertical-align:top;margin-right:100px;">
            <h4 style="display: inline-block;text-align: center;">وزارة التربية<br/><?php print $current_school['detail'];?><br><?php print $current_school['school_name'];?></h4>
        </div>
    </div>
</div>

<?php foreach($medical as $lastmedical){
    $dates = unserialize($lastmedical['list_days']);
    foreach($dates as $lastdate){}
    break;
}?>
<div class="row" style="margin-top:10px;">
    <div class="col-lg-12 text-center" style='margin-bottom:0px!important;'>
    <h5>
        <strong style="font-size: 18px;">
            <u>
                نموذج علاج طبي
            </u>
        </strong>
    </h5>
    </div>
</div>
<br>

<div class="row" style="margin:0px;">
    <div class="col-lg-12 pull-right" style="font-size:16px;text-align: left;">
      اسم الطالب/&nbsp;&nbsp;&nbsp;<?php print '<strong><u>'.$student_name.'</u></strong>';?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;الصف/&nbsp&nbsp&nbsp<?php print '<strong><u>'.$course_name.'</u></strong>';?>
    </div>
</div>
<div class="row" style="margin:0px;">
    <div class="col-lg-12 pull-right" style="font-size:16px;text-align: right;">
      الرقم المدني/&nbsp;&nbsp;&nbsp;<?php print '<strong><u>'.$studentid.'</u></strong>';?>&nbsp;&nbsp; المركز الصحي المحول إليه
    </div>
</div>
<div class="row" style="margin:0px;">
    <div class="col-lg-12 pull-left" style="font-size:16px;text-align: left;margin-left: 20px;">
        ‫يعتمد‬
    </div>
</div>
<div class="row" style="margin:0px;">
    <div class="col-lg-12 pull-left" style="font-size:16px;text-align: left;">
        مدير المدرسة
    </div>
</div>
<div class="row" style="margin:0px;">
    <div class="col-lg-12" style="font-size:16px;text-align: center;">
        <strong><u>لاستعمال الطبيب المعالج</u></strong>
    </div>
</div>
<br>
<div class="row" style="margin:0px;">
    <div class="col-lg-3 pull-right" style="font-size:16px;text-align: right;">
      العيادة التي عولج بها المريض
    </div>
    <div class="col-lg-3 pull-right" style="font-size:16px;text-align: right;">
        _____________
    </div>
    <div class="col-lg-3 pull-right" style="font-size:16px;text-align: right;">
        تاريخ المراجعة
    </div>
    <div class="col-lg-3 pull-right" style="font-size:16px;text-align: right;">
        ____/___/____
    </div>
</div>
<div class="row" style="margin:0px;">
    <div class="col-lg-12 pull-right" style="font-size:16px;text-align: right;">
<span style="margin-right:10px;">_________________________________________________</span>التشخيص وتوصيات الطبيب المعالج  
    </div>
</div>
<hr style="width:96%">
<hr style="width:96%">
<div class="row" style="margin:0px;">
    <div class="col-lg-12 pull-left" style="font-size:16px;text-align: left;margin-left:50px;">
        توقيع الطبيب وختمه
    </div>
</div>
<div class="row" style="margin:0px;">
    <div class="col-lg-12 pull-right" style="font-size:16px;text-align: right;margin-right: 15px;">
        <strong><u>‫‫ملاحظة هامة:</u></strong>
    </div>
</div>
<div class="row" style="margin:0px;">
    <div class="col-lg-12 pull-right" style="font-size:16px;text-align: right;">
    على الطالب أو ولي أمره تسليم الطبية إلى إدارة المدرسة بعد اعتمادها
    </div>
</div>
<div class="row" style="margin:0px;">
    <div class="col-lg-12 pull-right" style="font-size:16px;text-align: right;">
    من المرفق الصحي خلال يومين من الغياب وخلال فترة الاختبارات يجب تصديقها من الصحة المدرسية
    </div>
</div>


<?php if (!empty($medical)){?>
        <table class="table table-striped table-bordered table-hover">
            <tr>
                <th style="text-align:center;font-size:18px;font-weight:bold;" colspan="5">
                 الطبيات السابقة
                </th>
            </tr>
            <tr class="gradeX odd" >
                <th style="text-align:center;font-size:18px;font-weight:normal;" >
                    مالحظات
                </th>
                <th style="text-align:center;font-size:18px;font-weight:normal;width:20%" >
                    إلى تاريخ
                </th>
                <th style="text-align:center;font-size:18px;font-weight:normal;width:20%" >
                    من تاريخ
                </th>
                <th style="text-align:center;font-size:18px;font-weight:normal;width:8%" >
                    المدة
                </th>
                <th style="text-align:center;font-size:18px;font-weight:normal;width:8%" >
                    م
                </th>
            </tr>
            <?php $counter = 1;?>
            <?php foreach($medical as $item){?>
                <?php $dates = unserialize($item['list_days']);?>
                <tr class="gradeX odd" >
                    <td align="center" >

                    </td>
                    <td align="center" style="width:20%" >
                        <?php print date('Y-m-d', strtotime($item['start_date']));?>
                    </td>
                    <td align="center" style="width:20%" >
                        <?php 
                            foreach($dates as $date){}
                            print $date;
                        ?>
                    </td>
                    <td align="center" style="width:8%">
                        <?php print $item['amount_days'];?>
                    </td>
                    <td align="center" style="width:8%">
                        <?php print $counter;?>
                    </td>
                </tr>
                <?php $counter++;?>
            <?php } ?>
        </table>
<?php } ?>