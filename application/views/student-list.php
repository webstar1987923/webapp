<div class="right_col">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Students List</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    List of Students
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body blocking">
                    <?php $this->load->view('search-bar', array('plchldr' => "Search by name or email or student id")); ?>
                    <form action="<?php echo base_url('ajax/delete/student'); ?>" class="ajax-form" method="POST" >
                        <div class="courses-choose" style="display:none">
                            <?php if ($courses): ?>
                            <h4>Courses</h4>
                            <ul class="list-inline">
                                    <?php foreach ($courses as $course): ?>
                                        <li class="radio">
                                            <input type="radio" class="choose_course flat" autocomplete="off" name="course_id" value="<?php echo $course['cid']; ?>"  ><?php  echo $course['name']; ?>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                        </div>
                        <div id="list-load">
                            <div class="table-responsive">
                                <table class="table table-striped jambo_table bulk_action" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th class="column-title">Student ID</th>
                                            <th class="column-title">Pic</th>
                                            <th class="column-title">Name</th>
                                            <th class="column-title">Class</th>
                                            <th class="column-title">Email</th>
                                            <th class="column-title">Parent Name</th>
                                            <th class="column-title">Created</th>
                                            <th class="column-title">Action</th>
                                            <th class="column-title"><input type="checkbox" class="flat" autocomplete="off" name="all" id="all" ></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (isset($students) && !empty($students)): ?>
                                            <?php
                                            //                                    $students = array_reverse($students);
                                            foreach ($students as $student):
                                                ?>
                                                <tr class="odd gradeX">
                                                    <td><?php echo $student['student_id'] ?></td>
                                                    <td align="center">
                                                        <?php
                                                        $student['pic'] = ($student['pic']) ? $student['pic'] : 'nopic.png';
                                                        ?>
                                                        <img src="<?php echo base_url('uploads/' . $student['pic']); ?>" height="35" width="40" />
                                                        <?php ?>
                                                    </td>
                                                    <td><?php echo $student['name']; ?></td>
                                                    <td><?php echo $student['code']; ?></td>
                                                    <td><?php echo $student['email_id'] ?></td>
                                                    <td><?php echo $student['parent'] ?></td>
                                                    <!--<td><?php echo ($student['status'] == 1) ? 'active' : 'inactive'; ?></td>-->
            <!--                                        <td><?php
                                                    foreach ($langs as $lang) {
                                                        echo '<a href="' . base_url('manage/student/' . $student['sid'] . '?lang_id=' . $lang['ID']) . '" title="' . $lang['code'] . '">' . $lang['code'] . '</a>&nbsp;&nbsp;';
                                                    }
                                                    ?></td>-->
                                                    <td><?php echo date('d/m/Y H:i a', strtotime($student['datetime'])); ?></td>
                                                    <td>
                                                        <a href="<?php echo base_url('manage/student/' . $student['sid']); ?>" title="Edit" class="edit" ><i class="fa fa-pencil-square-o fa-fw"></i></a>
                                                        <a href="<?php echo base_url('assign/course/' . $student['sid']); ?>" class="assigns" title="Assign course"><i class="fa fa-tasks fa-fw"></i></a>
                                                        <a href="<?php echo base_url('assign/parents/' . $student['sid']); ?>" class="assigns" title="Assign parents"><i class="fa fa-user fa-fw"></i></a>
                                                        <a href="<?php echo base_url('delete/student/' . $student['sid']); ?>" title="Delete" class="delete"><i class="fa fa-times fa-fw"></i></a>
                                                    </td>
                                                    <td><input type="checkbox" autocomplete="off" name="delete_ids[]" value="<?php echo $student['sid'] ?>" class="select_me flat" ></td>
                                                </tr>
    <?php endforeach; ?>
<?php else: ?>
                                            <tr class="odd gradeX" >
                                                <td colspan="7">No students found.</td>
                                            </tr>
<?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
<?php $this->load->view('paginations'); ?>
                        </div>
                    </form>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>

