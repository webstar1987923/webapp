<!-- Page Content -->
<?php // print_r($student); ?>
<div class="right_col">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Settings</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <form role="form" action="" method="post" enctype="multipart/form-data">
        <?php $response = $this->session->flashdata('response'); 
        if(!empty($response) || isset($code)):
            $class = (!empty($response)) ? $response['class'] :  $code['class'];
            $msg   = (!empty($response)) ? $response['msg']  : $code['msg'];
        ?>
        <div class="alert alert-<?php echo $class ?> alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $msg; ?>
        </div>
        <?php endif; ?>
        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Primary settings
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <?php if(isset($new_lang_varient)  && !$new_lang_varient ): ?>
                                <div class="form-group">
                                    <label>Email ID : </label>
                                    <input class="form-control"  <?php if(isset($update_lang_varient) && $update_lang_varient) echo 'disabled=""'; ?> type="email" name="student[email_id]" placeholder="Enter email" value="<?php if(isset($student['email_id']) ) echo $student['email_id'];  ?>" >
                                </div>
                                <?php endif; ?>
                                <div class="form-group">
                                    <label>First Name: </label>
                                    <input class="form-control" type="text" name="student[first_name]" placeholder="Enter first name" value="<?php if(isset($student['first_name']) ) echo $student['first_name'];  ?>">
                                </div>
                                <div class="form-group">
                                    <label>Last Name : </label>
                                    <input class="form-control" type="text" name="student[last_name]" placeholder="Enter last name" value="<?php if(isset($student['last_name']) ) echo $student['last_name'];  ?>">
                                </div>
                                <div class="form-group">
                                    <label>Phone no : </label>
                                    <input class="form-control" type="tel" name="student[phone_no]" placeholder="Enter phone no" value="<?php if(isset($student['phone_no']) ) echo $student['phone_no'];  ?>">
                                </div>
                                <?php if(isset($new_lang_varient)  && !$new_lang_varient ): ?>
                                <div class="form-group">
                                    <label>Status :</label>
                                    <div class="form-inline">
                                        <label class="radio-inline">
                                            <input type="radio" name="student[status]" id="active-status" value="1" <?php if(isset($student['status'])){ if($student['status'] == 1) echo 'checked=""';}else{ echo 'checked=""'; } ?> >Active
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="student[status]" id="inactive-status" <?php if(isset($student['status'])){ if($student['status'] == 0) echo 'checked=""';} ?> value="0" >In active
                                        </label>
                                    </div>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Secondary settings
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Address : </label>
                                    <textarea class="form-control" name="student[meta][address]" placeholder="Enter address"><?php if(isset($meta) ) echo $meta['address'];  ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Personal Description : </label>
                                    <textarea class="form-control" name="student[meta][pinfo]" placeholder="Enter personal info"><?php if(isset($meta) ) echo $meta['pinfo'];  ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Actions
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <input type="hidden" name="student[lang_id]" value="<?php if(isset($student['lang_id']))echo $student['lang_id']; else echo (isset($lang_id)) ? $lang_id : $active_lang['ID']; ?>" >
                                    <?php if(isset($lang_id)): ?>
                                        <input type="hidden" name="student[parent]" value="<?php echo (isset($parent)) ? $parent : 0; ?>" >
                                    <?php endif; ?>
                                    <?php if(isset($new_lang_varient)  && !$new_lang_varient ): ?>
                                        <input type="hidden" name="lang_varient" value="1" >
                                    <?php endif; ?>
                                    <input type="hidden" name="action" value="<?php echo $action; ?>" >
                                    <input  type="submit" class="btn btn-primary pull-right">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- /#page-wrapper -->