<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Reset password</h3>
                </div>
                <div class="panel-body">
                    <form role="form" method="post" action="<?php echo base_url('login/resetPass'); ?>">
                        <div id="message">
                            <?php
                                $msg = $this->session->flashdata('msg');
                                if($msg){
                            ?>
                                <p> <?php echo $msg; ?></p>
                            <?php } ?>
                        </div>
                        <fieldset>
                            <div class="form-group">
                                <input class="form-control" placeholder="Password" name="password" type="password" autofocus>
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Confirm Password" name="cnfpassword" type="password" value="">
                            </div>
                            <!-- Change this to a button or input when using this as a form -->
                            <input type="hidden" name="resetpass"  value="1">
                            <input type="hidden" name="token"  value="<?php echo $token; ?>">
                            <input type="submit" class="btn btn-lg btn-success btn-block" value="Reset">
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>