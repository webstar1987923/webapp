<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Uploader
 *
 * @author Suchandan
 */
class S_upload {
    
    public $data;
    public $error;
    public $pic_name;
    
    public $height = 100;
    public $width = 100;
    public $create_thumb = false;
    public $maintain_ratio = true;
    
    public $config = array();
    private $CI;
    public function __construct($config = array()) {
        
        $this->CI = & get_instance();
        
        $this->config['pic_name']    = isset($config['pic_name'])    ? $config['pic_name']    : '';
        $this->config['name']        = isset($config['name'])        ? $config['name']        : 'file';
        $this->config['upload_path'] = isset($config['upload_path']) ? $config['upload_path'] : './uploads/';
        $this->config['max_size']    = isset($config['max_size'])    ? $config['max_size']    : '100';
        $this->config['resize']      = isset($config['resize'])      ? $config['resize']      : true;
        $this->config['data']        = isset($config['data'])        ? $config['data']        : array();
        
        $this->config = array_merge($this->config,$config);
        
//        print_r($this->config );
        
        $this->height                = isset($config['height'])       ? $config['height']            : 100;
        $this->width                 = isset($config['width'])        ? $config['width']             : 100;
        $this->create_thumb          = isset($config['create_thumb']) ? $config['create_thumb']      : false;
        $this->maintain_ratio        = isset($config['maintain_ratio']) ? $config['maintain_ratio']  : true;

        $this->do_upload($this->config);
        
        return $this;
    }
    private function get_content_type($filename) {

        $mime_types = array(

            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',

            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',

            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',

            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',

            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',

            // ms office
            'doc' => 'application/msword',
            'docx'=> 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',

            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );

        $ext = strtolower(array_pop(explode('.',$filename)));
        if (array_key_exists($ext, $mime_types)) {
            return $mime_types[$ext];
        }
        elseif (function_exists('finfo_open')) {
            
            $finfo = finfo_open(FILEINFO_MIME);
            $mimetype = finfo_file($finfo, $filename);
            finfo_close($finfo);
            
            return $mimetype;
        }
        else {
            return 'application/octet-stream';
        }
    }
    public function do_upload($data = array()){
        
        if(empty($data)){
            $data = $this->config;
        }
        extract($data);
        
        if(!isset($_FILES[$name])){
            return false;
        }
        
        $data['size'] = $_FILES[$name]['size'];
        
        $config['upload_path']   = $upload_path;
        $config['allowed_types'] = '*';
        $config['max_size']      = $max_size;
        $config['file_name']     = $pic_name;
        
        $ext = end(explode(".", $_FILES[$name]['name']));
        $file_name = reset(explode(".", $_FILES[$name]['name']));
        $type = $this->get_content_type($_FILES[$name]['tmp_name']);
        if($pic_name == ''){
            $pic_name = time();
            $pic_name = $pic_name.'_'.$file_name.'.'.$ext;
        }else{
            $pic_name = $pic_name.'.'.$ext;
        }
        
        
            
        $data['type'] = $type;
        $config['file_name'] = $pic_name;
//        $config['max_width']     = '1024';
//        $config['max_height']    = '768';
        $this->CI->load->library('upload', $config);

               
        if ( ! $this->CI->upload->do_upload($name))
        {
            $this->setOutputData('upload_error', $this->CI->upload->display_errors());

            return false;
        }else{

            if(isset($unlink) && $unlink){
                $unlink_path = $upload_path.$file_name;
                @unlink($unlink_path);
                
            }

            $path = $upload_path.$pic_name;

            if($resize){
                $this->resizeImage($path);

            }
            
            $this->pic_name = $pic_name;
            $this->data     = $data;
        }
    }
    public function resizeImage($path = '') {
        if($path == ''){
            return false;
        }
        $config['image_library'] = 'gd2';
        $config['source_image']	= $path;
        $config['create_thumb'] = $this->create_thumb;
        $config['maintain_ratio'] = $this->maintain_ratio;
        $config['width']	= $this->width;
        $config['height']	= $this->height;

        $this->CI->load->library('image_lib', $config); 

        if ( ! $this->CI->image_lib->resize()){
            
            $this->setOutputData('resize_error',  $this->CI->image_lib->display_errors());
        }
    }
    public function setOutputData($name,$data) {
        $this->error[$name] = $data;
    }
    public function uploadExtended($pic_name = '',$name = 'file') {
        
//       $handle = $this->load->library('uploadlib',$_FILES[$name]);
        require_once APPPATH.'libraries/uploadlib.php';
        $dir_dest = "./uploads/";
        
        $handle = new Uploadlib($_FILES[$name]);
        $pic_name = time().$pic_name;
        if ($handle->uploaded) {
            // we now process the image a second time, with some other settings
            $handle->image_resize            = true;
            $handle->image_ratio_y           = true;
            $handle->image_x                 = 100;
//            $handle->image_y                 = 100;
//            $handle->image_reflection_height = '25%';
            $handle->image_contrast          = 50;
            $handle->file_new_name_body      =  $pic_name;
            echo "process start.";
            $handle->Process($dir_dest);

            // we check if everything went OK
            if ($handle->processed) {
                return $handle->file_dst_name;
            } else {
                $this->setOutputData('image_process_error', $handle->error);
                return false;
            }
            $handle-> Clean();
        }else{
            $this->setOutputData('image_upload_error', $handle->error);
            return false;
        }
    }

}
