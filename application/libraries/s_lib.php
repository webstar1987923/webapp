<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of staticLib
 *
 * @author Suchandan
 */
class S_lib {

    private $CI;

    public function __construct() {
        $this->CI = & get_instance();
        if ($this->CI->input->get('activate')) {
            $lid = $this->CI->input->get('activate');
            $this->CI->session->set_userdata('lang_id', $lid);
            $frag = explode('/', $_SERVER['REQUEST_URI']);
            $slug = $frag[3];
            if (strpos($slug, '?') > 0) {
                $slug = substr($slug, 0, strpos($slug, '?'));
            }
            $slug = ($slug) ? '/' . $slug : '';
            $uri = base_url($frag[2] . $slug);
            redirect($uri);
        }
    }

    public function loadView($page = 'dashboard', $data = array()) {
//        $lang_val = $this->getLangs();
//        if($this->CI->input->get('lang_id')){
//            $lang_id = $this->CI->input->get('lang_id');
//            $lang_val['lang_id'] = $lang_id;
//        }
//        $data = array_merge($data, $lang_val);
        $controller = $this->CI->router->fetch_class();
        $action = $this->CI->router->fetch_method();
        $user_type = $this->CI->session->userdata('user_type');
        $menu = array();
        $menu_items = $this->getMenuItems();
        foreach($menu_items as $item){
            
            if (in_array($user_type, $item['access'])){
                if ($item['parent']*1){
                    if ($item['controller']==$controller && $item['slug']==$action){
                        $menu[$item['parent']]['active'] = 1;
                    }
                    $menu[$item['parent']]['childs'][] = $item;
                }else{
                    if (isset($menu[$item['id']])){
                        foreach($item as $field=>$value){
                            $menu[$item['id']][$field] = $value;
                        }
                    }else{
                        $menu[$item['id']] = $item;
                    }
                }
            }
        }
        $data['menu_items'] = $menu;
        $data['menu_items_admin'] = $this->getAdminmenu();
        $this->CI->load->view('header', $data);
        $this->CI->load->view('menu', $data);
        $this->CI->load->view($page, $data);
        $this->CI->load->view('footer', $data);
    }

    function checkMethodAccess($user_type, $controller, $action){
        $ret = true;
        $menu = $this->getMenuItems();
//        print $user_type." : " . $controller."===>>>>".$action;die;
        $publicurl = false;
        foreach($menu as $item){
//            print $item['controller']." && ".$item['controller']."==".$controller." && ".$item['slug']."==".$action."<hr>";
            if (!empty($item['controller']) && $item['controller']==$controller && $item['slug']==$action){
                if (in_array($user_type, $item['access'])){
                    $publicurl = true;
                }else{
                    $ret = false;
                }
            }
        }
        if (!$publicurl){
            $menu = $this->getAdminmenu();
//        if ($user_type==0){
            foreach($menu as $item){
//                print $item['controller']." && ".$item['controller']."==".$controller." && ".$item['slug']."==".$action."<hr>";
                if (!empty($item['controller']) && $item['controller']==$controller && $item['slug']==$action){
                    if (!in_array($user_type, $item['access'])){
                        $ret = false;
                    }
                }
            }
        }
//        }
//        die;
        if (empty($ret)){
            redirect(base_url('login'));
        }
    }
    
    private function getLangs() {
        $lang_actives = $this->CI->s_model->getData('lang', array('active' => 1));
        $langs = $this->CI->s_model->getDatas('lang', array('active' => 0));
        $data['langs'] = $langs;
        $data['active_lang'] = $lang_actives;
        return $data;
    }

    public function setVarientData($data, $id, $lid, $type = 'student') {
        if (empty($data) || empty($id) || empty($lid)) {
//            echo 'no pm';
//            exit;
            return $data;
        }

        switch ($type) {
            case 'student':
                $ex_data = $this->CI->s_model->getData('students', array('parent' => $id, 'lang_id' => $lid));
                if ($ex_data) {
                    return array(false, $ex_data);
                }
                $parent_data = $this->CI->s_model->getData('students', array('sid' => $id));
                if ($parent_data) {
                    $data['parent'] = $parent_data['sid'];
                    $data['email_id'] = $parent_data['email_id'];
                }
                $data['lang_id'] = $lid;
                break;
            case 'course':
                $ex_data = $this->CI->s_model->getData('courses', array('parent' => $id, 'lang_id' => $lid));
                if ($ex_data) {
                    return array(false, $ex_data);
                }
                $parent_data = $this->CI->s_model->getData('courses', array('cid' => $id));
                if ($parent_data) {
                    $data['parent'] = $parent_data['cid'];
                }
                $data['lang_id'] = $lid;

                break;

            default:
                break;
        }
//        print_r($data);
//
//        exit;
        return array(true, $data);
    }

    public function getDefaultLang() {
        $lang_actives = $this->CI->s_model->getData('lang', array('active' => 1));
        return $lang_actives;
    }

    private function getMenuItems() {

        return array(
            array(
                'id' => 1,
                'parent' => 0,
                'controller' => 'dashboard',
                'slug' => '',
                'access' => array(0, 1, 3, 5, 6, 7),
                'title' => 'Dashboard',
                'icon' => 'fa-dashboard',
                'attr' => array(
                )
            ),
            array(
                'id' => 7,
                'parent' => 0,
                'access' => array(0, 1, 3, 5, 6, 7),
                'title' => 'Reports',
                'icon' => 'fa-gear',
                'attr' => array(
                )
            ),
            array(
                'parent' => 7,
                'controller' => 'lists',
                'slug' => 'statattendance',
                'access' => array(0, 1, 3, 5, 6, 7),
                'title' => 'Statistical Report',
                'icon' => 'fa-list',
                'attr' => array(
                )
            ),
            array(
                'parent' => 7,
                'controller' => 'lists',
                'slug' => 'classabsent',
                'access' => array(0, 1, 3, 5, 6, 7),
                'title' => 'Absent Report',
                'icon' => 'fa-book',
                'attr' => array(
                )
            ),
            array(
                'parent' => 7,
                'controller' => 'lists',
                'slug' => 'medicalreport',
                'access' => array(0, 1, 6),
                'title' => 'Medical Report',
                'icon' => 'fa-gear',
                'attr' => array(
                )
            ),
            array(
                'parent' => 7,
                'controller' => 'lists',
                'slug' => 'studentsuspension',
                'access' => array(0, 1, 3),
                'title' => 'Suspension Report',
                'icon' => 'fa-gear',
                'attr' => array(
                )
            ),
            array(
                'parent' => 7,
                'controller' => 'lists',
                'slug' => 'warningreport',
                'access' => array(0, 1, 3, 5, 6, 7),
                'title' => 'Warning Report',
                'icon' => 'fa-warning',
                'attr' => array(
                )
            ),
            array(
                'id' => 2,
                'parent' => 0,
                'access' => array(0, 1, 5),
                'title' => 'Manage Attendance',
                'icon' => 'fa-gear',
                'attr' => array(
                )
            ),
            array(
                'parent' => 2,
                'controller' => 'lists',
                'slug' => 'manageattendance',
                'access' => array(0, 1, 5),
                'title' => 'Manage Absent',
                'icon' => 'fa-gear',
                'attr' => array(
                )
            ),
            array(
                'parent' => 2,
                'controller' => 'lists',
                'slug' => 'attendancenotes',
                'access' => array(0, 1),
                'title' => 'Manage Notes',
                'icon' => 'fa-gear',
                'attr' => array(
                )
            ),
            array(
                'id' => 9,
                'parent' => 0,
                'access' => array(0, 1),
                'title' => 'Add Data',
                'icon' => 'fa-user',
                'attr' => array(
                ),
            ),
            array(
                'parent' => 9,
                'controller' => 'manage',
                'slug' => 'user',
                'access' => array(0, 1),
                'title' => 'User Registration',
                'icon' => 'fa-user',
                'attr' => array(
                ),
            ),
            array(
                'parent' => 9,
                'controller' => 'manage',
                'slug' => 'student',
                'access' => array(0, 1),
                'title' => 'Student Registration',
                'attr' => array(
                ),
                'icon' => 'fa-graduation-cap'
            ),
            array(
                'parent' => 18,
                'controller' => 'lists',
                'slug' => 'news',
                'access' => array(0, 1),
                'icon' => 'fa-book',
                'title' => 'News list',
                'attr' => array(
                ),
            ),
            array(
                'parent' => 18,
                'controller' => 'lists',
                'slug' => 'user',
                'access' => array(0, 1),
                'icon' => 'fa-users',
                'title' => 'User list',
                'attr' => array(
                ),
            ),
            array(
                'parent' => 9,
                'controller' => 'manage',
                'slug' => 'course',
                'access' => array(0, 1),
                'icon' => 'fa-book',
                'title' => 'Class Registration',
                'attr' => array(
                ),
            ),
            array(
                'parent' => 9,
                'controller' => 'manage',
                'slug' => 'coursegroup',
                'access' => array(0, 1),
                'icon' => 'fa-plus',
                'title' => 'Create Class Group',
                'attr' => array(
                ),
            ),
            array(
                'parent' => 9,
                'controller' => 'manage',
                'slug' => 'news',
                'access' => array(0, 1),
                'icon' => 'fa-book',
                'title' => 'Add News',
                'attr' => array(
                ),
            ),            
            array(
                'parent' => 18,
                'controller' => 'lists',
                'slug' => 'course',
                'access' => array(0, 1),
                'icon' => 'fa-book',
                'title' => 'Class list',
                'attr' => array(
                ),
            ),
            array(
                'parent' => 18,
                'controller' => 'lists',
                'slug' => 'groups',
                'access' => array(0, 1),
                'icon' => 'fa-book',
                'title' => 'Class Group list',
                'attr' => array(
                ),
            ),
            array(
                'id' => 18,
                'parent' => 0,
                'access' => array(0, 1),
                'title' => 'Manage Data',
                'icon' => 'fa-graduation-cap',
                'attr' => array(
                ),
            ),
            array(
                'parent' => 18,
                'controller' => 'lists',
                'slug' => 'student',
                'access' => array(0, 1),
                'title' => 'Student list',
                'icon' => 'fa-graduation-cap',
                'attr' => array(
                ),
            ),
            array(
                'parent' => 18,
                'controller' => 'lists',
                'slug' => 'teachers',
                'access' => array(0, 1),
                'title' => 'Teacher list',
                'icon' => 'fa-users',
                'attr' => array(
                )
            ),
            array(
                'parent' => 18,
                'controller' => 'lists',
                'slug' => 'modarators',
                'access' => array(0, 1),
                'title' => 'Moderator list',
                'icon' => 'fa-users',
                'attr' => array(
                )
            ),
            array(
                'parent' => 18,
                'controller' => 'lists',
                'slug' => 'parents',
                'access' => array(0, 1),
                'title' => 'Parent list',
                'icon' => 'fa-users',
                'attr' => array(
                )
            ),
            array(
                'parent' => 18,
                'controller' => 'lists',
                'slug' => 'admin',
                'access' => array(0, 1),
                'title' => 'Admin list',
                'icon' => 'fa-users',
                'attr' => array(
                )
            ),
            array(
                'id' => 23,
                'parent' => 0,
                'access' => array(0, 1),
                'title' => 'Imports',
                'icon' => 'fa-upload',
                'attr' => array(
                )
            ),
            array(
                'parent' => 23,
                'controller' => 'import',
                'slug' => 'student',
                'access' => array(0, 1),
                'title' => 'Import Students',
                'icon' => 'fa-upload',
                'attr' => array(
                )
            ),
            array(
                'parent' => 23,
                'controller' => 'import',
                'slug' => 'teacher',
                'access' => array(0, 1),
                'title' => 'Import Teachers',
                'icon' => 'fa-upload',
                'attr' => array(
                )
            ),
            array(
                'parent' => 23,
                'controller' => 'import',
                'slug' => 'parents',
                'access' => array(0, 1),
                'title' => 'Import Parents',
                'icon' => 'fa-upload',
                'attr' => array(
                )
            ),
            array(
                'parent' => 23,
                'controller' => 'import',
                'slug' => 'attendance',
                'access' => array(0, 1),
                'title' => 'Import Attendance',
                'icon' => 'fa-upload',
                'attr' => array(
                )
            ),
            array(
                'id' => 27,
                'parent' => 0,
                'controller' => 'lock',
                'slug' => 'callender',
                'title' => 'Calender',
                'access' => array(0, 1),
                'icon' => 'fa-calendar',
                'attr' => array(
                )
            ),
            array(
                'id' => 28,
                'parent' => 0,
                'controller' => 'lists',
                'slug' => 'systemactivity',
                'access' => array(0, 1),
                'title' => 'System Activities',
                'icon' => 'fa-gear',
                'attr' => array(
                )
            ),
        );
    }

    private function getAdminmenu() {

        return array(
            array(
                'controller' => 'dashboard',
                'slug' => '',
                'access' => array(0),
                'title' => 'Dashboard',
                'icon' => 'fa-dashboard',
                'attr' => array(
                )
            ),
            array(
                'controller' => 'lists',
                'slug' => 'news',
                'access' => array(0),
                'icon' => 'fa-book',
                'title' => 'news list',
                'attr' => array(
                ),
            ),
            array(
                'controller' => 'manage',
                'slug' => 'news',
                'access' => array(0),
                'icon' => 'fa-book',
                'title' => 'news Add',
                'attr' => array(
                ),
            ),
            array(
                'controller' => 'lists',
                'slug' => 'plan',
                'access' => array(0),
                'icon' => 'fa-list',
                'title' => 'Plan list',
                'attr' => array(
                ),
            ),
            array(
                'controller' => 'manage',
                'slug' => 'plan',
                'access' => array(0),
                'icon' => 'fa-list',
                'title' => 'Plan Add',
                'attr' => array(
                ),
            ),
            array(
                'controller' => 'media',
                'slug' => 'index',
                'access' => array(0),
                'title' => 'Media',
                'icon' => 'fa-camera',
                'attr' => array(
                )
            ),
            array(
                'controller' => 'admanage',
                'slug' => 'assign',
                'access' => array(0),
                'title' => 'Assign Ads',
                'icon' => ' fa-sliders',
                'attr' => array(
                )
            ),
            array(
                'controller' => 'manage',
                'slug' => 'school',
                'access' => array(0),
                'title' => 'School Registration',
                'attr' => array(
                ),
                'icon' => 'fa-university'
            ),
            array(
                'controller' => 'lists',
                'slug' => 'school',
                'access' => array(0),
                'title' => 'School list',
                'icon' => 'fa-university',
                'attr' => array(
                ),
            ),
            array(
                'controller' => 'manage',
                'slug' => 'admin_school',
                'access' => array(0),
                'title' => 'Admin School',
                'icon' => 'fa-university',
                'attr' => array(
                ),
            ),
        );
    }

    public function checkSpecialCharecter($param) {
        if (empty($param)) {
            return false;
        }
        if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $param)) {
            return true;
        }
        return false;
    }

}
