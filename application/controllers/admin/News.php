<?php if(! defined('BASEPATH')) exit ('No direct script access allowed');

class News extends CI_Controller{

	public function __construct(){

	parent::__construct();
	
	$this->load->model('s_model');	
		
	}
	
	public function index(){
	
	$data['news'] = $this->s_model->newsGet('abs_news');
	$this->load->view('admin/header');
	$this->load->view('admin/newlist', $data);
	$this->load->view('admin/footer');
	
	}
	
	public function delete(){
		if($this->input->post('id')){
		$id = $this->input->post('id');
			$this->s_model->newsDelete('abs_news',$id);
			if($id == true){
			echo true;
			}
			
		}	
	}
	
	public function addEdit(){
		if($this->input->post('id')){
		$id = $this->input->post('id');
		$data = $this->input->post();
		$this->s_model->newsDelete('abs_news',$id);
		}else{
		$this->s_model->newsDelete('abs_news',$data);
		}
	}

	
}