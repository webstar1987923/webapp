<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of delete
 *
 * @author Suchandan
 */
class delete extends My_Controller {

    public function __construct() {
        parent::__construct();
    }
    public function student($id) {
        $this->s_model->deleteRecord('students',array('sid' => $id));
        $this->s_model->deleteRecord('student_courses',array('sid' => $id));
        $this->s_model->deleteRecord('attendance',array('sid' => $id));
        $this->s_model->deleteRecord('metadata',array('meta_type' => 2,'object_id' => $id));
        redirect(base_url('lists/student'));
    }
    public function course($id) {
        $this->s_model->deleteRecord('courses',array('cid' => $id));
        $this->s_model->deleteRecord('teacher_courses',array('cid' => $id));
        $this->s_model->deleteRecord('student_courses',array('cid' => $id));
        $this->s_model->deleteRecord('teacher_seminers',array('cid' => $id));
        $this->s_model->deleteRecord('metadata',array('meta_type' => 3,'object_id' => $id));
        redirect(base_url('lists/course'));
    }
     public function school($id) {
        $this->s_model->deleteRecord('school',array('id' => $id));
        $this->s_model->deleteRecord('users',array('school_id' => $id));
        $this->s_model->deleteRecord('students',array('school_id' => $id));
        $this->s_model->deleteRecord('courses',array('school_id' => $id));       
        redirect(base_url('lists/school'));
    }
    public function news($id) {
        $this->s_model->deleteRecord('news',array('id' => $id));        
        redirect(base_url('lists/news'));
    }
    
    public function plan($id) {
        $this->s_model->deleteRecord('plans',array('id' => $id));        
        redirect(base_url('lists/plan'));
    }
    
    public function user($id) {
        $this->s_model->deleteRecord('users',array('user_no' => $id));
        redirect(base_url('lists/user'));
    }
    public function teacher($id) {
        $this->s_model->deleteRecord('users',array('user_no' => $id));
        $this->s_model->deleteRecord('teacher_courses',array('user_no' => $id));
        $this->s_model->deleteRecord('teacher_seminers',array('user_no' => $id));
        $this->s_model->deleteRecord('metadata',array('meta_type' => 1,'object_id' => $id));
        redirect(base_url('lists/teacher'));
    }
    public function lang($id) {
        $this->s_model->deleteRecord('lang',array('ID' => $id));
        $this->s_model->deleteRecord('courses',array('lang_id' => $id));
        $this->s_model->deleteRecord('students',array('lang_id' => $id));
        redirect(base_url('lists/lang'));
    }
	
	public function ads($id=0){
		if($id>0){
			$tablename = "slideshow_ads_images";
			$this->s_model->deleteRecord($tablename,array('ID' => $id));
		}
		redirect(base_url('admanage/ads'));
	}
	
	public function group($id=0){
		if($id>0){
			$tablename = "course_groups";
			$this->s_model->deleteRecord($tablename,array('ID' => $id));
		}
		redirect(base_url('lists/groups'));
	}
}
