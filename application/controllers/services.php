<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of services
 *
 * @author Suchandan
 */
class services extends CI_Controller
{

    //Local variables for data holding
    private $response = array();
    private $data = array();
    //local variables for ids
    private $user_no;
    private $session_id;
    private $corse_id;
    private $student_id;
    //Local objects
    private $user;
    private $users;
    private $student;
    private $students;
    private $course;
    private $courses;
    //Distinct variables
    private $show_query = false;
    private $extraValidation = array();
    private $emptyFields = array();
    private $authDone = false;

    public function __construct()
    {
        parent::__construct();

        $this->output->set_header("Access-Control-Allow-Credentials: true");
        $this->output->set_header('Access-Control-Allow-Origin: *');
        $this->output->set_header('Access-Control-Expose-Headers: Access-Control-Allow-Origin');
        $lang_code = ($this->post('lang_code')) ? $this->post('lang_code') : 'en';
        $folder = 'english';
        if ($lang_code == 'arb') {
            $folder = 'arbian';
        }
        $this->lang->load($lang_code, $folder);
        $this->config->set_item('auth', true);
        $this->setVaribles();
    }

    public function index()
    {
        $this->setMessage("Bad request.");
        $this->unsetResponseData(array('success', 'session'));
        $this->output->set_status_header('400');
        $this->response();
    }

    private function post($param)
    {
        return $this->input->post($param);
    }

    private function setVaribles()
    {

        $this->response['success'] = false;
        $this->response['session'] = false;
        $this->response['msg'] = 'false';

        $this->emptyFields = array(
            'session_id',
            'user_no',
        );

        $this->user_no = $this->s_model->user_no = $this->post('user_no');
        $this->session_id = $this->s_model->session_id = $this->post('session_id');
        $this->corse_id = $this->s_model->corse_id = $this->post('corse_id');
        $this->student_id = $this->s_model->student_id = $this->post('student_id');

        $this->show_query = $this->s_model->show_query = $this->post('show_query');

        $this->s_model->data = &$this->data;
//        $this->s_model->data = &$this->data;
    }

    private function response($response = array(), $code = 200)
    {

        if (empty($response)) {
            $response = $this->response;
        } else {
            $response = array_merge($this->response, $response);
        }
        if ($this->post('show') == 1) {
            print_r($response);
        }
        $json_response = json_encode($response);

        $this->output->set_content_type('application/json')->set_status_header("$code")->set_output($json_response);
    }

    private function setSuccess($name = '')
    {
        $this->response['success'] = true;
        $this->response['msg'] = $this->langLine($name);
    }

    private function setData($name = '', $data = array())
    {
        if ($name == '') {
            return false;
        }
        $this->response[$name] = $data;
    }

    private function setMessage($name = '', $lang = false)
    {
        if ($lang) {
            $name = $this->langLine($name);
        }
        $this->response['msg'] = $name;
    }

    private function setLangMessage($name = '')
    {
        $this->setMessage($name, true);
    }

    private function unsetResponseData($names = '')
    {
        if ($names == '') {
            return false;
        }
        if (!is_array($names)) {
            $names = array($names);
        }
        foreach ($names as $name) {
            if (isset($this->response[$name])) {
                unset($this->response[$name]);
            }
        }
        return true;
    }

    private function authinticate($extra_validation = array())
    {

        $auth = $this->config->item('auth');

        if (!$auth && ENVIRONMENT == 'development') {
            $this->response['session'] = true;
            return false;
        }

        if ($this->authDone) {
            return true;
        }

        //Check user exists
        $this->checkUserExists();

        //Check session validation
        $this->checkSession();

        //Check extra validation if needed
        $this->extraValidations($extra_validation);

        $this->authDone = true;

        return true;
    }

    private function setSession($user_no = 0)
    {

        $user_id = ($user_no == 0) ? $this->user_no : $user_no;

        if (!$user_no) {
            return false;
        }

        if (($log = $this->checkIsLoggedIn($user_no)) === false) {
            $session_id = rand(100000000, 400000000);
        } else {
            $session_id = $log['session_id'];
        }
        $this->s_model->set_session($user_id, $session_id);
        $this->setData('session_id', $session_id);
        return $session_id;
    }

    private function clearSession($user_no = '', $session_id = '')
    {

        $data = array();
        $data['user_no'] = ($user_no == '') ? $this->user_no : $user_no;
        $data['session_id'] = ($session_id == '') ? $this->session_id : $session_id;

        $this->s_model->deleteRecord('session', $data);
        return true;
    }

    private function checkIsLoggedIn($user_no = '', $session_id = '')
    {

        $data = array();
        $data['logged_in'] = 0;

        $user_no = ($user_no == '') ? $this->user_no : $user_no;
        $session_id = ($session_id == '') ? $this->session_id : $session_id;

        $data['user_no'] = $user_no;

        $cond = array();

        if ($user_no != '') {
            $cond['user_no'] = $user_no;
        }
        if ($session_id != '') {
            $cond['session_id'] = session_id;
        }

        $result = $this->s_model->getData('session', $cond);
        if (count($result) > 0) {
            $data['logged_in'] = 1;
            //$this->showOutput($data);
            return $result;
        }
        // $this->showOutput($data);
        return false;
    }

    private function extraValidations($extra)
    {
        if (!is_array($this->extraValidation)) {
            $this->extraValidation = array($this->extraValidation);
        }
        $extra = array_merge($this->extraValidation, $extra);
        if (empty($extra)) {
            return false;
        }
        foreach ($extra as $function) {
            if ($function != '') {
                call_user_method($function, $this);
            }
        }
    }

    private function updateTime($user_no = '', $session_id = '')
    {

        $data['user_no'] = ($user_no == '') ? $this->user_no : $user_no;
        $data['session_id'] = ($session_id == '') ? $this->session_id : $session_id;

        $this->s_model->updateRecord('session', $data, array('datetime' => date('Y-m-d H:i:s')));
        return true;
    }

    private function checkSession($user_no = '', $session_id = '')
    {

        $user_no = ($user_no == '') ? $this->user_no : $user_no;
        $session_id = ($session_id == '') ? $this->session_id : $session_id;

        $session_time = $this->getSessionExpireTime();
        $current_time = time();

        $this->response['session'] = true;
        return $session_id;

        $logOut = false;

        if ($user_no == "" || $session_id == "") {
            $this->response['msg'] = $this->langLine('logged_out');
            $logOut = true;
        } else {

            $result = $this->s_model->getData('session', array('user_no' => $user_no, 'session_id' => $session_id));
//            print_r($result);
            $tar = ($current_time - strtotime($result['datetime']));
//                echo $current_time.'<br>';
            //                echo $tar.'<br>';
            //                echo strtotime($result['datetime']).'<br>';

            if (empty($result)) {

                $this->response['msg'] = $this->langLine('logged_out');
                $logOut = true;
            } elseif ($tar >= $session_time) {
                $this->clearSession($user_no, $session_id);
                $this->response['msg'] = $this->langLine('logged_out');
                $logOut = true;
            } else {
                $this->updateTime($user_no, $session_id);
            }
        }
        if ($logOut) {
//            $this->clearSession($user_no);
            $this->_die();
            return false;
        }

        $this->response['session'] = true;
        return $session_id;
    }

    private function getSessionExpireTime()
    {
        return 3600;
//        return $this->config->item('sess_expiration');
    }

    private function langLine($name, $replacer = '', $subject = '')
    {

        if (($line = $this->lang->line($name)) == 'false') {
            return $name;
        } else {
            $subject = ($subject == '') ? '{object}' : $subject;
            $line = str_replace($subject, $replacer, $line);
        }
        return $line;
    }

    private function _die($data = array())
    {
        if (!empty($data)) {
            $this->response = array_merge($this->response, $data);
        }
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Expose-Headers: Access-Control-Allow-Origin');
        header('Content-Type: application/json');
        die(json_encode($this->response));
    }

    private function checkPostEmptyFields($fields = array(), $type = 'post')
    {

        if (empty($fields)) {
            return false;
        }

        foreach ($fields as $field) {
            $val = $this->$type($field);
            if (empty($val)) {
                $this->setMessage("The $field can not be empty.");
                $this->_die();
            }
        }
    }

    public function checkRequiredEmptyFields($fields = array())
    {
        $fields = array_merge($this->emptyFields, $fields);
        $this->checkPostEmptyFields($fields);
    }

    public function dataUnsetter($data = array(), $fields = array())
    {
        if (empty($data) || empty($fields)) {
            return $data;
        }
        foreach ($fields as $field) {
            if (isset($data[$field])) {
                unset($data[$field]);
            }
        }
        return $data;
    }

    //S_controller functions
    //From here the custom functions will come
    public function login()
    {

        $email = $this->post('email_id');
        $password = $this->post('password');
        $device_id = $this->post('device_id');
        $school_id = $this->post('school_id');

        if (empty($email) || empty($password)) {
            $this->setLangMessage('empty_not');
            $this->_die();
        }
        $password = md5($password);
//        $user = $this->s_model->getData('users',array('email_id' => $email,'password' => $password));
        $user = $this->s_model->checkUserExistsApp($email, $password, $school_id);
        if ($user && !in_array($user['user_type'], array(5, 6, 7))) {
            $user['pic'] = ($user['pic']) ? $user['pic'] : 'nopic.png';
            $user['pic'] = base_url('uploads/' . $user['pic']);
            $this->setSession($user['user_no']);
            $this->updateDeviceId($user['user_no'], $user['user_type']);
            $user = $this->dataUnsetter($user, array('password', 'password_hint'));
            unset($this->response['session']);

            if ($user['user_type'] == 4) {
                $child = $this->s_model->getChildrens($user['user_no']);
                if (empty($child)) {
                    $child = false;
                }
                $user['child'] = $child;
            }

            $this->setSuccess();
            $this->setData('details', $user);
        } else {
            $this->setLangMessage('invalid_user');
            $this->_die();
        }
        $this->response();
    }

    public function updateDeviceId($user_no, $user_type)
    {

        $device_id = $this->post('device_id');
        $os_type = $this->post('os_type');
        $registration_id = $this->post('registration_id');
        if (empty($device_id) || empty($os_type)) {
            return false;
        }
        $dataDevice = $this->s_model->getData('devices', array('device_id' => $device_id));
        $data = $this->s_model->getData('devices', array('user_no' => $user_no, 'os_type' => $os_type, 'user_type' => $user_type));
        if ($dataDevice) {
            return false;
        }
        if ($data) {
            if ($data['device_id'] != $device_id) {
                $this->s_model->updateRecord('devices', array('user_no' => $user_no), array('device_id' => $device_id, 'registration_id' => $registration_id));
            }
        } else {
            $this->s_model->insertRecord('devices', array(
                'os_type' => $os_type,
                'device_id' => $device_id,
                'user_type' => $user_type,
                'user_no' => $user_no,
                'registration_id' => $registration_id,
            )
            );
        }
    }

    public function checkUserExists()
    {
        $user_no = $this->post('user_no');

        if (empty($user_no)) {
            $this->setMessage("Empty user no.");
            $this->_die();
        }
        $this->user = $this->s_model->getData('users', array('user_no' => $user_no));
        if (empty($this->user)) {
            $this->setLangMessage('user_not_exists');
            $this->_die();
        }
    }

    public function getCoursesByTeacher($tid)
    {
        $courses = $this->s_model->getTeacherCourses($tid);
        $this->data['courses'] = $courses;
        $this->data['students'] = $this->s_model->getDatas('students', array('status' => 1));
//        if($courses){
        //            foreach($courses as $key => $course){
        //                $courses[$key]['students'] = $this->s_model->getStudentCourses($course['cid']);
        //            }
        //        }
        $this->setSuccess();
        $this->response();
    }

    public function saveAttendance($school_id)
    {

        $this->authinticate();

        $attend_sheet = $this->post('sheet');
        $cid = $this->post('cid');
        $user_no = $this->post('user_no');
        $removal_sheet = $this->post('removal_sheet');
        $date = ($this->input->post('date')) ? $this->input->post('date') : date('Y-m-d');

        if ($this->user['user_type'] != 2) {
            if ($removal_sheet) {
                foreach ($removal_sheet as $rem) {
                    $cnd = array(
                        'sid' => $rem['sid'],
                        'cid' => $cid,
                        'semno' => $rem['sem'],
                        'datetime' => $date,
                    );
                    $this->s_model->deleteRecord('attendance', $cnd);
                    unset($cnd['datetime']);
                    $cnd['date'] = $date;
                    $ex = $this->s_model->getData('editables', $cnd);
                    if (empty($ex)) {
                        $cnd['edited_by'] = $user_no;
                        $this->s_model->insertRecord('editables', $cnd);
                    }
                }
            }
        }
        $absents = $edited = $saved = array(); //$absents = $edited = array();
        if ($attend_sheet) {
            foreach ($attend_sheet as $key => $val) {
                $sem_no = end(explode('-', $key));
                if ($val) {
                    //validate here that the sem inderded or enterd
                    /*$find_cond=array(
                    'cid' => $cid,
                    'semno' => $sem_no,
                    'datetime' => $date
                    );
                    $ex_att_sm = $this->s_model->getData('attendance', $find_cond);
                    if($ex_att_sm){
                    // edited
                    if(!in_array($sem_no,$edited) && !in_array($sem_no,$saved)){
                    $edited[]=$sem_no;
                    }
                    }
                    else{
                    // entered
                    if(!in_array($sem_no,$saved) && !in_array($sem_no,$edited)){
                    $saved[]=$sem_no;
                    }
                    }
                     */
                    foreach ($val as $k => $v) {
                        $sid = end(explode('-', $k));
                        $data['entered_by'] = $user_no;
                        $data['sid'] = $sid;
                        $data['cid'] = $cid;
                        $data['semno'] = $sem_no;
                        $data['attendance'] = $v;
                        if ($v == 0) {
                            $absents[] = $sem_no;
                        }
                        $cond = array(
                            'sid' => $sid,
                            'cid' => $cid,
                            'semno' => $sem_no,
                            'datetime' => $date,
                        );
                        $ex_att = $this->s_model->getData('attendance', $cond);

                        if ($ex_att) {
                            $this->s_model->updateRecord('attendance', $cond, array(
                                'attendance' => $v,
                                'updated_by' => $user_no,
                                'update_time' => date('Y-m-d H:i:s'),
                                'delay_submitted' => 0,
                            ));
                            $cond1 = array(
                                'cid' => $cid,
                                'semno' => $sem_no,
                                'datetime' => $date,
                            );
                            $rows = $this->s_model->getData('attendance', $cond1);
                            foreach ($rows as $key => $row) {
                                $cond2 = array(
                                    'sid' => $row['sid'],
                                    'cid' => $cid,
                                    'semno' => $sem_no,
                                    'datetime' => $date,
                                );
                                $this->s_model->updateRecord('attendance', $cond2, array(
                                    'updated_by' => $user_no,
                                ));
                            }
                        } else {
                            $data['datetime'] = $date;
                            //$saved[]=$sem_no; //$saves['sem-'.$sem_no] = true;
                            if (!in_array($sem_no, $saved) && !in_array($sem_no, $edited)) {
                                //$saved[]=$sem_no;
                            }
                            $data['school_id'] = $school_id;
                            $this->s_model->insertRecord('attendance', $data);
                        }
                        unset($cond['datetime']);
                        $cond['date'] = $date;
                        $this->s_model->deleteRecord('editables', $cond);
                    }
                }
            }
        }
        //attendance
        $edited = $this->s_model->getEditNwAttendances($cid, 0, 1);
        $saveds = $this->s_model->getEditNwAttendances($cid);
        $saved = array();
        if (is_array($saveds) && count($saveds) > 0) {
            foreach ($saveds as $key => $semno) {
                if (!in_array($semno, $edited)) {
                    $saved[] = $semno;
                }
            }
        }

        $last_sem = $this->s_model->getLastSemNo($cid);
        if ($last_sem) {
            $last_sem = $last_sem['max_cem'];
        }
        $this->setSuccess();
        $this->pushtoModarator($absents);
        $this->setData('last_sem', $last_sem);
        $this->setData('edited', $edited);
        $this->setData('saved', $saved);
        $this->setMessage($this->langLine('attendance_saved'));
        $this->response();
    }

    /*
     ** Savve Delay List
     */
    public function saveDelaylist($school_id, $delayRule = 5)
    {

        $this->authinticate();
        $attend_sheet = $this->post('sheet');
        $cid = $this->post('cid');
        $user_no = $this->post('user_no');
        $removal_sheet = $this->post('removal_sheet');
        $date = date('Y-m-d');

        //Set Delay Rule 5 delay = an absent (Static) Later will pass it with calling
        if (empty($delayRule)) {
            $delayRule = 5;
        }

        if ($attend_sheet) {

            foreach ($attend_sheet as $key => $val) {
                $sem_no = end(explode('-', $key));
                if ($val) {

                    foreach ($val as $k => $v) {
                        $sid = end(explode('-', $k));
                        $data['userid'] = $user_no;
                        $data['stuid'] = $sid;
                        $data['delayflag'] = $v;
                        $data['school_id'] = $school_id;
                        $data['submittedby'] = 2;
                        $data['markdate'] = $date;

                        //Count the  UseedForAbsent for 0
                        $delayCount = $this->ManroxCountStuDelay($data);

                        //if equals to X-1
                        // if($delayCount == ($delayRule-1)){
                        if ($delayCount == $delayRule) {
                            $update = $this->UpDate_Delay_Useed($data['stuid']);
                            if ($update) {
                                $data['useedforabsent'] = 1;
                            }
                        }

                        $Insert_Result = $this->s_model->insertDelay('delaylist', $data);
                    }
                }
            }
        }
        if ($Insert_Result) {
            echo json_encode($Insert_Result);
            die();
        } else {
            echo json_encode("Error ");
        }

    }
    public function ManroxTesting()
    {

        $data = file_get_contents("php://input");
        $objdata = json_decode($data);

        //Get data from array
        $stuId = $objdata->stuid;
        $data = array();
        $data['stuid'] = $stuId;
        $delayRule = 5;

        //count delay first
        $delayCount = $this->ManroxCountStuDelay($data);

        if ($delayCount == ($delayRule - 1)) {

            $update = $this->UpDate_Delay_Useed($data['stuid']);
            if ($update) {
                echo 'Set UseedForAbsent for 1';
            }

            die();
        }

    }

    public function ManroxChkAttToday($sid)
    {
        $retval = false;
        $attdonetoday = $this->s_model->chktodayatt($sid);
        if ($attdonetoday >= 1) {

            $retval = true;
        }
        $retval = true;
        echo json_encode($retval);
    }

    public function ManroxModeratorAllowedForEditChk($modid)
    {
        $retval = false;
        $attdonetoday = $this->s_model->chkmodforeditallow($modid);
        if ($attdonetoday >= 1) {

            $retval = true;
        }
        echo json_encode($retval);
    }

    public function ManroxTeacherAllowedForEditChk($teacherid)
    {
        $retval = false;
        $checkPermission = $this->s_model->chkteacherforeditallow($teacherid);

        $time_alloted = ($checkPermission['num_rows'] == 1) ? $checkPermission['alloted_time'] : null;

        $checkAttendaceSubmitted = $this->s_model->checkTodaysAttendanceStatus($teacherid);
        $checkPermission = ($checkPermission['num_rows'] == 1) ? true : false;

        if ($checkAttendaceSubmitted['num_rows'] == 0) {
            $attendanceSubmitted = false;
            $time_diffrence = null;
        }
        if ($checkAttendaceSubmitted['num_rows'] >= 1) {
            $attendanceSubmitted = true;
            $time_diffrence = $checkAttendaceSubmitted['time_diffrence'];
        }
        echo json_encode(array("editPermission" => $checkPermission, "isSubmitted" => $attendanceSubmitted, "time_diffrence" => $time_diffrence, "allotedtime" => $time_alloted));
    }

    public function ManroxTesting2($school_id, $submittedby)
    {

        $this->authinticate();
        $attend_sheet = $this->post('sheet');
        $cid = $this->post('cid');
        $user_no = $this->post('user_no');
        $removal_sheet = $this->post('removal_sheet');
        date_default_timezone_set('Asia/Kuwait');
        $date = date('Y-m-d');

        //Set Delay Rule 5 delay = an absent (Static) Later will pass it with calling

        $getDelayRule = $this->s_model->GetDelayRuleBySchool($school_id);
        $delayRule = $getDelayRule[0]['delay_rule'];
        $delayRule = ($delayRule == 0) ? 5 : $delayRule;

        if ($attend_sheet) {

            foreach ($attend_sheet as $key => $val) {
                $sem_no = end(explode('-', $key));
                if ($val) {

                    foreach ($val as $k => $v) {
                        $sid = end(explode('-', $k));
                        $data['userid'] = $user_no;
                        $data['stuid'] = $sid;
                        $data['delayflag'] = $v;
                        $data['school_id'] = $school_id;
                        $data['submittedby'] = $submittedby;
                        $data['delaydate'] = $date;
                        $data['markdate'] = $date;
                        $data['absentshifteddate'] = $date;
                        $data['useedforabsent'] = 0;
                        //check that request made by admin
                        if ($v == 0 && $submittedby == 2) {
                            continue;
                        }

                        //check that request made by Moderator
                        if ($v == 0 && $submittedby == 1) {

                            $this->s_model->DeleteByStuDelayFlag($sid, $submittedby);
                            $this->s_model->checkForReverseUseed($sid);
                            //update Attendance

                            /// Remove Entries
                            $this->s_model->updateAttendance($sid, 1, array('remove' => true));

                            continue;
                        }

                        ///check if available Entry for student in current date
                        $currentDayCount = $this->s_model->StudentCurrentDayCount($sid);
                        if ($currentDayCount >= 1) {

                            continue;
                        }

                        $Insert_Result = $this->s_model->insertDelay('delaylist', $data);
                        ///Count the  UseedForAbsent for 0
                        $delayCount = $this->ManroxCountStuDelay($data);
                        
                        //if equals to X-1
                        // if($delayCount == ($delayRule-1)){
                        if ($delayCount == $delayRule) {

                            $update = $this->UpDate_Delay_Useed($data['stuid'], $data['absentshifteddate']);
                            if ($update) {
                                $data['useedforabsent'] = 1;
                                // $this->s_model->updateAttendanceAbsentDelay($sid, 0, 1);
                                $modifyData = array(
                                    'cid' => $cid,
                                    'sid' => $sid,
                                    'entered_by' => $user_no,
                                    'course_tid' => 0,
                                    'school_id' => $school_id,
                                    'status' => 1,
                                    'attendance' => 0,
                                    'absentDueToDelay' => 1,
                                    'semno' => $sem_no,
                                    'updated_by' => '',
                                    'delay_submitted' => 1,
                                );
        
                                $this->s_model->updateAttendance($sid, 0, $modifyData);
                                $this->s_model->updateAbsentNote($sid, $cid, '');
                            }
                        }else{
                            $modifyData = array(
                                'cid' => $cid,
                                'sid' => $sid,
                                'entered_by' => $user_no,
                                'course_tid' => 0,
                                'school_id' => $school_id,
                                'status' => 1,
                                'attendance' => 3,
                                'semno' => $sem_no,
                                'updated_by' => '',
                                'delay_submitted' => 1,
                            );
    
                            $this->s_model->updateAttendance($sid, 3, $modifyData);
                        }
                    }
                }
            }
        }
        if ($Insert_Result) {
            echo json_encode($Insert_Result);
            die();
        } else {
            echo json_encode("msg return ");
        }

        // echo json_encode($data);
        //die();
    }

    ///ManroxCountStuDelay
    public function ManroxCountStuDelay($data)
    {

        $result = $this->s_model->delayListUserUpdate('delaylist', $data);
        //  echo  json_encode(array($Insert_Result));
        return $result;
    }
    //Update UseedForAbsent
    public function UpDate_Delay_Useed($stuid, $absentshifteddate)
    {
        $data = array('stuid' => $stuid, 'absentshifteddate' => $absentshifteddate);
        $return = $this->s_model->UpdateUseedForUser('delaylist', $data);
        return $return;
    }

    /*
     ** Savve Delay List
     */
    /*public function saveDelaylist()
    {
    $data = file_get_contents("php://input");
    $objdata = json_decode($data);

    //Get data from array
    $stuId=$objdata->stuid;
    $delay=$objdata->delay;
    $useedforabsent=$objdata->useedforabsent;
    $submittedby=$objdata->submittedby;
    $userid=$objdata->userid;
    /*$post_array=array(

    )
    $Insert_Result = $this->s_model->insertDelay('delaylist', $objdata);

    //echo $stuId.'-'.$delay.'-'.$useedforabsent.'-'.$submittedby.'-'.$userid;

    //$objdata->data //Accessing

    echo json_encode(array($Insert_Result));

    }*/
    public function pushtoModarator($absents = array())
    {

        if (empty($absents)) {
            return false;
        }

        $cid = $this->post('cid');

        $teacher = $this->user['first_name'] . ' ' . $this->user['last_name'];
        $course = $this->s_model->getData('courses', array('cid' => $cid));

//        $Sems = implode(',', $absents);
        $message = "$teacher " . $this->langLine('has_given_absent') . ' ' . $course['name'];

        $moradators = $this->s_model->getDatas('teacher_courses', array('cid' => $cid, 'type' => 3), array('user_no'));
        $registatoin_ids = array();
        if ($moradators) {
            foreach ($moradators as $moradator) {
                $data = $this->s_model->getData('devices', array('user_no' => $moradator['user_no']), array('registration_id'));
                if ($data) {
                    $registatoin_ids[] = $data['registration_id'];
                }
            }
        }
        $message = array('message' => $message);
        if ($registatoin_ids) {
            $result = $this->send_notification($registatoin_ids, $message);
//            $this->setData('push_result',$result);
            //            $this->setData('IDS',$registatoin_ids);
            //            $this->setData('MESSAGE',$message);
        }
    }

    public function testPush()
    {
        $red_id = $this->post('reg_id');
        $msg = $this->post('msg');
        $reg = array($red_id);
        $message = array('message' => $msg);
        print_r($this->send_notification($reg, $message));
    }

    public function send_notification($registatoin_ids, $message = array())
    {

        // Set POST variables
        // $url = 'https://android.googleapis.com/gcm/send';
        $url = 'https://fcm.googleapis.com/fcm/send';

        //$fields = array(
        ////APA91bGfHOCclVYOdjle9UlWLsFLXR4EwY_oQdDL19lUwPUxkH48g4Gudg4jxAjJwTZeuoO2lT_PIpLdY-N5g42iuJrWWUhtDqKqolm3FwSBnD38euUuae4tK3G5j0D3gU13VX0gxreGSfeBndZtEPTeHoA_9bjZ8A
        // 'registration_ids' => $registatoin_ids,
        //// 'registration_ids' => 'APA91bHbExwy4m9VjUFMpaN_gfx4ajcn-5L-thJtY14GWwpLXg8eAuP2p4ebAFkC-9g85tC02qw9SwZ21TYf3adu6IjCytgF9jx7hOUIG0i9dUpAwL5-Wsz4CQILnM49epOaqp6uTEGk',
        //   'data' => $message,
        // );
        $fields = array(
            //APA91bGfHOCclVYOdjle9UlWLsFLXR4EwY_oQdDL19lUwPUxkH48g4Gudg4jxAjJwTZeuoO2lT_PIpLdY-N5g42iuJrWWUhtDqKqolm3FwSBnD38euUuae4tK3G5j0D3gU13VX0gxreGSfeBndZtEPTeHoA_9bjZ8A
            'to' => $registatoin_ids,
            // 'registration_ids' => 'APA91bHbExwy4m9VjUFMpaN_gfx4ajcn-5L-thJtY14GWwpLXg8eAuP2p4ebAFkC-9g85tC02qw9SwZ21TYf3adu6IjCytgF9jx7hOUIG0i9dUpAwL5-Wsz4CQILnM49epOaqp6uTEGk',
            'data' => $message,
        );
        $headers = array(
            //AIzaSyDGEmxTQjKIKDTWvVR1Ptkw8RbUtKiDPTY
            //'Authorization: key=' . GOOGLE_API_KEY,
            'Authorization: key=AIzaSyDJyvyxdaQukCf8isARQCGS1OVtou4-a9Q',
            'Content-Type: application/json',
        );
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        // Execute post
        $result = curl_exec($ch);
        if ($result === false) {
            die('Curl failed: ' . curl_error($ch));
        }

        // Close connection
        curl_close($ch);
//        print_r($result);
        return $result;
    }

    public function sendtestnotification()
    {
        // Set POST variables
        // $url = 'https://android.googleapis.com/gcm/send';
        $url = 'https://fcm.googleapis.com/fcm/send';

        //  $fields = array(
        //APA91bGfHOCclVYOdjle9UlWLsFLXR4EwY_oQdDL19lUwPUxkH48g4Gudg4jxAjJwTZeuoO2lT_PIpLdY-N5g42iuJrWWUhtDqKqolm3FwSBnD38euUuae4tK3G5j0D3gU13VX0gxreGSfeBndZtEPTeHoA_9bjZ8A
        // 'registration_ids' => $registatoin_ids,
        //    'to' => 'APA91bGUqh7kim9SDn8fYRSf2oEPnkwRzkdfzf9MGlOwS9kI4AE8QbawYv6nQ_mHhKkN3va9HqFZVUqxxr3AbjHE0cl5OcHzKCWxcwbRC_qwhBxdW3pMiPNubtOMxr17YLavlVgSH3d9',
        //   'data' => {
        //"score": "5x1",
        //"time": "15:10"
        //},
        //    );
        $id = 'APA91bEk_EKNUTLk86WH-8gn9z2DD4mpqjYHYshHKteWtpSBWGr-_wnFiIQXbVFIS7i0i0JwIK0H9IW_V_3eb9N5ibd3tcD2phCDY6NGo2q-9UJlISzAtzZnr0Zx4wI_akbE8z8523xi';
        $msg = array
            (
            'title' => 'title',
            'message' => 'my message',
            'MyKey1' => 'MyData1',
            'MyKey2' => 'MyData2',

        );
        $fields = array
            (
            'to' => $id,
            'data' => $msg,
        );
        $headers = array(
            //AIzaSyDGEmxTQjKIKDTWvVR1Ptkw8RbUtKiDPTY
            //'Authorization: key=' . GOOGLE_API_KEY,
            'Authorization: key=AIzaSyDJyvyxdaQukCf8isARQCGS1OVtou4-a9Q',
            'Content-Type: application/json',
        );
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        // Execute post
        $result = curl_exec($ch);
        if ($result === false) {
            die('Curl failed: ' . curl_error($ch));
        }
        // Close connection
        curl_close($ch);
        print_r($result);
        // return $result;
    }

    public function getCourses($school_id)
    {

        $this->authinticate();
        $usersss = $this->user;

        $parent = false;
        if ($this->user['user_type'] == 1) {
            $admin = true;
        } elseif ($this->user['user_type'] == 4) {
            $admin = false;
            $data = $this->s_model->parentCourses();
            $this->setSuccess();
            $this->setData('courses', $data);
            $this->response();
            return false;
        } else {
            $admin = false;
        }
//        $this->s_model->show_query = 1;

        if ($admin) {
            $data = $this->s_model->getDatas('courses', array('parent' => 0, 'school_id' => $school_id), array('cid', 'name', 'code', 'desc'), "", "", "cid", "ASC");
        } else {
            $data = $this->s_model->getCourses($admin);
        }

        $this->setSuccess();
        $this->setData('courses', $data);
        // $this->setData('admin', $admin);
        // $this->setData('user_data', $this->user);
        $this->response();
    }

    public function getStudents($cid)
    {

        $this->authinticate();
        $user_no = $this->input->post('user_no');
        $date = $this->input->post('date');
        $parent = false;
        if ($this->user['user_type'] == 4) {
            $parent = true;
        }
        $data = $this->s_model->getStudentCourses($cid, $parent);
        $course = $this->s_model->getData('courses', array('cid' => $cid));
        if ($data) {
            foreach ($data as $key => $value) {
                $data[$key]['sheet'] = $this->s_model->getAttendances($cid, $value['sid']);

            }
        }
        $last_sem = $this->s_model->getLastSemNo($cid);
        $sem_teach = $this->s_model->getSemTeachers($cid);
        $sem_teacher = array();
        if ($sem_teach) {
            foreach ($sem_teach as $key => $val) {
                $sem_teacher['sem-' . $val['semno']] = $val;
            }
        }
        if ($last_sem) {
            $last_sem = $last_sem['max_cem'];
        }
        $sems = $this->s_model->getDatas('teacher_seminers', array('user_no' => $user_no, 'cid' => $cid), array('semno', 'cid', 'user_no'));
        $editables = $this->s_model->getDatas('editables', array('cid' => $cid, 'date' => $date));
        $editables_data = array();
        if ($editables) {
            foreach ($editables as $k => $v) {
                $editables_data['sid-' . $v['sid']]['cem-' . $v['semno']] = true;
            }
        }
        $modifications = $this->s_model->getModifiedAttendances($date);
        $mod = array();
        if ($modifications) {
            foreach ($modifications as $mod_new) {
                $mod['sem-' . $mod_new['semno']] = $mod_new['modific_count'];
            }
        }
        // attendance count
        $edited = $this->s_model->getEditNwAttendances($cid, 0, 1);
        $saveds = $this->s_model->getEditNwAttendances($cid);
        $saved = array();
        if (is_array($saveds) && count($saveds) > 0) {
            foreach ($saveds as $key => $semno) {
                if (!in_array($semno, $edited)) {
                    $saved[] = $semno;
                }
            }
        }
        $totalsems = isset($course['semno']) ? $course['semno'] : 0;
        $this->setSuccess();
        $this->setData('editables', $editables_data);
        $this->setData('modification', $mod);
        $this->setData('semnos', $sems);
        $this->setData('totalsems', $totalsems);
        $this->setData('semteacher', $sem_teacher);
        $this->setData('students', $data);
        $this->setData('last_cem', $last_sem);
        $this->setData('edited', $edited);
        $this->setData('saved', $saved);
        //$this->setData('lsaved', $saved);
        $this->response();
    }

    public function getStudents_delay($cid)
    {

        $this->authinticate();
        $user_no = $this->input->post('user_no');
        $school_id = $this->input->post('school_id');
        //echo $user_no;
        $date = $this->input->post('date');
        $parent = false;
        if ($this->user['user_type'] == 4) {
            $parent = true;
        }
        $data = $this->s_model->getStudentCourses($cid, $parent);
        $course = $this->s_model->getData('courses', array('cid' => $cid));
        if ($data) {
            foreach ($data as $key => $value) {
                // $data[$key]['sheet'] = $this->s_model->getAttendances($cid, $value['sid']);
                $data[$key]['useedforabsent'] = $this->s_model->CountUseedAbsentForStu($value['sid']);
                $data[$key]['sheet'] = $this->s_model->getAttendances_delay($cid, $value['sid']);
                //getAttendances_delay
            }
        }
        $last_sem = $this->s_model->getLastSemNo($cid);
        $sem_teach = $this->s_model->getSemTeachers($cid);
        $sem_teacher = array();
        if ($sem_teach) {
            foreach ($sem_teach as $key => $val) {
                $sem_teacher['sem-' . $val['semno']] = $val;
            }
        }
        if ($last_sem) {
            $last_sem = $last_sem['max_cem'];
        }
        $sems = $this->s_model->getDatas('teacher_seminers', array('user_no' => $user_no, 'cid' => $cid), array('semno', 'cid', 'user_no'));
        $editables = $this->s_model->getDatas('editables', array('cid' => $cid, 'date' => $date));
        $editables_data = array();
        if ($editables) {
            foreach ($editables as $k => $v) {
                $editables_data['sid-' . $v['sid']]['cem-' . $v['semno']] = true;
            }
        }
        $modifications = $this->s_model->getModifiedAttendances($date);
        $mod = array();
        if ($modifications) {
            foreach ($modifications as $mod_new) {
                $mod['sem-' . $mod_new['semno']] = $mod_new['modific_count'];
            }
        }
        // attendance count
        $edited = $this->s_model->getEditNwAttendances($cid, 0, 1);
        $saveds = $this->s_model->getEditNwAttendances($cid);
        $saved = array();
        if (is_array($saveds) && count($saveds) > 0) {
            foreach ($saveds as $key => $semno) {
                if (!in_array($semno, $edited)) {
                    $saved[] = $semno;
                }
            }
        }

        $totalsems = isset($course['semno']) ? $course['semno'] : 0;

        $delayResult = $this->s_model->GetDelayRuleBySchool($school_id);
        $delayRule = 5;
        if (count($delayResult) > 0) {
            $delayRule = $delayResult[0]['delay_rule'];
            $delayRule = ($delayRule == 0) ? 5 : $delayRule;
        }
        $this->setSuccess();
        $this->setData('editables', $editables_data);
        $this->setData('modification', $mod);
        $this->setData('semnos', $sems);
        $this->setData('totalsems', $totalsems);
        $this->setData('semteacher', $sem_teacher);
        $this->setData('students', $data);
        $this->setData('last_cem', $last_sem);
        $this->setData('edited', $edited);
        $this->setData('saved', $saved);
        $this->setData('delay_rule', $delayRule);
        //$this->setData('lsaved', $saved);
        $this->response();
    }

    public function viewStudent($sid)
    {
        $semi = $this->db->query("SELECT DISTINCT Sum(abs_attendance.attendance) , abs_attendance.semno, abs_attendance.datetime FROM abs_attendance WHERE abs_attendance.sid = '$sid' and abs_attendance.attendance = '0' GROUP BY abs_attendance.datetime having Sum(abs_attendance.attendance)<=6");

        $this->authinticate();
        $date = ($this->input->post('date')) ? $this->input->post('date') : date('Y-m-d');
        $cid = $this->post('cid');
        $data = $this->s_model->getData('students', array('sid' => $sid));
        if ($data) {
            $metas = $this->s_model->getMetas(2, $sid);
            $data['pic'] = ($data['pic']) ? $data['pic'] : 'nopic.png';
            $data['pic'] = base_url('uploads/' . $data['pic']);
            $data['meta'] = $metas;
            $cond = array(
                'sid' => $sid,

                'attendance' => 0,
//                'datetime' => $date
            );
            $student_courses = $this->s_model->getData('student_courses', array('sid' => $sid, 'moved' => null), array('cid'));

            if ($cid != '') {
                $cond['cid'] = $cid;
            }

            if ($cid == '') {
                $cid = $student_courses['cid'];
            }

            $data['course'] = $this->s_model->getData('courses', array('cid' => $cid), array('name'));
            $atts_attnd = $this->s_model->getDatas('attendance', $cond, array('*'), array(), array(), 'datetime');
            $atts_data = array();
            $date_array = array();
            $date_sems = array();
            if ($atts_attnd) {
                foreach ($atts_attnd as $key => $val) {

                    if (!in_array($val['datetime'], $date_array)) {
                        $a['date'] = $val['datetime'];
                        $a['notes'] = $this->s_model->getDatas('absent_note', array('sid' => $sid, 'cid' => $cid, 'date' => $val['datetime']), array('note', 'ID'));
                        $date_array[] = $val['datetime'];
                        $date_sems[$val['datetime']][] = $this->langLine('sem') . '-' . $val['semno'];
                        $atts_data[] = $a;
                    } else {
                        $date_sems[$val['datetime']][] = $this->langLine('sem') . '-' . $val['semno'];
                    }
                }
                foreach ($atts_data as $key => $atd) {
                    $atts_data[$key]['sem'] = implode(', ', $date_sems[$atd['date']]);
                }
            }
//            print_r($atts_data);
            //            print_r($date_sems);
            //            $atts = $this->s_model->getDatas('absent_note',array( 'sid' => $sid ),array('date','note'));
            $data['absents'] = $atts_data;
            //$data['absent_count'] = count($atts_data);
            $data['absent_count'] = count($semi->result_array()); // count($atts_attnd);
        }

        $this->setSuccess();
        $this->setData('details', $data);
//        $this->setData('session',false);
        $this->response();
    }

    public function deleteNote($note_id = '')
    {
        if ($note_id == '') {
            $this->setLangMessage('note_id_invalid');
            $this->_die();
        }
        $this->authinticate();
        $this->checkIsAdmin();

        $this->s_model->deleteRecord('absent_note', array('ID' => $note_id));

        $this->setSuccess();
        $this->setLangMessage('note_deleted');
        $this->response();
    }

    public function viewUser()
    {

        $this->checkRequiredEmptyFields();
        $this->authinticate();

        $user = $this->s_model->getData('users', array('user_no' => $this->user_no));

        $this->setSuccess();
        $this->setData('details', $user);

        $this->response();
    }

    public function checkIsAdmin()
    {
        if ($this->user['user_type'] == 2) {
            $this->setMessage($this->langLine('you_are_not_allowed'));
            $this->_die();
        }
    }

    public function manageCourse()
    {
        $this->authinticate();
        $this->checkIsAdmin();

        $course = $this->post('course');
        $id = $this->post('cid');
        $update = false;
        if ($id) {
            $update = true;
        }
        if ($update) {
            $this->s_model->updateRecord('courses', array('cid' => $id), $course);
        } else {
            $this->s_model->insertRecord('courses', $course);
        }
        $this->setSuccess();
        $this->setMessage('course_saved', true);
        $this->response();
    }

    public function saveStudent()
    {
        $this->authinticate();
        $this->checkIsAdmin();
        $sid = $this->post('sid');
        $student = $this->post('student');
        $this->s_model->updateRecord('students', array('sid' => $sid), $student);
        $this->setSuccess();
        $this->setLangMessage('student_saved');
        $this->response();
    }

    public function deleteStudent()
    {

        $this->authinticate();
        $this->checkIsAdmin();
        $sid = $this->post('sid');
        $cid = $this->post('cid');
        $this->s_model->deleteRecord('student_courses', array('sid' => $sid, 'cid' => $cid));
        $this->s_model->deleteRecord('attendance', array('sid' => $sid, 'cid' => $cid));
//        $this->s_model->deleteRecord('metadata',array('object_type' => 2,'object_id' => $id));
        $this->setSuccess();
        $this->setLangMessage("student_deleted");
        $this->response();
    }

    public function resetPass()
    {
        $email_id = $this->post('email_id');
        $user = $this->s_model->getData('users', array('email_id' => $email_id));
        if (empty($user) || empty($email_id)) {
            $this->setMessage($this->langLine('invalid_email'));
            $this->_die();
        } else {
            $token = $this->getToken(10);
//            $token = '123456789';
            $this->s_model->updateRecord('users', array('user_no' => $user['user_no']), array('reset_token' => $token));
            $message = base_url('/login/resetPass/' . $token);
            if (!mail($email_id, "Password reset", $message)) {
                $this->setMessage($this->langLine('mail_not_sent'));
                $this->_die();
            }
        }
        $this->setSuccess();
        $this->setMessage($this->langLine('password_reset_link_send'));
        $this->response();
    }

    public function getToken($length)
    {

        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet .= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet .= "0123456789";
        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[rand(0, strlen($codeAlphabet))];
        }
        return $token;
    }

    public function logout()
    {

        $this->session->sess_destroy();
        $this->clearSession();

        $data = array();

        $data['success'] = true; // false
        $data['msg'] = $this->langLine('logged_out');

        $this->response($data);
    }

    //The attendance app webserice
    public function saveNote()
    {
        $this->authinticate();
        $this->checkIsAdmin();

        $cid = $this->post('cid');
        $sid = $this->post('sid');
        $note = $this->post('note');
        $date = $this->post('date');
        $user_no = $this->post('user_no');

        $note_id = $this->s_model->insertRecord('absent_note', array(
            'cid' => $cid,
            'sid' => $sid,
            'note' => $note,
            'date' => $date,
            'created_by' => $user_no,
        ));

        $this->setSuccess();
        $this->setData('note_id', $note_id);
        $this->setMessage('note_sved', true);
        $this->response();
    }

    public function getHolidays($school_id)
    {

        $this->authinticate();
        $this->setSuccess();

        $this->response['holidays'] = $this->s_model->getDatas('holidays', array('school_id' => $school_id), array('date'));

        $holidat_string = array();
        if ($this->response['holidays']) {
            foreach ($this->response['holidays'] as $date) {
                $holidat_string[] = $date['date'];
            }
        }
        $this->response['holiday_string'] = $holidat_string;
        $this->response['holiday_string'] = implode(',', $holidat_string);
        $this->response();
    }

    public function saveUser()
    {
        $this->authinticate();
        $user_no = $this->post('user_no');
        $user = $this->post('user');
        $password = false;
        if (isset($user['oldpass']) || isset($user['newpass'])) {
            if (isset($user['oldpass']) && $user['oldpass'] == '') {
                $this->setMessage('old_empty_pass');
                $this->_die();
            }
            if (isset($user['oldpass']) && $user['oldpass']) {
                $data = $this->s_model->getData('users', array('user_no' => $user_no, 'password' => md5($user['oldpass'])));
            }
            if (empty($data)) {
                $this->setMessage('pass_not_match');
                $this->_die();
            } elseif ($user['oldpass'] == $user['newpass']) {
                $this->setMessage('pass_same');
                $this->_die();
            } elseif (isset($user['newpass']) && $user['newpass'] == '') {
                $this->setMessage('new_empty_pass');
                $this->_die();
            }
            $password = $user['newpass'];
            unset($user['oldpass']);
            unset($user['newpass']);
        }
        if ($password) {
            $user['password'] = md5($password);
            $user['password_hint'] = $password;
        }
        if ($user) {
            $this->s_model->updateRecord('users', array('user_no' => $user_no), $user);
        }
        $this->setSuccess();
        $this->setMessage('user_saved', true);
        $this->response();
    }

    public function getChildrens()
    {
        $user_no = $this->post('user_no');
        $childrens = $this->s_model->getData('student_parents', array('user_no' => $user_no), array('sid', 'name'));
        return $childrens;
    }

    public function sendMessage($school_id)
    {
        $this->authinticate();
        $user_no = $this->post('user_no');
        $notification_data = array();
        $send_to = array();
        $one_user_email = '';
        $notification = $this->post('notification');
        if ($notification) {
            if (isset($notification['send_to']) && $notification['send_to']) {
                foreach ($notification['send_to'] as $key => $v) {
                    if ($v) {
                        $send_to[] = $key;
                    }
                }
            }
            $notification_data['user_no'] = $user_no;
            $notification_data['notification'] = $notification['notification'];
            $notification_data['title'] = $notification['title'];
            $one_user_email = (isset($notification['one_user_email'])) ? $notification['one_user_email'] : '';
            $notification_data['date'] = date('Y-m-d H:i:s');
        }
        if ($notification_data) {
            $nid = $this->s_model->insertRecord('notifications', $notification_data);

            $not_data = array();

            $not_data['nid'] = $nid;
            $not_data['sender_id'] = $user_no;
            $not_data['school_id'] = $school_id;

            $user_types_to_send = array();
            if ($send_to) {
                foreach ($send_to as $type) {
                    switch ($type) {
                        case 'parents':$not_data['user_type'] = 4;
                            break;
                        case 'mod':$not_data['user_type'] = 3;
                            break;
                        case 'tech':$not_data['user_type'] = 2;
                            break;
                        case 'admin':$not_data['user_type'] = 1;
                            break;
                        case 'oneuser':
                            $not_data['user_type'] = 0;
                            $not_data['single_user'] = $one_user_email;
                            break;
                    }
                    $user_types_to_send[] = $not_data['user_type'];
//                    $not_data['notify_date'] = date('Y-m-d H:i:s');
                    $this->s_model->insertRecord('notify_user', $not_data);
                }
            }
            // $this->sendPushForMessage($user_types_to_send, $notification_data, $one_user_email);
        }

        $this->setSuccess();
        $this->setMessage('notific_send', true);
        $this->response();
    }

    private function sendPushForMessage($types, $notification_data, $one_user_email)
    {
        $types = implode("','", $types);
        $types = ($types) ? "('$types')" : $types;
        $ids = $this->s_model->getUserPushIDsWithUserType($types, $one_user_email);
        $registration_ids = array();
        if ($ids) {
            foreach ($ids as $val) {
                $registration_ids[] = $val['registration_id'];
            }
            $title = ($notification_data['title']) ? $notification_data['title'] : '';
            $message = array('message' => $title);
            $response_push = $this->send_notification($registration_ids, $message);

            // $response_push = $this->sendtestnotification();
        }

//        print_r($message);
        //        print_r($registration_ids);
        $this->setData('push_response', $response_push);
    }

    public function getNotifications($school_id)
    {
        $this->authinticate();
        $user_no = $this->post('user_no');
        $notifications = $this->s_model->getNotifications($school_id, $this->user);
//        print_r($notifications);
        foreach ($notifications as $key => $value) {
            $notifications[$key]['pic'] = ($notifications[$key]['pic']) ? $notifications[$key]['pic'] : 'nopic.png';
            $notifications[$key]['pic'] = base_url('uploads/' . $notifications[$key]['pic']);
            $notifications[$key]['user_right'] = 'U';
            if ($notifications[$key]['user_type'] == 1) {
                $notifications[$key]['user_right'] = 'A';
            } elseif ($notifications[$key]['user_type'] == 2) {
                $notifications[$key]['user_right'] = 'T';
            } elseif ($notifications[$key]['user_type'] == 3) {
                $notifications[$key]['user_right'] = 'M';
            } elseif ($notifications[$key]['user_type'] == 4) {
                $notifications[$key]['user_right'] = 'P';
            }
        }
        $this->setSuccess();
        $this->setData('list', $notifications);
//        $this->setData('list', $notifications);
        $this->response();
    }

    public function deleteNotifications()
    {
        $this->authinticate();
        $user_no = $this->post('user_no');
        $nid = $this->post('nid');
        $this->s_model->insertRecord('delete_notification', array('user_no' => $user_no, 'nid' => $nid));
        $this->setSuccess('notific_deleted');
        $this->response();
    }

    public function getAds()
    {
        $data = array();
        $ads = $this->s_model->getDatas('ads');
        if ($ads) {
            foreach ($ads as $key => $value) {
                if ($value['section']) {
                    $data[$value['section']]['ID'] = $value['ID'];
                    $data[$value['section']]['medias'] = $this->s_model->getAdMedias($value['ID']);
                }
            }
        }
        $this->setData('ads', $data);
        $this->setSuccess();
        $this->response();
    }

    public function getNewsjoin($group_no, $content_per_page, $sort_order)
    {

        $start = ceil($group_no * $content_per_page);

        $news = $this->s_model->newsGetjoin($start, $content_per_page, $sort_order);

        //$news = $this->s_model->newsGetjoin();
        foreach ($news as $key => $csm) {
            $news[$key]['ago'] = $this->time_elapsed_string($csm['newsdate']);
        }

        echo json_encode($news);

    }

    public function getNews()
    {

        $news = $this->s_model->newsGet('abs_news');
        foreach ($news as $key => $csm) {
            $news[$key]['ago'] = $this->time_elapsed_string($csm['date']);
        }

        echo json_encode($news);

    }

    /********** existing user add by hetram on 30th june, 2017 *********/
    public function exist_user($email = null)
    {

        $data = array();
        $users = $this->s_model->getUserData('users', array('email_id' => $email));

        if ($users) {
            foreach ($users as $key => $value) {
                $data['user_no'] = $value['user_no'];
                $data['first_name'] = $value['first_name'];
                $data['last_name'] = $value['last_name'];
                $data['email_id'] = $value['email_id'];
                $data['user_type'] = $value['user_type'];
                $data['phone_no'] = $value['phone_no'];
                $data['username'] = $value['username'];
                $data['reset_token'] = $value['reset_token'];
                $data['pic'] = $value['pic'];
                $data['status'] = $value['status'];
                $data['lang_id'] = $value['lang_id'];
                $data['parent'] = $value['parent'];
                $data['datetime'] = $value['datetime'];
            }
        }
        $this->setData('user', $data);
        $this->setSuccess();
        $this->response();
    }
    /********** end existing user *********/
    /********** existing user add by hetram on 30th june, 2017 *********/
    public function exist_usern($email = null)
    {

        $data = array();
        $users = $this->s_model->getUserData('users', array('username' => $email));

        if ($users) {
            foreach ($users as $key => $value) {
                $data['user_no'] = $value['user_no'];
                $data['first_name'] = $value['first_name'];
                $data['last_name'] = $value['last_name'];
                $data['email_id'] = $value['email_id'];
                $data['user_type'] = $value['user_type'];
                $data['phone_no'] = $value['phone_no'];
                $data['username'] = $value['username'];
                $data['reset_token'] = $value['reset_token'];
                $data['pic'] = $value['pic'];
                $data['status'] = $value['status'];
                $data['lang_id'] = $value['lang_id'];
                $data['parent'] = $value['parent'];
                $data['datetime'] = $value['datetime'];
            }
        }
        $this->setData('user', $data);
        $this->setSuccess();
        $this->response();
    }
    /********** end existing user *********/

    public function plan_subscribe()
    {

        if (strtolower($this->input->server('REQUEST_METHOD')) != 'post') {
            $response['status'] = false;
            $response['msg'] = 'Your request has been not accepted';
            echo json_encode($response);
            //  redirect('');
        } else {
            $data = $this->input->post();
            // echo json_encode($data);
            //exit;
            $resultId = $this->s_model->subscribePlan('subscribe_plans', $data);
            $add_school = $this->s_model->addSchool('school', $data, $resultId);

            $add_user = $this->s_model->addUser('users', $data, $add_school, $resultId);

            $to_email = "q8xbox@yahoo.com";
            $from_email = $this->post('f2');
            $name = 'Payment';
            $subject = 'Payment made.';
            $body = 'Your plan has subscribed successfully';
    
            //Load email library
            $this->load->library('email');
    
            $this->email->from($from_email, $name);
            $this->email->to($to_email);
            $this->email->subject($subject);
            $this->email->message($body);
    
            if ($this->email->send()) {
                // echo "success";
            }

            if ($add_user == 1) {
                $response['status'] = true;
                $response['msg'] = 'Your plan has subscribed successfully';
                echo json_encode($response);
                //redirect('');
            } else {
                $response['status'] = false;
                $response['msg'] = 'Please try again';
                echo json_encode($response);
                //redirect('');
            }
        }
    }

    public function getPlan()
    {

        $response['plans'] = $this->s_model->getPlans('plans');
        $response['status'] = true;
        echo json_encode($response);
    }

    public function getSchool()
    {
        $response['schools'] = $this->s_model->getSchools('school');
        $response['status'] = true;
        echo json_encode($response);
    }

    public function time_elapsed_string($datetime, $full = false)
    {
        $new_date = date("Y-m-d", strtotime($datetime));
        $newDate = date("d M y", strtotime($new_date));
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'd',
            'h' => 'h',
            'i' => 'm',
            's' => 's',
        );

        foreach ($string as $k => &$v) {

            if ($diff->$k) {
                $v = $diff->$k . '' . $v . ($diff->$k > 1 ? '' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) {
            $string = array_slice($string, 0, 1);
        }

        if (isset($string['w']) || isset($string['m']) || isset($string['y'])) {
            return $newDate;
        } else {
            return $string ? implode(',', $string) . '' : 'just now';
        }

    }

    public function sendContact()
    {

        $to_email = "q8xbox@yahoo.com";
        $from_email = $this->post('f2');
        $name = $this->post('f1');
        $subject = $this->post('f3');
        $body = $this->post('f4');

        //Load email library
        $this->load->library('email');

        $this->email->from($from_email, $name);
        $this->email->to($to_email);
        $this->email->subject($subject);
        $this->email->message($body);

        if ($this->email->send()) {
            echo "success";
        }

    }

    public function attendanceBySeminar($student, $date, $seminar)
    {
        $result_array = $this->s_model->seminarattendance($student, $date);
        $output = "";
        foreach ($result_array as $value) {
            $output .= $value['semno'];
        }

        //$data['result'] = $output;

        $data = "";
        for ($i = 1; $i <= ($seminar); $i++) {

            $data .= $i;
        }
        $result = str_split($data);
        $result_new = str_split($output);

        $string = $this->to_string($result);

        $aray = $this->to_string($result_new);
        $output_array = explode(',', $aray);

        $query = $this->check_inarray_multiple($string, $output_array);

        if ($query === true) {
            $response['result'] = '1';
        } else {
            $response['result'] = '0';
        }

        echo json_encode($response);

    }

    public function to_string($data, $glue = ', ')
    {
        $output = '';
        if (!empty($data) && count($data) > 0) {
            $values = array_values($data);
            $output = join($glue, $values);
        }
        return $output;
    }

    public function check_inarray_multiple($string, $aray)
    {

        $newarray = array();
        $result = array();
        $array = explode(',', $string);

        for ($i = 0; $i < count($array); $i++) {
            $newarray[] = $array[$i];

            if (in_array($newarray[$i], $aray)) {
                $result[] .= "1";
            } else {
                $result[] .= "2";
            }
        }
        if (count(array_unique($result)) === 1) {
            return $rs = true;
        } else {
            return $rs = false;
        }
    }

    public function addStudentNote()
    {

        //$data = $this->input->post();
        $sid = $this->input->post('sid');
        $note = $this->input->post('note');
        $userId = $this->input->post('user_id');
        $data = array('sid' => $sid, 'note' => $note, 'user_id' => $userId);

        $add = $this->s_model->addStudentNotes('notes', $data);

        if ($add) {
            $response['success'] = true;
            $response['note_id'] = $add;
        } else {
            $response['success'] = false;
        }

        echo json_encode($response);
    }

    public function deleteStudentNote($id)
    {

        $delete = $this->s_model->deleteStudentNotes('notes', $id);

        if ($delete) {
            $response['success'] = true;

        } else {
            $response['success'] = false;
        }

        echo json_encode($response);
    }

    public function getStudentNote($sid)
    {

        $response['notes'] = $this->s_model->getJoinNotes('notes', $sid);
        $response['status'] = true;

        echo json_encode($response);
    }

}
