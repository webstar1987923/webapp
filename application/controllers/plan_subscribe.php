<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of assign
 *
 * @author Suchandan
 */
class Plan_subscribe extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('s_model');
        $this->s_lib->checkMethodAccess($this->session->userdata('user_type'), $this->router->fetch_class(), $this->router->fetch_method());
    }

    public function index() {

      if(strtolower($this->input->server('REQUEST_METHOD')) != 'post') {
        $response['status'] = false;
        $response['msg'] = 'Your request has been not accepted';  
        echo json_encode($response);         
      //  redirect('');
     }else{      
        $data = $this->input->post();
       // echo json_encode($data);
        //exit;
        $result = $this->s_model->subscribePlan('subscribe_plans',$data);
        if($result == 1){         
        $response['status'] = true;
        $response['msg'] = 'Your plan has subscribed successfully';  
        echo json_encode($response);
          //redirect('');
        }
        else{          
        $response['status'] = false;
        $response['msg'] = 'Please try again';  
        echo json_encode($response);
           //redirect('');
        }
      }
    }

    public function getPlan(){

      $response['plans'] = $this->s_model->getPlans('plans');
      $response['status'] = true;      
      echo json_encode($response);
    }
}
