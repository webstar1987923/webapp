<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends My_Controller {

    public function __construct() {
        parent::__construct();  
        $this->s_lib->checkMethodAccess($this->session->userdata('user_type'), $this->router->fetch_class(), $this->router->fetch_method());
    }

    public function index() {
        $this->auth();
        if($this->session->userdata('is_school') == true){
        $school_id = $this->session->userdata('school_id');
        }else{
        $school_id = '';
        }
        $data['student_count'] = $this->s_model->countData('students', array('parent' => 0, 'school_id' => $school_id));
        $data['course_count'] = $this->s_model->countData('courses', array('parent' => 0, 'school_id' => $school_id));
        $data['school_count'] = $this->s_model->countData('users', array('user_type' => 1, 'school_id' => $school_id));
        $data['news_count'] = $this->s_model->countData('news', array());
        $data['school_count'] = $this->s_model->countData('school', array());
        $data['teacher_count'] = $this->s_model->countData('users', array('user_type' => 2, 'school_id' => $school_id));
        $data['parent_count'] = $this->s_model->countData('users', array('user_type' => 4, 'school_id' => $school_id));
        //get all class meanse cources      
        
        $courses = $this->s_model->getCourseList($start = 0, 1000, array('parent' => 0, 'school_id' => $school_id));
        $data['courses'] = $courses;
        $data['current_school'] = $this->s_model->getCurrentSchool();
//        $courses               = $this->s_model->countData('courses',array('parent' => 0));
//        $this->s_model->show_query = 1;
//        $attendance_sheet      = $this->s_model->getAttendance(); 
// get the filters data 

        $filtercond = array();
        $is_excel_export = false;
        
        if (strtolower($this->input->server('REQUEST_METHOD')) == 'post') {
            $start_date = $this->input->post('start_date');
            $end_date = $this->input->post('end_date');
            $class_id = $this->input->post('class_id');
            $hide_absent = $this->input->post('hide_absent');
            $studentid = $this->input->post('studentid');
            if (count($class_id)>1){
                $studentid = false;
            }
            $semno = $this->input->post('semno');
            foreach($class_id as $sem_key=>$sem_val){
                if (empty($sem_val)){
                    unset($class_id[$sem_key]);
                }
            }
            foreach($semno as $sem_key=>$sem_val){
                if (empty($sem_val)){
                    unset($semno[$sem_key]);
                }
            }
            $absent_number = $this->input->post('absent_number');
            $is_excel_export = $this->input->post('excel_export');
            $filtercond['orderby'] = 'cid, student_name ASC';
            if (!empty($is_excel_export)) {
                $is_excel_export = true;
            }
            //$class_id=2;
            if (!empty($start_date)) {
                $filtercond['start_date'] = date('Y-m-d', strtotime($start_date));
            }
            $filtercond['school_id'] = $school_id;
            if (is_array($semno) && !empty($semno)) {
                $filtercond['semno'] = $semno;
                
            }
            if (!empty($end_date)) {
                $filtercond['end_date'] = date("Y-m-d", strtotime($end_date));
            }
            if (!empty($class_id)) {
                $filtercond['class_id'] = $class_id;
            }
            if (!empty($hide_absent)) {
                $filtercond['hide_absent'] = $hide_absent;
            }
            if ($studentid*1) {
                $filtercond['studentid'] = $studentid;
            }
            if (!empty($absent_number)) {
                if ($absent_number < 0) {
                    $absent_number = (-1 * $absent_number);
                }
                $filtercond['absent_number'] = $absent_number;
            }
        }
        
        if ($is_excel_export) {
            // excel export section 
            $this->excelexport($filtercond);
        }
        if (!empty($start_date) || !empty($end_date) || !empty($class_id)) {
            $attendance = $this->s_model->getAttendance(true, $filtercond);
            //echo "<pre>"; print_r($attendance);
        }
        
        $absents = array();
//        $data['courses']          = $courses;
//        $data['attendance_sheet'] = $attendance_sheet;
        if ($attendance) {
            $students_ids = array();
            foreach ($attendance as $key => $atend) {
                $students_ids[] = $atend['sid'];
            }
            
            $students_absents = $this->s_model->getAbsentCounts($students_ids, $filtercond);
//            print_r($attendance);die;
            $students_seminars_absents = $this->s_model->getAbsentSeminarsCounts($students_ids, $filtercond);
            
//            print_r($attendance);
//            print_r($students_absents);
//            die;
            //new section imlements 
            foreach ($attendance as $key => $atend) {
                if ($students_absents[$atend['sid']]){
                    $attendance[$key]['abscount'] = $students_absents[$atend['sid']];
                }else{
                    unset($attendance[$key]);
                }
            }
            
//            print_r($attendance);die;
//            die('finsni');
            $students_ids = array();
            foreach ($attendance as $atend) {
                $students_ids[] = $atend['sid'];
            }
            $notes = $this->s_model->getStudentsNotes($students_ids, $filtercond);
            
        }
        
        $data['class_students'] = count($class_id)==1?$this->s_model->getCourseStudents(array_shift(array_slice($class_id, 0, 1))):array();
        $data['semno'] = $semno;
        $data['notes'] = $notes;
        $data['studentid'] = $studentid;
        $data['class_id'] = $class_id;
        $data['absents'] = $attendance;
        $data['students_seminars_absents'] = $students_seminars_absents;
        $data['filtercond'] = $filtercond;
//        print_r($absents);

        $this->s_lib->loadView('dashboard', $data);
    }

    public function excelexport($condition = array()) {
        $attendance = $this->s_model->getAttendance_excel(true, $condition);
        $this->load->helper(array('file', 'download'));
        if (!empty($attendance)) {
            header("Content-type: application/csv-tab-delimited-table; charset=utf-8");
            $this->load->dbutil();
            $extns = "csv";
            $file_data = $this->dbutil->csv_from_result($attendance);

            $filename = time() . "absent_report." . $extns;
            force_download($filename, $file_data);
        }
        exit;
    }   

    
}   

