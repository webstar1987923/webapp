<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of assign
 *
 * @author Suchandan
 */
class Assign extends My_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->s_lib->checkMethodAccess($this->session->userdata('user_type'), $this->router->fetch_class(), $this->router->fetch_method());
    }
    public function course($sid) {
       $school_id = $this->session->userdata('school_id');
       $courses =  $this->s_model->getDatas('courses',array('parent' => 0,'school_id' => $school_id));
       $data['courses'] = $courses;
       
       $student =  $this->s_model->getData('students',array('sid' => $sid, 'school_id' => $school_id));
       $data['student'] = $student;
       $assigned_courses = $this->s_model->getDatas('student_courses',array('sid' => $sid));
       $stcs = array();
       if(!empty($assigned_courses)){
            foreach($assigned_courses as $course){
                if (empty($course['moved'])){
                    $stcs[$course['cid']] = $course['cid'];
                }
            }
       }
       $data['assigned_courses'] = $stcs;
       $data['action'] = 1;
       
       if($this->post('action') == 1){
            $post_cporses = $this->post('courses');
//            print_r('asads');
//            print_r($_POST);
            if(!empty($post_cporses)){
                
//                foreach($post_cporses as $item){
//                    if (isset($stcs[$item])){
//                        unset($stcs[$item]);
//                    }
//                }
                
                if (!empty($stcs) || !empty($post_cporses)){
                    if (!empty($stcs)){
                        foreach($stcs as $stc){
                            $this->s_model->updateRecord('student_courses',array('sid' => $sid, 'cid'=>$stc*1), array('moved'=>date('Y-m-d H:i:s')));
                        }
                    }
                    
                    foreach($post_cporses as $cs){
                        
                        $existed_courses = $this->s_model->getDatas('student_courses',array('sid' => $sid, 'cid'=>$cs, 'moved !=' => 'NULL'));
                        if (empty($existed_courses)){
                            $this->s_model->insertRecord('student_courses',array(
                                'cid' => $cs,
                                'sid' => $sid,
                                'asssigned_by' => $this->session->userdata('user_no')
                            ));
                        }else{
                            $this->s_model->updateRecord('student_courses',array(
                                'cid' => $cs,
                                'sid' => $sid
                                ), array('moved'=>null, 'asssigned_by' => $this->session->userdata('user_no'))
                            );
                        }
                        $this->s_model->updateRecord('attendance',array(
                            'sid' => $sid
                            ), array('cid' => $cs)
                        );
                    }
                    
                }
                
                $this->redirectTO('assign/course/'.$sid, 'The course is successfully assigned.');
            }else{
                 
                $data['code']['status'] = 4;
                $data['code']['class'] = 'danger';
                $data['code']['msg'] = $this->langLine('select_course');
               
                $this->s_lib->loadView('assign-course',$data);
                return false;
            }
       }
       $this->s_lib->loadView('assign-course',$data);
    }
    public function users($user_no) {
       $school_id = $this->session->userdata('school_id');
       $courses =  $this->s_model->getDatas('courses',array('parent' => 0,'school_id' => $school_id));
       $data['courses'] = $courses;
       $data['section_header'] = "Assign Class";
       $school_id = $this->session->userdata('school_id');
       $teacher =  $this->s_model->getData('users',array('user_no' => $user_no,'school_id' => $school_id));
       $data['teacher'] = $teacher;
       $assigned_courses = $this->s_model->getDatas('teacher_courses',array('user_no' => $user_no));
       $stcs = array();
       $course_sems = array();
	   $attendance_edit_allow_cource=array();
	   $absent_notes=array();
       if(!empty($assigned_courses)){
            foreach($assigned_courses as $course){
                $stcs[] = $course['cid'];
				if($course['is_allow_edit_absent']){
					$attendance_edit_allow_cource[]=$course['cid'];
					$absent_notes[$course['cid']]=$course['absent_note'];
				}
                $assigned_sems = $this->s_model->getDatas('teacher_seminers',array('user_no' => $user_no,'cid' => $course['cid']));
                $stcsem = array();
                foreach($assigned_sems as $sem){
                    $stcsem[] = $sem['semno'];
                }
                $course_sems[$course['cid']]    = $stcsem;
            }
       }
       $data['assigned_courses'] = $stcs;
       $data['assigned_sems']    = $course_sems;
       $data['is_allow_edit_absent'] = $attendance_edit_allow_cource;
       $data['absent_notes'] = $absent_notes;
       $data['action'] = 1;
       
       if($this->post('action') == 1 ){
		   
			$post_cporses = $this->post('courses');
            $post_sems = $this->post('semnos');
			// mrin section 
			$absent_edit_allow = $this->post('absent_edit_allow');
			$absent_edit_allow_note = $this->post('absent_edit_allow_note');
			// mrin : End
            if(!empty($post_cporses)){
                $this->s_model->deleteRecord('teacher_courses',array('user_no' => $user_no));
                $this->s_model->deleteRecord('teacher_seminers',array('user_no' => $user_no));
                foreach($post_cporses as $cs){
                    $sems = $post_sems[$cs];
                    if($sems){
                        foreach ($sems as $sem) {
                            $this->s_model->insertRecord('teacher_seminers',array(
                                'cid' => $cs,
                                'semno' => $sem,
                                'user_no' => $user_no,
                                'type' => $teacher['user_type'],
                                'status' => 1,
                            ));
                        }
                    }
					// attendance edit permitions
					$is_allow_edit_absent=0;
					$absent_note='';
					if(!empty($absent_edit_allow)){
						if(array_key_exists($cs,$absent_edit_allow)){
							$is_allow_edit_absent=1;
							$absent_note=$absent_edit_allow_note[$cs];
						}
					}
                    $this->s_model->insertRecord('teacher_courses',array(
                        'cid' => $cs,
                        'user_no' => $user_no,
                        'type' => $teacher['user_type'],
                        'assigned_by' => $this->session->userdata('user_no'),
						'is_allow_edit_absent'=>$is_allow_edit_absent,
						'absent_note'=>$absent_note
                    ));
                }
                $this->redirectTO('assign/users/'.$user_no, 'The courses are successfully assigned.');
            }else{
                 
                $data['code']['status'] = 4;
                $data['code']['class'] = 'danger';
                $data['code']['msg'] = $this->langLine('select_course');
               
                $this->s_lib->loadView('assign-teacher',$data);
                return false;
            }
       }
       
       $this->s_lib->loadView('assign-teacher',$data);
    }
    public function student($cid) {
        $data = array();
        $school_id = $this->session->userdata('school_id');
        $data['course']     = $this->s_model->getData('courses',array('cid' => $cid,'school_id' => $school_id));
        $data['students']   = $this->s_model->getUnassignedStudents($school_id);
//        print_r($data['students']);
        if($data['students']){
            foreach($data['students'] as $key => $val){
                $codes = $this->s_model->getStudentCoursesCodes($val['sid']);
                $a = array();
                foreach ($codes as $code) {
                    if ($code['code'] != '') {
                        $a[] = $code['code'];
                    }
                }
                $codes_st = implode(',', $a);
                $data['students'][$key]['st_courses'] = $codes_st;
//                $data['students'][$key]['course_count'] = $this->s_model->getDatas('student_courses',array('sid' => $val['sid']),array(''));
//                $data['students'][$key]['course_count'] = $this->s_model->getDatas('student_courses',array('sid' => $val['sid']));
            }
        }
        $assigned_students = $this->s_model->getDatas('student_courses',array('cid' => $cid));
        $stcs = array();
        if(!empty($assigned_students)){
             foreach($assigned_students as $student){
                 $stcs[] = $student['sid'];
             }
        }
        $data['assigned_students'] = $stcs;
        $data['action'] = 1;
        
        if($this->post('action') == 1){
            $post_students = $this->post('students');
//            print_r('asads');
//            print_r($_POST);
            if(!empty($post_students)){
                $this->s_model->deleteRecord('student_courses',array('cid' => $cid));
                foreach($post_students as $cs){
                    $this->s_model->insertRecord('student_courses',array(
                        'cid' => $cid,
                        'sid' => $cs,
                        'asssigned_by' => $this->session->userdata('user_no')
                    ));
                }
                $this->redirectTO('assign/student/'.$cid, 'The students are successfully assigned.');
            }else{
                 
                $data['code']['status'] = 4;
                $data['code']['class'] = 'danger';
                $data['code']['msg'] = "Select some student first.";
               
                $this->s_lib->loadView('assign-course',$data);
                return false;
            }
       }
        $this->s_lib->loadView('assign-students',$data);
    }
    public function parents($sid) {
       $school_id = $this->session->userdata('school_id');
       $parents =  $this->s_model->getDatas('users',array('user_type' => 4, 'school_id' => $school_id));
       $data['parents'] = $parents;
       
       $student =  $this->s_model->getData('students',array('sid' => $sid, 'school_id' => $school_id));
       $data['student'] = $student;
       $assigned_parents = $this->s_model->getDatas('student_parents',array('sid' => $sid));
       $stcs = array();
       if(!empty($assigned_parents)){
            foreach($assigned_parents as $parent){
                $stcs[] = $parent['user_no'];
            }
       }
       $data['assigned_parents'] = $stcs;
       $data['action'] = 1;
       
       if($this->post('action') == 1){
            $post_cporses = $this->post('parents');
//            print_r('asads');
//            print_r($_POST);
            if(!empty($post_cporses)){
                $this->s_model->deleteRecord('student_parents',array('sid' => $sid));
                foreach($post_cporses as $cs){
                    $student =  $this->s_model->getData('students',array('sid' => $sid, 'school_id' => $school_id));
                    $this->s_model->insertRecord('student_parents',array(
                        'user_no' => $cs,
                        'student_id' => $student['student_id'],
                        'sid' => $sid,
                        'asssigned_by' => $this->session->userdata('user_no')
                    ));
                }
                $this->redirectTO('assign/parents/'.$sid, 'The parents is successfully assigned.');
            }else{
                $this->s_model->deleteRecord('student_parents',array('sid' => $sid));
                 
//                $data['code']['status'] = 4;
//                $data['code']['class'] = 'danger';
//                $data['code']['msg'] = $this->langLine('select_course');
//               
//                $this->s_lib->loadView('assign-parents',$data);
//                return false;
            }
       }
       $this->s_lib->loadView('assign-parents',$data);
    }

}
