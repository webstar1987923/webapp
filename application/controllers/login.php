<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    public function index() {

        $this->auth();
        $data['uri'] = $this->uri->segment(1);
        $data['schools'] = $this->s_model->getSchool();
        $data['bg_class'] = 'login';
        $this->load->view('header', $data);
        $this->load->view('login', $data);
        $this->load->view('footer');
    }

    public function auth() {
        $redirect = base_url('dashboard');
        if ($this->session->userdata('logged_in') === TRUE) {
            if ($this->input->get('redirect_to') != '') {
                $redirect = $this->input->get('redirect_to');
            }
            redirect($redirect);
        }
    }

    public function resetPass($token = '') {
        $token = ($token) ? $token : $this->input->post('token');
        $user = $this->s_model->getData('users', array('reset_token' => $token));
        if (empty($user)) {
            die('Token is invalid');
        }
        if ($this->input->post('resetpass')) {
            $password = $this->input->post('password');
            $cnfpassword = $this->input->post('cnfpassword');
            if ($password != $cnfpassword) {
                $this->session->set_flashdata('msg', 'Password missmatch.');
                redirect('login/resetPass/' . $token);
            }
            $this->s_model->updateRecord('users', array('user_no' => $user['user_no']), array('password' => md5($password), 'password_hint' => $password, 'reset_token' => ''));
            $this->session->set_flashdata('msg', 'Password is reseted successfully.');
            redirect('login');
        } else {
            $this->load->view('header');
            $this->load->view('reset-pass', array('token' => $token));
            $this->load->view('footer');
        }
    }

//         public function goLogin() {
//             $this->form_validation->set_rules('password', 'Password', 'required');
// //            $this->form_validation->set_rules('email_id', 'Email ID', 'required|valid_email');
//             $email_id   = $this->input->post('email_id');
//             $password   = md5($this->input->post('password'));
//             if ($this->form_validation->run() == FALSE)
//             {
//                 $this->session->set_flashdata('msg',  validation_errors());
//                 redirect(base_url('login'));
//             }else{
// //                $this->s_model->show_query = 1;
//                 $user = $this->s_model->checkUserExists($email_id,$password);
//                 if(!empty($user)){
//                     if($user['user_type'] != 1){
//                         $this->session->set_flashdata('msg','You have no access for login.');
//                         redirect('login');
//                     }
//                     $userdata = array(
//                         'user_no'   => $user['user_no'],
//                         'user_type' => $user['user_type'],
//                         'user_name' => $user['first_name'].' '.$user['last_name'],
//                         'logged_in' => true
//                     );
// //                    print_r($user);
//                     $this->session->set_userdata($userdata);
//                     redirect('dashboard');
//                 }
//                 else{
//                     $this->session->set_flashdata('msg','Invalid email ID or password');
//                     redirect('login');
//                 }
//             }
//         }

    public function goLogin() {

        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('email_id', 'Email', 'required');

        $email_id = $this->input->post('email_id');
        $password = md5($this->input->post('password'));

        if ($this->form_validation->run() == FALSE) {

            $this->session->set_flashdata('msg', validation_errors());
            redirect(base_url('login'));
        } else {
            $uri = $this->input->post('uri');

//                $this->s_model->show_query = 1;
            if ($uri == 'login' || $uri == '') {

                if ($this->input->post('school') != '') {
                    $schoolId = $this->input->post('school');
                    $user = $this->s_model->checkUserExists($email_id, $password);
                    
                    if (!empty($user)) {
                        if ($this->input->post('remember') != "") {
                            /* Set cookie to last 1 year */
                            $this->load->helper('cookie');
                            /* $cookie = array(
                              'name'   => 'userDetail',
                              'value'  => array('email'=>$email_id, 'password'=>$password,'school'=>$school_data[1]),
                              'expire' => '86500',
                              ); */
                            //$this->input->set_cookie($cookie);
                            setcookie('school', $user['school_id'], time() + 60 * 60 * 24 * 365);
                            setcookie('email', $email_id, time() + 60 * 60 * 24 * 365);
                            setcookie('password', $this->input->post('password'), time() + 60 * 60 * 24 * 365);
                        }
                        $school = $this->s_model->getCurrentSchool($schoolId);
                        if ($user['user_type'] == 0) {
                            $schooluser = $this->s_model->checkAdminSchoolExists($school['email_id'], $school['id']);
                            if (!empty($schooluser)) {
                                $userdata = array(
                                    'school_id' => $schooluser['school_id'],
                                    'user_no' => $schooluser['user_no'],
                                    'user_type' => $schooluser['user_type'],
                                    'user_name' => $user['first_name'] . ' ' . $user['last_name'],
                                    'logged_in' => true,
                                    'is_admin' => true,
                                    'is_school' => true
                                );

                                $this->session->set_userdata($userdata);
                                redirect('dashboard');
                            } else {

                                $this->session->set_flashdata('msg', 'Sorry there is no any school of this name');
                                redirect('login');
                            }
                        } elseif (in_array($user['user_type'], array(1, 3, 5, 6, 7))){

                            if ($user['school_id']==$schoolId) {
                                $userdata = array(
                                    'school_id' => $user['school_id'],
                                    'user_no' => $user['user_no'],
                                    'user_type' => $user['user_type'],
                                    'user_name' => $user['first_name'] . ' ' . $user['last_name'],
                                    'logged_in' => true,
                                    'is_school' => true
                                );


                                $this->session->set_userdata($userdata);
                                redirect('dashboard');
                            } else {

                                $this->session->set_flashdata('msg', 'plz select correct school');
                                redirect('login');
                            }
                        } else {
                            $this->session->set_flashdata('msg', 'plz login with select school name');
                            redirect('login');
                        }
                    } else {
                        $this->session->set_flashdata('msg', 'Invalid email ID or password or School Name');
                        redirect('login');
                    }
                } else {
                    $user = $this->s_model->checkUserExists($email_id, $password);
                    if (!empty($user)) {
                        if ($user['user_type'] == 0) {
                            $userdata = array(
                                'admin' => $user['id'],
                                'user_no' => $user['user_no'],
                                'user_type' => $user['user_type'],
                                'user_name' => $user['first_name'] . ' ' . $user['last_name'],
                                'logged_in' => true,
                                'is_admin' => true
                            );


                            $this->session->set_userdata($userdata);
                            redirect('dashboard');
                        } else {
                            $this->session->set_flashdata('msg', 'Sorry! please select your school for login');
                            redirect('login');
                        }
                    } else {
                        $this->session->set_flashdata('msg', 'Invalid email ID or password');
                        redirect('login');
                    }
                }
            }
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect(base_url('login'));
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */