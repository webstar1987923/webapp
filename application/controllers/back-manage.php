<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of manage
 *
 * @author Suchandan
 */
class Manage extends CI_Controller {
    
    private  $update = false;


    public function __construct() {
        parent::__construct();
        $this->s_lib->checkMethodAccess($this->session->userdata('user_type'), $this->router->fetch_class(), $this->router->fetch_method());
    }
    private function redirectTO($slug = 'dashboard',$msg = '',$status = 200) {
        $class = 'success';
        if($status == 400){
            $class = 'danger';
        }
        $this->session->set_flashdata('response',array('msg' => $msg , 'status' => $status , 'class' => $class));
        $url = base_url('manage/'.$slug);
        redirect($url);
    }
    private function redirectOnError($slug = 'dashboard',$msg = '') {
        $this->redirectTO($slug, $msg, 400);
    }
    private function langLine($name , $replacer = '',$subject = ''){
        
        if (($line = $this->lang->line($name)) == 'false') {
            return $name;
        } else {
            $subject = ($subject == '') ? '{object}' : $subject;
            $line = str_replace($subject, $replacer, $line);
        }
        return $line;
    }
    private function checkArrayEmptyField($fields = array(),$data = array()) {
        if( empty($fields) || empty($data) ){
            return false;
        }
        foreach($data as $key => $val){
            if(in_array($key, $fields) && empty($val)){

                return false;
            }
        }
       
        return true;
    }
    private function post($param) {
        return $this->input->post($param);
    }
    public function user($uid = '') {
        $data = array();
        $data['action'] = 1;
        $user_data = array();
        
        if($uid){
            $this->update = true;
            $data['action'] = 2;
            $lid = $this->input->get('lang_id');
            $cl = $this->s_lib->getDefaultLang();
            $cond = array('user_no' => $uid);
            $set_parent = false;
            
            if($lid){
               $cond['lang_id'] = $lid;
               $cond['parent'] = $uid;
               unset($cond['user_no']);
               $data['action'] = 1;
               $set_parent = true;
//               $user_data = $this->s_model->getData('users',array('user_no' => $uid , 'lang_id' => $lid)); 
            }elseif($cl){
                $cond['lang_id'] = $cl['ID'];
            }

            $user_data = $this->s_model->getData('users',$cond);

        }
        
        $acton = $this->post('action');
        $user  = $this->post('user');
        $meta = array();
        
        if(isset($user['meta'])){
            $meta  = $user['meta'];
            unset($user['meta']);
        }

        if($acton == 1){
            if( ! $this->checkArrayEmptyField(array('email_id','password'), $user) ){
                
                $data['user'] = $user;
                $data['meta'] = $meta;
                $data['code']['status'] = 4;
                $data['code']['class'] = 'danger';
                $data['code']['msg'] = $this->langLine('mandatory');
               
                $this->s_lib->loadView('user_reg',$data);
                return false;
                
//                $this->redirectOnError('user', $this->langLine('mandatory'));
            }elseif($user['confpassword'] != $user['password']){
                $data['user'] = $user;
                $data['meta'] = $meta;
                $data['code']['status'] = 4;
                $data['code']['class'] = 'danger';
                $data['code']['msg'] =  "Password dont match.";
               
                $this->s_lib->loadView('user_reg',$data);
                return false;
//                $this->redirectOnError('user', "Password dont match.");
            }else{
                $email_id = $user['email_id'];
                $user_ex = $this->s_model->getData('users',array('email_id'=>$email_id));
                if($user_ex){
                    $data['user'] = $user;
                    $data['meta'] = $meta;
                    $data['code']['status'] = 4;
                    $data['code']['class'] = 'danger';
                    $data['code']['msg'] = $this->langLine('email_exists');

                    $this->s_lib->loadView('user_reg',$data);
                    return false;
//                    $this->redirectOnError('user', $this->langLine('email_exists'));
                }
                unset($user['confpassword']);
                $user['password_hint'] = $user['password'];
                $user['password'] = md5($user['password']);
            }
            $user = $this->setLangData();
            $user_no = $this->s_model->insertRecord('users',$user);
            $this->redirectTO('user/'.$user_no, $this->langLine('user_registered'));
            
        }elseif($acton == 2){
            if( ! $this->checkArrayEmptyField(array('email_id'), $user)){

                $data['user'] = $user;
                $data['meta'] = $meta;
                $data['code']['status'] = 4;
                $data['code']['class'] = 'danger';
                $data['code']['msg'] = $this->langLine('mandatory');
               
                $this->s_lib->loadView('user_reg',$data);
                return false;
            }
            if(empty($user_data)){
                $this->redirectOnError('user/'.$uid, $this->langLine('user_not_exists'));
            }
            $this->s_model->show_query = 1;
            $this->s_model->updateRecord('users',array('user_no' => $uid),$user);
            $this->redirectTO('user/'.$uid, $this->langLine('user_updated'));
            
        } else{
            if($this->update == true){
                $data['user'] = $user_data;
            }
            if($set_parent && $data['action'] == 1){
                $data['parent'] = $uid;
            }

            $this->s_lib->loadView('user_reg',$data);
        }
    }
    private function setLangData($data,$uid,$lid) {
        
    }
    public function course($id = '') {
        
        $data = array();
        $data['action'] = 1;
        
        $data_value = array();
        
        if($id){
            $this->update = true;
            $data['action'] = 2;
            $data_value = $this->s_model->getData('courses',array('cid' => $id));
        }
        
        $acton = $this->post('action');
        $course  = $this->post('course');
        $meta    = array();
        
        if(isset($course['meta'])){
            $meta  = $course['meta'];
            unset($course['meta']);
        }
        if($acton == 1){
            if( ! $this->checkArrayEmptyField(array('name','desc','status'), $course) ){
                
                $data['course'] = $course;
                $data['meta']   = $meta;
                $data['code']['status'] = 4;
                $data['code']['class']  = 'danger';
                $data['code']['msg']    = $this->langLine('mandatory');
               
                $this->s_lib->loadView('course',$data);
                return false;
                
//                $this->redirectOnError('user', $this->langLine('mandatory'));
            }
            $course_id = $this->s_model->insertRecord('courses',$course);
            $this->redirectTO('course/'.$course_id, $this->langLine('user_registered', 'Course', 'User'));
        }elseif($acton == 2) {
            if( ! $this->checkArrayEmptyField(array('name','desc'), $course) ){
                
                $data['course'] = $course;
                $data['meta']   = $meta;
                $data['code']['status'] = 4;
                $data['code']['class']  = 'danger';
                $data['code']['msg']    = $this->langLine('mandatory');
               
                $this->s_lib->loadView('course',$data);
                return false;
                
//                $this->redirectOnError('user', $this->langLine('mandatory'));
            }
            $course_id = $this->s_model->updateRecord('courses',array('cid' => $id),$course);
            $this->redirectTO('course/'.$id, $this->langLine('user_updated', 'Course', 'User'));
        }else{
             if($this->update == true){
                $data['course'] = $data_value;
            }
            $this->s_lib->loadView('course',$data);
        }

    }
    public function student($id = '') {
        
        $data = array();
        $data['action'] = 1;
        
        $data_value = array();
        
        if($id){
            $this->update = true;
            $data['action'] = 2;
            $data_value = $this->s_model->getData('students',array('sid' => $id));
        }
        
        $acton = $this->post('action');
        $student  = $this->post('student');
        $meta    = array();
        
        if(isset($student['meta'])){
            $meta  = $student['meta'];
            unset($student['meta']);
        }
        if($acton == 1){
            if( ! $this->checkArrayEmptyField(array('email_id','first_name','last_name'), $student) ){
                
                $data['student'] = $student;
                $data['meta']   = $meta;
                $data['code']['status'] = 4;
                $data['code']['class']  = 'danger';
                $data['code']['msg']    = $this->langLine('mandatory');
               
                $this->s_lib->loadView('student',$data);
                return false;
                
//                $this->redirectOnError('user', $this->langLine('mandatory'));
            }
            $student_id = $this->s_model->insertRecord('students',$student);
            $this->redirectTO('student/'.$student_id, $this->langLine('user_registered', 'Student', 'User'));
        }elseif($acton == 2) {
            if( ! $this->checkArrayEmptyField(array('email_id','first_name','last_name'), $student) ){
                
                $data['student'] = $student;
                $data['meta']   = $meta;
                $data['code']['status'] = 4;
                $data['code']['class']  = 'danger';
                $data['code']['msg']    = $this->langLine('mandatory');
               
                $this->s_lib->loadView('student',$data);
                return false;
                
//                $this->redirectOnError('user', $this->langLine('mandatory'));
            }
            $student_id = $this->s_model->updateRecord('students',array('sid' => $id),$student);
            $this->redirectTO('student/'.$id, $this->langLine('user_updated', 'Student', 'User'));
        }else{
             if($this->update == true){
                $data['student'] = $data_value;
            }
            $this->s_lib->loadView('student',$data);
        }

    }
    public function lang($id = '') {
        $data = array();
        $data['action'] = 1;
        
        $data_value = array();
        
        if($id){
            $this->update = true;
            $data['action'] = 2;

            $data_value = $this->s_model->getData('lang',array('ID' => $id));
        }
        
        $acton = $this->post('action');
        $lang  = $this->post('lang');
        if($acton == 1){
            if( ! $this->checkArrayEmptyField(array('name','code'), $lang) ){
                
                $data['lang'] = $lang;
                $data['code']['status'] = 4;
                $data['code']['class']  = 'danger';
                $data['code']['msg']    = $this->langLine('mandatory');
               
                $this->s_lib->loadView('lang',$data);
                return false;
                
//                $this->redirectOnError('user', $this->langLine('mandatory'));
            }
            $lang_actives = $this->s_model->getDatas('lang',array('active' => 1));
            $langs        = $this->s_model->getDatas('lang');
            if($langs){
                if($lang_actives && $lang['active'] == 1){
                    $this->s_model->updateRecord('lang',array('active' => 1),array('active' => 0));
                }
            }
            
            $lang_id = $this->s_model->insertRecord('lang',$lang);
            $this->redirectTO('lang/'.$lang_id, $this->langLine('user_registered', 'Language', 'User'));
        }elseif($acton == 2) {
            if( ! $this->checkArrayEmptyField(array('name','code'), $lang) ){
                
                $data['lang'] = $lang;
                $data['code']['status'] = 4;
                $data['code']['class']  = 'danger';
                $data['code']['msg']    = $this->langLine('mandatory');
               
                $this->s_lib->loadView('lang',$data);
                return false;
                
//                $this->redirectOnError('user', $this->langLine('mandatory'));
            }
            $lang_actives = $this->s_model->getDatas('lang',array('active' => 1));
            $langs        = $this->s_model->getDatas('lang');
            if($langs){
                if($lang_actives && $lang['active'] == 1){
                    $this->s_model->updateRecord('lang',array('active' => 1),array('active' => 0));
                }
            }
            $lang_id = $this->s_model->updateRecord('lang',array('ID' => $id),$lang);
            $this->redirectTO('lang/'.$id, $this->langLine('user_updated', 'Language', 'User'));
        }else{
             if($this->update == true){
                $data['lang'] = $data_value;
            }
            $this->s_lib->loadView('lang',$data);
        }
    }

}
