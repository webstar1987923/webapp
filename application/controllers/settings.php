<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of settings
 *
 * @author Suchandan
 */
class settings extends My_Controller {
    
    public $settings; 

    public function __construct() {
        parent::__construct();
        $this->settings = $this->s_model->getSettings();
        $this->s_lib->checkMethodAccess($this->session->userdata('user_type'), $this->router->fetch_class(), $this->router->fetch_method());
    }
    public function index() {
        $data = array();
        $data['action'] = 1;
        $this->s_lib->loadView('settings',$data);
    }
    public function getSettings($name = '') {
        if($name == ''){
            return false;
        }
        return (isset($this->settings[$name])) ? $this->settings[$name] : false;
        
    }

}
