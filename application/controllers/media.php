<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of media
 *
 * @author Suchandan
 */
class Media extends My_Controller {

    public $data = array();

    public function __construct() {
        parent::__construct();
        $this->s_lib->checkMethodAccess($this->session->userdata('user_type'), $this->router->fetch_class(), $this->router->fetch_method());
    }

    public function index() {
        $this->auth();
        $this->data['medias'] = $this->s_model->getDatas('medias');
        $this->s_lib->loadView('media', $this->data);
    }

    public function upload() {
        $media_html = '';
        $file = $this->uploadFiles();
        if ($file) {
            $file['date'] = date('Y-m-d H:i:s');
            $media_id = $this->s_model->insertRecord('medias', $file);
            $media_html = '<div class="col-lg-2 media-item" data-id="' . $media_id . '">
                                        <a class="delete-media" data-id="' . $media_id . '">
                                            <i class="fa fa-times-circle-o fa-2x"></i>
                                        </a>
                                <div class="thumbnail">
                                    <img src="' . $file['url'] . '" />
                                </div>
                            </div>';
        }
        die($media_html);
    }
    public function remove() {
        $id = $this->post('id');
        $media = $this->s_model->getData('medias',array('ID' => $id));
        $upload_path =  $this->config->item('upload_path');
        if($media){
            if($media['file_name']){
                $file_path = $upload_path.'/'.$media['file_name'];
                unlink($file_path);
            }
            $this->s_model->deleteRecord('medias',array('ID' => $id));
            $this->s_model->deleteRecord('link_medias',array('media_id' => $id));
            die('done');
        }else{
            die('Not done');
        }
    }
    private function uploadFiles() {
        $conf = array(
            'upload_path' => $this->config->item('upload_path'),
            'max_size'  => '5000',
            'height'    => 270,
            'width'     => 500
        );
        if (isset($_FILES['file']['size']) && $_FILES['file']['size'] > intval($conf['max_size']) * 1024) {
            $data['student'] = $student;
            $data['meta'] = $meta;
            $data['code']['status'] = 4;
            $data['code']['class'] = 'danger';
            $data['code']['msg'] = "The file size exceed.";
            $this->s_lib->loadView('media', $data);
            exit;
        }
        $file_object = $_FILES['file'];
        $this->load->library('s_upload', $conf);
        $path = ($this->s_upload->pic_name) ? $this->s_upload->pic_name : '';

        $file_object['file_name'] = $this->s_upload->pic_name;

        $path = base_url('uploads') . '/' . $path;
        $file_object['url'] = $path;

        unset($file_object['error']);
        unset($file_object['tmp_name']);

        return $file_object;
    }
   
    public function removeAtttached() {
        $this->authAjax();
        $_ad_id = $this->post('adId');
        $media_id = $this->post('id');
//        $this->s_model->show_query = 1;
        $data  = $this->s_model->deleteRecord('link_medias',array(
            'object_type' => 'ad',
            'object_id' => $_ad_id,
            'media_id' => $media_id
        ));
        if($data){
            echo 'done';
        }else{
            echo 'not_done';
        }
        die();
    }

}
