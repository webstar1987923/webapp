<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of import
 *
 * @author Suchandan
 * @property s_model $s_model S_model
 */
class Import extends MY_Controller {

    public $data = array();

    public function __construct() {
        parent::__construct();
        $this->s_lib->checkMethodAccess($this->session->userdata('user_type'), $this->router->fetch_class(), $this->router->fetch_method());
    }

    public function student() {
        $this->data['heading'] = "Import students";
        $school_id = $this->session->userdata('school_id');
        if ($this->post('action') == 1) {
			$course_id = $this->post('course_id');
            if (isset($_FILES['file'])) {

                $path = $this->uploadFiles();
                
                $dataArray = $this->processfile($path);
		//$data['school_id'] = $school_id;
		
                $coloumns = $dataArray[1];
                unset($dataArray[1]);
                $data = array();
//                var_dump($dataArray);
                foreach ($dataArray as $key => $value) {
                    foreach ($value as $k => $v) {
                        if ($coloumns[$k] == 'full_name' && !is_null($v)) {
                            $data['name'] = $v;
                        }
                        if ($coloumns[$k] == 'cid' && !is_null($v)) {
                            $data['student_id'] = $v;
                        }
                    }
                    if ($data['student_id']*1) {                 
                    	$data['school_id'] = $school_id;
                        $sid = $this->addToDatabase('students', $data);
			if(!empty($sid)){
			if(!empty($course_id)){
			$stu_course = array(
				'cid'=>$course_id,
				'sid'=>$sid,
				'asssigned_by'=>$this->session->userdata('user_no'),
				'datetime'=>date("Y-m-d H:i:s")
			);
			$this->addToDatabase('student_courses', $stu_course);
			}
			}
                    }
                    $data = array();
                }
//                die('finish');
                $this->removeFile($path);
                
                $this->redirectTO('import/student', "The file imported successfully.");
            }
        }
        	
		$courses = $this->s_model->getDatas('courses',array('parent' => 0,'school_id' => $school_id));
		$this->data['courses']=$courses;
        $this->s_lib->loadView('import-data', $this->data);
    }

    public function teacher() {

        $this->data['heading'] = "Import Teacher";
	$school_id = $this->session->userdata('school_id');
        if ($this->post('action') == 1) {
            if (isset($_FILES['file'])) {

                $path = $this->uploadFiles();

                $dataArray = $this->processfile($path);
//                print_r($dataArray);
                $coloumns = $dataArray[1];
                unset($dataArray[1]);
                $data = array();

                foreach ($dataArray as $key => $value) {

                    foreach ($value as $k => $v) {
                        if ($coloumns[$k] == 'full_name' && !is_null($v)) {
                            $data['first_name'] = $v;
                            $data['user_type'] = 2;
                        }
                        if ($coloumns[$k] == 'cid' && !is_null($v)) {
                            $data['username'] = $v;
                            $data['user_type'] = 2;
                        }
                        if ($coloumns[$k] == 'fileno' && !is_null($v)) {
                            $data['password_hint'] = $v;
                            $data['password'] = md5($v);
                            $data['user_type'] = 2;
                        }
                    }

                    if (!empty($data['username'])) {
                    	$data['school_id'] = $school_id;
                        $this->addToDatabase('users', $data);
                    }
                    $data = array();
                }
                $this->removeFile($path);
                $this->redirectTO('import/teacher', "The data exported successfully from file. Now import.");
            }
        }
        $this->s_lib->loadView('import-data', $this->data);
    }

    private function set_attendance($formated, $max_sem = 7) {
//        print_r($formated);
        $cid = $this->post('cid');
        $all_users = array();
        $data = array();
        $school_id = $this->session->userdata('school_id');
        if ($formated) {
            $cid = ($cid) ? $cid : $formated[0]['cid'];

            foreach ($formated as $date => $date_array) {
                $data['datetime'] = $date;
//                $all_users = array_keys($date_array);
//                $users = $this->s_model->get_full_absent_students($all_users);
//                print_r($users);
                if ($date_array) {
                    foreach ($date_array as $sid => $attendances) {
                        $data['sid'] = $sid;
                        if ($attendances) {
//                            foreach ($attendances as $semno => $att) {
                            for ($i = 1; $i <= 7; $i++) {
                                if ($i > $max_sem) {
                                    continue;
                                }
                                $data['semno'] = $i;
                                if (isset($attendances[$i])) {
                                    $cid = $attendances[$i]['cid'];
                                    $data['attendance'] = 1;
                                } else {
                                    $data['attendance'] = 0;
                                }
                                $data['cid'] = $cid;
                                $cond = $data;
//                                print_r($data);
                                unset($cond['attendance']);
                                $ex_attendance = $this->s_model->getData('attendance', $cond);
                                if ($ex_attendance) {
//                                    $this->s_model->updateRecord('attendance', $cond, array('attendance' => $data['attendance']));
                                    continue;
                                } else {
                                $data['school_id'] = $school_id;
                                    $this->addToDatabase('attendance', $data);
                                }
                            }
//                            }
                        }
                    }
                }
//                if ($users) {
//                    foreach ($users as $u) {
//                        for ($i = 1; $i <= 7; $i++) {
//                            if ($i > $max_sem) {
//                                continue;
//                            }
//                            $data['semno'] = $i;
//                            $data['attendance'] = 0;
//                            $data['cid'] = $cid;
//                            $data['sid'] = $u['sid'];
//                            $cond = $data;
////                                print_r($data);
//                            unset($cond['attendance']);
//                            $ex_attendance = $this->s_model->getData('attendance', $cond);
//                            if ($ex_attendance) {
//                                $this->s_model->updateRecord('attendance', $cond, array('attendance' => $data['attendance']));
//                            } else {
//                                $this->addToDatabase('attendance', $data);
//                            }
//                        }
//                    }
//                }
            }
        }
    }

    private function import_atendance_updated_new($temp_datas) {

        $main_data = array();
        $max_semno = 0;

        if ($temp_datas) {
            foreach ($temp_datas as $attendance) {
                if ($max_semno < $attendance['seminer']) {
                    $max_semno = $attendance['seminer'];
                }
                $main_data[$attendance['date']][$attendance['student_id']][$attendance['seminer']] = $attendance;
            }
        }

        $this->set_attendance($main_data, $max_semno);
//        exit;
    }

    public function attendance() {

        $this->data['heading'] = "Import Attendance Step 1";
        $this->data['attendace_controller'] = 1;
        
        $school_id = $this->session->userdata('school_id');
        $this->data['courses'] = $this->s_model->getDatas('courses', array('school_id' => $school_id));
        
        $step = $this->get('step');

        $temp_atten_data = $this->s_model->get_temp_attendance();

        if ($step == 2 || count($temp_atten_data) > 0) {
            $this->data['heading'] = "Import Attendance Step 2";
            $this->data['step'] = $step = 2;
            $this->data['cid'] = $this->get('cid');

            if ($this->post('action') == 2 && $step == 2 && count($temp_atten_data) > 0) {
                $this->import_atendance_updated_new($temp_atten_data);
                $this->s_model->truncate('attendance_temp');
                $this->redirectTO('import/attendance', "The attendance is successfully imported.");
            }
        }

        if ($this->post('action') == 1) {

            $date = $this->post('date');
            $cid = $this->post('course_id');

            if (isset($_FILES['file'])) {

                $path = $this->uploadFiles();

                $dataArray = $this->processfile($path);
                $coloumns = $dataArray[1];
                unset($dataArray[1]);
//                print_r($dataArray);
                $data = array();

                foreach ($dataArray as $key => $value) {

                    foreach ($value as $k => $v) {
//                        if ($coloumns[$k] == 'Enroll ID' && !is_null($v)) {
//                            
//                        }
                        if ($coloumns[$k] == 'User ID' && !is_null($v)) {
                            $data['sid'] = $v;
                        }
                        if ($coloumns[$k] == 'Date' && !is_null($v)) {
                            $data['date'] = $v;
                        }
                        if ($date) {
                            $data['date'] = $date;
                        }
                        if ($coloumns[$k] == 'Time' && !is_null($v)) {
                            $data['time'] = $v;
                        }
                        if ($data) {
                            $data['cid'] = $cid;
                        }
                    }

                    if ($data) {
                        $data['attendance'] = 1;
                        $a = $this->s_model->getData('attendance_temp', $data);
                        if (!$a) {
                            $this->addToDatabase('attendance_temp', $data);
                        }
                    }

                    $data = array();
                }
                $this->removeFile($path);
                $this->redirectTO('import/attendance?step=2&cid=' . $cid, "The data successfully exported. Now import attendance.");
            }
        }
        $this->s_lib->loadView('import-data', $this->data);
    }

    public function parents() {

        $this->data['heading'] = "Import Parent";
        $school_id = $this->session->userdata('school_id');
        $sids_no_exists = array();
        if ($this->post('action') == 1) {
            if (isset($_FILES['file'])) {

                $path = $this->uploadFiles();

                $dataArray = $this->processfile($path);
//                print_r($dataArray);
                $coloumns = $dataArray[1];
                unset($dataArray[1]);
                $data = array();

                foreach ($dataArray as $key => $value) {

                    foreach ($value as $k => $v) {
                        if ($coloumns[$k] == 'sp_name' && !is_null($v)) {
                            $data['first_name'] = $v;
                            $data['user_type'] = 4;
                        }
                        if ($coloumns[$k] == 'cid' && !is_null($v)) {
                            $western_arabic = array('0','1','2','3','4','5','6','7','8','9');
                            $eastern_arabic = array('٠','١','٢','٣','٤','٥','٦','٧','٨','٩');
                            $data['sid'] = str_replace($eastern_arabic,$western_arabic, $v);
                            $data['user_type'] = 4;
                        }
                        if ($coloumns[$k] == 'sp_phone' && !is_null($v)) {
                            $data['phone_no'] = $v;
                        }
                        if ($coloumns[$k] == 'sp_cid' && !is_null($v)) {
                            $western_arabic = array('0','1','2','3','4','5','6','7','8','9');
                            $eastern_arabic = array('٠','١','٢','٣','٤','٥','٦','٧','٨','٩');
                            $data['username'] = str_replace($eastern_arabic,$western_arabic, $v);
                            $data['password_hint'] = $data['username'];
                            $data['password'] = md5($data['username']);
                            $data['user_type'] = 4;
                        }
                    }
                    if ($data['username']*1) {
                        $sid = ($data['sid']) ? $data['sid'] : false;
                        unset($data['sid']);
                        $data['school_id'] = $school_id;
                        $parent = $this->s_model->checkIfParentExists($school_id, $data['username']);
                        if (empty($parent)){
                            $user_no = $this->addToDatabase('users', $data);
                        }else{
                            $user_no = $parent['user_no'];
                        }
                        
                        if ($sid) {
                            $student = $this->s_model->getData('students', array('student_id' => $sid, 'school_id'=>$school_id));
                            if ($student) {
                                $assign_exists = $this->s_model->getData('student_parents', array('student_id' => $sid, 'user_no'=>$user_no));
                                if (empty($assign_exists)){
                                
                                    $this->s_model->insertRecord('student_parents', array(
                                        'student_id' => $sid,
                                        'asssigned_by' => $this->session->userdata('user_no'),
                                        'sid' => $student['sid'],
                                        'user_no' => $user_no
                                    ));
                                }
                            } else {
                                $sids_no_exists[] = $sid;
                            }
                        }
                    }
                    $data = array();
                }
                
                $this->removeFile($path);
                $this->redirectTO('import/parents', "The file imported successfully.");
            }
        }
        $this->s_lib->loadView('import-data', $this->data);
    }

    public function uploadFiles() {
        $conf = array(
            'upload_path' => $this->config->item('upload_path'),
            'max_size' => '5000',
            'pic_name' => 'xelfile' . time(),
        );
        if (isset($_FILES['file']['size']) && $_FILES['file']['size'] > intval($conf['max_size']) * 1024) {
            $data['student'] = $student;
            $data['meta'] = $meta;
            $data['code']['status'] = 4;
            $data['code']['class'] = 'danger';
            $data['code']['msg'] = "The file size exceed.";
            $this->s_lib->loadView('import-data', $data);
            exit;
        }
        $this->load->library('s_upload', $conf);
        $path = ($this->s_upload->pic_name) ? $this->s_upload->pic_name : '';
        $path = $this->config->item('upload_path') . '/' . $path;

        return $path;
    }

    public function addToDatabase($object = 'students', $data = array()) {
        if (empty($data)) {
            return false;
        }
        return $this->s_model->insertRecord($object, $data);
    }

    public function removeFile($path = '') {
        if ($path == '' || !is_file($path)) {
            return false;
        }
        return unlink($path);
    }

    public function processfile($file) {
        
        include APPPATH . 'third_party/PHPExcel/IOFactory.php';
        try {
            $objPHPExcel = PHPExcel_IOFactory::load($file);
        } catch (Exception $e) {
            return false;
//            die('Error loading file "'.pathinfo($file,PATHINFO_BASENAME).'": '.$e->getMessage());
        }

        $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
//        $arrayCount = count($allDataInSheet);  // Here get total count of row in that Excel sheet
        return $allDataInSheet;
    }

    public function set_seminer() {
//        $seminers = $this->get_seminers();
//        foreach($seminers as $seminer){
//            $this->addToDatabase('ceminer_times', $seminer);
//        }
//        echo "done";
    }

    private function get_seminers() {
        return array(
            array(
                'semno' => 1,
                'start_time' => '07:40:00',
                'end_time' => '08:25:00'
            ),
            array(
                'semno' => 2,
                'start_time' => '08:30:00',
                'end_time' => '09:15:00'
            ),
            array(
                'semno' => 3,
                'start_time' => '09:20:00',
                'end_time' => '10:05:00'
            ),
            array(
                'semno' => 0,
                'rest' => true,
                'start_time' => '10:05:00',
                'end_time' => '10:20:00'
            ),
            array(
                'semno' => 4,
                'start_time' => '10:20:00',
                'end_time' => '11:05:00'
            ),
            array(
                'semno' => 5,
                'start_time' => '11:10:00',
                'end_time' => '11:55:00'
            ),
            array(
                'semno' => 0,
                'rest' => true,
                'start_time' => '11:55:00',
                'end_time' => '12:05:00'
            ),
            array(
                'semno' => 6,
                'start_time' => '12:05:00',
                'end_time' => '12:50:00'
            ),
            array(
                'semno' => 7,
                'start_time' => '13:50:00',
                'end_time' => '03:35:00'
            ),
        );
    }

}
