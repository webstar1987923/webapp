<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of manage
 *
 * @author Suchandan
 */
class Manage extends My_Controller {

    private $update = false;

    public function __construct() {
        parent::__construct();
        $this->s_lib->checkMethodAccess($this->session->userdata('user_type'), $this->router->fetch_class(), $this->router->fetch_method());
    }

    public function user($uid = '') { //2nd parameter check that it is teacher or not 
        $data = array();
        $data['action'] = 1;
        $user_data = array();

        if ($uid) {
            $this->update = true;
            $data['action'] = 2;
            $user_data = $this->s_model->getData('users', array('user_no' => $uid));
        }

        $acton = $this->post('action');
        $user = $this->post('user');
        $meta = array();

        if (isset($user['meta'])) {
            $meta = $user['meta'];
            unset($user['meta']);
        }

        if ($acton == 1) {
            if (!$this->checkArrayEmptyField(array('email_id', 'password', 'confpassword', 'username'), $user)) {

                $data['user'] = $user;
                $data['meta'] = $meta;
                $data['code']['status'] = 4;
                $data['code']['class'] = 'danger';
                $data['code']['msg'] = $this->langLine('mandatory');

                $this->s_lib->loadView('user_reg', $data);
                return false;

//                $this->redirectOnError('user', $this->langLine('mandatory'));
            } elseif ($user['confpassword'] != $user['password']) {
                $data['user'] = $user;
                $data['meta'] = $meta;
                $data['code']['status'] = 4;
                $data['code']['class'] = 'danger';
                $data['code']['msg'] = "Password dont match.";

                $this->s_lib->loadView('user_reg', $data);
                return false;
//                $this->redirectOnError('user', "Password dont match.");
            }
//            elseif ($this->s_lib->checkSpecialCharecter($user['first_name'])) {
//                $data['user'] = $user;
//                $data['meta'] = $meta;
//                $data['code']['status'] = 4;
//                $data['code']['class'] = 'danger';
//                $data['code']['msg'] = "Special Charecter is not allowed in name.";
//
//                $this->s_lib->loadView('user_reg', $data);
//                return false;
//            } 
            else {
                $email_id = $user['email_id'];
                $username = $user['username'];
                $user_ex = $this->s_model->getData('users', array('email_id' => $email_id));
                $user_ex1 = $this->s_model->getData('users', array('username' => $username));
                if ($user_ex) {
                    $data['user'] = $user;
                    $data['meta'] = $meta;
                    $data['code']['status'] = 4;
                    $data['code']['class'] = 'danger';
                    $data['code']['msg'] = $this->langLine('email_exists');

                    $this->s_lib->loadView('user_reg', $data);
                    return false;
//                    $this->redirectOnError('user', $this->langLine('email_exists'));
                } elseif ($user_ex1) {
                    $data['user'] = $user;
                    $data['meta'] = $meta;
                    $data['code']['status'] = 4;
                    $data['code']['class'] = 'danger';
                    $data['code']['msg'] = $this->langLine('username_exists');

                    $this->s_lib->loadView('user_reg', $data);
                    return false;
//                    $this->redirectOnError('user', $this->langLine('email_exists'));
                }
                unset($user['confpassword']);
                $user['password_hint'] = $user['password'];
                $user['password'] = md5($user['password']);
            }

            $user['school_id'] = $this->input->post('school_id');

            $user_no = $this->s_model->insertRecord('users', $user);
            if ($user_no) {
                $this->updateMeta(1, $user_no, $meta);
            }
            $this->redirectTO('manage/user', $this->langLine('user_registered'));
        } elseif ($acton == 2) {

            //if($user['moderatorAttenEditPower']){ $moderatorAttenEditPower = $user['moderatorAttenEditPower'];}
            if ($user['user_type'] == 3)
                $user['moderatorAttenEditPower'] = ($user['moderatorAttenEditPower']) ? $user['moderatorAttenEditPower'] : 0;

            if ($user['user_type'] == 2):
                $user['TeacherAttenEditPower'] = ($user['TeacherAttenEditPower']) ? $user['TeacherAttenEditPower'] : 0;
                $user['editTimeForTeacher'] = ($user['TeacherAttenEditPower']) ? $user['editTimeForTeacher'] : 0;
            endif;


            if (!$this->checkArrayEmptyField(array('email_id', 'username'), $user)) {

                $data['user'] = $user;
                $data['meta'] = $meta;
                $data['code']['status'] = 4;
                $data['code']['class'] = 'danger';
                $data['code']['msg'] = $this->langLine('mandatory');

                $this->s_lib->loadView('user_reg', $data);
                return false;
            }
//            elseif ($this->s_lib->checkSpecialCharecter($user['first_name'])) {
//                $data['user'] = $user;
//                $data['meta'] = $meta;
//                $data['code']['status'] = 4;
//                $data['code']['class'] = 'danger';
//                $data['code']['msg'] = "Special Charecter is not allowed in name.";
//
//                $this->s_lib->loadView('user_reg', $data);
//                return false;
//            }

            if (empty($user_data)) {
                $this->redirectOnError('manage/user/' . $uid, $this->langLine('user_not_exists'));
            }
            if ($uid) {
                $this->updateMeta(1, $uid, $meta);
            }

            if (empty($user['password'])) {
                unset($user['password']);
            } else {
                if ($user['confpassword'] != $user['password']) {
                    $data['user'] = $user;
                    $data['meta'] = $meta;
                    $data['code']['status'] = 4;
                    $data['code']['class'] = 'danger';
                    $data['code']['msg'] = "Password dont match.";

                    $this->s_lib->loadView('user_reg', $data);
                    return false;
                    //                $this->redirectOnError('user', "Password dont match.");
                }
                $user['password_hint'] = $user['password'];
                $user['password'] = md5($user['password']);
            }
            unset($user['confpassword']);

//            $this->s_model->show_query = 1;
            $this->s_model->updateRecord('users', array('user_no' => $uid), $user);
            $this->redirectTO('manage/user/' . $uid, $this->langLine('user_updated'));
        } else {
            if ($this->update == true) {
                $data['user'] = $user_data;
//                $this->s_model->show_query = 1;
                $data['meta'] = $this->s_model->getMetas(1, $uid);
            }
            $this->s_lib->loadView('user_reg', $data);
        }
    }

    public function course($id = '') {

        $data = array();
        $data['action'] = 1;
        $data['new_lang_varient'] = false;

        $data_value = array();

        if ($id) {
            $this->update = true;
            $data['action'] = 2;
            $data_value = $this->s_model->getData('courses', array('cid' => $id));
        }

        $acton = $this->post('action');
        $course = $this->post('course');
        $meta = array();
        $lid = $this->get('lang_id');
        if ($lid && $id) {
            $cond = array('parent' => $id, 'lang_id' => $lid);
            $data['new_lang_varient'] = true;
            if ($this->get('update') == 1) {
                $cond['cid'] = $this->get('varient_no');
            }
            $varient = $this->s_model->getData('courses', $cond);
            if ($varient && !$this->input->get('update') && !$this->input->get('varient_no')) {
                redirect(base_url('manage/course/' . $id . '?update=1&varient_no=' . $varient['cid'] . '&lang_id=' . $lid));
            }
            if (empty($varient)) {
                $this->update = false;
                $data['action'] = 1;
                $data['parent'] = $id;
                $data['lang_id'] = $lid;
                $data['varient_new'] = true;
            } else {
                $data_value = $varient;
            }
        }
        if (isset($course['meta'])) {
            $meta = $course['meta'];
            unset($course['meta']);
        }
        if ($acton == 1) {
            if (!$this->checkArrayEmptyField(array('name', 'status'), $course)) {

                $data['course'] = $course;
                $data['meta'] = $meta;
                $data['code']['status'] = 4;
                $data['code']['class'] = 'danger';
                $data['code']['msg'] = $this->langLine('mandatory');

                $this->s_lib->loadView('course', $data);
                return false;
            }
//            elseif ($this->s_lib->checkSpecialCharecter($course['name']) || $this->s_lib->checkSpecialCharecter($course['name'])) {
//                $data['course'] = $course;
//                $data['meta'] = $meta;
//                $data['code']['status'] = 4;
//                $data['code']['class'] = 'danger';
//                $data['code']['msg'] = "Special Charecter is not allowed in name.";
//
//                $this->s_lib->loadView('course', $data);
//                return false;
//            }
            if ($lid) {
                list($rest, $course_varient) = $this->s_lib->setVarientData($course, $id, $lid, 'course');
                if ($rest === false) {
                    $data['student'] = $student;
                    $data['meta'] = $meta;
                    $data['code']['status'] = 4;
                    $data['code']['class'] = 'danger';
                    $anc = '<a href="' . base_url('manage/course/' . $id . '?update=1&varient_no=' . $course_varient['sid'] . '&lang_id=' . $lid) . '">Edit this</a>';
                    $data['code']['msg'] = "This language varient already exists $anc .";

                    $this->s_lib->loadView('student', $data);
                    return false;
                }
                $course = $course_varient;
            }
            $course['school_id'] = $this->input->post('school_id');
            $course_id = $this->s_model->insertRecord('courses', $course);
            $redirect = 'manage/course';
            if ($lid) {
                $course_id = $course['parent'] . '?update=1&varient_no=' . $course_id . '&lang_id=' . $lid;
                $redirect = 'manage/course/' . $course_id;
                $this->updateMeta(3, $id, $meta);
            } else {
                $this->updateMeta(3, $course_id, $meta);
            }
            $this->redirectTO($redirect, $this->langLine('user_registered', 'Course', 'User'));
        } elseif ($acton == 2) {

            if (!$this->checkArrayEmptyField(array('name'), $course)) {

                $data['course'] = $course;
                $data['meta'] = $meta;
                $data['code']['status'] = 4;
                $data['code']['class'] = 'danger';
                $data['code']['msg'] = $this->langLine('mandatory');

                $this->s_lib->loadView('course', $data);
                return false;
            }
//            elseif ($this->s_lib->checkSpecialCharecter($course['name']) || $this->s_lib->checkSpecialCharecter($course['name'])) {
//                $data['course'] = $course;
//                $data['meta'] = $meta;
//                $data['code']['status'] = 4;
//                $data['code']['class'] = 'danger';
//                $data['code']['msg'] = "Special Charecter is not allowed in name.";
//
//                $this->s_lib->loadView('course', $data);
//                return false;
//            }
            $rdirect = 'manage/course/' . $id;
            $id_for_meta = $id;
            if ($this->update == true && $data['new_lang_varient']) {
                $id = $this->get('varient_no');
                $rdirect = $rdirect . '?update=1&varient_no=' . $id . '&lang_id=' . $lid;
            }
            $course_id = $this->s_model->updateRecord('courses', array('cid' => $id), $course);
            if ($id) {
                $this->updateMeta(3, $id_for_meta, $meta);
            }
            $this->redirectTO($rdirect, $this->langLine('user_updated', 'Course', 'User'));
        } else {
            if ($this->update == true) {
                $data['course'] = $data_value;
                $data['meta'] = $this->s_model->getMetas(3, $id);
            }
            $this->s_lib->loadView('course', $data);
        }
    }

    public function school($id = '') {

        $data = array();
        $data['action'] = 1;
        $data['new_lang_varient'] = false;

        $data_value = array();

        if ($id) {
            $this->update = true;
            $data['action'] = 2;
            $data_value = $this->s_model->getData('school', array('id' => $id));
        }

        $acton = $this->post('action');
        $school = $this->post('school');
        $meta = array();
        $lid = $this->get('lang_id');
        if ($lid && $id) {
            $cond = array('parent' => $id, 'lang_id' => $lid);
            $data['new_lang_varient'] = true;
            if ($this->get('update') == 1) {
                $cond['id'] = $this->get('varient_no');
            }
            $varient = $this->s_model->getData('school', $cond);

            if ($varient && !$this->input->get('update') && !$this->input->get('varient_no')) {
                redirect(base_url('manage/school/' . $id . '?update=1&varient_no=' . $varient['id'] . '&lang_id=' . $lid));
            }
            if (empty($varient)) {
                $this->update = false;
                $data['action'] = 1;
                $data['parent'] = $id;
                $data['lang_id'] = $lid;
                $data['varient_new'] = true;
            } else {
                $data_value = $varient;
            }
        }
        if (isset($school['meta'])) {
            $meta = $school['meta'];
            unset($school['meta']);
        }
        if ($acton == 1) {
            if (!$this->checkArrayEmptyField(array('name', 'status'), $school)) {

                $data['school'] = $school;
                $data['meta'] = $meta;
                $data['code']['status'] = 4;
                $data['code']['class'] = 'danger';
                $data['code']['msg'] = $this->langLine('mandatory');

                $this->s_lib->loadView('school', $data);
                return false;
            }
//            elseif ($this->s_lib->checkSpecialCharecter($course['name']) || $this->s_lib->checkSpecialCharecter($course['name'])) {
//                $data['course'] = $course;
//                $data['meta'] = $meta;
//                $data['code']['status'] = 4;
//                $data['code']['class'] = 'danger';
//                $data['code']['msg'] = "Special Charecter is not allowed in name.";
//
//                $this->s_lib->loadView('course', $data);
//                return false;
//            }
            if (isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])) {
                $conf = array(
                    'upload_path' => $this->config->item('upload_path'),
                    'max_size' => '1024',
                    'pic_name' => 'news_pic' . time(),
                );
                if (isset($_FILES['file']['size']) && $_FILES['file']['size'] > intval($conf['max_size']) * 1024) {
                    $data['school'] = $school;
                    $data['meta'] = $meta;
                    $data['code']['status'] = 4;
                    $data['code']['class'] = 'danger';
                    $data['code']['msg'] = "The file size exceed.";
                    $this->s_lib->loadView('school', $data);
                    return false;
                }
                $this->load->library('s_upload', $conf);
                $school['pic'] = ($this->s_upload->pic_name) ? $this->s_upload->pic_name : '';
            }

            $school_id = $this->s_model->insertRecord('school', $school);
            $password = md5($school['password']);
            $school_data = array(
                'first_name' => $school['school_name'],
                'school_id' => $school_id,
                'password' => $password,
                'email_id' => $school['email_id'],
                'phone_no' => $school['contact'],
                'password_hint' => $school['password'],
                'user_type' => 1
            );

            $this->s_model->insertRecord('users', $school_data);
            $redirect = 'manage/school';
            if ($lid) {
                $school_id = $school['parent'] . '?update=1&varient_no=' . $school_id . '&lang_id=' . $lid;
                $redirect = 'manage/school/' . $school_id;
                $this->updateMeta(3, $id, $meta);
            } else {
                $this->updateMeta(3, $school_id, $meta);
            }
            $this->redirectTO($redirect, $this->langLine('user_registered', 'school', 'User'));
        } elseif ($acton == 2) {

            if (!$this->checkArrayEmptyField(array('name'), $school)) {

                $data['school'] = $school;
                $data['meta'] = $meta;
                $data['code']['status'] = 1;
                $data['code']['class'] = 'danger';
                $data['code']['msg'] = $this->langLine('mandatory');

                $this->s_lib->loadView('school', $data);
                return false;
            }
//            elseif ($this->s_lib->checkSpecialCharecter($course['name']) || $this->s_lib->checkSpecialCharecter($course['name'])) {
//                $data['course'] = $course;
//                $data['meta'] = $meta;
//                $data['code']['status'] = 4;
//                $data['code']['class'] = 'danger';
//                $data['code']['msg'] = "Special Charecter is not allowed in name.";
//
//                $this->s_lib->loadView('course', $data);
//                return false;
//            }
            if (isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])) {

                $conf = array(
                    'upload_path' => $this->config->item('upload_path'),
                    'max_size' => '1000',
                    'pic_name' => 'school_pic' . time(),
                );
                if (isset($_FILES['file']['size']) && $_FILES['file']['size'] > intval($conf['max_size']) * 1024) {
                    $data['school'] = $news;
                    $data['meta'] = $meta;
                    $data['code']['status'] = 4;
                    $data['code']['class'] = 'danger';
                    $data['code']['msg'] = "The file size exceed.";
                    $this->s_lib->loadView('school', $data);
                    return false;
                }
                $pic = $data_value['pic'];
                if ($pic != '') {
                    $pin_name = $pic;
                    $conf['unlink'] = true;
                    $conf['file_name'] = $pin_name;
                }
                $this->load->library('s_upload', $conf);
                $school['pic'] = ($this->s_upload->pic_name) ? $this->s_upload->pic_name : '';
            }
            $rdirect = 'manage/school/' . $id;
            $id_for_meta = $id;
            if ($this->update == true && $data['new_lang_varient']) {
                $id = $this->get('varient_no');
                $rdirect = $rdirect . '?update=1&varient_no=' . $id . '&lang_id=' . $lid;
            }
            $password = md5($school['password']);
            $school_data_updated = array(
                'first_name' => $school['school_name'],
                'password' => $password,
                'email_id' => $school['email_id'],
                'phone_no' => $school['contact'],
                'password_hint' => $school['password'],
                'user_type' => 1
            );
            $school_id = $this->s_model->updateRecord('school', array('id' => $id), $school);
            $this->s_model->updateRecord('users', array('school_id' => $id, 'user_type'=>1), $school_data_updated);
            if ($id) {
                $this->updateMeta(3, $id_for_meta, $meta);
            }
            $this->redirectTO($rdirect, $this->langLine('user_updated', 'school', 'User'));
        } else {
            if ($this->update == true) {
                $data['school'] = $data_value;
                $data['meta'] = $this->s_model->getMetas(3, $id);
            }
            $this->s_lib->loadView('school', $data);
        }
    }

    public function plan($id = '') {

        $data = array();
        $data['action'] = 1;
        $data['new_lang_varient'] = false;

        $data_value = array();

        if ($id) {
            $this->update = true;
            $data['action'] = 2;
            $data_value = $this->s_model->getData('plans', array('id' => $id));
        }

        $acton = $this->post('action');
        $plan = $this->post('plan');
        $meta = array();
        $lid = $this->get('lang_id');
        if ($lid && $id) {
            $cond = array('parent' => $id, 'lang_id' => $lid);
            $data['new_lang_varient'] = true;
            if ($this->get('update') == 1) {
                $cond['id'] = $this->get('varient_no');
            }
            $varient = $this->s_model->getData('plans', $cond);

            if ($varient && !$this->input->get('update') && !$this->input->get('varient_no')) {
                redirect(base_url('manage/plan/' . $id . '?update=1&varient_no=' . $varient['id'] . '&lang_id=' . $lid));
            }
            if (empty($varient)) {
                $this->update = false;
                $data['action'] = 1;
                $data['parent'] = $id;
                $data['lang_id'] = $lid;
                $data['varient_new'] = true;
            } else {
                $data_value = $varient;
            }
        }
        if (isset($plan['meta'])) {
            $meta = $plan['meta'];
            unset($plan['meta']);
        }
        if ($acton == 1) {
            if (!$this->checkArrayEmptyField(array('name', 'status'), $plan)) {

                $data['plan'] = $plan;
                $data['meta'] = $meta;
                $data['code']['status'] = 4;
                $data['code']['class'] = 'danger';
                $data['code']['msg'] = $this->langLine('mandatory');

                $this->s_lib->loadView('plan', $data);
                return false;
            }
//            elseif ($this->s_lib->checkSpecialCharecter($course['name']) || $this->s_lib->checkSpecialCharecter($course['name'])) {
//                $data['course'] = $course;
//                $data['meta'] = $meta;
//                $data['code']['status'] = 4;
//                $data['code']['class'] = 'danger';
//                $data['code']['msg'] = "Special Charecter is not allowed in name.";
//
//                $this->s_lib->loadView('course', $data);
//                return false;
//            }

            $plan_id = $this->s_model->insertRecord('plans', $plan);
            $redirect = 'manage/plan';
            if ($lid) {
                $plan_id = $plan['parent'] . '?update=1&varient_no=' . $plan_id . '&lang_id=' . $lid;
                $redirect = 'manage/plan/' . $plan_id;
                $this->updateMeta(3, $id, $meta);
            } else {
                $this->updateMeta(3, $plan_id, $meta);
            }
            $this->redirectTO($redirect, $this->langLine('user_registered', 'plan', 'User'));
        } elseif ($acton == 2) {

            if (!$this->checkArrayEmptyField(array('name'), $plan)) {

                $data['plan'] = $plan;
                $data['meta'] = $meta;
                $data['code']['status'] = 1;
                $data['code']['class'] = 'danger';
                $data['code']['msg'] = $this->langLine('mandatory');

                $this->s_lib->loadView('plan', $data);
                return false;
            }
//            elseif ($this->s_lib->checkSpecialCharecter($course['name']) || $this->s_lib->checkSpecialCharecter($course['name'])) {
//                $data['course'] = $course;
//                $data['meta'] = $meta;
//                $data['code']['status'] = 4;
//                $data['code']['class'] = 'danger';
//                $data['code']['msg'] = "Special Charecter is not allowed in name.";
//
//                $this->s_lib->loadView('course', $data);
//                return false;
//            }
            $rdirect = 'manage/plan/' . $id;
            $id_for_meta = $id;
            if ($this->update == true && $data['new_lang_varient']) {
                $id = $this->get('varient_no');
                $rdirect = $rdirect . '?update=1&varient_no=' . $id . '&lang_id=' . $lid;
            }
            $plan_id = $this->s_model->updateRecord('plans', array('id' => $id), $plan);
            if ($id) {
                $this->updateMeta(3, $id_for_meta, $meta);
            }
            $this->redirectTO($rdirect, $this->langLine('user_updated', 'plan', 'User'));
        } else {
            if ($this->update == true) {
                $data['plan'] = $data_value;
                $data['meta'] = $this->s_model->getMetas(3, $id);
            }
            $this->s_lib->loadView('plan', $data);
        }
    }

    public function news($id = '') {

        $data = array();
        $data['action'] = 1;
        $data['new_lang_varient'] = false;

        $data_value = array();

        if ($id) {
            $this->update = true;
            $data['action'] = 2;
            $data_value = $this->s_model->getData('news', array('id' => $id));
        }

        $acton = $this->post('action');
        $news = $this->post('news');

        $meta = array();
        $lid = $this->get('lang_id');
        if ($lid && $id) {
            $cond = array('parent' => $id, 'lang_id' => $lid);
            $data['new_lang_varient'] = true;
            if ($this->get('update') == 1) {
                $cond['id'] = $this->get('varient_no');
            }
            $varient = $this->s_model->getData('news', $cond);

            if ($varient && !$this->input->get('update') && !$this->input->get('varient_no')) {
                redirect(base_url('manage/news/' . $id . '?update=1&varient_no=' . $varient['id'] . '&lang_id=' . $lid));
            }
            if (empty($varient)) {
                $this->update = false;
                $data['action'] = 1;
                $data['parent'] = $id;
                $data['lang_id'] = $lid;
                $data['varient_new'] = true;
            } else {
                $data_value = $varient;
            }
        }
        if (isset($news['meta'])) {
            $meta = $news['meta'];
            unset($news['meta']);
        }
        if ($acton == 1) {
            if (!$this->checkArrayEmptyField(array('name', 'status'), $news)) {

                $data['news'] = $news;
                $data['meta'] = $meta;
                $data['code']['status'] = 4;
                $data['code']['class'] = 'danger';
                $data['code']['msg'] = $this->langLine('mandatory');

                $this->s_lib->loadView('news', $data);
                return false;
            }
//            elseif ($this->s_lib->checkSpecialCharecter($course['name']) || $this->s_lib->checkSpecialCharecter($course['name'])) {
//                $data['course'] = $course;
//                $data['meta'] = $meta;
//                $data['code']['status'] = 4;
//                $data['code']['class'] = 'danger';
//                $data['code']['msg'] = "Special Charecter is not allowed in name.";
//
//                $this->s_lib->loadView('course', $data);
//                return false;
//            }

            if (isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])) {
                $conf = array(
                    'upload_path' => $this->config->item('upload_path'),
                    'max_size' => '1024',
                    'pic_name' => 'news_pic' . time(),
                    'height' => 270,
                    'width' => 500
                );
                if (isset($_FILES['file']['size']) && $_FILES['file']['size'] > intval($conf['max_size']) * 1024) {
                    $data['news'] = $news;
                    $data['meta'] = $meta;
                    $data['code']['status'] = 4;
                    $data['code']['class'] = 'danger';
                    $data['code']['msg'] = "The file size exceed.";
                    $this->s_lib->loadView('news', $data);
                    return false;
                }
                $this->load->library('s_upload', $conf);
                $news['pic'] = ($this->s_upload->pic_name) ? $this->s_upload->pic_name : '';
            }

            $news_id = $this->s_model->insertRecord('news', $news);
            $redirect = 'manage/news';
            if ($lid) {
                $news_id = $plan['parent'] . '?update=1&varient_no=' . $plan_id . '&lang_id=' . $lid;
                $redirect = 'manage/news/' . $news_id;
                $this->updateMeta(3, $id, $meta);
            } else {
                $this->updateMeta(3, $news_id, $meta);
            }
            $this->redirectTO($redirect, $this->langLine('user_registered', 'news', 'User'));
        } elseif ($acton == 2) {

            if (!$this->checkArrayEmptyField(array('title'), $news)) {

                $data['news'] = $news;
                $data['meta'] = $meta;
                $data['code']['status'] = 1;
                $data['code']['class'] = 'danger';
                $data['code']['msg'] = $this->langLine('mandatory');

                $this->s_lib->loadView('news', $data);
                return false;
            }
//            elseif ($this->s_lib->checkSpecialCharecter($course['name']) || $this->s_lib->checkSpecialCharecter($course['name'])) {
//                $data['course'] = $course;
//                $data['meta'] = $meta;
//                $data['code']['status'] = 4;
//                $data['code']['class'] = 'danger';
//                $data['code']['msg'] = "Special Charecter is not allowed in name.";
//
//                $this->s_lib->loadView('course', $data);
//                return false;
//            }

            if (isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])) {

                $conf = array(
                    'upload_path' => $this->config->item('upload_path'),
                    'max_size' => '1024',
                    'pic_name' => 'news_pic' . time(),
                );
                if (isset($_FILES['file']['size']) && $_FILES['file']['size'] > intval($conf['max_size']) * 1024) {
                    $data['news'] = $news;
                    $data['meta'] = $meta;
                    $data['code']['status'] = 4;
                    $data['code']['class'] = 'danger';
                    $data['code']['msg'] = "The file size exceed.";
                    $this->s_lib->loadView('news', $data);
                    return false;
                }
                $pic = $data_value['pic'];
                if ($pic != '') {
                    $pin_name = $pic;
                    $conf['unlink'] = true;
                    $conf['file_name'] = $pin_name;
                }
                $this->load->library('s_upload', $conf);
                $news['pic'] = ($this->s_upload->pic_name) ? $this->s_upload->pic_name : '';
            }
            $rdirect = 'manage/news/' . $id;
            $id_for_meta = $id;
            if ($this->update == true && $data['new_lang_varient']) {
                $id = $this->get('varient_no');
                $rdirect = $rdirect . '?update=1&varient_no=' . $id . '&lang_id=' . $lid;
            }
            $news_id = $this->s_model->updateRecord('news', array('id' => $id), $news);
            if ($id) {
                $this->updateMeta(3, $id_for_meta, $meta);
            }
            $this->redirectTO($rdirect, $this->langLine('user_updated', 'news', 'User'));
        } else {
            if ($this->update == true) {
                $data['news'] = $data_value;
                $data['meta'] = $this->s_model->getMetas(3, $id);
            }

            $data['schools'] = $this->s_model->getSchool();
            $this->s_lib->loadView('news', $data);
        }
    }

    public function student($id = '') {

        $data = array();
        $data['action'] = 1;
        $data['new_lang_varient'] = false;
        $data_value = array();

        if ($id) {
            $this->update = true;
            $data['action'] = 2;
            $data_value = $this->s_model->getData('students', array('sid' => $id));
        }
        $lid = $this->input->get('lang_id');

        if ($lid && $id) {
            $lang_data = $this->s_model->getData('students', array('sid' => $id, 'lang_id' => $lid));
            if ($lang_data) {
                redirect(base_url('manage/student/' . $id));
            }
            $data['action'] = 1;
            $data['parent'] = $id;
            $data['new_lang_varient'] = true;
            $this->update = false;
            $varient_data = $this->s_model->getData('students', array('parent' => $id, 'lang_id' => $lid));
            if ($varient_data && !$this->input->get('update') && !$this->input->get('varient_no')) {
                redirect(base_url('manage/student/' . $id . '?update=1&varient_no=' . $varient_data['sid'] . '&lang_id=' . $lid));
            }
            if ($this->input->get('update') == 1) {
                $this->update = true;
                $data['action'] = 2;
                $data['update_lang_varient'] = true;
                $varient = $this->input->get('varient_no');

                if (empty($varient_data)) {
                    $data_value = $this->s_model->getData('students', array('parent' => $id, 'lang_id' => $lid, 'sid' => $varient));
                } else {
                    $data_value = $varient_data;
                }
            }
//            $data_value = $this->s_model->getData('students',array('sid' => $id));
        }
        $acton = $this->post('action');
        $student = $this->post('student');
        $meta = array();

        if (isset($student['meta'])) {
            $meta = $student['meta'];
            unset($student['meta']);
        }
        if ($acton == 1) {
            $fields = array('name', 'student_id');
            if ($this->post('lang_varient')) {
                unset($fields['email_id']);
            }
            if (!$this->checkArrayEmptyField($fields, $student)) {

                $data['student'] = $student;
                $data['meta'] = $meta;
                $data['code']['status'] = 4;
                $data['code']['class'] = 'danger';
                $data['code']['msg'] = $this->langLine('mandatory');

                $this->s_lib->loadView('student', $data);
                return false;
            }
//            elseif ($this->s_lib->checkSpecialCharecter($student['student_id'])) {
//                $data['student'] = $student;
//                $data['meta'] = $meta;
//                $data['code']['status'] = 4;
//                $data['code']['class'] = 'danger';
//                $data['code']['msg'] = "Special Charecter is not allowed in student id.";
//
//                $this->s_lib->loadView('student', $data);
//                return false;
//            }
            $user_ex = $this->s_model->getData('students', array('student_id' => $student['student_id']));
            if ($user_ex) {
                $data['student'] = $student;
                $data['meta'] = $meta;
                $data['code']['status'] = 4;
                $data['code']['class'] = 'danger';
                $data['code']['msg'] = "Student already exists with this ID.";

                $this->s_lib->loadView('student', $data);
                return false;
            }

            if ($lid) {
                list($rest, $student_varient) = $this->s_lib->setVarientData($student, $id, $lid);
                if ($rest === false) {
                    $data['student'] = $student;
                    $data['meta'] = $meta;
                    $data['code']['status'] = 4;
                    $data['code']['class'] = 'danger';
                    $anc = '<a href="' . base_url('manage/student/' . $id . '?update=1&varient_no=' . $student_varient['sid'] . '&lang_id=' . $lid) . '">Edit this</a>';
                    $data['code']['msg'] = "This language varient already exists $anc .";

                    $this->s_lib->loadView('student', $data);
                    return false;
                }
                $student = $student_varient;
            }
//            print_r($_FILES);
            if (isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])) {
                $conf = array(
                    'upload_path' => $this->config->item('upload_path'),
                    'max_size' => '1000',
                    'pic_name' => 'student_pic' . time(),
                );
                if (isset($_FILES['file']['size']) && $_FILES['file']['size'] > intval($conf['max_size']) * 1024) {
                    $data['student'] = $student;
                    $data['meta'] = $meta;
                    $data['code']['status'] = 4;
                    $data['code']['class'] = 'danger';
                    $data['code']['msg'] = "The file size exceed.";
                    $this->s_lib->loadView('student', $data);
                    return false;
                }
                $this->load->library('s_upload', $conf);
                $student['pic'] = ($this->s_upload->pic_name) ? $this->s_upload->pic_name : '';
            }
            $student_id = $this->s_model->insertRecord('students', $student);

            if ($lid) {
                $student_id = $student['parent'] . '?update=1&varient_no=' . $student_id . '&lang_id=' . $lid;
                $this->updateMeta(2, $id, $meta);
            } else {
                $this->updateMeta(2, $student_id, $meta);
            }
            //$redirect = 'manage/student/'.$student_id;
            $redirect = 'manage/student';
            $this->redirectTO($redirect, $this->langLine('user_registered', 'Student', 'User'));
        } elseif ($acton == 2) {
            $fields = array('name', 'student_id');
            if ($this->post('lang_varient')) {
                unset($fields['email_id']);
            }
            if (!$this->checkArrayEmptyField($fields, $student)) {

                $data['student'] = $student;
                $data['meta'] = $meta;
                $data['code']['status'] = 4;
                $data['code']['class'] = 'danger';
                $data['code']['msg'] = $this->langLine('mandatory');

                $this->s_lib->loadView('student', $data);
                return false;

//                $this->redirectOnError('user', $this->langLine('mandatory'));
            }
//            elseif ($this->s_lib->checkSpecialCharecter($student['name'], $student['student_id'])) {
//                $data['student'] = $student;
//                $data['meta'] = $meta;
//                $data['code']['status'] = 4;
//                $data['code']['class'] = 'danger';
//                $data['code']['msg'] = "Special Charecter is not allowed in name.";
//
//                $this->s_lib->loadView('student', $data);
//                return false;
//            }
//            $user_ex = $this->s_model->getData('students', array('student_id' => $student['student_id']));
//            if($user_ex){
//                $data['student'] = $student;
//                $data['meta'] = $meta;
//                $data['code']['status'] = 4;
//                $data['code']['class'] = 'danger';
//                $data['code']['msg'] = "Student already exists with is ID.";
//
//                $this->s_lib->loadView('student', $data);
//                return false;
//            }
            if (isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])) {

                $conf = array(
                    'upload_path' => $this->config->item('upload_path'),
                    'max_size' => '1000',
                    'pic_name' => 'student_pic' . time(),
                );
                if (isset($_FILES['file']['size']) && $_FILES['file']['size'] > intval($conf['max_size']) * 1024) {
                    $data['student'] = $student;
                    $data['meta'] = $meta;
                    $data['code']['status'] = 4;
                    $data['code']['class'] = 'danger';
                    $data['code']['msg'] = "The file size exceed.";
                    $this->s_lib->loadView('student', $data);
                    return false;
                }
                $pic = $data_value['pic'];
                if ($pic != '') {
                    $pin_name = $pic;
                    $conf['unlink'] = true;
                    $conf['file_name'] = $pin_name;
                }
                $this->load->library('s_upload', $conf);
                $student['pic'] = ($this->s_upload->pic_name) ? $this->s_upload->pic_name : '';
            }
            $rdirect = 'manage/student/' . $id;
            $id_for_meta = $id;
            if (isset($data['update_lang_varient']) && $data['update_lang_varient']) {
                $id = $this->get('varient_no');
                $rdirect = $rdirect . '?update=1&varient_no=' . $id . '&lang_id=' . $lid;
            }
            $student_id = $this->s_model->updateRecord('students', array('sid' => $id), $student);
            if ($id) {
                $this->updateMeta(2, $id_for_meta, $meta);
            }
            $this->redirectTO($rdirect, $this->langLine('user_updated', 'Student', 'User'));
        } else {
            if ($this->update == true) {
                $data['student'] = $data_value;
//                $this->s_model->show_query = 1;
                $data['meta'] = $this->s_model->getMetas(2, $id);
            }
//            print_r($data);
            $this->s_lib->loadView('student', $data);
        }
    }

    public function lang($id = '') {
        $data = array();
        $data['action'] = 1;

        $data_value = array();

        if ($id) {
            $this->update = true;
            $data['action'] = 2;
            $data_value = $this->s_model->getData('lang', array('ID' => $id));
        }

        $acton = $this->post('action');
        $lang = $this->post('lang');
        if ($acton == 1) {
            if (!$this->checkArrayEmptyField(array('name', 'code'), $lang)) {

                $data['lang'] = $lang;
                $data['code']['status'] = 4;
                $data['code']['class'] = 'danger';
                $data['code']['msg'] = $this->langLine('mandatory');

                $this->s_lib->loadView('lang', $data);
                return false;

//                $this->redirectOnError('user', $this->langLine('mandatory'));
            }
            $lang_actives = $this->s_model->getDatas('lang', array('active' => 1));
            $langs = $this->s_model->getDatas('lang');
            if ($langs) {
                if ($lang_actives && $lang['active'] == 1) {
                    $this->s_model->updateRecord('lang', array('active' => 1), array('active' => 0));
                }
            }

            $lang_id = $this->s_model->insertRecord('lang', $lang);
            $this->redirectTO('manage/lang/' . $lang_id, $this->langLine('user_registered', 'Language', 'User'));
        } elseif ($acton == 2) {
            if (!$this->checkArrayEmptyField(array('name', 'code'), $lang)) {

                $data['lang'] = $lang;
                $data['code']['status'] = 4;
                $data['code']['class'] = 'danger';
                $data['code']['msg'] = $this->langLine('mandatory');

                $this->s_lib->loadView('lang', $data);
                return false;

//                $this->redirectOnError('user', $this->langLine('mandatory'));
            }
            $lang_actives = $this->s_model->getDatas('lang', array('active' => 1));
            $langs = $this->s_model->getDatas('lang');
            if ($langs) {
                if ($lang_actives && $lang['active'] == 1) {
                    $this->s_model->updateRecord('lang', array('active' => 1), array('active' => 0));
                }
            }
            $lang_id = $this->s_model->updateRecord('lang', array('ID' => $id), $lang);
            $this->redirectTO('manage/lang/' . $id, $this->langLine('user_updated', 'Language', 'User'));
        } else {
            if ($this->update == true) {
                $data['lang'] = $data_value;
            }
            $this->s_lib->loadView('lang', $data);
        }
    }

    public function coursegroup($group_id = 0) {
        $data = array();
        $page_title = "Create Class Group";
        if ($group_id > 0) {
            $page_title = "Edit Class Group";
        }
        //$courses=array();
        $school_id = $this->session->userdata('school_id');
        $courses = $this->s_model->getDatas('courses', array('parent' => 0, 'school_id' => $school_id));
        $data['courses'] = $courses;
        $group_name = '';
        $class_ids = array();
        $this->load->library(array('form_validation'));
        $rules = array(
            array(
                'field' => 'group_name',
                'label' => 'Group name',
                'rules' => 'required',
                'errors' => array()
            ),
                /* array(
                  'field'=>'class_ids',
                  'label'=>'Class',
                  'rules'=>'required',
                  'errors'=>array()
                  ), */
        );
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() === true) {
            // save the data 

            $group_name = $this->post('group_name');
            $group_id = $this->post('group_id');
            $class_ids = $this->post('class_ids');
            if (!empty($class_ids)) {
                $class_ids = implode(',', $class_ids);
            } else {
                $class_ids = '';
            }
            $school_id = $this->input->post('school_id');
            $savedata = array(
                'group_name' => $group_name,
                'course_ids' => $class_ids,
                'school_id' => $school_id
            );
            if ($group_id > 0) {
                // update section 
                $this->s_model->updateRecord('course_groups', array('ID' => $group_id), $savedata);
                //$this->session->set_flashdata('response','Class group edited successfully');
                redirect('lists/groups');
            } else {
                $savedata['status'] = '1';
                $savedata['datetime'] = date('Y-m-d H:i:s');
                $group_id = $this->s_model->insertRecord('course_groups', $savedata);
                if ($group_id > 0) {
                    $this->session->set_flashdata('response', 'Class group created successfully');
                    redirect('manage/coursegroup');
                }
            }
        }
        // get the group data 
        if ($group_id > 0) {
            $groupdata = $this->s_model->getData('course_groups', array('ID' => $group_id));
            if (!empty($groupdata)) {
                $course_ids = $groupdata['course_ids'];
                $group_name = $groupdata['group_name'];
                if (!empty($course_ids)) {
                    $class_ids = explode(',', $course_ids);
                }
            }
        }
        $data['group_name'] = $group_name;
        $data['class_ids'] = $class_ids;
        $data['group_id'] = $group_id;
        $data['page_title'] = $page_title;
        $this->s_lib->loadView('group_manaj', $data);
    }

    public function admin_school($id) {

        $data = array();
        $data['action'] = 1;
        $data['new_lang_varient'] = false;
        $data_value = array();
        if ($id) {
            $this->update = true;
            $data['action'] = 2;
            $data_value = $this->s_model->getData('school', array('user_id' => $id));
        }
        
        $acton = $this->post('action');
        $school = $this->post('school');
        $meta = array();
        $lid = $this->get('lang_id');
        if ($lid && $id) {
            $cond = array('parent' => $id, 'lang_id' => $lid);
            $data['new_lang_varient'] = true;
            if ($this->get('update') == 1) {
                $cond['id'] = $this->get('varient_no');
            }
            $varient = $this->s_model->getData('school', $cond);

            if ($varient && !$this->input->get('update') && !$this->input->get('varient_no')) {
                redirect(base_url('manage/admin_school/' . $id . '?update=1&varient_no=' . $varient['id'] . '&lang_id=' . $lid));
            }
            if (empty($varient)) {
                $this->update = false;
                $data['action'] = 1;
                $data['parent'] = $id;
                $data['lang_id'] = $lid;
                $data['varient_new'] = true;
            } else {
                $data_value = $varient;
            }
        }
        if (isset($school['meta'])) {
            $meta = $school['meta'];
            unset($school['meta']);
        }
        
//        if ($acton == 1) {
//            if (!$this->checkArrayEmptyField(array('name', 'status'), $school)) {
//
//                $data['school'] = $school;
//                $data['meta'] = $meta;
//                $data['code']['status'] = 4;
//                $data['code']['class'] = 'danger';
//                $data['code']['msg'] = $this->langLine('mandatory');
//
//                $this->s_lib->loadView('admin_school', $data);
//                return false;
//            }
////            elseif ($this->s_lib->checkSpecialCharecter($course['name']) || $this->s_lib->checkSpecialCharecter($course['name'])) {
////                $data['course'] = $course;
////                $data['meta'] = $meta;
////                $data['code']['status'] = 4;
////                $data['code']['class'] = 'danger';
////                $data['code']['msg'] = "Special Charecter is not allowed in name.";
////
////                $this->s_lib->loadView('course', $data);
////                return false;
////            }
//            if (isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])) {
//                $conf = array(
//                    'upload_path' => $this->config->item('upload_path'),
//                    'max_size' => '1024',
//                    'pic_name' => 'news_pic' . time(),
//                );
//                if (isset($_FILES['file']['size']) && $_FILES['file']['size'] > intval($conf['max_size']) * 1024) {
//                    $data['school'] = $school;
//                    $data['meta'] = $meta;
//                    $data['code']['status'] = 4;
//                    $data['code']['class'] = 'danger';
//                    $data['code']['msg'] = "The file size exceed.";
//                    $this->s_lib->loadView('school', $data);
//                    return false;
//                }
//                $this->load->library('s_upload', $conf);
//                $school['pic'] = ($this->s_upload->pic_name) ? $this->s_upload->pic_name : '';
//            }
//            
//            $school_id = $this->s_model->insertRecord('school', $school);
//            $password = md5($school['password']);
//            $school_data = array(
//                'first_name' => $school['school_name'],
//                'school_id' => $school_id,
//                'password' => $password,
//                'email_id' => $school['email_id'],
//                'phone_no' => $school['contact'],
//                'password_hint' => $school['password'],
//                'user_type' => 1
//            );
//
//            $this->s_model->insertRecord('users', $school_data);
//            $redirect = 'manage/admin_school';
//            if ($lid) {
//                $school_id = $school['parent'] . '?update=1&varient_no=' . $school_id . '&lang_id=' . $lid;
//                $redirect = 'manage/admin_school/' . $school_id;
//                $this->updateMeta(3, $id, $meta);
//            } else {
//                $this->updateMeta(3, $school_id, $meta);
//            }
//            $this->redirectTO($redirect, $this->langLine('user_registered', 'admin_school', 'User'));
//        } else
        if ($acton == 2) {
            
            if (!$this->checkArrayEmptyField(array('name'), $school)) {

                $data['school'] = $school;
                $data['meta'] = $meta;
                $data['code']['status'] = 1;
                $data['code']['class'] = 'danger';
                $data['code']['msg'] = $this->langLine('mandatory');

                $this->s_lib->loadView('admin_school', $data);
                return false;
            }
//            elseif ($this->s_lib->checkSpecialCharecter($course['name']) || $this->s_lib->checkSpecialCharecter($course['name'])) {
//                $data['course'] = $course;
//                $data['meta'] = $meta;
//                $data['code']['status'] = 4;
//                $data['code']['class'] = 'danger';
//                $data['code']['msg'] = "Special Charecter is not allowed in name.";
//
//                $this->s_lib->loadView('course', $data);
//                return false;
//            }
//            if (isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])) {
//
//                $conf = array(
//                    'upload_path' => $this->config->item('upload_path'),
//                    'max_size' => '1000',
//                    'pic_name' => 'school_pic' . time(),
//                );
//                if (isset($_FILES['file']['size']) && $_FILES['file']['size'] > intval($conf['max_size']) * 1024) {
//                    $data['school'] = $news;
//                    $data['meta'] = $meta;
//                    $data['code']['status'] = 4;
//                    $data['code']['class'] = 'danger';
//                    $data['code']['msg'] = "The file size exceed.";
//                    $this->s_lib->loadView('admin_school', $data);
//                    return false;
//                }
//                $pic = $data_value['pic'];
//                if ($pic != '') {
//                    $pin_name = $pic;
//                    $conf['unlink'] = true;
//                    $conf['file_name'] = $pin_name;
//                }
//                $this->load->library('s_upload', $conf);
//                $school['pic'] = ($this->s_upload->pic_name) ? $this->s_upload->pic_name : '';
//            }
            $rdirect = 'manage/admin_school/' . $id;
            $id_for_meta = $id;
            
            if ($this->update == true && $data['new_lang_varient']) {
                $id = $this->get('varient_no');
                $rdirect = $rdirect . '?update=1&varient_no=' . $id . '&lang_id=' . $lid;
            }
            $password = md5($school['password_hint']);
            $school_data_updated = array(
                'first_name' => $school['school_name'],
                'password' => $password,
                'email_id' => $school['email_id'],
                'phone_no' => $school['contact'],
                'password_hint' => $school['password_hint'],
                'user_type' => 1
            );
            
            $school_id = $this->s_model->updateRecord('school', array('id' => $id), $school);
            $this->s_model->updateRecord('users', array('school_id' => $id), $school_data_updated);
            if ($id) {
                $this->updateMeta(3, $id_for_meta, $meta);
            }
            $this->redirectTO($rdirect, $this->langLine('user_updated', 'admin_school', 'User'));
        } else {
            if ($this->update == true) {
                $data['school'] = $data_value;
                $data['meta'] = $this->s_model->getMetas(3, $id);
            }
            $this->s_lib->loadView('admin_school', $data);
        }
    }

}
