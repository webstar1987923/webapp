<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ajax
 *
 * @author Suchandan
 */
class Ajax extends My_Controller {

    public $response;
    public $type;

    public function __construct() {
        parent::__construct(true);
        $session = $this->authAjax();
        if (!$session) {
            die('Session expired.');
        }
        $this->response['session'] = $session;
    }

    public function delete($type = '') {

        $table = '';
        $id = '';
        $load_url = '';
        $_caller = '';
        $delete_ids = $this->post('delete_ids');
        if ($type == '') {
            return false;
        }

        $this->type = $type;

        switch ($type) {
            case 'course':
                $table = 'courses';
                $id = 'cid';
                $_caller = 'course';
                $load_url = base_url('lists/course');
                break;
            case 'student':
                $table = 'students';
                $id = 'sid';
                $_caller = 'student';
                $load_url = base_url('lists/student');
                break;
            case 'teachers':
                $table = 'users';
                $id = 'user_no';
                $_caller = 'user';
                $load_url = base_url('lists/teachers');
                break;
            case 'modarators':
                $table = 'users';
                $id = 'user_no';
                $_caller = 'user';
                $load_url = base_url('lists/modarators');
                break;
            case 'user':
                $table = 'users';
                $id = 'user_no';
                $_caller = 'user';
                $load_url = base_url('lists/user');
                break;
            case 'parents':
                $table = 'users';
                $id = 'user_no';
                $_caller = 'user';
                $load_url = base_url('lists/parents');
                break;
            case 'admin':
                $table = 'users';
                $id = 'user_no';
                $_caller = 'user';
                $load_url = base_url('lists/admin');
                break;

            default:
                break;
        }
        if (!empty($delete_ids)) {
            foreach ($delete_ids as $did) {
                $this->s_model->deleteRecord($table, array($id => $did));
                call_user_func_array(array($this, $_caller), array($did));
            }
        }
        echo $load_url;
    }

    public function student($id) {
        $this->s_model->deleteRecord('students', array('sid' => $id));
        $this->s_model->deleteRecord('student_courses', array('sid' => $id));
        $this->s_model->deleteRecord('metadata', array('meta_type' => 2, 'object_id' => $id));
//        redirect(base_url('lists/student'));
    }
    
    function getCourseStudents($course){
        $ret = array();
        if ($course*1){
            $ret = $this->s_model->getCourseStudents($course);
        }
        header('Content-Type: application/json');
        print json_encode($ret);
        exit(0);
    }
    
    public function course($id) {
        $this->s_model->deleteRecord('courses', array('cid' => $id));
        $this->s_model->deleteRecord('teacher_courses', array('cid' => $id));
        $this->s_model->deleteRecord('student_courses', array('cid' => $id));
        $this->s_model->deleteRecord('teacher_seminers', array('cid' => $id));
        $this->s_model->deleteRecord('metadata', array('meta_type' => 3, 'object_id' => $id));
//        redirect(base_url('lists/course'));
    }

    public function user($id) {
        $this->s_model->deleteRecord('users', array('user_no' => $id));
//        redirect(base_url('lists/user'));
    }

    public function teacher($id) {
        $this->s_model->deleteRecord('users', array('user_no' => $id));
        $this->s_model->deleteRecord('teacher_courses', array('user_no' => $id));
        $this->s_model->deleteRecord('teacher_seminers', array('user_no' => $id));
        $this->s_model->deleteRecord('metadata', array('meta_type' => 1, 'object_id' => $id));
//        redirect(base_url('lists/teacher'));
    }
    public function parents($id) {
        $this->s_model->deleteRecord('student_parents', array('user_no' => $id));
        $this->s_model->deleteRecord('users', array('user_no' => $id));
//        redirect(base_url('lists/teacher'));
    }

    public function lang($id) {
        $this->s_model->deleteRecord('lang', array('ID' => $id));
        $this->s_model->deleteRecord('courses', array('lang_id' => $id));
        $this->s_model->deleteRecord('students', array('lang_id' => $id));
//        redirect(base_url('lists/lang'));
    }
    public function adssort() {
        $data = $this->post('data');
        $this->s_model->saveAdsSort($data);
//        redirect(base_url('lists/lang'));
    }

    
    
    public function assign($type = 'course') {
        $post_students = $this->post('delete_ids');
        $cid = $this->post('course_id');
        $school_id = $this->session->userdata('school_id');
        if ($cid && $post_students) {
            if (!empty($post_students)) {
//                $this->s_model->deleteRecord('student_courses', array('cid' => $cid));
                foreach ($post_students as $cs) {
                    if ($type=="tcourse"){
                        $teacher = $this->s_model->getData('users', array('user_no' => $cs, 'school_id' => $school_id));
                        $this->s_model->deleteRecord('teacher_courses', array(
                            'user_no' => $cs
                        ));
                        foreach($cid as $ciditem){
                            $assigned_courses = $this->s_model->getDatas('teacher_courses',array('user_no' => $cs, 'cid'=>$ciditem));
                            if (empty($assigned_courses)){
                                $this->s_model->insertRecord('teacher_courses', array(
                                    'cid' => $ciditem,
                                    'user_no' => $cs,
                                    'type' => $teacher['user_type'],
                                    'assigned_by' => $this->session->userdata('user_no'),
                                    'is_allow_edit_absent' => 0,
                                    'absent_note' => 0
                                ));
                            }
                        }
                        
                    }else{
                        $assigned_courses = $this->s_model->getDatas('student_courses',array('sid' => $cs));
                        $stcs = array();
                        if(!empty($assigned_courses)){
                             foreach($assigned_courses as $course){
                                 if (empty($course['moved'])){
                                     if ($cid==$course['cid']){
                                         $this->s_model->deleteRecord('student_courses',array('ID' => $course['ID']));
                                     }
                                     $stcs[$course['cid']] = $course['cid'];
                                 }
                             }
                        }
                        if (isset($stcs[$cid])){
                            unset($stcs[$cid]);
                        }

                        if (!empty($stcs) || $cid*1){
                            if (!empty($stcs)){
                                foreach($stcs as $stc){
                                    $this->s_model->updateRecord('student_courses',array('sid' => $cs, 'cid'=>$stc*1), array('moved'=>date('Y-m-d H:i:s')));
                                }
                            }


                            $existed_courses = $this->s_model->getDatas('student_courses',array('sid' => $cs, 'cid'=>$cid, 'moved !=' => 'NULL'));
                            if (empty($existed_courses)){
                                $this->s_model->insertRecord('student_courses',array(
                                    'cid' => $cid,
                                    'sid' => $cs,
                                    'asssigned_by' => $this->session->userdata('user_no')
                                ));
                            }else{
                                $this->s_model->updateRecord('student_courses',array(
                                    'cid' => $cid,
                                    'sid' => $cs
                                    ), array('moved'=>null, 'asssigned_by' => $this->session->userdata('user_no'))
                                );
                            }
                            $this->s_model->updateRecord('attendance',array(
                                'sid' => $cs
                                ), array('cid' => $cid)
                            );

                        }

                        $this->s_model->updateRecord('attendance',array(
                                'sid' => $cs
                                ), array('cid' => $cid)
                            );
                    }
                    
                    
                }
            }
        }
        if ($type=="tcourse"){
            die(base_url('lists/teachers'));
        }else{
            die(base_url('lists/student'));
        }
        
    }
    public function getMedia() {
        
    }
    
    
    
    function saveStudentExcused(){
        $ret = array();
        
        $student = $this->post('student');
        $dates = $this->post('dates');
        $cid = $this->post('cid');
        $note = 'غياب بعذر مقبول';
        if ($student*1){
            $medical_days = array();
            $medical_data = $this->s_model->getDatas('student_medical', array('sid' => $student, 'cid' => $cid), array('list_days','amount_days','end_date', 'start_date', 'id'), array(),array(), 'start_date');
            foreach($medical_data as $medical_item){
                $m_days = unserialize($medical_item['list_days']);
                foreach($m_days as $m_day){
                    $medical_days[$m_day] = $m_day; 
                }
            }
            $this->s_model->clearStudentAttendanceExcused($cid, $student, $medical_days);
            $notes_data = $this->s_model->getDatas('absent_note', array('sid' => $student, 'cid' => $cid), array('note', 'date', 'ID'));
            foreach($notes_data as $note_item){

                $query = "SELECT * "
                        . "FROM abs_student_medical "
                        . "WHERE cid=".$cid." and sid=".$student." and '".$note_item['date']."' BETWEEN start_date AND end_date";
                $courses = $this->s_model->groupClassesName($query);
                $medical_exists = $courses->result_array();
                $query = "SELECT * "
                        . "FROM abs_student_suspend "
                        . "WHERE cid=".$cid." and sid=".$student." and '".$note_item['date']."' BETWEEN start_date AND end_date";
                $courses = $this->s_model->groupClassesName($query);
                $suspend_exists = $courses->result_array();
                if (empty($medical_exists) && empty($suspend_exists)){
                    $this->s_model->deleteRecord('absent_note', array(
                        'cid' => $cid,
                        'sid' => $student,
                        'date' => $note_item['date']
                    ));
                }
            }
            if (!empty($dates)){
                foreach($dates as $date){
                    
                    $query = "SELECT * "
                            . "FROM abs_student_medical "
                            . "WHERE cid=".$cid." and sid=".$student." and '".$date."' BETWEEN start_date AND end_date";
                    $courses = $this->s_model->groupClassesName($query);
                    $medical_exists = $courses->result_array();
                    $query = "SELECT * "
                            . "FROM abs_student_suspend "
                            . "WHERE cid=".$cid." and sid=".$student." and '".$date."' BETWEEN start_date AND end_date";
                    $courses = $this->s_model->groupClassesName($query);
                    $suspend_exists = $courses->result_array();
                    
                    if (empty($medical_exists) && empty($suspend_exists)){
                        $this->s_model->setStudentAttendanceExcused($student, $date);
                        $this->s_model->insertRecord('absent_note', array(
                            'cid' => $cid,
                            'sid' => $student,
                            'note' => $note,
                            'date' => $date,
                            'created_by' => $this->session->userdata('user_no')
                        ));
                    }
                }
            }
            
        }
        $ret['note'] = $note;
        header('Content-Type: application/json');
        print json_encode($ret);
        exit(0);
    }
    function saveStudentNote(){
        $ret = array();
        
        $student = $this->post('student');
        $date = $this->post('day');
        $cid = $this->post('cid');
        $note = $this->post('note');
        
        if ($student*1){
            $this->s_model->deleteRecord('absent_note', array(
                'cid' => $cid,
                'sid' => $student,
                'date' => $date
            ));
            if (!empty($note)){
                $this->s_model->insertRecord('absent_note', array(
                    'cid' => $cid,
                    'sid' => $student,
                    'note' => $note,
                    'date' => $date,
                    'created_by' => $this->session->userdata('user_no')
                ));
            }
        }
        header('Content-Type: application/json');
        print json_encode($ret);
        exit(0);
    }
    function saveStudentSuspend(){
        $ret = array();
        
        $student = $this->post('student');
        $amount_days = $this->post('days');
        $start_date = $this->post('start_date');
        $cid = $this->post('cid');
        $dates = array();
        $school_id = $this->session->userdata('school_id');
        
        $course = $this->s_model->getData('courses', array('cid' => $cid, 'school_id'=>$school_id));
        $suspend_data = array();
        $medical_exists_dates = array();
        if ($amount_days*1>0){
            $end_date = $start_date;
            do{
                $query = "SELECT * "
                        . "FROM abs_student_medical "
                        . "WHERE cid=".$cid." and sid=".$student." and '".$end_date."' BETWEEN start_date AND end_date";
                $courses = $this->s_model->groupClassesName($query);
                $medical_exists = $courses->result_array();
                
                
                if (empty($medical_exists)){
                    if (!in_array(date('w', strtotime($end_date)), array(5, 6))){
                        $dates[] = $end_date;
                    }
                }else{
                    $medical_exists_dates[] = $medical_exists;
                }
                
                if (count($dates)<$amount_days){
                    $end_date = date('Y-m-d', strtotime($end_date.'+1 day'));
                }
            }while(count($dates)<$amount_days);
            if ($student*1){
                if (empty($medical_exists_dates)){
                
                    $this->s_model->insertRecord('student_suspend', array(
                            'cid' => $cid,
                            'sid' => $student,
                            'start_date' => $start_date,
                            'end_date' => $end_date,
                            'amount_days' => $amount_days,
                            'list_days' => serialize($dates),
                            'created_by' => $this->session->userdata('user_no')
                        ));
                    foreach($dates as $date){
                        $cond = array(
                            'cid' => $cid,
                            'sid' => $student,
                            'datetime' => $date
                        );
    //                    print_r($cond);

                        $this->s_model->deleteRecord('attendance', $cond);
                        for($sem=1;$sem<=$course['semno'];$sem++){
                            $data = array();
                            $data['entered_by'] = $this->session->userdata('user_no');
                            $data['sid'] = $student;
                            $data['cid'] = $cid;
                            $data['school_id'] = $school_id;
                            $data['semno'] = $sem;
                            $data['attendance'] = 0;
                            $data['datetime'] = $date;
                            $data['api_side'] = 1;
                            $this->s_model->insertRecord('attendance', $data);
                        }

                        $this->s_model->insertRecord('absent_note', array(
                            'cid' => $cid,
                            'sid' => $student,
                            'note' => "قرار فصل سلوكي للطالب",
                            'date' => $date,
                            'created_by' => $this->session->userdata('user_no')
                        ));
                    }
                }
                $suspend_data = $this->s_model->getDatas('student_suspend', array('sid' => $student, 'cid' => $cid), array('list_days','amount_days', 'start_date', 'end_date', 'id'), array(),array(), 'start_date');
            }
        }
        $data['suspend'] = $suspend_data;
        $data['medical_exists_dates'] = $medical_exists_dates;
        print $this->load->view('suspend_history', $data, true);
        die;
    }
    function removeStudentSuspend(){
        $id = $this->post('id');
        $record = $this->s_model->getData('student_suspend', array('id' => $id));
        
        $dates = unserialize($record['list_days']);
        if (!empty($dates)){
            foreach($dates as $date){
                $cond = array(
                    'cid' => $record['cid'],
                    'sid' => $record['sid'],
                    'datetime' => $date
                );
    //                    print_r($cond);
                $this->s_model->deleteRecord('attendance', $cond);
                $this->s_model->deleteRecord('absent_note', array(
                    'cid' => $record['cid'],
                    'sid' => $record['sid'],
                    'date' => $date
                ));
            }
        }
        $this->s_model->deleteRecord('student_suspend', array('id' => $id));
        $suspend_data = $this->s_model->getDatas('student_suspend', array('sid' => $record['sid'], 'cid' => $record['cid']), array('list_days','amount_days', 'end_date','start_date', 'id'), array(),array(), 'start_date');
        $data['suspend'] = $suspend_data;
        print $this->load->view('suspend_history', $data, true);
        die;
    }
    function medicalPrintReport(){
        $sid = $this->post('student');
        $cid = $this->post('cid');
        $students = $this->s_model->getCourseStudents($cid);
        $medical_data = $this->s_model->getDatas('student_medical', array('sid' => $sid, 'cid' => $cid), array('list_days','amount_days', 'end_date','start_date', 'id'), array(),array(), 'start_date');
        
        // get all class 
        $school_id = $this->session->userdata('school_id');
        $query = "SELECT * FROM abs_courses WHERE status=1 and school_id=".($school_id*1)." ORDER by cid";
        $courses_query = $this->s_model->groupClassesName($query);
        $courses = $courses_query->result_array();
//        print_r($medical_data);die;
        foreach($students as $student){
            if ($student['sid']==$sid){
                $student_name = $student['name'];
                $studentid = $student['student_id'];
            }
        }
        foreach($courses as $course){
            if ($course['cid']==$cid){
                $course_name = $course['name'];
            }
        }
                        
        $data['cid'] = $cid;
        $data['sid'] = $sid;
        $data['medical'] = $medical_data;
        $data = array(
            'medical' => $medical_data, 
            'course_name'=>$course_name, 
            'student_name'=>$student_name, 
            'studentid'=>$studentid
        );
        
        print $this->load->view('medical_report_print', $data, true);
        die;
    }
    function suspensionPrintReport(){
        $sid = $this->post('student');
        $cid = $this->post('cid');
        $school_id = $this->session->userdata('school_id');
        $students = $this->s_model->getCourseStudents($cid);
        $suspend_data = $this->s_model->getDatas('student_suspend', array('sid' => $sid, 'cid' => $cid), array('list_days','amount_days', 'end_date','start_date', 'id'), array(),array(), 'start_date');
        
        // get all class 
        $query = "SELECT * FROM abs_courses WHERE status=1 and school_id=".($school_id*1)." ORDER by cid";
        $courses_query = $this->s_model->groupClassesName($query);
        $courses = $courses_query->result_array();
//        print_r($medical_data);die;
        foreach($students as $student){
            if ($student['sid']==$sid){
                $student_name = $student['name'];
                $studentid = $student['student_id'];
            }
        }
        foreach($courses as $course){
            if ($course['cid']==$cid){
                $course_name = $course['name'];
            }
        }
                        
        $data['cid'] = $cid;
        $data['sid'] = $sid;
        $data = array(
            'suspend' => $suspend_data, 
            'course_name'=>$course_name, 
            'student_name'=>$student_name, 
            'studentid'=>$studentid
        );
        
        print $this->load->view('student_suspension_report', $data, true);
        die;
    }
    function saveStudentMedical(){
        $ret = array();
        
        $student = $this->post('student');
        $amount_days = (int)$this->post('days');
        $start_date = $this->post('start_date');
        $cid = $this->post('cid');
        $dates = array();
        $school_id = $this->session->userdata('school_id');
        
        $course = $this->s_model->getData('courses', array('cid' => $cid));
        $suspend_data = array();
        $suspend_exists_dates = array();
        if ($amount_days*1>0){
            $end_date = $start_date;
            do{
                
                $query = "SELECT * "
                        . "FROM abs_student_suspend "
                        . "WHERE cid=".$cid." and sid=".$student." and '".$end_date."' BETWEEN start_date AND end_date";
                $courses = $this->s_model->groupClassesName($query);
                $suspend_exists = $courses->result_array();
                
                
                if (empty($suspend_exists)){
                    if (!in_array(date('w', strtotime($end_date)), array(5, 6))){
                        $dates[] = $end_date;
                    }
                }else{
                    $suspend_exists_dates[] = $suspend_exists;
                }
                if (count($dates)<$amount_days){
                    $end_date = date('Y-m-d', strtotime($end_date.'+1 day'));
                }
            }while(count($dates)<$amount_days);
            if ($student*1){
                if (empty($suspend_exists_dates)){
                    $this->s_model->insertRecord('student_medical', array(
                            'cid' => $cid,
                            'sid' => $student,
                            'start_date' => $start_date,
                            'end_date' => $end_date,
                            'amount_days' => $amount_days,
                            'list_days' => serialize($dates),
                            'created_by' => $this->session->userdata('user_no')
                        ));
                    foreach($dates as $date){
                        $cond = array(
                            'cid' => $cid,
                            'sid' => $student,
                            'datetime' => $date
                        );
    //                    print_r($cond);

                        $this->s_model->deleteRecord('attendance', $cond);
                        for($sem=1;$sem<=$course['semno'];$sem++){
                            $data = array();
                            $data['entered_by'] = $this->session->userdata('user_no');
                            $data['sid'] = $student;
                            $data['cid'] = $cid;
                            $data['excused'] = 1;
                            $data['semno'] = $sem;
                            $data['school_id'] = $school_id;
                            $data['attendance'] = 0;
                            $data['datetime'] = $date;
                            $data['api_side'] = 1;
                            $this->s_model->insertRecord('attendance', $data);
                        }

                        $this->s_model->insertRecord('absent_note', array(
                            'cid' => $cid,
                            'sid' => $student,
                            'note' => "غياب بعذر طبي مقبول",
                            'date' => $date,
                            'created_by' => $this->session->userdata('user_no')
                        ));
                    }
                }
                $medical_data = $this->s_model->getDatas('student_medical', array('sid' => $student, 'cid' => $cid), array('list_days','amount_days','end_date', 'start_date', 'id'), array(),array(), 'start_date');
//                die;
            }
        }
        $data['medical'] = $medical_data;
        $data['suspend_exists_dates'] = $suspend_exists_dates;
        print $this->load->view('medical_history', $data, true);
        die;
    }
    function removeStudentMedical(){
        $id = $this->post('id');
        $record = $this->s_model->getData('student_medical', array('id' => $id));
        
        $dates = unserialize($record['list_days']);
        if (!empty($dates)){
            foreach($dates as $date){
                $cond = array(
                    'cid' => $record['cid'],
                    'sid' => $record['sid'],
                    'datetime' => $date
                );
    //                    print_r($cond);
                $this->s_model->deleteRecord('attendance', $cond);
                $this->s_model->deleteRecord('absent_note', array(
                    'cid' => $record['cid'],
                    'sid' => $record['sid'],
                    'date' => $date
                ));
            }
        }
        $this->s_model->deleteRecord('student_medical', array('id' => $id));
        $medical_data = $this->s_model->getDatas('student_medical', array('sid' => $record['sid'], 'cid' => $record['cid']), array('list_days','amount_days','end_date', 'start_date', 'id'), array(),array(), 'start_date');
        $data['medical'] = $medical_data;
        print $this->load->view('medical_history', $data, true);
        die;
    }
    
}
