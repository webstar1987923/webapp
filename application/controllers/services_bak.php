<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of services
 *
 * @author Suchandan
 */
class services extends CI_Controller {

    //Local variables for data holding
    private $response = array();
    private $data = array();
    //local variables for ids
    private $user_no;
    private $session_id;
    private $corse_id;
    private $student_id;
    //Local objects
    private $user;
    private $users;
    private $student;
    private $students;
    private $course;
    private $courses;
    //Distinct variables
    private $show_query = false;
    private $extraValidation = array();
    private $emptyFields = array();
    private $authDone = false;

    public function __construct() {
        parent::__construct();

        $this->output->set_header("Access-Control-Allow-Credentials: true");
        $this->output->set_header('Access-Control-Allow-Origin: *');
        $this->output->set_header('Access-Control-Expose-Headers: Access-Control-Allow-Origin');
        $lang_code = ($this->post('lang_code')) ? $this->post('lang_code') : 'en';
        $folder = 'english';
        if ($lang_code == 'arb') {
            $folder = 'arbian';
        }
        $this->lang->load($lang_code, $folder);
        $this->config->set_item('auth', true);
        $this->setVaribles();
    }

    public function index() {
        $this->setMessage("Bad request.");
        $this->unsetResponseData(array('success', 'session'));
        $this->output->set_status_header('400');
        $this->response();
    }

    private function post($param) {
        return $this->input->post($param);
    }

    private function setVaribles() {

        $this->response['success'] = false;
        $this->response['session'] = false;
        $this->response['msg'] = 'false';

        $this->emptyFields = array(
            'session_id',
            'user_no'
        );

        $this->user_no = $this->s_model->user_no = $this->post('user_no');
        $this->session_id = $this->s_model->session_id = $this->post('session_id');
        $this->corse_id = $this->s_model->corse_id = $this->post('corse_id');
        $this->student_id = $this->s_model->student_id = $this->post('student_id');

        $this->show_query = $this->s_model->show_query = $this->post('show_query');

        $this->s_model->data = &$this->data;
//        $this->s_model->data = &$this->data;
    }

    private function response($response = array(), $code = 200) {

        if (empty($response)) {
            $response = $this->response;
        } else {
            $response = array_merge($this->response, $response);
        }
        if ($this->post('show') == 1) {
            print_r($response);
        }
        $json_response = json_encode($response);

        $this->output->set_content_type('application/json')->set_status_header("$code")->set_output($json_response);
    }

    private function setSuccess($name = '') {
        $this->response['success'] = true;
        $this->response['msg'] = $this->langLine($name);
    }

    private function setData($name = '', $data = array()) {
        if ($name == '') {
            return false;
        }
        $this->response[$name] = $data;
    }

    private function setMessage($name = '', $lang = false) {
        if ($lang) {
            $name = $this->langLine($name);
        }
        $this->response['msg'] = $name;
    }

    private function setLangMessage($name = '') {
        $this->setMessage($name, true);
    }

    private function unsetResponseData($names = '') {
        if ($names == '') {
            return false;
        }
        if (!is_array($names)) {
            $names = array($names);
        }
        foreach ($names as $name) {
            if (isset($this->response[$name])) {
                unset($this->response[$name]);
            }
        }
        return true;
    }

    private function authinticate($extra_validation = array()) {

        $auth = $this->config->item('auth');

        if (!$auth && ENVIRONMENT == 'development') {
            $this->response['session'] = true;
            return false;
        }

        if ($this->authDone) {
            return true;
        }
		
        //Check user exists
        $this->checkUserExists();

        //Check session validation
        $this->checkSession();

        //Check extra validation if needed
        $this->extraValidations($extra_validation);

        $this->authDone = true;

        return true;
    }

    private function setSession($user_no = 0) {

        $user_id = ($user_no == 0) ? $this->user_no : $user_no;

        if (!$user_no)
            return false;

        if (( $log = $this->checkIsLoggedIn($user_no) ) === FALSE) {
            $session_id = rand(100000000, 400000000);
        } else {
            $session_id = $log['session_id'];
        }
        $this->s_model->set_session($user_id, $session_id);
        $this->setData('session_id', $session_id);
        return $session_id;
    }

    private function clearSession($user_no = '', $session_id = '') {

        $data = array();
        $data['user_no'] = ($user_no == '') ? $this->user_no : $user_no;
        $data['session_id'] = ($session_id == '') ? $this->session_id : $session_id;

        $this->s_model->deleteRecord('session', $data);
        return true;
    }

    private function checkIsLoggedIn($user_no = '', $session_id = '') {

        $data = array();
        $data['logged_in'] = 0;

        $user_no = ($user_no == '') ? $this->user_no : $user_no;
        $session_id = ($session_id == '') ? $this->session_id : $session_id;

        $data['user_no'] = $user_no;

        $cond = array();

        if ($user_no != '') {
            $cond['user_no'] = $user_no;
        }
        if ($session_id != '') {
            $cond['session_id'] = session_id;
        }

        $result = $this->s_model->getData('session', $cond);
        if (count($result) > 0) {
            $data['logged_in'] = 1;
            //$this->showOutput($data);
            return $result;
        }
        // $this->showOutput($data);
        return false;
    }

    private function extraValidations($extra) {
        if (!is_array($this->extraValidation)) {
            $this->extraValidation = array($this->extraValidation);
        }
        $extra = array_merge($this->extraValidation, $extra);
        if (empty($extra)) {
            return false;
        }
        foreach ($extra as $function) {
            if ($function != '') {
                call_user_method($function, $this);
            }
        }
    }

    private function updateTime($user_no = '', $session_id = '') {

        $data['user_no'] = ($user_no == '') ? $this->user_no : $user_no;
        $data['session_id'] = ($session_id == '') ? $this->session_id : $session_id;

        $this->s_model->updateRecord('session', $data, array('datetime' => date('Y-m-d H:i:s')));
        return true;
    }

    private function checkSession($user_no = '', $session_id = '') {

        $user_no = ($user_no == '') ? $this->user_no : $user_no;
        $session_id = ($session_id == '') ? $this->session_id : $session_id;

        $session_time = $this->getSessionExpireTime();
        $current_time = time();

        $this->response['session'] = true;
        return $session_id;

        $logOut = false;

        if ($user_no == "" || $session_id == "") {
            $this->response['msg'] = $this->langLine('logged_out');
            $logOut = true;
        } else {

            $result = $this->s_model->getData('session', array('user_no' => $user_no, 'session_id' => $session_id));
//            print_r($result);
            $tar = ( $current_time - strtotime($result['datetime']) );
//                echo $current_time.'<br>';
//                echo $tar.'<br>';
//                echo strtotime($result['datetime']).'<br>';

            if (empty($result)) {

                $this->response['msg'] = $this->langLine('logged_out');
                $logOut = true;
            } elseif ($tar >= $session_time) {
                $this->clearSession($user_no, $session_id);
                $this->response['msg'] = $this->langLine('logged_out');
                $logOut = true;
            } else {
                $this->updateTime($user_no, $session_id);
            }
        }
        if ($logOut) {
//            $this->clearSession($user_no);
            $this->_die();
            return false;
        }

        $this->response['session'] = true;
        return $session_id;
    }

    private function getSessionExpireTime() {
        return 3600;
//        return $this->config->item('sess_expiration');
    }

    private function langLine($name, $replacer = '', $subject = '') {

        if (($line = $this->lang->line($name)) == 'false') {
            return $name;
        } else {
            $subject = ($subject == '') ? '{object}' : $subject;
            $line = str_replace($subject, $replacer, $line);
        }
        return $line;
    }

    private function _die($data = array()) {
        if (!empty($data)) {
            $this->response = array_merge($this->response, $data);
        }
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Expose-Headers: Access-Control-Allow-Origin');
        header('Content-Type: application/json');
        die(json_encode($this->response));
    }

    private function checkPostEmptyFields($fields = array(), $type = 'post') {

        if (empty($fields)) {
            return false;
        }

        foreach ($fields as $field) {
            $val = $this->$type($field);
            if (empty($val)) {
                $this->setMessage("The $field can not be empty.");
                $this->_die();
            }
        }
    }

    public function checkRequiredEmptyFields($fields = array()) {
        $fields = array_merge($this->emptyFields, $fields);
        $this->checkPostEmptyFields($fields);
    }

    public function dataUnsetter($data = array(), $fields = array()) {
        if (empty($data) || empty($fields)) {
            return $data;
        }
        foreach ($fields as $field) {
            if (isset($data[$field])) {
                unset($data[$field]);
            }
        }
        return $data;
    }

    //S_controller functions 
    //From here the custom functions will come
    public function login() {

        $email = $this->post('email_id');
        $password = $this->post('password');
        $device_id = $this->post('device_id');

        if (empty($email) || empty($password)) {
            $this->setLangMessage('empty_not');
            $this->_die();
        }
        $password = md5($password);
//        $user = $this->s_model->getData('users',array('email_id' => $email,'password' => $password));
        $user = $this->s_model->checkUserExists($email, $password);
        if ($user) {
            $user['pic'] = ($user['pic']) ? $user['pic'] : 'nopic.png';
            $user['pic'] = base_url('uploads/' . $user['pic']);
            $this->setSession($user['user_no']);
            $this->updateDeviceId($user['user_no'], $user['user_type']);
            $user = $this->dataUnsetter($user, array('password', 'password_hint'));
            unset($this->response['session']);

            if ($user['user_type'] == 4) {
                $child = $this->s_model->getChildrens($user['user_no']);
                if (empty($child)) {
                    $child = false;
                }
                $user['child'] = $child;
            }

            $this->setSuccess();
            $this->setData('details', $user);
        } else {
            $this->setLangMessage('invalid_user');
            $this->_die();
        }
        $this->response();
    }

    public function updateDeviceId($user_no, $user_type) {

        $device_id = $this->post('device_id');
        $os_type = $this->post('os_type');
        $registration_id = $this->post('registration_id');
        if (empty($device_id) || empty($os_type)) {
            return false;
        }
        $dataDevice = $this->s_model->getData('devices', array('device_id' => $device_id));
        $data = $this->s_model->getData('devices', array('user_no' => $user_no, 'os_type' => $os_type, 'user_type' => $user_type));
        if ($dataDevice) {
            return false;
        }
        if ($data) {
            if ($data['device_id'] != $device_id) {
                $this->s_model->updateRecord('devices', array('user_no' => $user_no), array('device_id' => $device_id, 'registration_id' => $registration_id));
            }
        } else {
            $this->s_model->insertRecord('devices', array(
                'os_type' => $os_type,
                'device_id' => $device_id,
                'user_type' => $user_type,
                'user_no' => $user_no,
                'registration_id' => $registration_id
                    )
            );
        }
    }

    public function checkUserExists() {
        $user_no = $this->post('user_no');
		
        if (empty($user_no)) {
            $this->setMessage("Empty user no.");
            $this->_die();
        }
        $this->user = $this->s_model->getData('users', array('user_no' => $user_no));
        if (empty($this->user)) {
            $this->setLangMessage('user_not_exists');
            $this->_die();
        }
    }

    public function getCoursesByTeacher($tid) {
        $courses = $this->s_model->getTeacherCourses($tid);
        $this->data['courses'] = $courses;
        $this->data['students'] = $this->s_model->getDatas('students', array('status' => 1));
//        if($courses){
//            foreach($courses as $key => $course){
//                $courses[$key]['students'] = $this->s_model->getStudentCourses($course['cid']);
//            }
//        }
        $this->setSuccess();
        $this->response();
    }

    public function saveAttendance() {

        $this->authinticate();

        $attend_sheet = $this->post('sheet');
        $cid = $this->post('cid');
        $user_no = $this->post('user_no');
        $removal_sheet = $this->post('removal_sheet');
        $date = ($this->input->post('date')) ? $this->input->post('date') : date('Y-m-d');
        
        if ($this->user['user_type'] != 2) {
            if ($removal_sheet) {
                foreach ($removal_sheet as $rem) {
                    $cnd = array(
                        'sid' => $rem['sid'],
                        'cid' => $cid,
                        'semno' => $rem['sem'],
                        'datetime' => $date
                    );
                    $this->s_model->deleteRecord('attendance', $cnd);
                    unset($cnd['datetime']);
                    $cnd['date'] = $date;
                    $ex = $this->s_model->getData('editables', $cnd);
                    if (empty($ex)) {
                        $cnd['edited_by'] = $user_no;
                        $this->s_model->insertRecord('editables', $cnd);
                    }
                }
            }
        }
        $absents = $edited = $saved = array(); //$absents = $edited = array();
        if ($attend_sheet) {
            foreach ($attend_sheet as $key => $val) {
                $sem_no = end(explode('-', $key));
                if ($val) {
					//validate here that the sem inderded or enterd 
					/*$find_cond=array(
						'cid' => $cid,
						'semno' => $sem_no,
						'datetime' => $date
					);
					$ex_att_sm = $this->s_model->getData('attendance', $find_cond);
					if($ex_att_sm){
						// edited
						if(!in_array($sem_no,$edited) && !in_array($sem_no,$saved)){
							$edited[]=$sem_no;
						}
					}
					else{
						// entered 
						if(!in_array($sem_no,$saved) && !in_array($sem_no,$edited)){
							$saved[]=$sem_no;
						}
					}
					*/
                    foreach ($val as $k => $v) {
                        $sid = end(explode('-', $k));
                        $data['entered_by'] = $user_no;
                        $data['sid'] = $sid;
                        $data['cid'] = $cid;
                        $data['semno'] = $sem_no;
                        $data['attendance'] = $v;
                        if ($v == 0) {
                            $absents[] = $sem_no;
                        }
                        $cond = array(
                            'sid' => $sid,
                            'cid' => $cid,
                            'semno' => $sem_no,
                            'datetime' => $date
                        );
                        $ex_att = $this->s_model->getData('attendance', $cond);

                        if ($ex_att) {
                            //$edited[]=$sem_no; //$edited['sem-'.$sem_no] = true;
							if(!in_array($sem_no,$edited) && !in_array($sem_no,$saved)){
								//$edited[]=$sem_no;
							}
                            $this->s_model->updateRecord('attendance', $cond, array(
                                'attendance' => $v,
                                'updated_by' => $user_no,
                                'update_time' => date('Y-m-d H:i:s')
                            ));
                        } else {
                            $data['datetime'] = $date;
							//$saved[]=$sem_no; //$saves['sem-'.$sem_no] = true;
							if(!in_array($sem_no,$saved) && !in_array($sem_no,$edited)){
								//$saved[]=$sem_no;
							}
                            $this->s_model->insertRecord('attendance', $data);
                        }
                        unset($cond['datetime']);
                        $cond['date'] = $date;
                        $this->s_model->deleteRecord('editables', $cond);
                    }
                }
            }
        }
		//attendance 
        $edited = $this->s_model->getEditNwAttendances($cid,0, 1);
        $saveds = $this->s_model->getEditNwAttendances($cid);
		$saved=array();
		if(is_array($saveds) && count($saveds)>0){
			foreach($saveds as $key=>$semno){
				if(!in_array($semno,$edited)){
					$saved[]=$semno;
				}
			}
		}
		
        $last_sem = $this->s_model->getLastSemNo($cid);
        if ($last_sem) {
            $last_sem = $last_sem['max_cem'];
        }
        $this->setSuccess();
        $this->pushtoModarator($absents);
        $this->setData('last_sem', $last_sem);
        $this->setData('edited', $edited);
        $this->setData('saved', $saved);
        $this->setMessage($this->langLine('attendance_saved'));
        $this->response();
    }

    public function pushtoModarator($absents = array()) {

        if (empty($absents)) {
            return false;
        }

        $cid = $this->post('cid');

        $teacher = $this->user['first_name'] . ' ' . $this->user['last_name'];
        $course = $this->s_model->getData('courses', array('cid' => $cid));

//        $Sems = implode(',', $absents);
        $message = "$teacher " . $this->langLine('has_given_absent') . ' ' . $course['name'];

        $moradators = $this->s_model->getDatas('teacher_courses', array('cid' => $cid, 'type' => 3), array('user_no'));
        $registatoin_ids = array();
        if ($moradators) {
            foreach ($moradators as $moradator) {
                $data = $this->s_model->getData('devices', array('user_no' => $moradator['user_no']), array('registration_id'));
                if ($data) {
                    $registatoin_ids[] = $data['registration_id'];
                }
            }
        }
        $message = array('message' => $message);
        if ($registatoin_ids) {
            $result = $this->send_notification($registatoin_ids, $message);
//            $this->setData('push_result',$result);
//            $this->setData('IDS',$registatoin_ids);
//            $this->setData('MESSAGE',$message);
        }
    }

    public function testPush() {
        $red_id = $this->post('reg_id');
        $msg = $this->post('msg');
        $reg = array($red_id);
        $message = array('message' => $msg);
        print_r($this->send_notification($reg, $message));
    }

    function send_notification($registatoin_ids, $message = array()) {

        // Set POST variables
        $url = 'https://android.googleapis.com/gcm/send';

        $fields = array(
            'registration_ids' => $registatoin_ids,
            'data' => $message,
        );

        $headers = array(
            'Authorization: key=' . GOOGLE_API_KEY,
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }

        // Close connection
        curl_close($ch);
//        print_r($result);
        return $result;
    }

    public function getCourses() {

        $this->authinticate();
		$usersss = $this->user;
		
        $parent = false;
        if ($this->user['user_type'] == 1) {
            $admin = true;
        } elseif ($this->user['user_type'] == 4) {
            $admin = false;
            $data = $this->s_model->parentCourses();
            $this->setSuccess();
            $this->setData('courses', $data);
            $this->response();
            return false;
        } else {
            $admin = false;
        }
//        $this->s_model->show_query = 1;
	
        if ($admin) {
            $data = $this->s_model->getDatas('courses', array('parent' => 0), array('cid', 'name', 'code'));
        } else {
            $data = $this->s_model->getCourses($admin);
        }

        $this->setSuccess();
        $this->setData('courses', $data);
       // $this->setData('admin', $admin);
       // $this->setData('user_data', $this->user);
        $this->response();
    }

    public function getStudents($cid) {

        $this->authinticate();
        $user_no = $this->input->post('user_no');
        $date = $this->input->post('date');
        $parent = false;
        if ($this->user['user_type'] == 4) {
            $parent = true;
        }
        $data = $this->s_model->getStudentCourses($cid, $parent);
        $course = $this->s_model->getData('courses', array('cid' => $cid));
        if ($data) {
            foreach ($data as $key => $value) {
                $data[$key]['sheet'] = $this->s_model->getAttendances($cid, $value['sid']);
            }
        }
        $last_sem = $this->s_model->getLastSemNo($cid);
        $sem_teach = $this->s_model->getSemTeachers($cid);
        $sem_teacher = array();
        if ($sem_teach) {
            foreach ($sem_teach as $key => $val) {
                $sem_teacher['sem-' . $val['semno']] = $val;
            }
        }
        if ($last_sem) {
            $last_sem = $last_sem['max_cem'];
        }
        $sems = $this->s_model->getDatas('teacher_seminers', array('user_no' => $user_no, 'cid' => $cid), array('semno', 'cid', 'user_no'));
        $editables = $this->s_model->getDatas('editables', array('cid' => $cid, 'date' => $date));
        $editables_data = array();
        if ($editables) {
            foreach ($editables as $k => $v) {
                $editables_data['sid-' . $v['sid']]['cem-' . $v['semno']] = true;
            }
        }
        $modifications = $this->s_model->getModifiedAttendances($date);
        $mod = array();
        if($modifications){
            foreach($modifications as $mod_new){
                $mod['sem-'.$mod_new['semno']] =  $mod_new['modific_count'];
            }
        }
		// attendance count 
		$edited = $this->s_model->getEditNwAttendances($cid,0, 1);
        $saveds = $this->s_model->getEditNwAttendances($cid);
		$saved=array();
		if(is_array($saveds) && count($saveds)>0){
			foreach($saveds as $key=>$semno){
				if(!in_array($semno,$edited)){
					$saved[]=$semno;
				}
			}
		}
		$totalsems = isset($course['semno'])?$course['semno']:0;
        $this->setSuccess();
        $this->setData('editables', $editables_data);
        $this->setData('modification', $mod);
        $this->setData('semnos', $sems);
        $this->setData('totalsems', $totalsems);
        $this->setData('semteacher', $sem_teacher);
        $this->setData('students', $data);
        $this->setData('last_cem', $last_sem);
		$this->setData('edited', $edited);
        $this->setData('saved', $saved);
        //$this->setData('lsaved', $saved);
        $this->response();
    }

    public function viewStudent($sid) {
        $semi = $this->db->query("SELECT DISTINCT Sum(abs_attendance.attendance) , abs_attendance.semno, abs_attendance.datetime FROM abs_attendance WHERE abs_attendance.sid = '$sid' and abs_attendance.attendance = '0' GROUP BY abs_attendance.datetime having Sum(abs_attendance.attendance)<=6"); 
         
        $this->authinticate();
        $date = ($this->input->post('date')) ? $this->input->post('date') : date('Y-m-d');
        $cid = $this->post('cid');
        $data = $this->s_model->getData('students', array('sid' => $sid));
        if ($data) {
            $metas = $this->s_model->getMetas(2, $sid);
            $data['pic'] = ($data['pic']) ? $data['pic'] : 'nopic.png';
            $data['pic'] = base_url('uploads/' . $data['pic']);
            $data['meta'] = $metas;
            $cond = array(
                'sid' => $sid,
                
                'attendance' => 0
//                'datetime' => $date
            );
            $student_courses = $this->s_model->getData('student_courses', array('sid' => $sid), array('cid'));

            if ($cid!=''){
                $cond['cid'] = $cid;
            }

            if ($cid==''){
            $cid = $student_courses['cid'];
            }

 
            $data['course'] = $this->s_model->getData('courses', array('cid' => $cid), array('name'));
            $atts_attnd = $this->s_model->getDatas('attendance', $cond, array('*'), array(), array(), 'datetime');
            $atts_data = array();
            $date_array = array();
            $date_sems = array();
            if ($atts_attnd) {
                foreach ($atts_attnd as $key => $val) {

                    if (!in_array($val['datetime'], $date_array)) {
                        $a['date'] = $val['datetime'];
                        $a['notes'] = $this->s_model->getDatas('absent_note', array('sid' => $sid, 'cid' => $cid, 'date' => $val['datetime']), array('note', 'ID'));
                        $date_array[] = $val['datetime'];
                        $date_sems[$val['datetime']][] = $this->langLine('sem') . '-' . $val['semno'];
                        $atts_data[] = $a;
                    } else {
                        $date_sems[$val['datetime']][] = $this->langLine('sem') . '-' . $val['semno'];
                    }
                }
                foreach ($atts_data as $key => $atd) {
                    $atts_data[$key]['sem'] = implode(', ', $date_sems[$atd['date']]);
                }
            }
//            print_r($atts_data);
//            print_r($date_sems);
//            $atts = $this->s_model->getDatas('absent_note',array( 'sid' => $sid ),array('date','note'));
            $data['absents'] = $atts_data;
            //$data['absent_count'] = count($atts_data);
            $data['absent_count'] = count($semi->result_array()); // count($atts_attnd);
        }

        $this->setSuccess();
        $this->setData('details', $data);
//        $this->setData('session',false);
        $this->response();
    }

    public function deleteNote($note_id = '') {
        if ($note_id == '') {
            $this->setLangMessage('note_id_invalid');
            $this->_die();
        }
        $this->authinticate();
        $this->checkIsAdmin();

        $this->s_model->deleteRecord('absent_note', array('ID' => $note_id));

        $this->setSuccess();
        $this->setLangMessage('note_deleted');
        $this->response();
    }

    public function viewUser() {

        $this->checkRequiredEmptyFields();
        $this->authinticate();

        $user = $this->s_model->getData('users', array('user_no' => $this->user_no));

        $this->setSuccess();
        $this->setData('details', $user);

        $this->response();
    }

    public function checkIsAdmin() {
        if ($this->user['user_type'] == 2) {
            $this->setMessage($this->langLine('you_are_not_allowed'));
            $this->_die();
        }
    }

    public function manageCourse() {
        $this->authinticate();
        $this->checkIsAdmin();

        $course = $this->post('course');
        $id = $this->post('cid');
        $update = false;
        if ($id) {
            $update = true;
        }
        if ($update) {
            $this->s_model->updateRecord('courses', array('cid' => $id), $course);
        } else {
            $this->s_model->insertRecord('courses', $course);
        }
        $this->setSuccess();
        $this->setMessage('course_saved', true);
        $this->response();
    }

    public function saveStudent() {
        $this->authinticate();
        $this->checkIsAdmin();
        $sid = $this->post('sid');
        $student = $this->post('student');
        $this->s_model->updateRecord('students', array('sid' => $sid), $student);
        $this->setSuccess();
        $this->setLangMessage('student_saved');
        $this->response();
    }

    public function deleteStudent() {

        $this->authinticate();
        $this->checkIsAdmin();
        $sid = $this->post('sid');
        $cid = $this->post('cid');
        $this->s_model->deleteRecord('student_courses', array('sid' => $sid, 'cid' => $cid));
        $this->s_model->deleteRecord('attendance', array('sid' => $sid, 'cid' => $cid));
//        $this->s_model->deleteRecord('metadata',array('object_type' => 2,'object_id' => $id));
        $this->setSuccess();
        $this->setLangMessage("student_deleted");
        $this->response();
    }

    public function resetPass() {
        $email_id = $this->post('email_id');
        $user = $this->s_model->getData('users', array('email_id' => $email_id));
        if (empty($user) || empty($email_id)) {
            $this->setMessage($this->langLine('invalid_email'));
            $this->_die();
        } else {
            $token = $this->getToken(10);
//            $token = '123456789';
            $this->s_model->updateRecord('users', array('user_no' => $user['user_no']), array('reset_token' => $token));
            $message = base_url('/login/resetPass/' . $token);
            if (!mail($email_id, "Password reset", $message)) {
                $this->setMessage($this->langLine('mail_not_sent'));
                $this->_die();
            }
        }
        $this->setSuccess();
        $this->setMessage($this->langLine('password_reset_link_send'));
        $this->response();
    }

    function getToken($length) {

        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet .= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet .= "0123456789";
        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[rand(0, strlen($codeAlphabet))];
        }
        return $token;
    }

    function logout() {

        $this->session->sess_destroy();
        $this->clearSession();

        $data = array();

        $data['success'] = true;                // false
        $data['msg'] = $this->langLine('logged_out');

        $this->response($data);
    }

    //The attendance app webserice
    public function saveNote() {
        $this->authinticate();
        $this->checkIsAdmin();

        $cid = $this->post('cid');
        $sid = $this->post('sid');
        $note = $this->post('note');
        $date = $this->post('date');
        $user_no = $this->post('user_no');

        $note_id = $this->s_model->insertRecord('absent_note', array(
            'cid' => $cid,
            'sid' => $sid,
            'note' => $note,
            'date' => $date,
            'created_by' => $user_no
        ));

        $this->setSuccess();
        $this->setData('note_id', $note_id);
        $this->setMessage('note_sved', true);
        $this->response();
    }

    public function getHolidays() {

        $this->authinticate();
        $this->setSuccess();

        $this->response['holidays'] = $this->s_model->getDatas('holidays', array(), array('date'));

        $holidat_string = array();
        if ($this->response['holidays']) {
            foreach ($this->response['holidays'] as $date) {
                $holidat_string[] = $date['date'];
            }
        }
        $this->response['holiday_string'] = $holidat_string;
        $this->response['holiday_string'] = implode(',', $holidat_string);
        $this->response();
    }

    public function saveUser() {
        $this->authinticate();
        $user_no = $this->post('user_no');
        $user = $this->post('user');
        $password = false;
        if (isset($user['oldpass']) || isset($user['newpass'])) {
            if (isset($user['oldpass']) && $user['oldpass'] == '') {
                $this->setMessage('old_empty_pass');
                $this->_die();
            }
            if (isset($user['oldpass']) && $user['oldpass']) {
                $data = $this->s_model->getData('users', array('user_no' => $user_no, 'password' => md5($user['oldpass'])));
            }
            if (empty($data)) {
                $this->setMessage('pass_not_match');
                $this->_die();
            } elseif ($user['oldpass'] == $user['newpass']) {
                $this->setMessage('pass_same');
                $this->_die();
            } elseif (isset($user['newpass']) && $user['newpass'] == '') {
                $this->setMessage('new_empty_pass');
                $this->_die();
            }
            $password = $user['newpass'];
            unset($user['oldpass']);
            unset($user['newpass']);
        }
        if ($password) {
            $user['password'] = md5($password);
            $user['password_hint'] = $password;
        }
        if ($user) {
            $this->s_model->updateRecord('users', array('user_no' => $user_no), $user);
        }
        $this->setSuccess();
        $this->setMessage('user_saved', true);
        $this->response();
    }

    public function getChildrens() {
        $user_no = $this->post('user_no');
        $childrens = $this->s_model->getData('student_parents', array('user_no' => $user_no), array('sid', 'name'));
        return $childrens;
    }

    public function sendMessage() {
        $this->authinticate();
        $user_no = $this->post('user_no');
        $notification_data = array();
        $send_to = array();
        $one_user_email = '';
        $notification = $this->post('notification');
        if ($notification) {
            if (isset($notification['send_to']) && $notification['send_to']) {
                foreach ($notification['send_to'] as $key => $v) {
                    if ($v) {
                        $send_to[] = $key;
                    }
                }
            }
            $notification_data['user_no'] = $user_no;
            $notification_data['notification'] = $notification['notification'];
            $notification_data['title'] = $notification['title'];
            $one_user_email = (isset($notification['one_user_email'])) ? $notification['one_user_email'] : '';
            $notification_data['date'] = date('Y-m-d H:i:s');
        }
        if ($notification_data) {
            $nid = $this->s_model->insertRecord('notifications', $notification_data);
            
            $not_data = array();
            
            $not_data['nid'] = $nid;
            $not_data['sender_id'] = $user_no;
            
            $user_types_to_send = array();
            if ($send_to) {
                foreach ($send_to as $type) {
                    switch ($type) {
                        case 'parents' : $not_data['user_type'] = 4;
                            break;
                        case 'mod' : $not_data['user_type'] = 3;
                            break;
                        case 'tech' : $not_data['user_type'] = 2;
                            break;
                        case 'admin' : $not_data['user_type'] = 1;
                            break;
                        case 'oneuser' :
                            $not_data['user_type'] = 0;
                            $not_data['single_user'] = $one_user_email;
                            break;
                    }
                    $user_types_to_send[] = $not_data['user_type'];
//                    $not_data['notify_date'] = date('Y-m-d H:i:s');
                    $this->s_model->insertRecord('notify_user', $not_data);
                }
            }
            $this->sendPushForMessage($user_types_to_send, $notification_data, $one_user_email);
        }

        $this->setSuccess();
        $this->setMessage('notific_send', true);
        $this->response();
    }

    private function sendPushForMessage($types, $notification_data, $one_user_email) {
        $types = implode("','", $types);
        $types = ($types) ? "('$types')" : $types;
        $ids = $this->s_model->getUserPushIDsWithUserType($types, $one_user_email);
        $registration_ids = array();
        if ($ids) {
            foreach ($ids as $val) {
                $registration_ids[] = $val['registration_id'];
            }
            $title = ($notification_data['title']) ? $notification_data['title'] : '';
            $message = array('message' => $title);
            $response_push = $this->send_notification($registration_ids, $message);
        }

//        print_r($message);
//        print_r($registration_ids);
        $this->setData('push_response',$response_push);
    }

    public function getNotifications() {
        $this->authinticate();
        $user_no = $this->post('user_no');
        $notifications = $this->s_model->getNotifications($this->user);
//        print_r($notifications);
        foreach ($notifications as $key => $value) {
            $notifications[$key]['pic'] = ($notifications[$key]['pic']) ? $notifications[$key]['pic'] : 'nopic.png';
            $notifications[$key]['pic'] = base_url('uploads/' . $notifications[$key]['pic']);
            $notifications[$key]['user_right'] = 'U';
            if ($notifications[$key]['user_type'] == 1) {
                $notifications[$key]['user_right'] = 'A';
            } elseif ($notifications[$key]['user_type'] == 2) {
                $notifications[$key]['user_right'] = 'T';
            } elseif ($notifications[$key]['user_type'] == 3) {
                $notifications[$key]['user_right'] = 'M';
            } elseif ($notifications[$key]['user_type'] == 4) {
                $notifications[$key]['user_right'] = 'P';
            }
        }
        $this->setSuccess();
        $this->setData('list', $notifications);
//        $this->setData('list', $notifications);
        $this->response();
    }

    public function deleteNotifications() {
        $this->authinticate();
        $user_no = $this->post('user_no');
        $nid = $this->post('nid');
        $this->s_model->insertRecord('delete_notification', array('user_no' => $user_no, 'nid' => $nid));
        $this->setSuccess('notific_deleted');
        $this->response();
    }

    public function getAds() {
        $data = array();
        $ads = $this->s_model->getDatas('ads');
        if ($ads) {
            foreach ($ads as $key => $value) {
                if ($value['section']) {
                    $data[$value['section']]['ID'] = $value['ID'];
                    $data[$value['section']]['medias'] = $this->s_model->getAdMedias($value['ID']);
                }
            }
        }
        $this->setData('ads', $data);
        $this->setSuccess();
        $this->response();
    }

}
