<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of list
 *
 * @author Suchandan
 */
class lists extends My_Controller {

    public $per_page = 25;

    public function __construct() {
        parent::__construct();
        $this->s_lib->checkMethodAccess($this->session->userdata('user_type'), $this->router->fetch_class(), $this->router->fetch_method());
    }

     public function user($page = '') {

        $page = ($page == '' || $page == 0 ) ? 1 : $page;
        $start = ($page > 1) ? ($page - 1) * $this->per_page : 0;
        $school_id = $this->session->userdata('school_id');
        $users = $this->s_model->getUsers($start, $this->per_page, array('school_id' => $school_id, 'not_in_user_type' => array(2,4)));
        $data['users'] = $users;
        $data['page_title'] = 'User List';
        $count = $this->s_model->getTotalCount();
        $data['pagination'] = $this->createPagination('user', $count);
        $data['utype'] = 'user';

        $data['total'] = $count;
        $data['start'] = $start + 1;
        $data['end'] = $start + count($users);

        $this->s_lib->loadView('user-list', $data);
    }

    public function course($page = '') {

        $page = ($page == '' || $page == 0 ) ? 1 : $page;
        $start = ($page > 1) ? ($page - 1) * $this->per_page : 0;
        $school_id = $this->session->userdata('school_id');
       
        $courses = $this->s_model->getCourseList($start, $this->per_page, array('parent' => 0,'school_id' => $school_id));

        $count = $this->s_model->getTotalCount();
        if ($courses) {
            foreach ($courses as $key => $course) {
                $count_datas = $this->s_model->getDatas('student_courses', array('cid' => $course['cid']));
                $courses[$key]['student_count'] = count($count_datas);
            }
        }
        $data['courses'] = $courses;
        $data['utype'] = 'course';
        $data['pagination'] = $this->createPagination('course', $count);

        $data['total'] = $count;
        $data['start'] = $start + 1;
        $data['end'] = $start + count($courses);
        $this->s_lib->loadView('course-list', $data);
    }
    
     public function school($page = '') {

        $page = ($page == '' || $page == 0 ) ? 1 : $page;
        $start = ($page > 1) ? ($page - 1) * $this->per_page : 0;

        $school = $this->s_model->getSchoolList($start, $this->per_page, array());

        $count = $this->s_model->getTotalCount();
        
        $data['school'] = $school;
        $data['utype'] = 'school';
        $data['pagination'] = $this->createPagination('school', $count);

        $data['total'] = $count;
        $data['start'] = $start + 1;
        $data['end'] = $start + count($school);
        $this->s_lib->loadView('school-list', $data);
    }
    
    public function plan($page = '') {

        $page = ($page == '' || $page == 0 ) ? 1 : $page;
        $start = ($page > 1) ? ($page - 1) * $this->per_page : 0;

        $plans = $this->s_model->getPlanList($start, $this->per_page, array('status' => 1));

        $count = $this->s_model->getTotalCount();
        if ($plans) {
            foreach ($plans as $key => $plan) {
                $count_datas = $this->s_model->getDatas('student_courses', array('cid' => $plan['id']));
                //$plans[$key]['student_count'] = count($count_datas);
            }
        }
        $data['plans'] = $plans;
        $data['utype'] = 'plans';
        $data['pagination'] = $this->createPagination('plans', $count);

        $data['total'] = $count;
        $data['start'] = $start + 1;
        $data['end'] = $start + count($plans);
        $this->s_lib->loadView('plan-list', $data);
    }
    
     public function news($page = '') {

        $page = ($page == '' || $page == 0 ) ? 1 : $page;
        $start = ($page > 1) ? ($page - 1) * $this->per_page : 0;
	if($school_id = $this->session->userdata('school_id')){
        $news = $this->s_model->getNewsList($start, $this->per_page, array('status' => 1, 'school_id' => $school_id));
	}else{
	$news = $this->s_model->getNewsList($start, $this->per_page, array('status' => 1));
	}
        $count = $this->s_model->getTotalCount();
        
        $data['news'] = $news;
        $data['utype'] = 'news';
        $data['pagination'] = $this->createPagination('news', $count);

        $data['total'] = $count;
        $data['start'] = $start + 1;
        $data['end'] = $start + count($news);
        $this->s_lib->loadView('news-list', $data);
    }

    public function student($page = '') {
//       $this->s_model->show_query = 1;
        $page = ($page == '' || $page == 0 ) ? 1 : $page;
        $start = ($page > 1) ? ($page - 1) * $this->per_page : 0;
        $school_id = $this->session->userdata('school_id');
        $students = $this->s_model->getStudent($start, $this->per_page, array('parent' => 0, 'school_id' => $school_id));
        $count = $this->s_model->getTotalCount();
//        $courses = $this->s_model->getCourses(true);
        $courses = $this->s_model->getDatas('courses', array('school_id' => $school_id));
        if ($students) {
            foreach ($students as $key => $value) {
                $codes = $this->s_model->getStudentCoursesCodes($value['sid']);
                $parents = $this->s_model->studentParnt($value['student_id']);
                $a = array();
                foreach ($codes as $code) {
                    if ($code['code'] != '') {
                        //$a[] = $code['code'];
                        $a[] = $code['name'];
                    }
                }
                $codes_st = implode(',', $a);
                $students[$key]['code'] = $codes_st;
                $students[$key]['parent'] = ($parents) ? $parents['first_name'] . ' ' . $parents['last_name'] : "";
            }
        }
        $data['students'] = $students;
        $tmpcourses = array();
        foreach($courses as $item){
            $tmpcourses[$item['cid']] = $item;
        }
        $data['courses'] = $tmpcourses;
        $data['pagination'] = $this->createPagination('student', $count);
        $data['utype'] = 'student';

        $data['total'] = $count;
        $data['start'] = $start + 1;
        $data['end'] = $start + count($students);
        $data['student_list'] = true;

        $this->s_lib->loadView('student-list', $data);
    }

    public function lang() {
        $langs = $this->s_model->getDatas('lang');
        $data['langs_list'] = $langs;
        $this->s_lib->loadView('lang-list', $data);
    }

    public function teachers($page = '') {
        $page = ($page == '' || $page == 0 ) ? 1 : $page;
        $start = ($page > 1) ? ($page - 1) * $this->per_page : 0;
        $school_id = $this->session->userdata('school_id');
        $users = $this->s_model->getUsers($start, $this->per_page, array('user_type' => 2, 'school_id' => $school_id));
        
        $courses = $this->s_model->getDatas('courses', array('school_id' => $school_id));
        
        $tmpcourses = array();
        foreach($courses as $item){
            $tmpcourses[$item['cid']] = $item;
        }
        $data['courses'] = $tmpcourses;
        
        $data['teacher'] = true;
        $data['page_title'] = 'Teachers ';
        $count = $this->s_model->getTotalCount();
        $data['pagination'] = $this->createPagination('teachers', $count);
        $data['utype'] = 'teachers';

        $data['users'] = $this->userCourses($users);
        $data['total'] = $count;
        $data['start'] = $start + 1;
        $data['end'] = $start + count($users);

        $this->s_lib->loadView('user-list', $data);
    }

    public function userCourses($users) {
        if ($users) {
            foreach ($users as $key => $value) {
                $classes = $this->s_model->getUserClasses($value['user_no']);
                $a = array();
                if ($classes) {
                    foreach ($classes as $code) {
                        //$a[] = $code['code'];
                        $a[] = $code['name'];
                    }
                }
                $users[$key]['classes'] = implode(',', $a);
            }
        }
        return $users;
    }

    public function modarators($page = '') {
        $page = ($page == '' || $page == 0 ) ? 1 : $page;
        $start = ($page > 1) ? ($page - 1) * $this->per_page : 0;
        $school_id = $this->session->userdata('school_id');
        $users = $this->s_model->getUsers($start, $this->per_page, array('user_type' => 3, 'school_id' => $school_id));

        $data['teacher'] = true;
        $data['page_title'] = 'Modarators ';
        $count = $this->s_model->getTotalCount();
        $data['pagination'] = $this->createPagination('modarators', $count);
        $data['utype'] = 'modarators';
        $data['users'] = $this->userCourses($users);
        $data['total'] = $count;
        $data['start'] = $start + 1;
        $data['end'] = $start + count($users);
        $this->s_lib->loadView('user-list', $data);
    }

    private function setChildrens($users) {
        if (!$users) {
            return $users;
        }
        foreach ($users as $key => $user) {
            $cildrens = $this->s_model->getChildrens($user['user_no']);
            $children_text = array();
            if ($cildrens) {
                foreach ($cildrens as $child) {
                    $children_text[] = $child['name'];
                }
            }
            $users[$key]['childrens'] = implode(',', $children_text);
        }
        return $users;
    }

    public function parents($page = '') {
        $page = ($page == '' || $page == 0 ) ? 1 : $page;
        $start = ($page > 1) ? ($page - 1) * $this->per_page : 0;
        $school_id = $this->session->userdata('school_id');
        $users = $this->s_model->getUsers($start, $this->per_page, array('user_type' => 4, 'school_id' => $school_id));

        $data['teacher'] = false;
        $data['page_title'] = 'Parents ';
        $count = $this->s_model->getTotalCount();
        $data['pagination'] = $this->createPagination(__FUNCTION__, $count);
        $data['utype'] = 'parents';
        $data['parents'] = true;

        $users = $this->setChildrens($users);


        $data['users'] = $users;
        $data['total'] = $count;
        $data['start'] = $start + 1;
        $data['end'] = $start + count($users);
        $this->s_lib->loadView('user-list', $data);
    }

    public function ads($page = '') {
        $page = ($page == '' || $page == 0 ) ? 1 : $page;
        $start = ($page > 1) ? ($page - 1) * $this->per_page : 0;
        $Ads = $this->s_model->getAds($start, $this->per_page);
        if ($Ads) {
            foreach ($Ads as $k => $v) {
                $Ads[$k]['img_count'] = $this->s_model->countData('link_medias', array('object_type' => 'ad'));
            }
        }
        $data['ads'] = $Ads;
        $data['page_title'] = 'Ads ';
        $count = $this->s_model->getTotalCount();
        $data['pagination'] = $this->createPagination(__FUNCTION__, $count);
        $data['utype'] = __FUNCTION__;

        $data['total'] = $count;
        $data['start'] = $start + 1;
        $data['end'] = $start + count($Ads);
        $this->s_lib->loadView('ads-list', $data);
    }

    private function createPagination($slug, $count, $page = '') {

        $obj = $this->get('s');

        $this->load->library('pagination');

        $base_url = base_url('lists/' . $slug);
        if ($page) {
            $base_url = $base_url . "/" . $page;
        }
        if ($obj) {
            $base_url = $base_url . "?s=" . $obj;
        }

        $config['base_url'] = $base_url;
        $config['total_rows'] = $count;
        $config['per_page'] = $this->per_page;
        $config["uri_segment"] = 3;
        $config['num_links'] = 3;
        $config['use_page_numbers'] = TRUE;
        $config['full_tag_open'] = '<ul class="pagination" id="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['next_link'] = '&raquo;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo;';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="">';
        $config['cur_tag_close'] = '</a></li>';

        $this->pagination->initialize($config);

        return $this->pagination->create_links();
    }

    public function systemactivity($page = '') {
        // user_type param values :  1= , 2=teacher, 3=modarator, 4=
        $page = ($page == '' || $page == 0 ) ? 1 : $page;
        $start = ($page > 1) ? ($page - 1) * $this->per_page : 0;
        // get all users 
        $action = 1;
        $start_date = date('Y-m-d');
        $data = array();
        $users = array();
        $count = 0;
        if ($this->post('action')) {
            $action = $this->post('action');
            $start_date = $this->post('start_date');
            if (empty($start_date)) {
                $action = 0;
            }
        }
        // fetch all users according to conditions 
        if ($action) {
            $users = $this->s_model->systemactivity(array('user_type' => array('2', '3'), 'start_date' => $start_date));
            $count = count($users);
        }

        // assigning data 
        $data['action'] = $action;
        $data['start_date'] = $start_date;
        $data['users'] = $users;
        $data['pagination'] = $this->createPagination('systemactivity', $count);
        $data['utype'] = '';
        $data['total'] = $count;
        $data['start'] = $start + 1;
        $data['end'] = $start + count($users);
        $data['user_list'] = true;
        $this->s_lib->loadView('system_activity_list', $data);
    }
    
    

     public function classabsent($page = '') {
        $page = ($page == '' || $page == 0 ) ? 1 : $page;
        $start = ($page > 1) ? ($page - 1) * $this->per_page : 0;
        $data = array();
        $count = 0;
        $action = 0;
        $class_date = date('Y-m-d');
        $course_id = array();
        $students = array();
        $sem_teachers = array();
        // get filter data section 
        if ($this->post('action')) {
            $action = $this->post('action');
            $class_date = $this->post('start_date');
            $course_id = $this->post('course_id');
            if (empty($class_date) || empty($course_id)) {
                $action = 0;
            }
            if (!is_array($course_id)) {
                $course_id = array();
            }
        }
        // data manupulation section
        $school_id = $this->session->userdata('school_id');
        $courses = $this->s_model->getDatas('courses', array('parent' => 0,'school_id' => $school_id));
        
        if ($action) {
            //$students = $this->s_model->getDatas('students',array('parent' => 0));
            if (!in_array('-1', $course_id)) {
                //old logic going well
                $students = $this->s_model->getStudentInCourses($course_id);
                // get each students absent and present counts 
                if (is_array($students) && count($students) > 0) {
                    $condition = array(
                        'class_date' => $class_date,
                        'course_id' => $course_id
                    );
                    foreach ($students as $key => $student) {
                        $student_id = $student['sid'];
                        $condition['sid'] = $student_id;
                        $class_sem_report = $this->s_model->studentSemReports($condition);
                        // print_r($class_sem_report);die;
                        $students[$key]['sem_report'] = $class_sem_report;
                    }
                }

                // get the teachers name according the course semno
                $sem_teachers = $this->s_model->semTecherName(array('course_id' => $course_id));
            } else {
                // new fol all calss
                //set_time_limit(0); //infinite time set

                $students = array();
                $course_ids = array();
                foreach ($courses as $course) {

                    $course_ids[] = $course['cid'];

                    /* //old
                      $cls_id = array($course['cid']);
                      $cls_students = $this->s_model->getStudentInCourses($cls_id);
                      // get each students absent and present counts
                      if(is_array($cls_students) && count($cls_students)>0){
                      $condition=array(
                      'class_date'=>$class_date,
                      'course_id'=>$cls_id
                      );

                      foreach($cls_students as $key=>$student){
                      $student_id = $student['sid'];
                      $condition['sid']=$student_id;
                      $class_sem_report = $this->s_model->studentSemReports($condition);
                      $cls_students[$key]['sem_report']=$class_sem_report;
                      }
                      }
                      $students = array_merge($students,$cls_students); */
                }
                //new section 


                if (!empty($course_ids)) {
                    $cls_students = $this->s_model->getStudentInCourses($course_ids);
                    // get each students absent and present counts 
                    if (is_array($cls_students) && count($cls_students) > 0) {
                        $condition = array(
                            'class_date' => $class_date,
                                //'course_id'=>$cls_id
                        );
                        foreach ($cls_students as $key => $student) {
                            if (!isset($students[$student['cid']])){
                                $students[$student['cid']] = array();
                            }
                            $student_id = $student['sid'];
                            $condition['sid'] = $student_id;
                            $condition['course_id'] = array($student['cid']);
                            $class_sem_report = $this->s_model->studentSemReports($condition);
                            //$cls_students[$key]['sem_report']=$class_sem_report;
                            $student['sem_report'] = $class_sem_report;
                            array_push($students[$student['cid']], $student);
                            //sleep(1);
                        }
//                        $limit = 500;
//                        $offset = 0;
//                        $sts = array_slice($cls_students, $offset, $limit);
//                        while (count($userAry = array_slice($cls_students, $offset, $limit)) > 0) {
//                            $offset += $limit;
//                            foreach ($cls_students as $key => $student) {
//                                if (!isset($students[$student['cid']])){
//                                    $students[$student['cid']] = array();
//                                }
//                                $student_id = $student['sid'];
//                                $condition['sid'] = $student_id;
//                                $condition['course_id'] = array($student['cid']);
//                                $class_sem_report = $this->s_model->studentSemReports($condition);
//                                //$cls_students[$key]['sem_report']=$class_sem_report;
//                                $student['sem_report'] = $class_sem_report;
//                                array_push($students[$student['cid']], $student);
//                                //sleep(1);
//                            }
//                            break;
//                        }
                        
                        
                        /* foreach($cls_students as $key=>$student){
                          $student_id = $student['sid'];
                          $condition['sid']=$student_id;
                          $condition['course_id']=array($student['cid']);
                          $class_sem_report = $this->s_model->studentSemReports($condition);
                          $cls_students[$key]['sem_report']=$class_sem_report;
                          } */
                    }
                    //$students = array_merge($students,$cls_students);
                }
                //$sem_teachers = $this->s_model->semTecherName(array('course_id'=>$course_id));
                $sem_teachers = array();
            }
        }
        // get the course name
        $course_code = '';
        if (!empty($course_id)) {
            if (!empty($courses)) {
                foreach ($courses as $course) {
                    if ($course['cid'] == $course_id[0]) {
                        $course_code = $course['name'];
                        break;
                    }
                }
                /*
                  $key = array_search($course_id, array_column($courses, 'cid'));
                  if(isset($courses[$key])){
                  $course = $courses[$key];
                  $course_code = $course['code'];
                  } */
            }
        }
        //$count=$this->s_model->getTotalCount();
        $count = count($students);
        $data['action'] = $action;
        
        $tmpcourses = array();
        foreach($courses as $item){
            $tmpcourses[$item['cid']] = $item;
        }
        $data['courses'] = $tmpcourses;
        $data['students'] = $students;
        $data['pagination'] = $this->createPagination('systemactivity', $count);
        $data['utype'] = '';
        $data['current_school'] = $this->s_model->getCurrentSchool();
        $data['total'] = $count;
        $data['start'] = $start + 1;
        $data['end'] = $start + count($students);
        $data['start_date'] = $class_date;
        $data['course_id'] = $course_id;
        $data['course_code'] = $course_code;
        $data['sem_teachers'] = $sem_teachers;
        $this->s_lib->loadView('class_absent_list', $data);
    }
    
    
    
    function manageattendance($cid=0, $sid=0){
        $action = 1;
        $start_date = date('Y-m-d');
        
        $data = $students = $attendance = array();
//        $notes = array();
        if ($cid*1){
            $students = $this->s_model->getCourseStudents($cid);
        }
        if ($sid*1){
            $students_seminars_absents = $this->s_model->getAbsentSeminarsCounts(array($sid), array());
            $atendance = $students_seminars_absents[$sid];
            $notes_data = $this->s_model->getDatas('absent_note', array('sid' => $sid), array('note', 'date', 'ID'));
            foreach($notes_data as $note){
                $notes[$note['date']] = $note['note'];
            }
            
            $medicals = array();
            $suspended = array();
            $suspend_data = $this->s_model->getDatas('student_suspend', array('sid' => $sid, 'cid' => $cid), array('list_days','amount_days', 'end_date', 'start_date', 'id'), array(),array(), 'start_date');
            foreach($suspend_data as $suspend_item){
                $days = unserialize($suspend_item['list_days']);
                foreach($days as $day){
                    $suspended[$day] = $day;
                }
            }
            $medical_data = $this->s_model->getDatas('student_medical', array('sid' => $sid, 'cid' => $cid), array('list_days','amount_days', 'end_date','start_date', 'id'), array(),array(), 'start_date');
            foreach($medical_data as $medical_item){
                $days = unserialize($medical_item['list_days']);
                foreach($days as $day){
                    $medicals[$day] = $day;
                }
            }
        }
        // get all class 
        
        $school_id = $this->session->userdata('school_id');
        $query = "SELECT * FROM abs_courses WHERE status=1 and school_id=".($school_id*1)." ORDER by cid";
        $courses = $this->s_model->groupClassesName($query);
        $data['courses'] = $courses->result_array();
        
        
                        
        $data['cid'] = $cid;
        $data['sid'] = $sid;
        $data['medicals'] = $medicals;
        $data['suspended'] = $suspended;
        $data['notes'] = $notes;
        $data['current_school'] = $this->s_model->getCurrentSchool();
        $data['page_title'] = 'Manage Absent ';
        $data['students'] = $students;
        $data['atendance'] = $atendance;
        $this->s_lib->loadView('manage_attendance', $data);
    }
    function attendancenotes($cid=0, $sid=0){
        $action = 1;
        $start_date = date('Y-m-d');
        
        $data = $students = $attendance = array();
//        $notes = array();
        if ($cid*1){
            $students = $this->s_model->getCourseStudents($cid);
        }
        if ($sid*1){
            $students_seminars_absents = $this->s_model->getAbsentSeminarsCounts(array($sid), array());
            $atendance = $students_seminars_absents[$sid];
            $notes_data = $this->s_model->getDatas('absent_note', array('sid' => $sid, 'cid' => $cid), array('note', 'date', 'ID'));
            foreach($notes_data as $note){
                $notes[$note['date']] = $note['note'];
            }
            $medicals = array();
            $suspended = array();
            $suspend_data = $this->s_model->getDatas('student_suspend', array('sid' => $sid, 'cid' => $cid), array('list_days','amount_days', 'end_date', 'start_date', 'id'), array(),array(), 'start_date');
            foreach($suspend_data as $suspend_item){
                $days = unserialize($suspend_item['list_days']);
                foreach($days as $day){
                    $suspended[$day] = $day;
                }
            }
            $medical_data = $this->s_model->getDatas('student_medical', array('sid' => $sid, 'cid' => $cid), array('list_days','amount_days', 'end_date','start_date', 'id'), array(),array(), 'start_date');
            foreach($medical_data as $medical_item){
                $days = unserialize($medical_item['list_days']);
                foreach($days as $day){
                    $medicals[$day] = $day;
                }
            }
        }
        // get all class 
        $school_id = $this->session->userdata('school_id');
        $query = "SELECT * FROM abs_courses WHERE status=1 and school_id=".($school_id*1)." ORDER by cid";
        $courses = $this->s_model->groupClassesName($query);
        $data['courses'] = $courses->result_array();
        
        
                        
        $data['cid'] = $cid;
        $data['sid'] = $sid;
        $data['suspended'] = $suspended;
        $data['medicals'] = $medicals;
        $data['notes'] = $notes;
        $data['current_school'] = $this->s_model->getCurrentSchool();
        $data['page_title'] = 'Manage Notes';
        $data['students'] = $students;
        $data['atendance'] = $atendance;
        $this->s_lib->loadView('attendance_notes', $data);
    }
    function studentsuspension($cid=0, $sid=0){
        $action = 1;
        $start_date = date('Y-m-d');
        
        $data = $students = $attendance = array();
//        $notes = array();
        if ($cid*1){
            $students = $this->s_model->getCourseStudents($cid);
        }
        if ($sid*1){
            $students_seminars_absents = $this->s_model->getAbsentSeminarsCounts(array($sid), array());
            $atendance = $students_seminars_absents[$sid];
            $suspend_data = $this->s_model->getDatas('student_suspend', array('sid' => $sid, 'cid' => $cid), array('list_days','amount_days', 'end_date','start_date', 'id'), array(),array(), 'start_date');
            $medical_data = $this->s_model->getDatas('student_medical', array('sid' => $sid, 'cid' => $cid), array('list_days','amount_days', 'end_date','start_date', 'id'), array(),array(), 'start_date');
        }
        // get all class 
        $school_id = $this->session->userdata('school_id');
        $query = "SELECT * FROM abs_courses WHERE status=1 and school_id=".($school_id*1)." ORDER by cid";
        $courses = $this->s_model->groupClassesName($query);
        $data['courses'] = $courses->result_array();
        
        
        
                        
        $data['cid'] = $cid;
        $data['sid'] = $sid;
        $data['suspend'] = $suspend_data;
        $data['medical'] = $medical_data;
        $data['current_school'] = $this->s_model->getCurrentSchool();
        $data['page_title'] = 'Student Suspension';
        $data['students'] = $students;
        $data['atendance'] = $atendance;
        $this->s_lib->loadView('student_suspension', $data);
    }
    function medicalreport($cid=0, $sid=0){
        $action = 1;
        $start_date = date('Y-m-d');
        
        $data = $students = $attendance = array();
//        $notes = array();
        if ($cid*1){
            $students = $this->s_model->getCourseStudents($cid);
        }
        if ($sid*1){
            $students_seminars_absents = $this->s_model->getAbsentSeminarsCounts(array($sid), array());
            $atendance = $students_seminars_absents[$sid];
            $suspend_data = $this->s_model->getDatas('student_suspend', array('sid' => $sid, 'cid' => $cid), array('list_days','amount_days', 'end_date', 'start_date', 'id'), array(),array(), 'start_date');
            $medical_data = $this->s_model->getDatas('student_medical', array('sid' => $sid, 'cid' => $cid), array('list_days','amount_days', 'end_date','start_date', 'id'), array(),array(), 'start_date');
        }
        $school_id = $this->session->userdata('school_id');
        
        // get all class 
        $query = "SELECT * FROM abs_courses WHERE status=1 and school_id=".($school_id*1)." ORDER by cid";
        $courses = $this->s_model->groupClassesName($query);
        $data['courses'] = $courses->result_array();
        
        
        
                        
        $data['cid'] = $cid;
        $data['sid'] = $sid;
        $data['suspend'] = $suspend_data;
        $data['medical'] = $medical_data;
        $data['current_school'] = $this->s_model->getCurrentSchool();
        $data['page_title'] = 'Medical Report';
        $data['students'] = $students;
        $data['atendance'] = $atendance;
        $this->s_lib->loadView('medical_report', $data);
    }
    
    function warningreport(){
        $absent_number = $this->input->post('absent_number');
        $excused = $this->input->post('excused');
        $class_id = $this->input->post('class_id');
        $data = $filtercond = $attendance = array();
        $school_id = $this->session->userdata('school_id');
        if ($this->input->server('REQUEST_METHOD')=="POST") {
            foreach($class_id as $sem_key=>$sem_val){
                if (empty($sem_val)){
                    unset($class_id[$sem_key]);
                }
            }
            if (isset($excused) && $excused!==''){
                $filtercond['excused'] = $excused;
            }
            if (!empty($class_id)) {
                $filtercond['class_id'] = $class_id;
            }
            $filtercond['orderby'] = 'cid, student_name ASC';
            
            if (!empty($absent_number)){
                $filtercond['absent_number'] = $absent_number;
            }
            
            $filtercond['school_id'] = $school_id;
            $absents = $this->s_model->getAttendance(true, $filtercond);
            $students_ids = array();
            foreach ($absents as $key => $atend) {
                $students_ids[] = $atend['sid'];
            }
            $students_absents = $this->s_model->getAbsentCounts($students_ids, $filtercond);
            
            foreach ($absents as $key => $atend) {
                if ($students_absents[$atend['sid']]){
                    $absents[$key]['abscount'] = $students_absents[$atend['sid']];
                }else{
                    unset($absents[$key]);
                }
            }
        }
        $courses = $this->s_model->getCourseList($start = 0, 1000, array('parent' => 0, 'school_id'=>$school_id), 'cid', 'ASC');
        $data['courses'] = $courses;
        // get all class 
        $data['page_title'] = 'Absent Report';
        $data['absents'] = $absents;
        $data['current_school'] = $this->s_model->getCurrentSchool();
        $data['class_id'] = $class_id;
        $data['filtercond'] = $filtercond;
        $this->s_lib->loadView('warningreport', $data);
    }

    public function statattendance() {
        $action = 1;
        $start = 0;
        $start_date = date('Y-m-d');
        $data = array();
        $classes = array();
        $semno = 1;
        if ($this->post('action')) {
            $semno = $this->post('semno');
            $action = $this->post('action');
            $start_date = $this->post('start_date');
            if (empty($start_date)) {
                $action = 0;
            }
        }
        // get all class 
        if ($action) {
            //$classes = $this->s_model->statattendance($start, $this->per_page, array(),$start_date);
        }

        if ($action) {
            $school_id = $this->session->userdata('school_id');
            $class_groups = $this->s_model->getDatas('course_groups', array('status' => '1' , 'school_id' => $school_id), array('ID', 'group_name', 'course_ids'));
            if (is_array($class_groups) && count($class_groups) > 0) {
                // gett all the class details 
                foreach ($class_groups as $key => $class_group) {
                    $class_ids = $class_group['course_ids'];
                    $classes = array();
                    if ($class_ids != '') {
                        $classes = $this->s_model->statattendancenw($start_date, $class_ids, $semno);
                    }
                    $class_groups[$key]['classess'] = $classes;
                }
            }
            
        }

        // data set section 
        $data['class_groups'] = $class_groups;
        $data['semno'] = $semno;
        $data['action'] = $action;
        $data['current_school'] = $this->s_model->getCurrentSchool();
        $data['start_date'] = $start_date;
        $data['classes'] = $classes;
        $this->s_lib->loadView('stat_attendance_list', $data);
    }
    public function statecsv($datetime = '') {
        if (!empty($datetime)) {
            $start_date = date('Y-m-d', $datetime);
            $classes = $this->s_model->statattendance_csv(array(), $start_date);
            //print_r($classes);
            $this->load->helper(array('file', 'download'));
            if (!empty($classes)) {
                // 
                //$classes=array("hh","fff","rrr");
                header("Content-type: application/csv-tab-delimited-table; charset=utf-8");
                $this->load->dbutil();

                $extns = "csv";
                $file_data = $this->dbutil->csv_from_result($classes);

                $filename = time() . "_report." . $extns;

                //$filename = "downloads/".time()."_doumload.".$extns;

                /*  Now use it to write file. write_file helper function will do it */

                //write_file($filename,$file_data); // create  and stor the file

                force_download($filename, $file_data); // create and download the file with out stor
            }
        }
        exit;
    }

    public function admin($page = '') {
        $page = ($page == '' || $page == 0 ) ? 1 : $page;
        $start = ($page > 1) ? ($page - 1) * $this->per_page : 0;
        $school_id = $this->session->userdata('school_id');
        $users = $this->s_model->getUsers($start, $this->per_page, array('user_type' => 1, 'school_id' => $school_id));

        $data['teacher'] = false;
        $data['page_title'] = 'Admins ';
        $count = $this->s_model->getTotalCount();
        $data['pagination'] = $this->createPagination('admin', $count);
        $data['utype'] = 'admin';

        $data['users'] = $users;
        $data['total'] = $count;
        $data['start'] = $start + 1;
        $data['end'] = $start + count($users);

        $this->s_lib->loadView('user-list', $data);
    }

    public function classabsent_excl($statr_date = '', $class_id = 0, $student_id_show=false) {
        $current_school = $this->s_model->getCurrentSchool();
        $school_id = $this->session->userdata('school_id');
        if (!empty($statr_date) && $class_id >= -1) {
            $this->load->library('Excel');
            //echo $statr_date;
            //echo "<br>";
            //echo $class_id;
            $course_id = array($class_id);
            $course_name = '';
            $course_code = '';
            if (!in_array('-1', $course_id)) {
                $students = $this->s_model->getStudentInCourses($course_id);
                // get each students absent and present counts 
                if (is_array($students) && count($students) > 0) {
                    $course = $this->s_model->getData('courses', array('cid' => $course_id[0], 'school_id'=>$school_id));
                    $course_name = $course['name'];
                    $course_code = $course['code'];

                    $condition = array(
                        'class_date' => $statr_date,
                        'course_id' => $course_id
                    );
                    foreach ($students as $key => $student) {
                        $student_id = $student['sid'];
                        $condition['sid'] = $student_id;
                        $class_sem_report = $this->s_model->studentSemReports($condition);
                        //echo "<pre>";
                        //print_r($class_sem_report);
                        $students[$key]['sem_report'] = $class_sem_report;
                    }
                }
            } else {
                // new fol all calss
                $courses = $this->s_model->getDatas('courses', array('parent' => 0, 'school_id'=>$school_id));
                $students = array();
                foreach ($courses as $course) {
                    $cls_id = array($course['cid']);
                    $cls_students = $this->s_model->getStudentInCourses($cls_id);
                    // get each students absent and present counts 
                    if (is_array($cls_students) && count($cls_students) > 0) {
                        $condition = array(
                            'class_date' => $statr_date,
                            'course_id' => $cls_id
                        );

                        foreach ($cls_students as $key => $student) {
                            $student_id = $student['sid'];
                            $condition['sid'] = $student_id;
                            $class_sem_report = $this->s_model->studentSemReports($condition);
                            $cls_students[$key]['sem_report'] = $class_sem_report;
                            $cls_students[$key]['course_name'] = $course['name'];
                        }
                    }
                    $students = array_merge($students, $cls_students);
                }
                //$sem_teachers = $this->s_model->semTecherName(array('course_id'=>$course_id));
                $course_code = 'all_classes';
            }
            // now make the excel file : 
            $this->excel->setActiveSheetIndex(0);
            $this->excel->getActiveSheet()->setTitle('classAbsentReport');
            $this->excel->getActiveSheet()->mergeCells('A1:C1');
            $this->excel->getActiveSheet()->setCellValue('A1', $current_school['school_name']);
            // $this->excel->getActiveSheet()->setCellValue('A1', 'ثانوية عبداللطيف ثنيان الغانم');
            $this->excel->getActiveSheet()->mergeCells('D1:G2');
            $this->excel->getActiveSheet()->setCellValue('D1', 'كشف الغياب اليومي');
            if (in_array('-1', $course_id)){
                $this->excel->getActiveSheet()->mergeCells('H1:L1');
            }else{
                $this->excel->getActiveSheet()->mergeCells('H1:K1');
            }
            $this->excel->getActiveSheet()->setCellValue('H1', 'وزارة التربية');
            
            
            // sub title
            //$this->excel->getActiveSheet()->setCellValue('B2', date('Y/m/d'));
            $this->excel->getActiveSheet()->setCellValue('B2', $statr_date);
            $this->excel->getActiveSheet()->setCellValue('C2', 'تاريخ');
            $this->excel->getActiveSheet()->setCellValue('H2', $course_name);
            if (in_array('-1', $course_id)){
                $this->excel->getActiveSheet()->setCellValue('L2', 'الصف');
            $this->excel->getActiveSheet()->mergeCells('A3:L3');
            }else{
                $this->excel->getActiveSheet()->setCellValue('K2', 'الصف');
                $this->excel->getActiveSheet()->mergeCells('A3:K3');
            }
            
            // record titile 
            $this->excel->getActiveSheet()->setCellValue('A4', 'ملاحظات');
            $this->excel->getActiveSheet()->setCellValue('B4', '7');
            $this->excel->getActiveSheet()->setCellValue('C4', '6');
            $this->excel->getActiveSheet()->setCellValue('D4', '5');
            $this->excel->getActiveSheet()->setCellValue('E4', '4');
            $this->excel->getActiveSheet()->setCellValue('F4', '3');
            $this->excel->getActiveSheet()->setCellValue('G4', '2');
            $this->excel->getActiveSheet()->setCellValue('H4', '1');
            // $this->excel->getActiveSheet()->setCellValue('I4', $student_id_show?'الرقم المدني':'الأسم');
            
            if (in_array('-1', $course_id)){
                $this->excel->getActiveSheet()->setCellValue('I4', 'الفصل');
                $this->excel->getActiveSheet()->setCellValue('J4', 'الأسم');
                $this->excel->getActiveSheet()->setCellValue('K4', 'الرقم المدني');
                $this->excel->getActiveSheet()->setCellValue('L4', 'م');
            }else{
                $this->excel->getActiveSheet()->setCellValue('I4', 'الأسم');
                $this->excel->getActiveSheet()->setCellValue('J4', 'الرقم المدني');
                $this->excel->getActiveSheet()->setCellValue('K4', 'م');
            }
            
            // data set 
            $i = 5;
            $j = 1;
            $sev_present = 0;
            $six_present = 0;
            $five_present = 0;
            $four_present = 0;
            $three_present = 0;
            $two_present = 0;
            $one_present = 0;
            foreach ($students as $student) {
                $sem_report = $student['sem_report'];
                $this->excel->getActiveSheet()->setCellValue('A' . $i, '');
                //$this->excel->getActiveSheet()->getStyle('B'.$i)->getBorders()->setDiagonalDirection(PHPExcel_Style_Borders::DIAGONAL_BOTH);
                $this->excel->getActiveSheet()->setCellValue('H' . $i, $this->__getAttendanceValueForExcelExport($sem_report[0]));

                if ($sem_report[0]['attendance']) {
                    $sev_present++;
                }
                $this->excel->getActiveSheet()->setCellValue('G' . $i, $this->__getAttendanceValueForExcelExport($sem_report[1]));
                if ($sem_report[1]['attendance']) {
                    $six_present++;
                }
                $this->excel->getActiveSheet()->setCellValue('F' . $i, $this->__getAttendanceValueForExcelExport($sem_report[2]));
                if ($sem_report[2]['attendance']) {
                    $five_present++;
                }
                $this->excel->getActiveSheet()->setCellValue('E' . $i, $this->__getAttendanceValueForExcelExport($sem_report[3]));
                if ($sem_report[3]['attendance']) {
                    $four_present++;
                }
                $this->excel->getActiveSheet()->setCellValue('D' . $i, $this->__getAttendanceValueForExcelExport($sem_report[4]));
                if ($sem_report[4]['attendance']) {
                    $three_present++;
                }
                $this->excel->getActiveSheet()->setCellValue('C' . $i, $this->__getAttendanceValueForExcelExport($sem_report[5]));
                if ($sem_report[5]['attendance']) {
                    $two_present++;
                }
                $this->excel->getActiveSheet()->setCellValue('B' . $i, $this->__getAttendanceValueForExcelExport($sem_report[6]));
                if ($sem_report[6]['attendance']) {
                    $one_present++;
                }
                if (in_array('-1', $course_id)){
                    $this->excel->getActiveSheet()->setCellValue('I' . $i, $student['course_name']);
                    $this->excel->getActiveSheet()->setCellValue('J' . $i, $student['name']);
                    $this->excel->getActiveSheet()->setCellValue('K' . $i, $student['student_id']);
                    $this->excel->getActiveSheet()->setCellValue('L' . $i, str_pad($j++, 2, 0, STR_PAD_LEFT));
                }else{
                    $this->excel->getActiveSheet()->setCellValue('I' . $i, $student['name']);
                    $this->excel->getActiveSheet()->setCellValue('J' . $i, $student['student_id']);
                    $this->excel->getActiveSheet()->setCellValue('K' . $i, str_pad($j++, 2, 0, STR_PAD_LEFT));
                }
                $i++;
            }
            $styleArray = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('argb' => '000'),
                    ),
                ),
            );
            $this->excel->getActiveSheet()->getStyle('A1:K' . ($i - 1))->applyFromArray($styleArray);
            // footer summery 
            ++$i;
            $this->excel->getActiveSheet()->setCellValue('A' . $i, 'يعتمد ،،،');
            $this->excel->getActiveSheet()->setCellValue('H' . $i, $sev_present);
            $this->excel->getActiveSheet()->setCellValue('G' . $i, $six_present);
            $this->excel->getActiveSheet()->setCellValue('F' . $i, $five_present);
            $this->excel->getActiveSheet()->setCellValue('E' . $i, $four_present);
            $this->excel->getActiveSheet()->setCellValue('D' . $i, $three_present);
            $this->excel->getActiveSheet()->setCellValue('C' . $i, $two_present);
            $this->excel->getActiveSheet()->setCellValue('B' . $i, $one_present);
            //$this->excel->getActiveSheet()->setCellValue('I'.$i,'عدد الغائبين');
            $this->excel->getActiveSheet()->setCellValue('I' . $i, 'عدد الحاضرين');
            $this->excel->getActiveSheet()->setCellValue('J' . $i, 'عدد الحاضرين');
            if (in_array('-1', $course_id)){
                $this->excel->getActiveSheet()->setCellValue('K' . $i, 'عدد الحاضرين');
                $this->excel->getActiveSheet()->setCellValue('L' . $i, 'اجمالي الشعبة');
            }else{
                $this->excel->getActiveSheet()->setCellValue('K' . $i, 'اجمالي الشعبة');
            }
            
            ++$i;
            $total_present = count($students);
            $this->excel->getActiveSheet()->setCellValue('A' . $i, 'مدير المدرسة');
            $this->excel->getActiveSheet()->setCellValue('B' . $i, ($total_present - $sev_present));
            $this->excel->getActiveSheet()->setCellValue('C' . $i, ($total_present - $six_present));
            $this->excel->getActiveSheet()->setCellValue('D' . $i, ($total_present - $five_present));
            $this->excel->getActiveSheet()->setCellValue('E' . $i, ($total_present - $four_present));
            $this->excel->getActiveSheet()->setCellValue('F' . $i, ($total_present - $three_present));
            $this->excel->getActiveSheet()->setCellValue('G' . $i, ($total_present - $two_present));
            $this->excel->getActiveSheet()->setCellValue('H' . $i, ($total_present - $one_present));
            //$this->excel->getActiveSheet()->setCellValue('I'.$i,'عدد الحاضرين');
            $this->excel->getActiveSheet()->setCellValue('I' . $i, 'عدد الغائبين');
            $this->excel->getActiveSheet()->setCellValue('J' . $i, 'عدد الغائبين');
            if (in_array('-1', $course_id)){
                $this->excel->getActiveSheet()->setCellValue('K' . $i, 'عدد الغائبين');
                $this->excel->getActiveSheet()->setCellValue('L' . $i, $total_present);
            }else{
                $this->excel->getActiveSheet()->setCellValue('K' . $i, $total_present);
            }
            
            //techer name 
            ++$i;
            $sem_teachers = $this->s_model->semTecherName(array('course_id' => $course_id));
            //print_r($sem_teachers);

            if (!empty($sem_teachers)) {
                $sem_teachers = array_reverse($sem_teachers);
                $this->excel->getActiveSheet()->setCellValue('A' . $i, '');
                $this->excel->getActiveSheet()->setCellValue('B' . $i, $sem_teachers[0]['name']);
                $this->excel->getActiveSheet()->setCellValue('C' . $i, $sem_teachers[1]['name']);
                $this->excel->getActiveSheet()->setCellValue('D' . $i, $sem_teachers[2]['name']);
                $this->excel->getActiveSheet()->setCellValue('E' . $i, $sem_teachers[3]['name']);
                $this->excel->getActiveSheet()->setCellValue('F' . $i, $sem_teachers[4]['name']);
                $this->excel->getActiveSheet()->setCellValue('G' . $i, $sem_teachers[5]['name']);
                $this->excel->getActiveSheet()->setCellValue('H' . $i, $sem_teachers[6]['name']);
                $this->excel->getActiveSheet()->setCellValue('I' . $i, 'اسم المدرس');
                $this->excel->getActiveSheet()->setCellValue('J' . $i, '');
                if (in_array('-1', $course_id)){
                    $this->excel->getActiveSheet()->setCellValue('K' . $i, '');
                    $this->excel->getActiveSheet()->setCellValue('L' . $i, '');
                }else{
                    $this->excel->getActiveSheet()->setCellValue('K' . $i, '');
                }
                
            } else {
                $this->excel->getActiveSheet()->setCellValue('A' . $i, '');
                $this->excel->getActiveSheet()->setCellValue('B' . $i, '');
                $this->excel->getActiveSheet()->setCellValue('C' . $i, '');
                $this->excel->getActiveSheet()->setCellValue('D' . $i, '');
                $this->excel->getActiveSheet()->setCellValue('E' . $i, '');
                $this->excel->getActiveSheet()->setCellValue('F' . $i, '');
                $this->excel->getActiveSheet()->setCellValue('G' . $i, '');
                $this->excel->getActiveSheet()->setCellValue('H' . $i, '');
                $this->excel->getActiveSheet()->setCellValue('I' . $i, 'اسم المدرس');
                $this->excel->getActiveSheet()->setCellValue('J' . $i, '');
                if (in_array('-1', $course_id)){
                    $this->excel->getActiveSheet()->setCellValue('K' . $i, '');
                    $this->excel->getActiveSheet()->setCellValue('L' . $i, '');
                }else{
                    $this->excel->getActiveSheet()->setCellValue('K' . $i, '');
                }
            }
            /* $styleArray = array(
              'borders' => array(
              'allborders' => array(
              'style' => PHPExcel_Style_Border::BORDER_THICK,
              'color' => array('argb' => 'FFFF0000'),
              ),
              ),
              ); */
            $this->excel->getActiveSheet()->getStyle('B' . ($i - 2) . ':I' . $i)->applyFromArray($styleArray);
            if (in_array('-1', $course_id)){
                $this->excel->getActiveSheet()->getStyle('L' . ($i - 2) . ':L' . ($i - 1))->applyFromArray($styleArray);
                //$this->excel->getActiveSheet()->getStyle('A1:J'.$i)->getAlignment()->setWrapText(true);
                $this->excel->getActiveSheet()->getStyle('A1:L' . $i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('A1:L' . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }else{
                $this->excel->getActiveSheet()->getStyle('K' . ($i - 2) . ':K' . ($i - 1))->applyFromArray($styleArray);
                //$this->excel->getActiveSheet()->getStyle('A1:J'.$i)->getAlignment()->setWrapText(true);
                $this->excel->getActiveSheet()->getStyle('A1:K' . $i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('A1:K' . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }
            
            $filename = date("Y-m-d", strtotime($statr_date)) . '_' . $course_code . '.xls'; //save our workbook as this file name
            
            //$filename=date("Y-m-d").'_classAbsentReport.xls'; //save our workbook as this file name
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
            header('Cache-Control: max-age=0');
            $objWriter->save('php://output');
        }
        exit(0);
    }

    /* public function groups($page = ''){
      $page = ($page == '' || $page == 0 ) ? 1 : $page;
      $start = ($page > 1) ? ($page - 1) * $this->per_page : 0;
      $groups = $this->s_model->getDatas('course_groups');

      $data['page_title'] = 'Class Group List ';
      $count = count($groups);
      $data['pagination'] = $this->createPagination('course_groups', $count);
      $data['groups'] = $groups;
      $data['total'] = $count;
      $data['start'] = $start + 1;
      $data['end'] = $start + $count;

      $this->s_lib->loadView('group_list', $data);
      } */

   public function groups($page = '') {
        $page = ($page == '' || $page == 0 ) ? 1 : $page;
        $start = ($page > 1) ? ($page - 1) * $this->per_page : 0;
        $school_id = $this->session->userdata('school_id');
        $find_cond = array('school_id' => $school_id);
        $groups = $this->s_model->getDatas('course_groups', $find_cond);
        if ($groups) {
            foreach ($groups as $key => $group) {
                //get all class name from the
                $cids = $group['course_ids'];
                
                
                $query = "SELECT name FROM abs_courses WHERE cid IN(" . $cids . ")";
                $courses = $this->s_model->groupClassesName($query);
                $classnames = '';
                if ($courses) {
                    $courses = $courses->result_array();
                    $course_names = array();
                    foreach ($courses as $course) {
                        $course_names[] = $course['name'];
                    }
                    if (count($course_names) > 0) {
                        $classnames = implode(',', $course_names);
                    }
                }
                $groups[$key]['class_names'] = $classnames;
            }
        }
        $data['page_title'] = 'Class Group List ';
        $count = count($groups);
        $data['pagination'] = $this->createPagination('course_groups', $count);
        $data['groups'] = $groups;
        $data['total'] = $count;
        $data['start'] = $start + 1;
        $data['end'] = $start + $count;

        $this->s_lib->loadView('group_list', $data);
    }
    
    private function __getAttendanceValueForExcelExport($values){
        $ret = '-';
        if ($values['attendance_orig']!=''){
            $ret = ($values['attendance_orig']*1?'1':"0");
        }
        return $ret;
    }
    
}
