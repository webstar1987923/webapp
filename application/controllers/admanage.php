<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admanage
 *
 * @author Suchandan
 */
class Admanage extends My_Controller {

    public $data = array();

    public function __construct() {
        parent::__construct();
        $this->s_lib->checkMethodAccess($this->session->userdata('user_type'), $this->router->fetch_class(), $this->router->fetch_method());
    }

    public function index() {
        $this->manage();
    }

    public function manage($ad_id = '') {

        $this->auth();
        $this->data['action'] = 'save';

        if ($ad_id) {
            $ad_check = $this->s_model->getData('ads', array('ID' => $ad_id));
            if (!$ad_check) {
                show_error("No ad exists with this ID");
            }
//            print_r($ad_check);
            $this->data['action'] = 'update';
            print_r($this->data['ad_medias']);
            $this->data['ad_medias'] = $this->s_model->getAdMedias($ad_id);
            $this->data['ads'] = $ad_check;
        }
        $this->data['medias'] = $this->s_model->getDatas('medias');
        $action = $this->post('action');
        if ($action == 'save') {
            $ad = $this->post('ads');
//            print_r($ad);

            $medias = $ad['media'];
            unset($ad['media']);
            $ad_id = $this->s_model->insertRecord('ads', $ad);
            $this->addMedia($ad_id, 'ad', $medias);
            $this->redirectTO('admanage', "Ad successfully saved.");
        } elseif ($action == 'update') {
            $ad = $this->post('ads');
            $medias = $ad['media'];
            unset($ad['media']);
            $this->s_model->updateRecord('ads', array('ID' => $ad_id), $ad);
            $this->s_model->deleteRecord('link_medias', array('object_id' => $ad_id));
            $this->addMedia($ad_id, 'ad', $medias);
            $this->redirectTO('admanage/manage/' . $ad_id, "Ad successfully saved.");
        }
        $this->s_lib->loadView('add-manage', $this->data);
    }

    public function delete($ad_id = 0) {
        if (!$ad_id) {
            show_error('Invalid ad ID.');
        }

        $this->auth();
        $ad = $this->s_model->getData('ads', array('ID' => $ad_id));
        if (!$ad) {
            show_error('This ad does not exists.');
        }
        $this->s_model->deleteRecord('ads', array('ID' => $ad_id));
        $this->s_model->deleteRecord('link_medias', array('object_id' => $ad_id, 'object_type' => 'ad'));
        $this->redirectTO('lists/ads', 'The ad successfully deleted.');
    }

    public function addMedia($object_id, $object_type = 'ad', $medias = array()) {
        if ($object_id && $medias) {
            foreach ($medias as $media_id) {
                $this->s_model->insertRecord('link_medias', array(
                    'media_id' => $media_id,
                    'object_id' => $object_id,
                    'object_type' => $object_type,
                    'date' => date('Y-m-d H:i:s')
                ));
            }
        }
    }

    public function assign() {
        $this->auth();
        $this->data['action'] = 'save';
        $ads_data = array();
        $this->data['ads'] = array();
        $ads = $this->s_model->getDatas('ads');
        $ads = $this->s_model->getDatas('ads');
        if($ads){
            foreach ($ads as $key => $value) {
                if($value['section']){
                    $this->data['ads'][$value['section']]['ID']     = $value['ID'];
                    $this->data['ads'][$value['section']]['medias'] = $this->s_model->getAdMedias($value['ID']);
                }
            }
            
        }
        $action = $this->post('action');
        if ($action == 'save') {
            $ad = $this->post('ads');
            if ($ad) {
                foreach ($ad as $key => $val) {
                    $adNew = array(
                        'section' => $key
                    );
                    if(isset($val['ID'])){
                        $this->s_model->deleteRecord('link_medias', array('object_id' => $val['ID']));
                        $ad_id = $val['ID'];
                    }else{
                        $ad_id = $this->s_model->insertRecord('ads', $adNew);
                    }
                    if(isset($val['media']) && $val['media']){
                        $this->addMedia($ad_id, 'ad', $val['media']);
                    }
                }
            }
            $this->redirectTO('admanage/'.__FUNCTION__, "Ad successfully saved.");
        }
//        print_r($this->data['ads']);
        $this->data['medias'] = $this->s_model->getDatas('medias', array(), array('*'), array(), array(), 'sort');
        $this->s_lib->loadView('ads-assign', $this->data);
    }
    public function keep($param) {
        //        if ($ad_id) {
//            $ad_check = $this->s_model->getData('ads', array('ID' => $ad_id));
//            if (!$ad_check) {
//                show_error("No ad exists with this ID");
//            }
//            $this->data['action'] = 'update';
//            $this->data['ad_medias'] = $this->s_model->getAdMedias($ad_id);
//            $this->data['ads'] = $ad_check;
//        }

        
        $action = $this->post('action');
        if ($action == 'save') {
            $ad = $this->post('ads');
            $media = $ad['media'];
            if ($media) {
                foreach ($media as $key => $val) {
                    $adNew = array(
                        'section' => $key
                    );
                    $ad_id = $this->s_model->insertRecord('ads', $adNew);
                    $this->addMedia($ad_id, 'ad', $val);
                }
            }
            $this->redirectTO('admanage/'.__FUNCTION__, "Ad successfully saved.");
        } elseif ($action == 'update') {
            $ad = $this->post('ads');
            print_r($ad);
            exit;
            $medias = $ad['media'];
            unset($ad['media']);
            $this->s_model->updateRecord('ads', array('ID' => $ad_id), $ad);
            $this->s_model->deleteRecord('link_medias', array('object_id' => $ad_id));
            $this->addMedia($ad_id, 'ad', $medias);
            $this->redirectTO('admanage/'.__FUNCTION__, "Ad successfully saved.");
        }
//        print_r($this->data['ads']);
    }
	
	public function add(){
		
	}
	
	public function ads($id=0){
		$data = array();
		$action = 0;
		$tablename = "slideshow_ads_images";
		$ads_image_id=0;
		$old_file_name='';
		$maxsize='';//in kb
		$req_width="";
		$reg_height="";
		$error_mgs='';
		$success_mgs='';
		if($this->post('action')){
			$action=1;
			$ads_image_id = $this->post('ads_image_id');
			
			if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])){
				// image present 
				$basepath = "uploads/slideshow_ads/";
				$size = $_FILES['image']['size'];
				list($imgwidth,$imgheight) = getimagesize($_FILES['image']['tmp_name']);
				
				print_r($imgwidth);
				print_r($imgheight);
				$imagevalid=true;
				if(!empty($req_width)){
					// restriction here 
					if($req_width!=$imgwidth){
						$imagevalid=false;
						$error_mgs.="Require widh is ".$req_width." px<br>";
					}
					if($reg_height!=$imgheight){
						$imagevalid=false;
						$error_mgs.="Require height is ".$reg_height." px<br>";
					}
					if($maxsize<$size){
						$imagevalid=false;
						$error_mgs.="Require max size is ".$maxsize." KB<br>";
					}
				}
				
				if($imagevalid){
					$filename = time()."_".str_replace(array(' '),'',$_FILES['image']['name']);
					if(move_uploaded_file($_FILES['image']['tmp_name'],$basepath.$filename)){
						// successfully upload the file 
						
						if(empty($ads_image_id)){
							$this->s_model->insertRecord($tablename, array(
								'image' => $filename,
								'datetime' => date('Y-m-d H:i:s')
							));
							$success_mgs="Ads image add successfully";
						}
						else{
							// update the image ads
							$updata = array(
								'image'=>$filename,
								'update_datetime' => date('Y-m-d H:i:s')
							);
							$this->s_model->updateRecord($tablename, array('ID' => $ads_image_id), $updata);
							$success_mgs="Ads image update successfully";
						}
					}
				}
			}
		}
		// now gel all the uploaded images 
		if($id>0){
			$ads_image = $this->s_model->getData($tablename, array('ID' => $id));
			if(!empty($ads_image)){
				$ads_image_id = $ads_image['ID'];
				$old_file_name = $ads_image['image'];
			}
		}
		
		$ads_images = $this->s_model->getDatas($tablename);
		$data['ads_images']=$ads_images;
		$data['ads_image_id']=$ads_image_id;
		$data['old_file_name']=$old_file_name;
		$data['error_mgs']=$error_mgs;
		$data['success_mgs']=$success_mgs;
		$this->s_lib->loadView('slideshow_ads', $data);
	}
	
	public function changestatus($id=0,$status=0){
		if($id>0){
			$tablename = "slideshow_ads_images";
			$updata = array(
				'status'=>$status
			);
			$this->s_model->updateRecord($tablename, array('ID' => $id), $updata);
		}
		redirect(base_url('admanage/ads'));
	}

}
