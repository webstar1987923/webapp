<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of lock
 *
 * @author Suchandan
 */
class lock extends My_Controller{
    public $data = array();
    public function __construct() {
        parent::__construct();
        $this->s_lib->checkMethodAccess($this->session->userdata('user_type'), $this->router->fetch_class(), $this->router->fetch_method());
    }
    public function callender() {
     $school_id = $this->session->userdata('school_id'); 
        $this->data['heading']   = "Lock Holidays";
        $this->data['action']    = 1;
        $this->data['holidays']  = $this->s_model->getDatas('holidays',array('school_id' => $school_id),array('date'));
        $holidat_string = array();
        if($this->data['holidays']){
            foreach($this->data['holidays'] as $date){
                $holidat_string[] = $date['date'];
            }  
        }
        $this->data['holiday_array']  = $holidat_string;
        $this->data['holiday_string']  = implode(',', $holidat_string);
        if($this->post('action') == 1){
            $dates = $this->post('dates');
//            $remove_date = $this->post('remove_date');
//            $remove_date = str_replace(',', "','", $remove_date);
//            $this->s_model->show_query = 1;
//            $hl_tab = $this->s_model->getTableName('holidays');
//            print_r($dates);
//            print_r($remove_date);
//            print_r("DELETE FROM $hl_tab WHERE date IN ('$remove_date')");
//            $this->db->query("DELETE FROM $hl_tab WHERE date IN ('$remove_date')");
//            exit;
	    $school_id = $this->session->userdata('school_id'); 
            $hl_tab = $this->s_model->getTableName('holidays');
            $this->db->where('school_id',$school_id);
            $this->db->delete($hl_tab);
          
            //$this->db->query("TRUNCATE TABLE $hl_tab");
            if(empty($dates)){
                $this->redirectTO('lock/callender');
            }else{
                $dates_array = explode(',', $dates);
                foreach($dates_array as $date){
                    if($date != ''){
                        $data = $this->s_model->getData('holidays',array('date' => $date, 'school_id' => $school_id));
                        if(empty($data)){
                            $this->s_model->insertRecord('holidays',array(
                                'date' => $date,
                                'school_id' => $school_id
                            ));
                        }
                    }
                }
                $this->redirectTO('lock/callender', 'The calender saved successfully.');
            }
        }
        $this->s_lib->loadView('calender',  $this->data);
    }

}
