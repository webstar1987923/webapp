<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$lang['invalid_user']                       = "خطأ في إدخال البيانات";
$lang['some_error']                         = "حدث خطأ برجاء المحاولة مره أخرى";
$lang['logged_out']                         = "تم تسجيل الخروج بنجاح";
$lang['mandatory']                          = "الحقول إلزامية لا يمكن أن تكون فارغة";
$lang['select_course']                      = "الرجاء تحديد اسم المرحلة";
$lang['course_saved']                       = "تم حفظ اسم المرحلة بنجاح";
$lang['select_course']                      = "الرجاء تحديد اسم المرحلة";
$lang['attendance_saved']                   = "تم حفظ الغياب بنجاح";
$lang['you_are_not_allowed']                = "غير مسموح لك القيام بذلك";
$lang['invalid_email']                      = "البريد الإلكتروني غير صحيح";
$lang['mail_not_sent']                      = "حدث خطأ أثناء الإرسال";
$lang['password_reset_link_send']           = "تم إرسال كلمة المرور الجديدة إلى بريدك الإلكتروني";
$lang['note_sved']                          = "تم حفظ الملاحظة بنجاح";
$lang['student_saved']                      = "تم حفظ البيانات بنجاح";
$lang['student_deleted']                    = "تم حذف الطالب بنجاح";
$lang['has_given_absent']                   = "تم إدخال الغياب مسبقاً";
$lang['sem']                                = "الحصة";
$lang['empty_not']                          = "لا يمكن أن يكون الحقل فارغ";
$lang['note_id_invalid']                    = "لا يمكن أن يكون الحقل فارغ";
$lang['note_deleted']                       = "تم حذف الملاحظة بنجاح";
$lang['user_saved']                         = "تم حفظ البيانات بنجاح";
$lang['pass_not_match']                     = "كلمة المرور السابقة غير صحيحة";
$lang['pass_same']                          = "كلمة المرور هي نفس كلمة المرور السابقة";
$lang['old_empty_pass']                     = "كلمة المرور السابقة فارغة";
$lang['new_empty_pass']                     = "كلمة المرور الجديدة فارغة";
$lang['notific_send']                       = "تم الإرسال بنجاح";
$lang['notific_deleted']                    = "تم حذف الرسالة بنجاح";
