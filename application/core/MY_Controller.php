<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of My_Controller
 *
 * @author Suchandan
 */
class My_Controller extends CI_Controller {
    
    public $data = array();
	
    public function __construct($ajax = false) {
        parent::__construct();
        //date_default_timezone_set('Asia/Kuwait');
        //ini_set('max_execution_time', 5000);
        if( !$ajax ){
           $this->auth(); 
        }
    }
    public function auth() {
        $logged_in = $this->session->userdata('logged_in');
        if(empty($logged_in) || !$logged_in){
            redirect(base_url('login'));
        }
    }
    public function authAjax() {
        $logged_in = $this->session->userdata('logged_in');
        if(empty($logged_in) || !$logged_in){
            return false;
        }
        return true;
    }
    public function redirectTO($slug = 'dashboard',$msg = '',$status = 200) {
        $class = 'success';
        if($status == 400){
            $class = 'danger';
        }
        $this->session->set_flashdata('response',array('msg' => $msg , 'status' => $status , 'class' => $class));
        $url = base_url($slug);
        redirect($url);
    }
    public function redirectOnError($slug = 'dashboard',$msg = '') {
        $this->redirectTO($slug, $msg, 400);
    }
    public function langLine($name , $replacer = '',$subject = ''){
        
        if (($line = $this->lang->line($name)) == 'false') {
            return $name;
        } else {
            $subject = ($subject == '') ? '{object}' : $subject;
            $line = str_replace($subject, $replacer, $line);
        }
        return $line;
    }
    public function checkArrayEmptyField($fields = array(),$data = array()) {
        if( empty($fields) || empty($data) ){
            return false;
        }
        foreach($data as $key => $val){
            if(in_array($key, $fields) && empty($val)){

                return false;
            }
        }
       
        return true;
    }
    public function post($param) {
        return $this->input->post($param);
    }
    public function get($param) {
        return $this->input->get($param);
    }
    public function updateMeta($type,$object_id,$meta_datas) {
        if(empty($meta_datas) || empty($type)){
            return false;
        }
        if(is_array($meta_datas)){
            foreach ($meta_datas as $key => $value){
                $this->s_model->setMetaData($type,$object_id,$key,$value);
            }
        }
    }

}
