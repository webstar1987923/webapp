<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of s_model
 *
 * @author Suchandan
 */
class s_model extends CI_Model {

    //Local variables for data holding
    public $data = array();
    //local variables for ids
    public $user_no;
    public $corse_id;
    public $student_id;
    public $tables = array();
    public $where = array();
    public $join = array();
    public $fields = array();
    public $query = '';
    public $count_query = '';
    public $extra = '';
    public $query_type = '';
    public $last_query = '';
    public $group_by = '';
    public $order_by = '';
    public $order_type = 'DESC';
    public $limit = '';
    public $lang_id;
    public $show_query = false;
    // Mrin section
    public $having = '';

    public function __construct() {
        parent::__construct();
        $lang_actives = $this->getData('lang', array('active' => 1));
        if ($lang_actives) {
            $this->lang_id = $lang_actives['ID'];
        } else {
            $this->lang_id = false;
        }
    }

    public function resetVarables() {
        $this->tables = array();
        $this->where = array();
        $this->join = array();
        $this->fields = array();
        $this->query = '';
        $this->extra = '';
        $this->query_type = 'SELECT';
        $this->group_by = '';
        $this->order_by = '';
        $this->order_type = 'DESC';
        $this->limit = '';
    }

    public function set_session($user_no, $session_id) {

        $query = $this->deleteRecord('session', array('user_no' => $user_no));

        $data = array(
            'user_no' => $user_no,
            'session_id' => $session_id,
        );

        $query = $this->insertRecord('session', $data);
        return $query;
    }

    public function countData($table, $cond = array(), $fields = array('COUNT(*) as count'), $join = array(), $limit = array(), $order_by = '', $order = 'DESC', $grop_by = '') {
        $res = $this->getData($table, $cond, $fields, $join, $limit, $order_by, $order, $grop_by);
        return $res['count'];
    }

    public function insertRecord($table, $data) {
        return $this->runQuery($table, 'insert', array('data' => $data));
    }

    public function deleteRecord($table, $data) {
        return $this->runQuery($table, 'delete', array('where' => $data));
    }

    public function updateRecord($table, $cond, $data) {
        return $this->runQuery($table, 'update', array('where' => $cond, 'data' => $data));
    }
    public function saveAdsSort($data) {
        foreach($data as $ads=>$index){
//            $this->db->set('sort', $index);
//            $this->db->where('ID', $ads);
//            $this->db->update('abs_ads');
//            print $ads."===>>>".$index;
            $this->runQuery('medias', 'update', array('where' => array('ID'=>$ads), 'data' => array('sort'=>$index)));
        }
//        print_r($data);die;
    }

    public function getData($table, $cond = array(), $fields = array('*'), $join = array(), $grop_by = '') {
        $query_data = array(
            'fields' => $fields,
            'where' => $cond,
            'join' => $join,
            'gooup_by' => $grop_by
        );
        $result = $this->runQuery($table, 'select', $query_data);
        return $result->row_array();
    }

    public function getDatas($table, $cond = array(), $fields = array('*'), $join = array(), $limit = array(), $order_by = '', $order = 'DESC', $grop_by = '') {
        $ret = array();
        $query_data = array(
            'fields' => $fields,
            'where' => $cond,
            'join' => $join,
            'gooup_by' => $grop_by,
            'order_by' => $order_by,
            'order' => $order,
            'limit' => $limit
        );
        $result = $this->runQuery($table, 'select', $query_data);
        $data = $result->result_array();
        foreach($data as $item){
            $ret[] = $item;
//            $ret[$item['cid']] = $item;
        }
        return $ret;
    }

    private function runQuery($table = '', $query_type = 'select', $query_data = array(), $custom = false, $query = '') {

        $result = false;

        $query_data_default['data'] = array();
        $query_data_default['fields'] = array('*');
        $query_data_default['join'] = array();
        $query_data_default['where'] = array();
        $query_data_default['all'] = false;
        $query_data_default['gooup_by'] = '';
        $query_data_default['order_by'] = '';
        $query_data_default['order'] = '';
        $query_data_default['limit'] = array();


        $query_data = array_merge($query_data_default, $query_data);


        extract($query_data);

        $fields = implode(',', $fields);

        if ($table == '')
            return false;
        $table = $this->db->dbprefix($table);

        if ($custom) {
            $result = $this->db->query($query);
        } else {
            switch ($query_type) {
                case 'select':
                    $this->db->select($fields);
                    $this->db->from($table);
                    if (!empty($join)) {
                        foreach ($join as $key => $value) {
                            $cond = (isset($value['cond'])) ? $value['cond'] : '';
                            $type = (isset($value['type'])) ? $value['cond'] : 'left';
                            $this->db->join($key, $cond, $type);
                        }
                    }
                    $this->db->where($where);

                    if (!empty($gooup_by)) {
                        $this->db->group_by($gooup_by);
                    }
                    if (!empty($order_by)) {
                        $this->db->order_by($order_by, $order);
                    }
                    if (!empty($limit)) {
                        $this->db->limit(10, 20);
                    }

                    $result = $this->db->get();
                    // print_r($this->db->last_query());
                    break;
                case 'update' :
                    $this->db->where($where);
                    $result = $this->db->update($table, $data);
                    $result = $this->db->affected_rows();
                    break;
                case 'insert' :
                    $result = $this->db->insert($table, $data);
                    if ($this->db->insert_id()) {
                        $result = $this->db->insert_id();
                        ;
                    }
                    break;
                case 'delete' : $this->db->delete($table, $where);
                    $result = $this->db->affected_rows();
                    break;
            }
        }
        if ($this->show_query) {
            ob_start();
            print_r('||');
            print_r($this->db->last_query());
        }
        return $result;
    }

    private function query($query) {
        $result = $this->db->query($query);
        if ($this->show_query) {
            ob_start();
            print_r('||');
            print_r($this->db->last_query());
        }
        return $result->result();
    }

    public function setQuery() {

        if (!is_array($this->tables)) {
            $this->tables = array($this->tables);
        }
        if (!is_array($this->fields)) {
            $this->fields = array($this->fields);
        }
        if (!is_array($this->where)) {
            $this->where = array($this->where);
        }
//        foreach($this->tables as $key => $tab){
//            $this->tables[$key] = $this->db->dbprefix($tab);
//        }
        $join_string = (empty($this->join)) ? '' : implode($this->join, ' ');
        $where_string = (empty($this->where)) ? '' : implode($this->where, ' AND ');
        $fields_string = (empty($this->fields)) ? '*' : implode($this->fields, ',');
        $table = (empty($this->tables)) ? '' : implode($this->tables, ',');

        $groupby = ($this->group_by != '' ) ? "GROUP BY " . $this->group_by : '';
        $order_type = $this->order_type;
        $order_by = ($this->order_by != '' ) ? "ORDER BY " . $this->order_by . " $order_type" : '';

        $limit = ($this->limit != '' ) ? "LIMIT " . $this->limit : '';

        $extra = $this->extra;
        $query_type = ($this->query_type == '') ? "SELECT " : $this->query_type;

        $where_string = ($where_string == '' ) ? "" : " WHERE " . $where_string;

        $from = "FROM";

        if ($query_type == "UPDATE" || $query_type == "INSERT" || $query_type == "TRUNCATE")
            $from = "";

        if ($query_type == "DELETE")
            $fields_string = '';

        // Mrin section
        $having = '';
        if ($this->having != '') {
            $having = "HAVING " . $this->having;
        }


        $this->query = "$query_type $extra $fields_string $from $table $join_string $where_string $groupby $having $order_by $limit";
        $this->count_query = "$query_type * $from $table $join_string $where_string $groupby $having $order_by";

        $this->last_query = $this->query;

        $this->resetVarables();

        return $this->last_query;
    }

    public function selectRun($table) {

        $this->tables = array($table);

        $sql_query = $this->setQuery();

        if ($this->show_query) {
            print_r('||');
            print_r($sql_query);
        }

        $result = $this->db->query($sql_query);

        return $result;
    }

    public function getFormatedWhere($where = array()) {
        $where_array = array();
        if (!empty($where)) {
            foreach ($where as $field => $value) {
                $where_array[] = "$field = '$value'";
            }
        }
        return $where_array;
    }

    public function execute($query = '') {

        if ($query == '') {
            $query = $this->setQuery();
        }

        if ($this->show_query) {
            print_r('<br>||');
            print_r($query);
        }

        $result = $this->db->query($query);
        return $result;
    }

    public function getTableName($table = '') {
        if ($table == '') {
            return false;
        }
        return $this->db->dbprefix($table);
    }

    //Custom functions
    public function getCourses($admin) {
        $user_no = $this->input->post('user_no');
        $this->tables = $this->getTableName('teacher_courses');
        $course = $this->getTableName('courses');
        $this->fields = array(
            "$this->tables.cid",
            "$course.name",
            "$course.code"
        );
        $this->join[] = "LEFT JOIN $course ON $course.cid = $this->tables.cid";
        $this->where[] = "$course.parent = '0'";
        $this->order_by = "$course.cid";
        if (!$admin) {
            $this->where[] = "$this->tables.user_no = '$user_no'";
        }
        $this->group_by = "$this->tables.cid";
        $this->order_type = "DESC";

        $result = $this->execute()->result_array();

        return $result;
    }

    public function parentCourses() {
        $user_no = $this->input->post('user_no');
        $this->tables = $this->getTableName('student_courses');
        $course = $this->getTableName('courses');
        $student_parents = $this->getTableName('student_parents');
        $this->fields = array(
            "$this->tables.cid",
            "$course.name",
            "$course.code"
        );
        $this->join[] = "LEFT JOIN $course ON $course.cid = $this->tables.cid";
//        $this->join[]   = "LEFT JOIN $student_parents ON $student_parents.sid = $this->tables.sid";
//        $this->where[]  = "$course.parent = '0'";
        $this->order_by = "$course.cid";
//        if( !$admin ){
        $this->where[] = "$this->tables.sid IN (SELECT sid FROM $student_parents WHERE $student_parents.user_no = '$user_no')";
//        }
        $this->group_by = "$this->tables.cid";
        $this->order_type = "DESC";

        $result = $this->execute()->result_array();

        return $result;
    }

    public function getTeacherCourses($tid) {

        $this->tables = $this->getTableName('teacher_courses');
        $course_table = $this->getTableName('courses');

//        $this->fields[] = "";

        $this->join[] = "LEFT JOIN $course_table ON $course_table.cid = $this->tables.cid";

        $this->where[] = "$this->tables.user_no = '$tid'";

        $result = $this->execute()->result_array();

        return $result;
    }

    public function getLastSemNo($cid) {
        $date = ($this->input->post('date')) ? $this->input->post('date') : date('Y-m-d');
        $this->tables = $this->getTableName('attendance');
        $this->fields[] = "IFNULL(MAX($this->tables.semno),0) as max_cem";
        $this->where[] = "$this->tables.cid = '$cid'";
        $this->where[] = "$this->tables.datetime = '$date'";

        return $this->execute()->row_array();
    }

    public function getModifiedAttendances($date) {
        $this->tables = $this->getTableName('attendance');
//                $this->show_query = 1;
        $this->fields[] = "COUNT($this->tables.ID) as modific_count";
        $this->fields[] = "$this->tables.semno";
        $this->where[] = "$this->tables.updated_by != 0";
        $this->where[] = "$this->tables.datetime = '$date'";
        $this->group_by = "$this->tables.semno";
        return $this->execute()->result_array();
    }

    public function getAttendances($cid, $sid) {
        $date = ($this->input->post('date')) ? $this->input->post('date') : date('Y-m-d');
        $data = array();
        $this->tables = $this->getTableName('attendance');
        $user_table = $this->getTableName('users');
        $this->setWhere(array('datetime' => $date, 'cid' => $cid, 'sid' => $sid));
        $this->fields = array('cid', 'sid', 'semno', 'attendance', 'ID',
//            'CONCAT(teacher.first_name," ",teacher.last_name) as teacher',
//            'CONCAT(modarator.first_name," ",modarator.last_name) as modarator'
        );
        $this->join[] = "LEFT JOIN $user_table teacher ON teacher.user_no = $this->tables.entered_by";
        $this->join[] = "LEFT JOIN $user_table modarator ON modarator.user_no = $this->tables.updated_by";
//        $atts = $this->getDatas('', ,);
        $this->group_by = "$this->tables.ID";
        $atts = $this->execute()->result_array();
        if ($atts) {
            foreach ($atts as $key => $value) {
                $sem_no = $value['semno'];
                $sid = $value['sid'];
                $attendance = ($value['attendance']) ? true : false;
                $data['cem-' . $sem_no] = $attendance;
//                $data['teacher'] = $value['teacher'];
//                $data['modaarator'] = $value['modarator'];
            }
        }
        return $data;
    }

    public function getSemTeachers($cid) {
        $date = ($this->input->post('date')) ? $this->input->post('date') : date('Y-m-d');
        $this->tables = $this->getTableName('attendance');
        $user_table = $this->getTableName('users');
        $this->setWhere(array('datetime' => $date, 'cid' => $cid));
        $this->fields = array('semno', 'CONCAT(teacher.first_name," ",teacher.last_name) as teacher',
            'CONCAT(modarator.first_name," ",modarator.last_name) as modarator');

        $this->join[] = "LEFT JOIN $user_table teacher ON teacher.user_no = $this->tables.entered_by";
        $this->join[] = "LEFT JOIN $user_table modarator ON modarator.user_no = $this->tables.updated_by";

        $this->group_by = "$this->tables.semno";

        return $this->execute()->result_array();
    }

    public function getStudentCourses($cid, $parent) {
        $user_no = $this->input->post('user_no');
        $this->tables = $this->getTableName('student_courses');
//        $attndance_table = $this->getTableName('attendance');
        $students = $this->getTableName('students');
        $student_parents = $this->getTableName('student_parents');

        $this->fields[] = "$students.name";
        $this->fields[] = "$this->tables.cid";
        $this->fields[] = "$students.sid";

        $this->join[] = "LEFT JOIN $students ON $students.sid = $this->tables.sid";

        $this->where[] = "$this->tables.cid = '$cid'";
        if ($parent) {
            $this->where[] = "$this->tables.sid IN (SELECT sid FROM $student_parents WHERE $student_parents.user_no = '$user_no')";
        }

        $this->group_by = "$this->tables.sid";

        $result = $this->execute()->result_array();

        return $result;
    }

    public function getSettingsValue($name) {
        $data = $this->getData('settings', array('key' => $name));
        return ($data) ? $data['value'] : false;
    }

    public function getSettings() {
        $settings_object = array();
        $settings = $this->s_model->getDatas('settings');
        if ($settings) {
            foreach ($settings as $setting) {
                $settings_object[$setting['key']] = $setting['value'];
            }
        }
        return $settings_object;
    }

    public function getMetaData($type, $key, $object_id) {
        $lang_id = ($this->input->get('lang_id')) ? $this->input->get('lang_id') : $this->lang_id;
        $cond = array('meta_key' => $key, 'meta_type' => $type, 'object_id' => $object_id);
        $cond['lang_id'] = $lang_id;
        $data = $this->getData('metadata', $cond);
        return ($data) ? $data['meta_value'] : false;
    }

    public function getMetas($type, $object_id) {
        $lang_id = ($this->input->get('lang_id')) ? $this->input->get('lang_id') : $this->lang_id;
        $metas = array();
        $cond = array('meta_type' => $type, 'object_id' => $object_id);
        $cond['lang_id'] = $lang_id;
        $data = $this->getDatas('metadata', $cond);
        if ($data) {
            foreach ($data as $value) {
                $metas[$value['meta_key']] = $value['meta_value'];
            }
        }
        return $metas;
    }

    public function setMetaData($type, $object_id, $key, $value) {
        $lang_id = ($this->input->get('lang_id')) ? $this->input->get('lang_id') : $this->lang_id;
        if (($data = $this->getMetaData($type, $key, $object_id)) === false) {
            $data_ins = array('meta_key' => $key, 'meta_type' => $type, 'object_id' => $object_id, 'meta_value' => $value);
            $data_ins['lang_id'] = $lang_id;
            $this->insertRecord('metadata', $data_ins);
        } else {
            $cond = array('meta_key' => $key, 'meta_type' => $type, 'object_id' => $object_id);
            $cond['lang_id'] = $lang_id;
            $this->updateRecord('metadata', $cond, array('meta_value' => $value));
        }
        return true;
    }

    public function getAttendance($onlyabsents = true, $filterdata = array()) {
//        $this->show_query = 1;
        $this->tables = $this->getTableName('attendance');
        $course_table = $this->getTableName('courses');
        $students_table = $this->getTableName('students');
//        $this->fields[] = "$this->tables.*";
        $this->fields[] = "$course_table.name";
        $this->fields[] = "$course_table.code";
        $this->fields[] = "$course_table.name as class_name";
        $this->fields[] = "$course_table.cid";
        $this->fields[] = "$students_table.sid";
        $this->fields[] = "$students_table.student_id";
        $this->fields[] = "COUNT($this->tables.attendance) as absent_count";
        $this->fields[] = "$this->tables.datetime";
        $this->fields[] = "$students_table.name as student_name";
        // mrin section
        $this->fields[] = "concat('Sem-',GROUP_CONCAT(DISTINCT CAST($this->tables.semno AS CHAR) ORDER BY $this->tables.semno SEPARATOR ', Sem-')) as sems";
        $this->fields[] = "count($this->tables.sid) as abscount";
        // end
        $this->join[] = "INNER JOIN $course_table ON $course_table.cid = $this->tables.cid";
        $this->join[] = "INNER JOIN $students_table ON $students_table.sid = $this->tables.sid";
        $datetime = date('Y-m-d');
        // Mrin edit section 
        //print_r($filterdata);
        if (empty($filterdata)) {
            $this->where[] = "$this->tables.datetime = CURDATE()";
        } else {
            if ((isset($filterdata['start_date']) && !empty($filterdata['start_date'])) && (isset($filterdata['end_date']) && !empty($filterdata['end_date']))) {
                // both date present 
                $this->where[] = "$this->tables.datetime >= '" . $filterdata['start_date'] . "' AND $this->tables.datetime <='" . $filterdata['end_date'] . "'";
            } else {
                //only one date mantion
                if ((isset($filterdata['start_date']) && !empty($filterdata['start_date']))) {
                    // start 
                    $this->where[] = "$this->tables.datetime = '" . $filterdata['start_date'] . "'";
                } elseif ((isset($filterdata['end_date']) && !empty($filterdata['end_date']))) {
                    // end
                    $this->where[] = "$this->tables.datetime = '" . $filterdata['end_date'] . "'";
                }
            }
        }
        if (isset($filterdata['class_id']) && !empty($filterdata['class_id'])) {
            $this->where[] = "$course_table.cid = '" . $filterdata['class_id'] . "'";
        }
        //mrin:end
        if ($onlyabsents) {
            $this->where[] = "$this->tables.attendance = '0'";
        }
        // 
        //print_r($this->where);
        //
        $this->order_by = "$students_table.sid";
        $this->group_by = "$this->tables.sid";
        $this->order_type = "DESC";
        //mrin section having section 
        if ((isset($filterdata['absent_number']) && !empty($filterdata['absent_number']))) {
            $this->having = " abscount >= '" . $filterdata['absent_number'] . "'";
        }
        $result = $this->execute()->result_array();
//        die('inside');
        return $result;
    }

    public function getAbsentCount($sid) {
//        $this->show_query = 1;
        $this->tables = $this->getTableName('attendance');

//        $this->fields[] = "$this->tables.*";
        $this->fields[] = "COUNT($this->tables.ID) as abscount";
        $this->where[] = "$this->tables.attendance = '0'";
        $this->where[] = "$this->tables.sid = '$sid'";
        $this->group_by = "$this->tables.datetime";

        return $this->execute()->result_array();
    }
    public function getAbsentCounts($sids, $filters=array()) {
        $ret = array();
//        $this->show_query = 1;
        $this->tables = $this->getTableName('attendance');
        $where = '';
        if (isset($filters['start_date']) && !empty($filters['start_date'])){
            $where .= " $this->tables.datetime>='".$filters['start_date']."' and ";
        }
        if (isset($filters['end_date']) && !empty($filters['end_date'])){
            $where .= " $this->tables.datetime<='".$filters['end_date']."' and ";
        }
//        $filtercond['start_date']
//        $filtercond['end_date']
//        $this->fields[] = "$this->tables.*";
        $query = "SELECT sid, COUNT($this->tables.ID) as abscount "
                . "FROM ".$this->tables." "
                . "WHERE  $where $this->tables.attendance = 0 and $this->tables.sid IN (".implode(',',$sids).") GROUP by $this->tables.datetime, $this->tables.sid ";
//        print $query;
        $result = $this->db->query($query);
        $data = $result->result_array();
        foreach($data as $item){
            $ret[$item['sid']] += 1;
        }    
                
        return $ret;
    }

    public function setWhere($cond = array()) {
        if (!empty($cond)) {
            foreach ($cond as $key => $val) {
                $this->where[] = "$this->tables.$key = '$val'";
            }
        }
    }

    public function getAds($start, $limit, $cond = array()) {
//        $this->show_query = 1;
        $s = $this->input->get('s');
        $this->tables = $this->getTableName('ads');
        if (!empty($cond)) {
            foreach ($cond as $key => $val) {
                $this->where[] = "$this->tables.$key = '$val'";
            }
        }
//        if($s){
//            $this->where[] = "($this->tables.first_name LIKE '%$s%')";
//        }
        if ($s) {
            $this->where[] = "$this->tables.name LIKE '%$s%'";
        }

        $this->limit = "$start,$limit";
//        $this->order_by = "$this->tables.user_no";
        $this->order_type = "DESC";
        return $this->execute()->result_array();
    }

    public function getUsers($start, $limit, $cond = array()) {
        $s = $this->input->get('s');
        $this->tables = $this->getTableName('users');
        if (!empty($cond)) {
            foreach ($cond as $key => $val) {
                $this->where[] = "$this->tables.$key = '$val'";
            }
        }
        if ($s) {
            $this->where[] = "($this->tables.first_name LIKE '%$s%' OR $this->tables.last_name LIKE '%$s%' OR email_id LIKE '%$s%' "
                    . " OR CONCAT(first_name,' ',last_name) LIKE '%$s%'"
                    . " OR username LIKE '%$s%'"
                    . " OR CONCAT(first_name,last_name) LIKE '%$s%')";
        }
        $this->limit = "$start,$limit";
        $this->order_by = "$this->tables.user_no";
        $this->order_type = "DESC";
        return $this->execute()->result_array();
    }

    public function getUserClasses($user_id) {
        $this->tables = $this->getTableName('teacher_courses');
        $classes_table = $this->getTableName('courses');
        $this->fields = array(
            "$classes_table.code",
            "$classes_table.name",
        );
        $this->join[] = "LEFT JOIN $classes_table ON $this->tables.cid = $classes_table.cid";
        $this->where[] = "$this->tables.user_no = '$user_id'";

        return $this->execute()->result_array();
    }

    public function getStudent($start, $limit, $cond = array()) {
        $s = $this->input->get('s');
        $this->tables = $this->getTableName('students');
//        $student_courses = $this->getTableName('student_courses');
//        $table_course = $this->getTableName('courses');

        $this->fields[] = "$this->tables.*";
//        $this->fields[] = "$table_course.code";
//        $this->join[] = "LEFT JOIN $student_courses ON $student_courses.sid = $this->tables.sid";
//        $this->join[] = "LEFT JOIN $table_course ON $table_course.cid = $student_courses.cid";

        if (!empty($cond)) {
            foreach ($cond as $key => $val) {
                $this->where[] = "$this->tables.$key = '$val'";
            }
        }
        if ($s) {
            $this->where[] = "($this->tables.name LIKE '%$s%' OR $this->tables.student_id LIKE '%$s%')";
        }

        $this->limit = "$start,$limit";

        $this->order_by = "$this->tables.sid";
//        $this->group_by = "$this->tables.sid";
        $this->order_type = "DESC";

        return $this->execute()->result_array();
    }

    public function studentParnt($sid = 0) {
        $this->tables = $this->getTableName('student_parents');
        $user_table = $this->getTableName('users');

        $this->fields[] = "$user_table.first_name";
        $this->fields[] = "$user_table.last_name";
        $this->join[] = "LEFT JOIN $user_table ON $this->tables.user_no = $user_table.user_no";
        $this->where[] = "$this->tables.sid = $sid";
        return $this->execute()->row_array();
    }

    public function getAbsentDetails($start = 0, $limit = 5, $cond = array()) {

        $this->tables = $this->getTableName('attendance');

        if (!empty($cond)) {
            foreach ($cond as $key => $val) {
                $this->where[] = "$this->tables.$key = '$val'";
            }
        }

        $this->group_by = "$this->tables.datetime";
        $this->limit = "$start,$limit";
        $this->order_by = "$this->tables.datetime";
        $this->order_type = "DESC";

        return $this->execute()->result_array();
    }

    public function getCourseList($start, $limit, $cond = array()) {
        $s = $this->input->get('s');
        $this->tables = $this->getTableName('courses');
        $table_student_courses = $this->getTableName('student_courses');

        $this->fields[] = "$this->tables.*";
//        $this->fields[] = "COUNT($table_student_courses.sid) as student_count";
//        $this->join[] = "LEFT JOIN $table_student_courses ON $table_student_courses.cid = $this->tables.cid";
        if (!empty($cond)) {
            foreach ($cond as $key => $val) {
                $this->where[] = "$this->tables.$key = '$val'";
            }
        }
        if ($s) {
            $this->where[] = "$this->tables.name LIKE '%$s%'";
        }
//        $this->where[] = "$this->tables.parent = '0'";
        $this->limit = "$start,$limit";
        $this->order_by = "$this->tables.cid";
//        $this->group_by = "$this->tables.cid";

        $this->order_type = "DESC";
        return $this->execute()->result_array();
    }

    public function getTotalCount() {
        if ($this->show_query) {
            echo '<br>' . $this->count_query . '<br>';
        }
        return $this->db->query($this->count_query)->num_rows;
    }

    public function getStudentCoursesCodes($sid) {

        $this->tables = $this->getTableName('student_courses');
        $table_course = $this->getTableName('courses');

        $this->fields[] = "$table_course.code";
        $this->fields[] = "$table_course.name";
        $this->fields[] = "$this->tables.cid";

        $this->join[] = "LEFT JOIN $table_course ON $table_course.cid = $this->tables.cid";

        $this->where[] = "$this->tables.sid = '$sid'";

        return $this->execute()->result_array();
    }

    public function getUnassignedStudents() {
        $this->tables = $this->getTableName('students');
        $course_student_table = $this->getTableName('student_courses');
        $this->fields[] = "$this->tables.*";
//        $this->fields[] = "COUNT($course_student_table.ID) as course_count";
//        $this->join[] = "LEFT JOIN $course_student_table ON $course_student_table.sid = $this->tables.sid";
//        $this->where[] = "$this->tables.sid NOT IN (SELECT $course_student_table.sid FROM $course_student_table)";
//        $this->group_by = "$this->tables.sid";
        return $this->execute()->result_array();
    }

    public function checkUserExists($email, $password) {
        $this->tables = $this->getTableName('users');
        $con = "$this->tables.email_id = '$email' OR $this->tables.username = '$email'";
        if ($password) {
            $con .= " AND $this->tables.password = '$password'";
        }
        $this->where[] = $con;

        return $this->execute()->row_array();
    }

    public function getAbsences() {
        
    }

    public function getChildrens($user_no) {



        $data = array();
        $this->tables = $this->getTableName('student_parents');
        $abs_students = $this->getTableName('students');
        $this->fields[] = "$this->tables.sid";
        $this->fields[] = "$abs_students.name";
        $this->fields[] = "$abs_students.student_id";
        $this->join[] = "INNER JOIN $abs_students ON $abs_students.sid = $this->tables.sid";
        $this->where[] = "$this->tables.user_no = '$user_no'";
        $this->group_by = "$this->tables.sid";
        $this->order_by = "$this->tables.sid";

        $res = $this->execute()->result_array();

        foreach ($res as $r) {
            $semi = $this->db->query("SELECT DISTINCT Sum(abs_attendance.attendance) , abs_attendance.semno, abs_attendance.datetime FROM abs_attendance WHERE abs_attendance.sid = '" . $r['sid'] . "' and abs_attendance.attendance = '0' GROUP BY abs_attendance.datetime having Sum(abs_attendance.attendance)<=6");
            $r['attendance'] = count($semi->result_array());
            $data[] = $r;
        }

        return $data;
    }

    public function getUserPushIDsWithUserType($types, $one_user_email) {

        $this->tables = $this->getTableName('users');
        $devices = $this->getTableName('devices');
        $this->fields[] = 'registration_id';

        $this->join[] = "INNER JOIN $devices ON $devices.user_no = $this->tables.user_no AND $devices.os_type='1'";

        $where = "$this->tables.user_type IN $types ";
        if (!empty($one_user_email)) {
            $where .= "AND ( $this->tables.email_id = '$one_user_email' OR $this->tables.user_no = '$one_user_email') ";
        }
        $this->where[] = $where;

        return $this->execute()->result_array();
    }

    public function getNotifications($user) {

//        $this->show_query = 1;
        $this->tables = $this->getTableName('notify_user');
        $notifications_table = $this->getTableName('notifications');
        $users_table = $this->getTableName('users');
        $delete_notification_table = $this->getTableName('delete_notification');

        $this->fields[] = "$notifications_table.*";
        $this->fields[] = "$users_table.user_type";
        $this->fields[] = "$users_table.first_name";
        $this->fields[] = "$users_table.pic";

        $this->join[] = "LEFT JOIN $notifications_table ON $notifications_table.ID = $this->tables.nid";
        $this->join[] = "LEFT JOIN $users_table ON $users_table.user_no = $this->tables.sender_id";

        $this->where[] = "( $this->tables.user_type = '" . $user['user_type'] . "' OR $this->tables.single_user = '" . $user['email_id'] . "' OR $this->tables.single_user = '" . $user['username'] . "')";
        $this->where[] = " $this->tables.nid NOT IN (SELECT nid FROM $delete_notification_table WHERE user_no = '" . $user['user_no'] . "')";
        $this->where[] = "$this->tables.sender_id != '" . $user['user_no'] . "'";
        $this->where[] = "$this->tables.notify_date >= '" . $user['datetime'] . "'";
        $this->order_by = "$notifications_table.date";

        return $this->execute()->result_array();
    }

    public function getAdMedias($ad_id) {

        $this->tables = $this->getTableName("link_medias");
        $media_table = $this->getTableName("medias");

        $this->join[] = "LEFT JOIN $media_table ON $media_table.ID = $this->tables.media_id";
        $this->where[] = "$this->tables.object_type = 'ad'";
        $this->where[] = "$this->tables.object_id = $ad_id";
        $this->order_by = "$media_table.sort";
        $this->order_type = "ASC";
        return $this->execute()->result_array();
    }

    public function check_attendance($time, $date = '') {
////        echo $time.'<br>';
//        $H = date('H', strtotime($time));
//        $m = date('i', strtotime($time));
//        $s = date('s', strtotime($time));
////        echo $time.'<br>'; 
//        if($H < 5){
//            $H = 12 + $H;
//            $time = $H.":".$m.":".$s; 
//        }

        $this->tables = $this->getTableName('ceminer_times');
        $this->where[] = "'$time' BETWEEN $this->tables.start_time AND $this->tables.end_time AND rest != 1";
        return $this->execute()->row_array();
    }

    public function get_temp_attendance() {
        $this->tables = $this->getTableName('attendance_temp');
        $student_table = $this->getTableName('students');


        $this->fields = array("$this->tables.*,$student_table.sid as student_id,get_sem_no($this->tables.time) as seminer");

        $this->join[] = "INNER JOIN $student_table ON $student_table.student_id = $this->tables.sid";
        return $this->execute()->result_array();
    }

    public function get_stdent_with_course() {
        $this->tables = $this->getTableName('students');
//        $student_courses = $this->getTableName('student_courses');
        $this->fields = array(
            "$this->tables.*",
            "get_student_courses($this->tables.sid) as courses",
        );
//        $this->join[] = "LEFT JOIN $student_courses ON $student_courses.sid = $this->tables.sid";
        return $this->execute()->result_array();
    }

    public function truncate($tables = array()) {
        if ($tables && is_array($tables)) {
            foreach ($tables as $tab) {
                $tab = $this->db->dbprefix($tab);
                $this->db->query("TRUNCATE TABLE $tab");
            }
        } elseif ($tables) {
            $tab = $this->db->dbprefix($tables);
            $this->db->query("TRUNCATE TABLE $tab");
        } else {
            return false;
        }
        return true;
    }

    public function get_full_absent_students($users = array()) {
        if ($users) {
            $users = implode(',', $users);
            $this->tables = $this->getTableName('student_courses');
            $this->fields[] = "sid";
            $this->where[] = "sid NOT IN($users)";

            return $this->execute()->result_array();
        }
        return array();
    }

    public function getUserModarators($start, $limit, $cond = array()) {
        $s = $this->input->get('s');
        $this->tables = $this->getTableName('users');
        if (!empty($cond)) {
            foreach ($cond as $key => $val) {
                if (is_array($val) && count($val) > 0) {
                    $val = implode(',', $val);
                    $this->where[] = "$this->tables.$key IN ($val)";
                } else {
                    $this->where[] = "$this->tables.$key = '$val'";
                }
            }
        }
        if ($s) {
            $this->where[] = "($this->tables.first_name LIKE '%$s%' OR $this->tables.last_name LIKE '%$s%' OR email_id LIKE '%$s%' "
                    . " OR CONCAT(first_name,' ',last_name) LIKE '%$s%'"
                    . " OR username LIKE '%$s%'"
                    . " OR CONCAT(first_name,last_name) LIKE '%$s%')";
        }
        $this->limit = "$start,$limit";
        $this->order_by = "$this->tables.user_no";
        $this->order_type = "DESC";
        return $this->execute()->result_array();
    }

    // mrin section 
    public function statattendance($start, $limit, $cond = array(), $attendance_date = '') {
        $this->tables = $this->getTableName('courses');
        $attendance_table = $this->getTableName('attendance');
        $tbl_array = array('courses', 'attendance');
        $semno = '1';
        if (!empty($attendance_date)) {
            $attendance_date = " AND DATE($attendance_table.datetime)='$attendance_date'";
        }
        if (!empty($cond)) {
            foreach ($cond as $key => $val) {
                if (in_array($key, $tbl_array)) {
                    $t_name = $this->getTableName($key);
                    if (is_array($val) && count($val) > 0) {
                        foreach ($val as $val_key => $val_val) {
                            if (is_array($val_val) && count($val_val) > 0) {
                                $val_val = implode(',', $val_val);
                                $this->where[] = "$t_name.$val_key IN ($val_val)";
                            } else {
                                $this->where[] = "$t_name.$val_key = '$val_val'";
                            }
                        }
                    }
                } else {
                    if (is_array($val) && count($val) > 0) {
                        $val = implode(',', $val);
                        $this->where[] = "$this->tables.$key IN ($val)";
                    } else {
                        $this->where[] = "$this->tables.$key = '$val'";
                    }
                }
            }
        }
        // select fields 
        $this->fields[] = "$this->tables.*";
        $this->fields[] = "ifnull(SUM($attendance_table.attendance),0) total_attendance";
        $this->fields[] = "GROUP_CONCAT($attendance_table.datetime) AS all_datetime";
        $this->fields[] = "ifnull($attendance_table.semno,$semno) semno";

        //$this->join[] = "INNER JOIN $course_table ON $course_table.cid = $this->tables.cid";

        $this->join[] = "LEFT JOIN $attendance_table ON $this->tables.cid = $attendance_table.cid AND $attendance_table.attendance=1 AND $attendance_table.semno=$semno.$attendance_date";

        //$this->limit = "$start,$limit";
        $this->group_by = "$this->tables.cid";
        $this->order_by = "$this->tables.cid";
        $this->order_type = "ASC";
        return $this->execute()->result_array();
    }

    public function statattendance_csv($cond = array(), $attendance_date = '') {
        $this->tables = $this->getTableName('courses');
        $attendance_table = $this->getTableName('attendance');
        $tbl_array = array('courses', 'attendance');
        $semno = '1';
        if (!empty($attendance_date)) {
            $attendance_date = " AND DATE($attendance_table.datetime)='$attendance_date'";
        }
        if (!empty($cond)) {
            foreach ($cond as $key => $val) {
                if (in_array($key, $tbl_array)) {
                    $t_name = $this->getTableName($key);
                    if (is_array($val) && count($val) > 0) {
                        foreach ($val as $val_key => $val_val) {
                            if (is_array($val_val) && count($val_val) > 0) {
                                $val_val = implode(',', $val_val);
                                $this->where[] = "$t_name.$val_key IN ($val_val)";
                            } else {
                                $this->where[] = "$t_name.$val_key = '$val_val'";
                            }
                        }
                    }
                } else {
                    if (is_array($val) && count($val) > 0) {
                        $val = implode(',', $val);
                        $this->where[] = "$this->tables.$key IN ($val)";
                    } else {
                        $this->where[] = "$this->tables.$key = '$val'";
                    }
                }
            }
        }
        // select fields 
        $this->fields[] = "$this->tables.code";
        $this->fields[] = "$this->tables.name";
        //$this->fields[] = "GROUP_CONCAT($attendance_table.datetime) AS all_datetime";
        $this->fields[] = "ifnull($attendance_table.semno,$semno) seminar";
        $this->fields[] = "ifnull(SUM($attendance_table.attendance),'0') attendance";

        //$this->join[] = "INNER JOIN $course_table ON $course_table.cid = $this->tables.cid";

        $this->join[] = "LEFT JOIN $attendance_table ON $this->tables.cid = $attendance_table.cid AND $attendance_table.attendance=1 AND $attendance_table.semno=$semno.$attendance_date";

        //$this->limit = "$start,$limit";
        $this->group_by = "$this->tables.cid";
        $this->order_by = "$this->tables.cid";
        $this->order_type = "ASC";
        //return $this->execute()->result_array();
        return $this->execute();
    }

    public function statattendancenw($start_date = '', $class_ids = '') {
        if ($start_date == '' || $class_ids == '') {
            return array();
        }
        $this->tables = $this->getTableName('courses');
        $attendance_table = $this->getTableName('attendance');
        $student_courses = $this->getTableName('student_courses');
        //select section
        /* $this->fields[] = "$this->tables.cid,$this->tables.name course_name,$this->tables.code";
          $this->fields[] = "ifnull(SUM($attendance_table.attendance),0) total_attendance";
          $this->fields[] = "ifnull(count($student_courses.sid),0) total_student";

          $this->join[] = "LEFT JOIN $attendance_table ON $this->tables.cid = $attendance_table.cid AND $attendance_table.datetime=$start_date";

          $this->join[] = "LEFT JOIN $student_courses ON $this->tables.cid = $student_courses.cid";

          $this->where[] = "$this->tables.cid IN ($class_ids)";

          $this->group_by = "$this->tables.cid";
          return $this->execute()->result_array(); */

        $query = "SELECT cd.*,ifnull(count($student_courses.sid),0) total_student FROM ( SELECT $this->tables.cid,$this->tables.name course_name,$this->tables.code,ifnull(SUM($attendance_table.attendance),0) total_attendance,ifnull(count(abs_attendance.attendance)-(SUM(abs_attendance.attendance)),0) total_absent FROM $this->tables LEFT JOIN $attendance_table ON $this->tables.cid = $attendance_table.cid AND $attendance_table.datetime='$start_date' AND $attendance_table.semno='1' WHERE $this->tables.cid IN ($class_ids) GROUP BY $this->tables.cid ) cd LEFT JOIN $student_courses ON cd.cid = $student_courses.cid GROUP BY cd.cid";
        $result = $this->db->query($query)->result_array();

        return $result;
    }

    public function getStudentInCourses($course_ids = array()) {
        $students = array();
        if (is_array($course_ids) && count($course_ids) > 0) {
            $this->tables = $this->getTableName('students');
            $student_courses = $this->getTableName('student_courses');
            // fields 
            $this->fields[] = "$this->tables.sid";
            $this->fields[] = "$this->tables.student_id";
            $this->fields[] = "$this->tables.name";
            $this->fields[] = "count($student_courses.sid) course_number";
            $this->fields[] = "$student_courses.cid";

            $this->join[] = "LEFT JOIN $student_courses ON $this->tables.sid = $student_courses.sid";
            $courids = count($course_ids);
            if ($courids > 1) {
                $course_ids = implode(',', $course_ids);
                $this->where[] = "$student_courses.cid IN ($course_ids)";
            } else {
                $this->where[] = "$student_courses.cid=$course_ids[0]";
            }
            $this->group_by = "$student_courses.sid";
            $this->order_by = "$this->tables.sid";
            $this->order_type = "ASC";
            //$this->having = "course_number=$courids";
            $students = $this->execute()->result_array();
        }
        return $students;
    }

    public function studentSemReports($condition = array()) {
        $sem_reports = array();
        if (is_array($condition) && count($condition) > 0) {
            $class_date = $condition['class_date'];
            $sid = $condition['sid'];
            $course_ids = $condition['course_id'];
            if (is_array($course_ids) && !empty($course_ids)) {
                if (count($course_ids) > 1) {
                    $course_ids = implode(',', $course_ids);
                } else {
                    $course_ids = $course_ids[0];
                }
            }
            if (is_array($sid) && !empty($sid)) {
                if (count($sid) > 1) {
                    $sid = implode(',', $sid);
                } else {
                    $sid = $sid[0];
                }
            }

            // create sems table 
            $sem_names = array('sem-1', 'sem-2', 'sem-3', 'sem-4', 'sem-5', 'sem-6', 'sem-7');
            // create month table 
            $sem_tbl = '';
            for ($i = 1; $i <= count($sem_names); $i++) {
                $sem_name = ucfirst($sem_names[$i - 1]);
                if ($sem_tbl == '') {
                    $sem_tbl .= " SELECT '" . $sem_name . "' AS sem_name, '" . $i . "' AS sem_number ";
                } else {
                    $sem_tbl .= " UNION SELECT '" . $sem_name . "' AS sem_name, '" . $i . "' AS sem_number";
                }
            }
            $attendance_table = $this->getTableName('attendance');
            /* $query_str = "SELECT SM.*,ifnull($attendance_table.attendance,0) attendance FROM ($sem_tbl) AS SM LEFT JOIN $attendance_table ON SM.sem_number=$attendance_table.semno AND $attendance_table.sid=$sid AND $attendance_table.cid IN($course_ids) AND $attendance_table.datetime='$class_date' order by SM.sem_number"; */
            $query_str = "SELECT SM.*,ifnull($attendance_table.attendance,0) attendance FROM ($sem_tbl) AS SM LEFT JOIN $attendance_table ON SM.sem_number=$attendance_table.semno AND $attendance_table.sid IN($sid) AND $attendance_table.cid IN($course_ids) AND $attendance_table.datetime='$class_date' order by SM.sem_number";
            
            $sem_reports = $this->db->query($query_str)->result_array();
        }
        return $sem_reports;
    }

    public function semTecherName($condition = array()) {
        $course_ids = $condition['course_id'];
        if (is_array($course_ids) && !empty($course_ids)) {
            if (count($course_ids) > 1) {
                $course_ids = implode(',', $course_ids);
            } else {
                $course_ids = $course_ids[0];
            }
        }
        // create sems table 
        $sem_names = array('sem-1', 'sem-2', 'sem-3', 'sem-4', 'sem-5', 'sem-6', 'sem-7');
        // create month table 
        $sem_tbl = '';
        for ($i = 1; $i <= count($sem_names); $i++) {
            $sem_name = ucfirst($sem_names[$i - 1]);
            if ($sem_tbl == '') {
                $sem_tbl .= " SELECT '" . $sem_name . "' AS sem_name, '" . $i . "' AS sem_number ";
            } else {
                $sem_tbl .= " UNION SELECT '" . $sem_name . "' AS sem_name, '" . $i . "' AS sem_number";
            }
        }
        $teacher_seminers = $this->getTableName('teacher_seminers');
        $users = $this->getTableName('users');
        $query_str = "SELECT SM.*,ifnull(CST.name,'-') name FROM ($sem_tbl) AS SM LEFT JOIN (SELECT $teacher_seminers.semno,concat($users.first_name,' ',$users.last_name) as name FROM $teacher_seminers,$users WHERE $teacher_seminers.user_no=$users.user_no AND $users.user_type=2 AND $teacher_seminers.cid IN ($course_ids)) AS CST ON SM.sem_number=CST.semno";
        $sem_techers = $this->db->query($query_str)->result_array();
        return $sem_techers;
    }

    public function systemactivity($condition = array()) {
        $allusers = array();
        if (!empty($condition)) {
            $user_type = $condition['user_type'];
            $start_date = $condition['start_date'];
            if (is_array($user_type)) {
                if (count($user_type) > 1) {
                    $user_type = implode(',', $user_type);
                } else {
                    $user_type = $user_type[0];
                }
            }
            $this->tables = $this->getTableName('attendance');
            $users = $this->getTableName('users');
            $courses = $this->getTableName('courses');

            //$sql_query = "SELECT $this->tables.attendance,$this->tables.semno,$this->tables.datetime,$this->tables.update_time, $users.first_name,$users.last_name,$users.username,$courses.name,$courses.code FROM $this->tables, $users, $courses WHERE $this->tables.updated_by=$users.user_no AND $this->tables.cid=$courses.cid AND $users.user_type IN ($user_type) AND DATE($this->tables.update_time)='$start_date'";

            $sql_query = "SELECT $this->tables.attendance,$this->tables.semno,$this->tables.datetime,$this->tables.update_time,$this->tables.entered_by,$this->tables.updated_by,$courses.code,$courses.name course_name,concat($this->tables.cid,'_',$this->tables.semno) cid_semno FROM $this->tables,$courses WHERE $this->tables.cid=$courses.cid AND (DATE($this->tables.update_time)='$start_date' OR DATE($this->tables.datetime)='$start_date') AND ($this->tables.entered_by>0 OR $this->tables.updated_by>0) GROUP BY concat($this->tables.cid,'_',$this->tables.semno) order by $this->tables.update_time DESC, $this->tables.datetime DESC";

            $allusers = $this->db->query($sql_query)->result_array();
            if (is_array($allusers) && count($allusers) > 0) {
                foreach ($allusers as $key => $alluser) {
                    // checke if updater present 
                    $user_id = $alluser['updated_by'];
                    if ($user_id == 0) {
                        $user_id = $alluser['entered_by'];
                    }
                    $user_qr = "SELECT first_name,last_name,username,user_type FROM $users WHERE user_no='$user_id'";
                    $userdata = $this->db->query($user_qr)->result_array();
                    if (is_array($userdata) && count($userdata) > 0) {
                        $allusers[$key] = array_merge($allusers[$key], $userdata[0]);
                    }
                }
            }
        }
        return $allusers;
    }

    public function getAttendance_excel($onlyabsents = true, $filterdata = array()) {
//        $this->show_query = 1;
        $this->tables = $this->getTableName('attendance');
        $course_table = $this->getTableName('courses');
        $students_table = $this->getTableName('students');
        $this->fields[] = "$students_table.student_id as studentID";
        $this->fields[] = "$students_table.name as name";
        $this->fields[] = "$course_table.code as class";
        $this->fields[] = "concat('Sem-',GROUP_CONCAT(DISTINCT CAST($this->tables.semno AS CHAR) ORDER BY $this->tables.semno SEPARATOR ', Sem-')) as sems";
        $this->fields[] = "count($this->tables.sid) as abscount";
//        $this->fields[] = "$this->tables.*";
        //$this->fields[] = "$course_table.name";
        //$this->fields[] = "$course_table.cid";
        //$this->fields[] = "$students_table.sid";
        //$this->fields[] = "COUNT($this->tables.attendance) as absent_count";
        //$this->fields[] = "$this->tables.datetime";
        // mrin section
        // end
        $this->join[] = "INNER JOIN $course_table ON $course_table.cid = $this->tables.cid";
        $this->join[] = "INNER JOIN $students_table ON $students_table.sid = $this->tables.sid";
        $datetime = date('Y-m-d');
        // Mrin edit section 
        //print_r($filterdata);
        if (empty($filterdata)) {
            $this->where[] = "DATE($this->tables.datetime) = CURDATE()";
        } else {
            if ((isset($filterdata['start_date']) && !empty($filterdata['start_date'])) && (isset($filterdata['end_date']) && !empty($filterdata['end_date']))) {
                // both date present 
                $this->where[] = "DATE($this->tables.datetime) BETWEEN '" . $filterdata['start_date'] . "' AND '" . $filterdata['end_date'] . "'";
            } else {
                //only one date mantion
                if ((isset($filterdata['start_date']) && !empty($filterdata['start_date']))) {
                    // start 
                    $this->where[] = "DATE($this->tables.datetime) = '" . $filterdata['start_date'] . "'";
                } elseif ((isset($filterdata['end_date']) && !empty($filterdata['end_date']))) {
                    // end
                    $this->where[] = "DATE($this->tables.datetime) = '" . $filterdata['end_date'] . "'";
                }
            }
        }
        if (isset($filterdata['class_id']) && !empty($filterdata['class_id'])) {
            $this->where[] = "$course_table.cid = '" . $filterdata['class_id'] . "'";
        }
        //mrin:end
        if ($onlyabsents) {
            $this->where[] = "$this->tables.attendance = '0'";
        }
        // 
        //print_r($this->where);
        //
        $this->order_by = "$students_table.sid";
        $this->group_by = "$this->tables.sid";
        $this->order_type = "DESC";
        //mrin section having section 
        if ((isset($filterdata['absent_number']) && !empty($filterdata['absent_number']))) {
            $this->having = " abscount >= '" . $filterdata['absent_number'] . "'";
        }
        //$result = $this->execute()->result_array();
        $result = $this->execute();
        return $result;
    }

    public function getEditNwAttendances($cid = 0, $sid = 0, $is_edited = 0) {
        $date = ($this->input->post('date')) ? $this->input->post('date') : date('Y-m-d');
        $this->tables = $this->getTableName('attendance');
        if ($is_edited) {
            if ($sid > 0) {
                $this->where[] = "datetime='" . $date . "' AND cid='" . $cid . "' AND sid='" . $sid . "' AND updated_by >0";
            } else {
                $this->where[] = "datetime='" . $date . "' AND cid='" . $cid . "' AND updated_by >0";
            }
        } else {
            if ($sid > 0) {
                $this->setWhere(array('datetime' => $date, 'cid' => $cid, 'sid' => $sid, 'updated_by' => 0));
            } else {
                $this->setWhere(array('datetime' => $date, 'cid' => $cid, 'updated_by' => 0));
            }
        }
        $this->fields = array('DISTINCT semno');
        $atts = $this->execute()->result_array();
        $semnos = array();
        if (is_array($atts) && count($atts) > 0) {
            foreach ($atts as $val) {
                if (!in_array($val['semno'], $semnos)) {
                    $semnos[] = $val['semno'];
                }
            }
        }
        return $semnos;
    }

    public function groupClassesName($query = '') {
        if (empty($query)) {
            return array();
        }
        $courses = $this->s_model->runQuery($table = 'courses', $query_type = 'select', $query_data = array(), $custom = true, $query);
        return $courses;
    }

}
